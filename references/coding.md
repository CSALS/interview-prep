<hr>

Check this =
Inter IIT Placement Questions = https://docs.google.com/document/u/1/d/e/2PACX-1vRrEqBNm5jh7DCc_6JI4ZbDmvZvnQqEC8_kfc2r8ULikjz6GH4A5sKBjedEoFoCLvhWvzsI9aKRxuYc/pub#h.zg4dqbm8f5pw

Codeforces Blogs <br>
* https://codeforces.com/blog/entry/57282
* https://codeforces.com/blog/entry/13529
* https://codeforces.com/blog/entry/21991
* https://codeforces.com/blog/entry/13520
* https://codeforces.com/blog/entry/15729 (DS)
* https://codeforces.com/blog/entry/16221 (Graph Algo)

<hr>

# Leetcode templates/patterns on topics

1. https://leetcode.com/discuss/general-discussion/662866/dp-for-beginners-problems-patterns-sample-solutions
2. https://leetcode.com/discuss/general-discussion/657507/sliding-window-for-beginners-problems-template-sample-solutions
3. https://leetcode.com/discuss/general-discussion/655708/graph-problems-for-beginners-practice-problems-and-sample-solutions
4. https://leetcode.com/problems/find-all-anagrams-in-a-string/discuss/92007/Sliding-Window-algorithm-template-to-solve-all-the-Leetcode-substring-search-problem
5. https://leetcode.com/problems/minimum-window-substring/discuss/26808/here-is-a-10-line-template-that-can-solve-most-substring-problems
6. https://leetcode.com/discuss/general-discussion/458695/Dynamic-Programming-Patterns 
8. https://leetcode.com/discuss/general-discussion/691825/binary-search-for-beginners-problems-patterns-sample-solutions
9. https://leetcode.com/problems/combination-sum/discuss/16502/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning)


https://leetcode.com/discuss/general-discussion/665604/Important-and-Useful-links-from-all-over-the-Leetcode (contains all)




**Constraints**
- Range of int = -2 * 10^9 to 2 * 10^9
- long is same as int
- Range of long long = -9 * 10^18 to 9 * 10^18
- 1LL * x = converting to long long where x is int
- STL containers.size() = size_t (unsgined int). Use (int) type conversion
- log2(x) = 3.3 * log10(x)
-  Time complexity [ideal , maximum] (Operations per Second)
    1. O(n) [ 10^6 , 10^8]
    2. O(n^2) [ 10^3 , 10^4 ]
    3. O(n^3) [ 100 , 500 ]
    4. O(n^4) [ 50 , 100 ]
    5. O(n logn) [ 10^5 , 5*(10^6) ]
    6. O(log n) [ 10^100 , 2^(10^7) ]
    7. O(2^n) [ 20 , 26 ]

<hr>

<hr>

<hr>

<hr>








<hr>



<hr>




<hr>


<hr>

## **MATH**
Some tricks = 
Modular Arithmetic
- Always do this when negative numbers are present **(x % K + K) % K**
- x % K = (x + K) % K = (x – K) % K.
- Two Numbers Are Divisible By Same Number If their remainders are same. (**IMP FACT. USED IN MANY PROBLEMS**)
https://www.geeksforgeeks.org/how-to-compute-mod-of-a-big-number/ (a+b)%m = (a%m+b%m)%m num=a*1000+b*100+c*10+d do it for this
- (i, j) can be represented as single number K = i * N + j
and from K we can decode i, j using K/N = i & K%N = j
- 1 * 10 = 10 * 10 + 2 = 12 * 10 + 3 = 123
* https://www.geeksforgeeks.org/sum-of-all-submatrices-of-a-given-matrix/
number of submatrices an index (i, j) belongs to = (i+1)*(j+1)*(n-i)*(n-j)
as bottom right corner = (i+1)*(j+1)
as top left corner = (n-i)*(n-j)

- (**IMP**) 
sum exactly between A and B = sum atmost B - sum atmost A

Problems = 
* https://www.geeksforgeeks.org/find-number-perfect-squares-two-given-numbers/
* https://www.interviewbit.com/problems/sum-of-squares/?ref=random-problem

XOR <br>
* https://www.geeksforgeeks.org/find-the-maximum-subarray-xor-in-a-given-array/
* https://www.geeksforgeeks.org/maximum-xor-value-of-a-pair-from-a-range/
* https://www.geeksforgeeks.org/maximum-xor-of-two-numbers-in-an-array/

<hr>

## **TIME COMPLEXITY**
* A takes O(logN) but B doesn't **terminate**
```
A. for(i = 1; i < n; i *= 2)
 
B. for(i = n; i > -1; i /= 2)
n -> n/2 -> .... -> 1 -> 1/2(0) -> 0/2 (0) -> 0 -> 0 ....
```

## **PUZZLES**
- https://www.interviewbit.com/problems/prisoners-and-poison/ (Think in terms of binary also)
- 29x + 30y + 31z = 366 (Think in terms of leap year)
- https://www.interviewbit.com/problems/marbles-in-jar/ (Think carefully)
- https://www.interviewbit.com/problems/measure-time-by-hourglass/

<hr>

## OTHERS

* https://codeforces.com/contest/915/problem/D
* https://codeforces.com/contest/937/problem/D
* https://www.youtube.com/watch?v=57SUNQL4JFA&list=PL1ZTzFG1rg2WwTMHSJP3xhWpl8w5nmNkm&index=170

Tricky Implementation =
- https://www.geeksforgeeks.org/minimize-the-maximum-difference-between-adjacent-elements-in-an-array/
- https://www.geeksforgeeks.org/minimize-the-maximum-minimum-difference-after-one-removal-from-array/?ref=rp

DS IN REAL LIFE <br>
* https://www.cs.cornell.edu/courses/cs2110/2014sp/L09-Lists/data_structures.pdf
* https://www.quora.com/How-do-I-use-algorithms-and-data-structure-in-real-life

AMAZON QUESTIONS
* https://practice.geeksforgeeks.org/explore/?company%5B%5D=Amazon&page=1
* MCQ = https://www.indiabix.com/placement-papers/amazon/2992


<hr>

