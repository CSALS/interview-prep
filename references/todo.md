# GENERAL

- stl detailed= https://qr.ae/pNyPQx
- os algorithms (round robin, etc.)
- subject wise = https://www.youtube.com/watch?v=Mrgs8VlU6Xo
- resume video = https://www.youtube.com/watch?v=TPIwSA3h7hA
- prepare DS chart of complexities :-
        array,linkedlist,doublylinkedlist,graph,tree,bst,heap,trie,stacks&queues
- https://www.geeksforgeeks.org/real-time-application-of-data-structures/?ref=leftbar-rightbar
- imp real life application = https://www.linkedin.com/posts/harshitagrwa_programminglife-codingisfun-datastructures-activity-6667754525597802496-xS0k 
- data structure i wll use to redo and undo in a editor 

# TIP = think of modifying structure to improve

https://www.geeksforgeeks.org/nutanix-interview-experience-campus-placements/ (solve everything)

# cache
lru cache, lfu cache (write-back, write-through caches)
distributed cache, consistent hashing, implemented hashing

# how is garbage collection implemented (destructors, constructors etc. in java,c++)

# MUST
https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/submissions/
https://www.geeksforgeeks.org/find-number-subarrays-even-sum/
https://www.geeksforgeeks.org/minimum-steps-needed-to-cover-a-sequence-of-points-on-an-infinite-grid/
- Minimum deletions required to make frequency of each letter unique

- random shuffling of cards function

# RANDOM (DONT SEE TOPICS)

https://leetcode.com/problems/couples-holding-hands/
https://www.geeksforgeeks.org/minimum-positive-points-to-reach-destination/
https://www.geeksforgeeks.org/count-strings-can-formed-using-b-c-given-constraints/
https://www.geeksforgeeks.org/count-number-of-ways-to-partition-a-set-into-k-subsets/

https://www.codechef.com/problems/CLKLZM

https://github.com/kaushal02/interview-coding-problems/blob/master/findTheShape.cpp (IMP)
You'll be given a sequence in the form of alphabets a-z which will represent a balanced binary tree. Find which symbol does it represent: >, <, \, /. If none print #.
Eg: c
/
b
/
a
This represents " / ".

c
/
a
\
b
This represents "<" and so on.

task scheduler (leetcode)
Implement OS scheduler. N tasks with burst time and K-core processor
Graph with Red-Black nodes. Minimum weight to reach from source to dest such that abs(count(red)-count(black))



https://www.youtube.com/watch?v=ANOR78G90hE (flipkart questions)

https://www.geeksforgeeks.org/count-ways-express-number-sum-consecutive-numbers/

https://codeforces.com/problemset/problem/999/E?fbclid=IwAR30iDBaTwhijzGQLsyaUllist9Y7j4R8ZuYABEY6lIKa1PqcmAPgTR6SkY
https://codeforces.com/contest/1006/problem/E?fbclid=IwAR3pE7aSajyGa9wJgiQ2ExTCOZ2zmZB4XsyYK1CoxK5ond9fo5m_FpuCoqU

0. https://leetcode.com/problems/valid-parenthesis-string/
5. https://www.geeksforgeeks.org/find-smallest-range-containing-elements-from-k-lists/
6. https://www.geeksforgeeks.org/longest-consecutive-subsequence/
7. https://www.interviewbit.com/problems/palindrome-pairs/?ref=random-problem
8. https://www.interviewbit.com/problems/sum-of-fibonacci-numbers/?ref=random-problem (dont see topic. think please)
9. https://leetcode.com/problems/cherry-pickup/
10. https://leetcode.com/problems/perfect-squares/
11. https://www.interviewbit.com/problems/arrange-ii/?ref=random-problem
12. https://www.interviewbit.com/problems/stepping-numbers/?ref=random-problem
13. https://leetcode.com/problems/remove-invalid-parentheses/
14. https://www.geeksforgeeks.org/ugly-numbers/
15. https://www.geeksforgeeks.org/sum-of-maximum-elements-of-all-possible-sub-arrays-of-an-array/
16. https://www.geeksforgeeks.org/sliding-window-maximum-maximum-of-all-subarrays-of-size-k-using-stack-in-on-time/?ref=rp
17. https://www.geeksforgeeks.org/smallest-multiple-of-a-given-number-made-of-digits-0-and-9-only/?ref=rp
18. https://www.geeksforgeeks.org/maximum-product-increasing-subsequence-size-3/
19. https://www.geeksforgeeks.org/number-of-circular-tours-that-visit-all-petrol-pumps/
20. https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/discuss/93735/a-concise-template-for-overlapping-interval-problem
21. https://www.interviewbit.com/problems/sum-of-all-submatrices/
23. https://www.geeksforgeeks.org/minimum-number-page-turns-get-desired-page/?ref=lbp




# TODO
https://stackoverflow.com/questions/16690249/what-is-the-difference-between-dynamic-programming-and-greedy-approach
https://leetcode.com/discuss/general-discussion/680706/article-on-trie-general-template-and-list-of-problems
https://www.geeksforgeeks.org/solve-crossword-puzzle/


# LINKED LIST
- https://www.interviewbit.com/problems/insertion-sort-list/

# STRINGS
- https://www.geeksforgeeks.org/closest-palindrome-number-whose-absolute-difference-min/ (IMPPPPP)
- strings 2 ways to process = either from left to right or from right to left (depends on problem)
- https://leetcode.com/problems/backspace-string-compare/
- longest palindrome

# BINARY SEARCH
- https://www.interviewbit.com/problems/smallest-good-base/
- https://www.interviewbit.com/problems/implement-power-function/?ref=dash-reco

# STACKS
monotonically increasing stack
- https://www.geeksforgeeks.org/reverse-a-stack-using-recursion/
- https://www.geeksforgeeks.org/sort-a-stack-using-recursion/
implement stack, queue from scratch

# BITS
bitwise and of a range (leetcode)

# general
Given two words, tell if they are anagrams or not. Extend your solution for unicode as well. (what is unicode etc. things)


-----------------------------
# sql
pl/sql

# c lang operators
- https://qr.ae/pNKjZA


-----------

- sde problem set (striver) = https://docs.google.com/document/d/1SM92efk8oDl8nyVw8NHPnbGexTS9W-1gmTEYfEurLWQ/edit
- https://docs.google.com/document/d/1zE_RXhAIFI0ez1PsmFK-nRDSRpAsSOpLjXHIKtaIhSk/edit?fbclid=IwAR3PdHTLdhfcwvzN5t2ZaGzoVBiXkkAeBayq9Dn6Mwt2_W_4WZ3aghy-wZ0
- https://www.geeksforgeeks.org/tag/amazon/ (Amazon problem sets)
- https://medium.com/@himanshumundhra98/nirvana-the-placement-saga-7d4f5e4a8f27 
- https://github.com/shmundhra/Algorithmic-Programming
- https://github.com/shmundhra/Systems-and-Networks