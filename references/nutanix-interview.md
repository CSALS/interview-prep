# ONLINE ROUND
- Dijkstra
- heap
- graphs
- probabilistic dp

# DEBUG
- os codes mutex, semaphore like prod-consumer problem
- c++ mutex codes
- deadlocks, mutex was not locked etc. etc.
- The locks were misplaced and etc
- reader-writer problem

# INTERVIEW1
- MST
- find cost of min. spanning forest
- Topo sort
- tree vertex splitting
- design LRU cache, LFU cache, cache algos (write-back, write-through)
- Direct memory access
- Basic linux questions
- OS networks

MST question =>
the ques was that each node has a value, and each edge has some weight
in 1st part all nodes must necessarily be connected, and you must choose least value node
so that is simple. Just take MST and take smallest value node
2nd part was that you dont need to connect all nodes necessarily
in this case, there can be multiple components.
The cost of that component = MST of that connected component  + smallest value node of that component
sum of cost of all components must be minimized


# INTERVIEW2
- marketing company = 1000s req. per sec,
how do you prevent execution of 2 req. simultaneously?
store reqs in queue => fetch from queue to load balancer
- logger implement => which attributes to print
multi-threading, locks
how to handle unlimited threads (queue = maintain threads. if overflow then too many)
- The basic idea was building nutanix and how one would schedule different tasks on a distributed system. 
-  related to Databases (log recovery)

# HR
- What do you know about Nutanix
- why choose Nutanix