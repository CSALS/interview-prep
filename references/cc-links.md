# coding

proof of correctness=https://www.cs.cornell.edu/courses/cs2112/2015fa/lectures/lec_loopinv/index.html
https://qr.ae/TWRdle
https://stackoverflow.com/questions/3332947/when-is-it-practical-to-use-depth-first-search-dfs-vs-breadth-first-search-bf
https://www.geeksforgeeks.org/difference-between-bfs-and-dfs/
https://leetcode.com/discuss/general-discussion/591369/learn-about-graphs-dp
https://www.topcoder.com/community/competitive-programming/tutorials/dynamic-programming-from-novice-to-advanced/

# snackdown bootcamp
https://blog.codechef.com/2016/07/11/snackdown-training-camp-day-1/

# theory
https://www.geeksforgeeks.org/most-asked-computer-science-subjects-interview-questions-in-amazon-microsoft-flipkart/

## Follow

### PRACTICE
https://www.a2oj.com/Ladders.html 
https://www.a2oj.com/Categories.html

### SYLLABUS
GFG Course
https://www.hackerearth.com/practice/notes/getting-started-with-the-sport-of-programming/
https://cses.fi/book/book.pdf

## Resources
[Codeforces Tutorials-1](https://codeforces.com/blog/entry/13529)
[Codeforces Tutorials-2](https://codeforces.com/blog/entry/57282)
[Competitive Coding Handbook](https://cses.fi/book/book.pdf) <br>
[Lecture Slides](http://web.stanford.edu/class/cs97si/) <br>
[Codechef DSA Syllabus](https://www.codechef.com/certification/data-structures-and-algorithms/prepare?fbclid=IwAR32w_2jhSWifaEurkWFALoolwmeAcvfSrHmBcp-ClGbaWjpI-Zt9UHR_eU) <br>
[Programming Syllabus](https://docs.google.com/document/d/1_dc3Ifg7Gg1LxhiqMMmE9UbTsXpdRiYh4pKILYG2eA4/edit) <br>
[Interview Preparation-Huge List](http://p.ip.fi/3Vh1) <br>
[Hackerearth Syllabus](https://www.hackerearth.com/practice/notes/getting-started-with-the-sport-of-programming/)
