C++
if need to use more than two datatypes in pair, use struct instead

TREES
Leaf node = connected to only one node
In mathematics, and more specifically in graph theory, a tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any acyclic connected graph is a tree. A forest is a disjoint union of trees.

GRAPH

MATH
No of days between two dates A and B =
find no of days from starting of 0th year to A
find no of days from starting of 0th year to B
subtract them to get the answer

Fraction binary to Decimal = 
convert the binary part to decimal using normal procedure and then divide the result with 2^count of bits in the fractional binary
.01 = decimal(01)/2^2 = 0.25

ARRAY
can only modify array in-place then think of using SWAP / INDEX of array as ELEMENT

TIPS
auto suggestion - using tries

Binary tree ->
at level K -> 2^k nodes.
merge sort log(n) + 1 levels

SNIPPETS
Important condition while using BFS + delimiter for level ending
    while(q.size() > 1 && !q.empty()) {
        ppi node = q.front(); q.pop();
        if(node.first == -1) {
            q.push({-1, -1});
            total_time++;
            continue;
        }
        itr(k, 4) {
            int x = node.first + row[k], y = node.second + col[k];
            if(x >= 0 && y >= 0 && x < r && y < c && !vis[x][y] && matrix[x][y] == 1) {
                vis[x][y] = true;
                q.push({x, y});
            }
        }
    }


BIG INTEGER CLASS IN JAVA
https://www.geeksforgeeks.org/biginteger-class-in-java/

FINDING FACTORIAL OF LARGE NUMBER IN C++/C
https://www.geeksforgeeks.org/factorial-large-number/

PERMUTATIONS
https://github.com/mission-peace/interview/blob/master/src/com/interview/recursion/StringPermutation.java

FAST I/O
https://www.geeksforgeeks.org/fast-io-for-competitive-programming/
https://www.geeksforgeeks.org/getchar_unlocked-faster-input-cc-competitive-programming/

TRICKS
https://www.geeksforgeeks.org/overcoming-common-problems-in-competitive-programming/
https://www.geeksforgeeks.org/some-useful-c-tricks-for-beginners-in-competitive-programming/
https://www.geeksforgeeks.org/stdgcd-c-inbuilt-function-finding-gcd/
https://www.geeksforgeeks.org/inbuilt-function-calculating-lcm-cpp/
https://www.geeksforgeeks.org/important-shortcuts-competitive-programming/
https://www.geeksforgeeks.org/c-tricks-competitive-programming-c-11/
https://www.geeksforgeeks.org/bit-tricks-competitive-programming/


BITWISE
1. https://www.geeksforgeeks.org/bitwise-operators-in-c-cpp/
2. https://www.geeksforgeeks.org/bitwise-hacks-for-competitive-programming/

DATA TYPES IN C
https://www.geeksforgeeks.org/data-types-in-c/


STRINGS TO NUMBERS IN C/C++
https://www.geeksforgeeks.org/converting-strings-numbers-cc/
https://www.geeksforgeeks.org/sprintf-in-c/

long + int in C = long (implicit type casting) u don't need to do define explicitly

FIDNING SUM OF LARGE NUMBER

IN C -
using strings
char str[10000] = "123.... //a very large number
for(int i=0;i<strlen(str);i++)
sum = sum + str[i] - '0';

IN JAVA
use BigInteger
