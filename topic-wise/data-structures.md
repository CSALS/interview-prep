modify the data structure itself to get constant space solution

https://stackoverflow.com/questions/7333376/difference-between-average-case-and-amortized-analysis

average case time vs amortized time
in average case we make assumptions about
what will the average case (on the input)
amortized makes no such assumptions but
it considers total performance of sequence of such operations
instead of just one operation

vectors(resizable arrays)
push_back = O(1) amortized time
so when size is full if we want to insert
a double-size array is created and current array is
copied into it.
more precisely after n such operations it is O(N) 
for rest it is O(1)

https://www.geeksforgeeks.org/real-time-application-of-data-structures/


