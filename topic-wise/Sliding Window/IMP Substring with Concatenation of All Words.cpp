#include <bits/stdc++.h>
using namespace std;

/*
Fixed window size sliding window. Doesn't follow the template of sliding window

https://leetcode.com/problems/substring-with-concatenation-of-all-words/
https://www.interviewbit.com/problems/substring-concatenation/
*/

/*
A good question to ask if words list is unique or there can be same multiple words
in the leetcode question, there can duplicate words in the list. so we should maintain freq_map not just unordered_set 
this is mistake I did. need to compare freqeunces of words in the window to the original list words
*/
vector<int> findSubstring(string s, vector<string>& words) {
    if (words.empty()) {
        return {};
    }

    vector<int> result;
    int n = (int)s.length();
    int totalWindowSize = 0;
    unordered_map<string, int> wordsFreq;
    for (string word : words) {
        wordsFreq[word]++;
        totalWindowSize += word.length();
    }
    int wordLength = words[0].length();  //given that each word has same length

    for (int L = 0; L < n; ++L) {
        int R = totalWindowSize + L - 1;
        if (R >= n) break;

        //s[L...R] for each wordLength string check if it exists in wordDict
        int index = L;
        unordered_map<string, int> freq;
        while (index <= R) {
            string word = s.substr(index, wordLength);
            freq[word]++;
            index = index + wordLength;
        }
        bool found = true;
        for (auto it : freq) {
            if (it.second != wordsFreq[it.first]) {
                found = false;
                break;
            }
        }
        if (found) result.push_back(L);
    }

    return result;
}