#include <bits/stdc++.h>
using namespace std;

/** STRINGS **/

/*
https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/

Given a string s consisting only of characters a, b and c.
Return the number of substrings containing at least one occurrence of all these characters a, b and c.
*/
class count_substrings_with_atleast_one_occurence_of_three_chars {
   public:
    int numberOfSubstrings(string s) {
        int n = s.length();

        if (n <= 2) {
            return 0;
        }

        vector<int> counter(3, 0);
        int lo = 0, hi = 0;
        int count = 0;

        while (hi < n) {
            counter[s[hi] - 'a']++;

            while (matchings(counter)) {
                count += (n - hi);  //fixing lo as start
                counter[s[lo++] - 'a']--;
            }

            ++hi;
        }
        return count;
    }

   private:
    bool matchings(vector<int> &counter) {
        for (int freq : counter) {
            if (freq == 0) return false;
        }
        return true;
    }
};

//https://leetcode.com/problems/longest-substring-without-repeating-characters/
int lengthOfLongestSubstringWithoutRepeatingChars(string s) {
    if (s.empty())
        return 0;
    unordered_map<char, int> counter;
    int lo = 0, hi = 0;
    int n = s.length();
    int maxLen = 1;

    while (hi < n) {
        counter[s[hi]]++;

        /* IMP
            We only need to check freq of s[hi] since we are updating s[hi] freq at this point.
            already [lo...hi-1] have no repeating chars
        */
        while (counter[s[hi]] > 1 && lo <= hi) {
            counter[s[lo]]--;
            lo++;
        }

        // if(counter[s[hi]] == 1) redundant checking
        maxLen = max(maxLen, hi - lo + 1);
        ++hi;
    }

    return maxLen;
}

/* USING MATCHINGS INSTEAD OF COMPARING HASHMAPS */

/*
https://leetcode.com/problems/minimum-window-substring
1. to find if current window contains all chars we could compare the two hashmaps everytime
2. instead of that maintain matchings variable

variations => https://leetcode.com/problems/find-all-anagrams-in-a-string/ 
    instead of comparing hash maps use matchings
*/
string minWindow(string s, string t) {
    unordered_map<char, int> s_freq, t_freq;

    for (char c : t) t_freq[c]++;
    int lo = 0, hi = 0;

    int start = 0;
    int minSize = 0;

    int matchings = 0;  //number of chars in the current window matching to chars of t
    while (hi < (int)s.length()) {
        s_freq[s[hi]]++;
        //if the current char whose freq we increased has less than required freq then we found a matchings to that corresponding char
        if (s_freq[s[hi]] <= t_freq[s[hi]]) {
            matchings++;
        }

        if (matchings == t.length()) {
            //try to compress the window while maintaing the constraint
            //lo might point to char which is not present in t
            //or that char has more freq than required
            while (lo <= hi && (t_freq.find(s[lo]) == t_freq.end() || s_freq[s[lo]] > t_freq[s[lo]])) {
                s_freq[s[lo]]--;
                if (s_freq[s[lo]] == 0) s_freq.erase(s[lo]);
                ++lo;
            }
            //at this point we might min size possible for substring ending at hi
            if (minSize == 0 || minSize > hi - lo + 1) {
                minSize = hi - lo + 1;
                start = lo;
            }
        }

        ++hi;
    }
    return s.substr(start, minSize);
}

/*
https://www.geeksforgeeks.org/length-smallest-sub-string-consisting-maximum-distinct-characters/

Given a string of length N, find the length of the smallest sub-string consisting of maximum distinct characters.

    Max Distinct chars possible in any substring is distinct chars in the string
    So need to find smallest substring s.t. it contains all distinct chars of the string. in the substring those chars might repeat

    Same as above min. window substring except here we need to check if it contains all distinct chars of itself.
    in-fact we could same function by constructing string t with all distinct chars of s and calling that function (but may time lot of time)
*/
int min_window_size_containing_max_distinct_elements(string s) {
    unordered_map<char, int> distinctChars;
    for (char c : s) distinctChars[c]++;
    int totalDistinct = distinctChars.size();

    int lo = 0, hi = 0;
    int counter = 0;  //counting distinct chars in our window
    unordered_map<char, int> freq;
    int minSize = INT_MAX;
    while (hi < (int)s.length()) {
        freq[s[hi]]++;
        if (freq[s[hi]] == 1) counter++;

        while (counter == totalDistinct) {
            minSize = min(minSize, hi - lo + 1);
            freq[s[lo]]--;
            if (freq[s[lo]] == 0) counter--;
            ++lo;
        }

        ++hi;
    }
    return (minSize == INT_MAX ? 0 : minSize);
}

/**
 * https://leetcode.com/problems/find-all-anagrams-in-a-string
 * Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
 * Same as above question. but here window size is fixed. you just need to compare hashmaps.
 * but instead of comparing hashmaps you can maintain matchings variable
 */
vector<int> findAnagrams(string s, string p) {
    if (p.length() > s.length()) return {};

    unordered_map<char, int> s_freq, p_freq;
    for (char c : p) p_freq[c]++;

    vector<int> result;

    int lo = 0, hi = 0;
    int k = p.length(), n = s.length();

    int matchings = 0;

    while (hi < k) {
        s_freq[s[hi]]++;
        if (s_freq[s[hi]] <= p_freq[s[hi]]) ++matchings;
        ++hi;
    }

    if (matchings == k) result.push_back(lo);

    while (hi < n) {
        //remove s[lo] from window

        //removing s[lo] will reduce matchings
        if (s_freq[s[lo]] <= p_freq[s[lo]]) {
            matchings--;
        }
        s_freq[s[lo++]]--;

        //add s[hi] to window
        s_freq[s[hi]]++;
        if (s_freq[s[hi]] <= p_freq[s[hi]]) ++matchings;
        ++hi;

        //[lo...hi] is the window. check if freq maps are same
        if (matchings == k) result.push_back(lo);
    }
    return result;
}
