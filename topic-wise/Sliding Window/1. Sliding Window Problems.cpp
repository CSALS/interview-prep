#include <bits/stdc++.h>
using namespace std;

/*
TEMPLATE (variable size)
    1. Maintain counter / hash-map to count specific array input
    2. Increment window towards right using outer loop
    3. Inside the loop
        a) 1st change counter / hash-map depending on current element
        b) 2nd use a while loop to reduce window size based on constraints of the problem
        c) Persist the window size / add the count


BUCKETS
- DS - arrays, strings
- Identify - subarrays | substrings | contiguous | window
- Find Max Window Size | Min Window Size | Count of windows/subarrays s.t  (positive intergers only in arrays)
    1. distinct elements in a window
        exactly K distinct, atmost K distinct, maximum distinct elements
    2. arrays => Sum of subarray
        exactly K, <= K, >= K
        count/min length/max length subarray whose sum is exactly K = solved by prefix sum + hashing
    3. contain only specific type like
       contain all distinct of another array/string, only 1s or 0s etc.
    4. fixed window size. (max/min on subarrays of size K = DEQUE)
    5. subarray sum with negative numbers then sliding window won't work. follow below methods.

    If sliding window doesn't work then for subarrays use deque, prefix sum, hashing, binary search

- instead of comparing hash maps use matchings
*/

class distinct_elements {
   public:
    /* WINDOW SIZE */

    //https://www.lintcode.com/en/old/problem/longest-substring-with-at-most-k-distinct-characters/#
    //https://www.geeksforgeeks.org/longest-subarray-not-k-distinct-elements/
    int max_window_size_atmost_k_distinct_elements(vector<int> &arr, int K) {
        //max window size s.t it contains atmost K distinct elements
        //Longest subarray not having more than K distinct elements
        int maxSize = 0;
        int lo = 0, hi = 0;
        int n = arr.size();
        unordered_map<int, int> counter;  //distinct element->count
        while (hi < n) {
            counter[arr[hi]]++;

            //Loop inside for to reduce the window size based on constraint
            while (counter.size() > K && lo <= hi) {
                counter[arr[lo]]--;
                if (counter[arr[lo]] == 0)
                    counter.erase(arr[lo]);
                lo++;
            }
            if (counter.size() <= K) {
                //At this point counter.size() = distinct elements in window <= K
                maxSize = max(maxSize, hi - lo + 1);
            }
            ++hi;
        }
        return maxSize;
    }

    //https://www.geeksforgeeks.org/minimum-length-substring-with-exactly-k-distinct-characters/
    //when want min window size then in the inner loop if we have k distinct eles
    //then try compressing till you have the k distinct eles and also keep updating minSize
    //in this we won't encounter case of counter.size()>k since we don't go futher if we found k distinct eles in a window
    //lo...hi if we find k distinct eles we don't need to go further. we need to keep incrementing lo till there are k distinct
    int min_window_size_exactly_k_distinct_elements(vector<int> arr, int K) {
        //Smallest subarray with k distinct numbers
        int minSize = INT_MAX;
        int lo = 0, hi = 0;
        unordered_map<int, int> counter;  //distinct element -> count
        while (hi < arr.size()) {
            counter[arr[hi]]++;

            while (counter.size() == K && lo <= hi) {
                minSize = min(minSize, hi - lo + 1);
                counter[arr[lo]]--;
                if (counter[arr[lo]] == 0)
                    counter.erase(arr[lo]);
                lo++;
            }

            ++hi;
        }
        return (minSize == INT_MAX) ? 0 : minSize;
    }

    /* COUNT */

    int count_atmost_k_distinct_elements(vector<int> &A, int K) {
        //count of subarrays containing atmost K distinct integers
        int n = A.size();
        int count = 0;
        int lo = 0, hi = 0;
        unordered_map<int, int> counter;  //element -> count in the current window

        while (hi < n) {
            counter[A[hi]]++;

            //Loop inside for to reduce the window size based on constraint
            while (lo <= hi && counter.size() > K) {
                counter[A[lo]]--;
                if (counter[A[lo]] == 0)
                    counter.erase(A[lo]);
                lo++;
            }
            if (counter.size() <= K) {
                //At this point counter.size() = distinct elements in window <= K
                //ending point fixed at hi.
                count += (hi - lo + 1);
            }
            ++hi;
        }
        return count;
    }

    /*
        https://www.geeksforgeeks.org/count-substrings-character-occurring-k-times/
        Given a string S. Count number of substrings in which each character occurs at most k times

        Combination of count_atmost_k_distinct_elements() and lengthOfLongestSubstringWithoutRepeatingChars() logic
    */
    int findSubstrings(string s, int k) {
        int lo = 0, hi = 0;
        unordered_map<char, int> counter;

        int count = 0;
        while (hi < (int)s.length()) {
            counter[s[hi]]++;
            //at this point s[hi] freq is increased. so check that only
            while (counter[s[hi]] > k && lo <= hi) {
                counter[s[lo++]]--;
            }
            count += (hi - lo + 1);
            ++hi;
        }
        return count;
    }

    //IMPPPPP
    int count_exactly_k_distinct_elements(vector<int> &A, int K) {
        //count of subarrays containing exactly K distinct integers
        //This is to count all subarrays having no. of distinct elements in this range [K]
        //To count for a given range like [low, high] use [ atmostK(high) - atmostK(low - 1) ]
        return count_atmost_k_distinct_elements(A, K) - count_atmost_k_distinct_elements(A, K - 1);
    }
};

//something similar to atmost k distinct, exactly k distinct type
class sum_based {
   public:
    //Array contains only non-negative integers
    //Number of subarrays having sum less than equal K
    int count_less_k_sum(vector<int> arr, int K) {
        int count = 0;
        int lo = 0, hi = 0;
        int sum = 0;
        while (hi < arr.size()) {
            sum += arr[hi];
            while (sum > K && lo <= hi) {
                sum -= arr[lo];
                lo++;
            }
            if (sum <= K) {
                count += (hi - lo + 1);
            }
            hi++;
        }
        return count;
    }

    //IMPPP.. https://www.interviewbit.com/problems/numrange/
    //Number of subarrays having sum in the range of [low, high]
    //Variation = count subarrays having sum exactly K. use this function count_range(K, K)
    int count_range(vector<int> arr, int low, int high) {
        return count_less_k_sum(arr, high) - count_less_k_sum(arr, low - 1);
    }

    // Longest Subarray having sum of elements atmost ‘k’
    int max_size_atmost_k_sum(vector<int> arr, int K) {
        int maxLen = INT_MIN;
        int lo = 0, hi = 0;
        int sum = 0;
        while (hi < arr.size()) {
            sum += arr[hi];
            while (sum > K && lo <= hi) {
                sum -= arr[lo];
                lo++;
            }
            if (sum <= K) {
                maxLen = max(maxLen, hi - lo + 1);
            }
            hi++;
        }
        return maxLen;
    }

    //Smallest subarray with sum greater than a given value
    //https://www.geeksforgeeks.org/minimum-length-subarray-sum-greater-given-value/
    int min_size_greater_k(vector<int> arr, int k) {
        int lo = 0, hi = 0;
        int sum = 0;
        int minSize = INT_MAX;
        while (hi < arr.size()) {
            sum += arr[hi];
            //use sum >= k if you want greater than equal to given value
            while (sum > k && lo <= hi) {
                minSize = min(hi - lo + 1, minSize);
                sum -= arr[lo];
                lo++;
            }
            hi++;
        }
        return (minSize == INT_MAX) ? 0 : minSize;
    }
};

class fixed_window_size {
   public:
    /*
        Count distinct eles in every window of size K
        https://www.geeksforgeeks.org/count-distinct-elements-in-every-window-of-size-k/
        
        @variation https://www.geeksforgeeks.org/count-of-substrings-of-length-k-with-exactly-k-distinct-characters/?ref=leftbar-rightbar
        need to count substrings of length k with k distinct elemnets
        do the same procedure and if distinct eles count is K then increment the count
    */
    vector<int> count_distinct_elements(vector<int> arr, int K) {
        unordered_map<int, int> counter;  //distinct ele -> freq
        int lo = 0, hi = 0;
        while (hi < K) {
            counter[arr[hi++]]++;
        }

        vector<int> result;
        result.push_back(counter.size());

        while (hi < arr.size()) {
            counter[arr[hi]]++;
            counter[arr[lo]]--;
            if (counter[arr[lo]] == 0)
                counter.erase(arr[lo]);
            result.push_back(counter.size());
            lo++;
            hi++;
        }

        return result;
    }

    /**
     * DEQUE = max/min on all subarrays of size K => use deque
     * DEQUE = DS which helps in taking maximum/minimum of all possible ranges in an array.
     * The Deque is a double ended queue which comes in handy when we are working with continuous ranges
     * Variations =
     * 1. Find Max of every window of size K (maintain deque in decreasing order)
     * 2. Find Min of every window of size K (maintain deque in increasing order)
     * 3. https://www.geeksforgeeks.org/minimize-the-maximum-difference-between-adjacent-elements-in-an-array/ (Super Hard. Tricky)
     */

    vector<int> max_of_every_window_size_k(vector<int> nums, int k) {
        //Maintain deque in decreasing order
        //Top will be the max in the current window
        deque<int> dq;
        for (int i = 0; i < k; ++i) {
            while (!dq.empty() && dq.back() < nums[i])
                dq.pop_back();
            dq.push_back(nums[i]);
        }
        vector<int> result = {dq.front()};
        for (int i = k; i < nums.size(); ++i) {
            //remove left most element from window
            int remove_element = nums[i - k];
            if (dq.front() == remove_element)
                dq.pop_front();

            //add new element to the window
            while (!dq.empty() && dq.back() < nums[i])
                dq.pop_back();
            dq.push_back(nums[i]);

            result.push_back(dq.front());
        }
        return result;
    }
};

//Misc. Follow the same template

//ARRAYS

/**
 * https://leetcode.com/problems/max-consecutive-ones-iii/
 * Given an array A of 0s and 1s, we may change up to K values from 0 to 1.
 * Return the length of the longest (contiguous) subarray that contains only 1s.
 * Problem = max window size containing 1s + atmost K 0s
 */
int longestOnes(vector<int> &A, int K) {
    if (A.empty())
        return 0;
    int counter = 0;
    int N = A.size();
    int lo = 0, hi = 0;
    int maxLength = INT_MIN;
    while (hi < N) {
        if (A[hi] == 0)
            ++counter;

        while (counter > K && lo <= hi) {
            if (A[lo++] == 0)
                --counter;
        }
        if (counter <= K) {
            //At this point counter <= K so can update
            maxLength = max(maxLength, hi - lo + 1);
        }
        ++hi;
    }
    return maxLength;
}

/* MISC */

/**
 * Given ladder containing N steps. If from every step we can take a jump of size {1, 2, 3, .. K}. Find number of ways to reach from 0 to N
 * Solved using DP (N*K Time)
 * Sliding Window (N + K Time)
 * Since every value = sum of previous K values
 * Maintain window sum of size K
 */