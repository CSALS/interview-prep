#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-zeroes-to-be-flipped-so-that-number-of-consecutive-1s-is-maximized/?ref=lbp
 * 
 * Given a binary array and an integer m, find the position of zeroes flipping which creates maximum number of consecutive 1’s in array.
 * 
 * @solution
 * basically finding max. length window with atmost m 0s and any number of 1s
 */
int max_window_size_atmost_k_zeros(vector<int> &arr, int k) {
    int lo = 0, hi = 0;
    int counter = 0;  //track of number of zeros flipped
    int maxSize = 0;
    while (hi < (int)arr.size()) {
        if (arr[hi] == 0) ++counter;

        while (counter > k && lo <= hi) {
            counter -= (arr[lo++] == 0);
        }

        maxSize = max(maxSize, hi - lo + 1);
        ++hi;
    }
    return maxSize;
}

/*
https://www.geeksforgeeks.org/number-subarrays-product-less-k/
Given an Array of positive numbers, calculate the number of possible contiguous subarrays having product lesser than a given number K.
*/
int countsubarray(int arr[], int n, int k) {
    int count = 0;
    int lo = 0, hi = 0;
    long long windowProduct = 1;

    while (hi < n) {
        windowProduct *= arr[hi];
        while (windowProduct > k && lo <= hi) {
            windowProduct /= arr[lo++];
        }
        //count all valid subarrays ending at hi
        count += (hi - lo + 1);
        ++hi;
    }
    return count;
}

//TODO: https://www.geeksforgeeks.org/count-substrings-that-contain-all-vowels-set-2/