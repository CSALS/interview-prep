#include <bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/union-and-intersection-of-two-sorted-arrays-2/

/**
 * O(M + N) Time [Sorted Array]
 * Another approach if n >>>> m or m >>>> n. For every element in shorter array just do binary search on large array
 */
int printIntersectioSorted(int arr1[], int arr2[], int m, int n) {
    int i = 0, j = 0;
    while (i < m && j < n) {
        if (arr1[i] < arr2[j])
            i++;
        else if (arr2[j] < arr1[i])
            j++;
        else /* if arr1[i] == arr2[j] */
        {
            cout << arr2[j] << " ";
            i++;
            j++;
        }
    }
}

/**
 * O(M + N) Time [Sorted Array]
 * Inner while loops to handle duplicates. Instead you can use sets to handle duplicates
 */
int unionArraysSorted(int arr1[], int arr2[], int n1, int n2) {
    vector<int> unionArr;
    int i = 0, j = 0;
    while (i < n1 && j < n2) {
        if (arr1[i] == arr2[j]) {
            unionArr.push_back(arr1[i]);
            while (i < n1 - 1 && arr1[i] == arr1[i + 1]) {
                i++;
            }
            i++;
            while (j < n2 - 1 && arr2[j] == arr2[j + 1]) {
                j++;
            }
            j++;
        } else if (arr1[i] < arr2[j]) {
            unionArr.push_back(arr1[i]);
            while (i < n1 - 1 && arr1[i] == arr1[i + 1]) {
                i++;
            }
            i++;
        } else {
            unionArr.push_back(arr1[j]);
            while (j < n2 - 1 && arr2[j] == arr2[j + 1]) {
                j++;
            }
            j++;
        }
    }
    while (i < n1) {
        unionArr.push_back(arr1[i]);
        while (i < n1 - 1 && arr1[i] == arr1[i + 1]) {
            i++;
        }
        i++;
    }
    while (j < n2) {
        unionArr.push_back(arr2[j]);
        while (j < n2 - 1 && arr2[j] == arr2[j + 1]) {
            j++;
        }
        j++;
    }
    return unionArr.size();
}

/**
 * Unsorted Arrays https://www.geeksforgeeks.org/find-union-and-intersection-of-two-unsorted-arrays/
 * 1. Sort them and use the above functions. Time = O(MlogM + NlogM)
 * 2. In case of n >>> m or m >>> n (large differences in size of array)
 *      A) Union => Sort smaller array
 *                  vector<int> unionResult = smallerArray;
 *                  Then for each element in array, do binary search on smaller array to check if it is present.
 *                  If it is not present then addd it to unionResult
 *      B) Intersection => Sort smaller array. for each element in larger array add it to result if it is present in smaller array (binary search)
 *                  if m <<< n => MlogM + NlogM => O((M + N)logM)
 */