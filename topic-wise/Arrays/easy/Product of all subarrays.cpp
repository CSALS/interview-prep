#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/product-of-all-subarrays-of-an-array/
https://www.geeksforgeeks.org/product-of-all-subarrays-of-an-array-set-2/?ref=rp

Number of subarrays in any array => C(N, 2) + N (individual eles subarray) => N*(N+1)/2

1. Iterate over all subarrays and calculate product
    O(N^3) T | O(1) S

2. Iterate over all subarrays but use prefix product
    O(N^2) T | O(1) S

3. Find how many times a number will appear in subarray
    O(N) T | O(1) S
*/

long long product1(vector<int> arr) {
    int n = arr.size();
    long long product = 1;
    for (int i = 0; i < n; ++i) {
        for (int j = i; j < n; ++j) {
            //iterate over the subarray
            for (int k = i; k <= j; ++k) {
                product *= arr[k];
            }
        }
    }
    return product;
}
long long product2(vector<int> arr) {
    int n = arr.size();
    long long product = 1;
    for (int i = 0; i < n; ++i) {
        long long prefixProduct = 1;
        for (int j = i; j < n; ++j) {
            prefixProduct *= arr[j];
            product *= prefixProduct;
        }
    }
    return product;
}

/*
0 1 2 3 4 
a b c d e

consider subarrays starting with c => c, cd, cde will be (n - i) such subarrays
consider subarrays containing c =>
bc, bcd, bcde
abc, abcd, abcde
with i such elements before * (n - i)

so total times c occurs is (n-i)+i*(n-i)=>(i+1)*(n-i)
*/

//return a^b
long long getPower(int a, int b) {
    return (long long)(pow(a, b) + 0.5);
}
long long product3(vector<int> arr) {
    int n = arr.size();
    long long product = 1;
    for (int i = 0; i < n; ++i) {
        product *= getPower(arr[i], (i + 1) * (n - i));
    }
    return product;
}

int main() {
    cout << product1({2, 4}) << endl;  //64
    cout << product2({2, 4}) << endl;  //64
    cout << product3({2, 4}) << endl;  //64

    cout << product1({10, 3, 7}) << endl;  //27783000
    cout << product2({10, 3, 7}) << endl;  //27783000
    cout << product3({10, 3, 7}) << endl;  //27783000

    return 0;
}