/*

https://www.geeksforgeeks.org/count-of-subarrays-which-start-and-end-with-the-same-element/?ref=leftbar-rightbar

Given an array A of size N where the array elements contain values from 1 to N with duplicates,
the task is to find total number of subarrays which start and end with the same element.

Input: A[] = {1, 2, 1, 5, 2}
Output: 7

    Answer = N + For all i (freq[i] * (freq[i] - 1))/2 [Choose 2]
*/