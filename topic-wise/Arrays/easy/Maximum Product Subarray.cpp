#include <bits/stdc++.h>
using namespace std;

int maxProduct(vector<int>& arr) {
    if (arr.empty()) return 0;
    int n = arr.size();

    int max_dp[n];  //max_dp[i] = max product ending at arr[i]
    int min_dp[n];  //min_dp[i] = min product ending at arr[i]

    int max_product = arr[0];

    max_dp[0] = min_dp[0] = arr[0];

    for (int i = 1; i < n; ++i) {
        max_dp[i] = max({arr[i], arr[i] * max_dp[i - 1], arr[i] * min_dp[i - 1]});
        min_dp[i] = min({arr[i], arr[i] * min_dp[i - 1], arr[i] * max_dp[i - 1]});
        max_product = max(max_product, max_dp[i]);
    }

    return max_product;
}

//Since in the recurrence the ith value depends only on (i-1)th value we could just use two variables
int maxProductOptimized(vector<int>& arr) {
    if (arr.empty()) return 0;
    int n = arr.size();

    int max_product = arr[0];
    int max_dp_prev = arr[0];
    int min_dp_prev = arr[0];

    for (int i = 1; i < n; ++i) {
        int max_dp_curr = max({arr[i], arr[i] * max_dp_prev, arr[i] * min_dp_prev});
        int min_dp_curr = min({arr[i], arr[i] * max_dp_prev, arr[i] * min_dp_prev});
        max_product = max(max_product, max_dp_curr);
        max_dp_prev = max_dp_curr;
        min_dp_prev = min_dp_curr;
    }

    return max_product;
}