#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/sort-an-almost-sorted-array-where-only-two-elements-are-swapped/
 * There will be either 1 out of order element (when consecutive no.s are swapped)
 * Or there will be 2 out of order elements
 */
void sortByOneSwap(vector<int> arr, int n) {
    int firstOutoforder = -1, secondOutoforder = -1;
    for (int i = 0; i < n - 1; ++i) {
        if (arr[i] > arr[i + 1]) {
            if (firstOutoforder == -1)
                firstOutoforder = i;
            else
                secondOutoforder = i;
        }
    }
    if (secondOutoforder == -1) {
        //adjacent elements are swapepd
        swap(arr[firstOutoforder], arr[firstOutoforder + 1]);
    } else {
        swap(arr[firstOutoforder], arr[secondOutoforder + 1]);
    }
    for (int i = 0; i < n; ++i) cout << arr[i] << " ";
    cout << endl;
}

int main() {
    sortByOneSwap({1, 2, 4, 3, 5, 6, 7}, 7);
    sortByOneSwap({1, 2, 6, 4, 5, 3, 7}, 7);
}