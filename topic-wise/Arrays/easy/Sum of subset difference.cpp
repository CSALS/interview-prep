#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/sum-subset-differences/

Given a set S consisting of n numbers, find the sum of difference between last and first element of each subset.
We find first and last element of every subset by keeping them in same order as they appear in input set S.
i.e., sumSetDiff(S) = ∑ (last(s) – first(s)),
where sum goes over all subsets s of S.
Note: Elements in the subset should be in the same order as in the set S.
Input  = S = {5, 2, 9, 6}, n = 4
Output = 21


Variation = https://www.geeksforgeeks.org/find-sum-maximum-difference-possible-subset-given-array/
find sum of max(s)-min(s) for all possible subsets

Input : arr[] = {1, 2, 3}
Output : result = 6

        1. sort the given array.
        2. follow the same algo (total_max, total_min)
*/
//return a^b
int getPower(int a, int b) {
    return (int)(pow(a, b) + 0.5);
}

int sumDiff(int S[], int n) {
    int combinedFirst = 0, combinedLast = 0;
    for (int i = 0; i < n; ++i) {
        combinedLast += (getPower(2, i) - 1) * S[i];
        combinedFirst += (getPower(2, n - i - 1) - 1) * S[i];
    }
    return combinedLast - combinedFirst;
}