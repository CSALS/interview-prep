#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/to-find-smallest-and-second-smallest-element-in-an-array/
 * 1. Scan array twice
 * 2. Scan array only once
 * Variation = 1st max and 2nd max element
 */
void print2Smallest(int arr[], int arr_size) {
    int i, first, second;

    /* There should be atleast two elements */
    if (arr_size < 2) {
        cout << " Invalid Input ";
        return;
    }

    first = second = INT_MAX;
    for (i = 0; i < arr_size; i++) {
        /* If current element is smaller than first  
        then update both first and second */
        if (arr[i] < first) {
            second = first;
            first = arr[i];
        }

        /* If arr[i] is in between first and second  
        then update second. 2nd condition coz we don't want duplicates*/
        else if (arr[i] < second && arr[i] != first)
            second = arr[i];
    }
}