#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/a-product-array-puzzle/
 * Given an array arr[] of n integers, construct a Product Array prod[] (of same size) such that
 * prod[i] is equal to the product of all the elements of arr[] except arr[i]. 
 * Solve it without division operator in O(n) time (might not work when 0 is present)
 * Variation = https://www.geeksforgeeks.org/construct-an-array-from-xor-of-all-elements-of-array-except-element-at-same-index/
 * Use xor property. Take xor of all elements. Then for each take xor of arr[i] and finalxor to give the result at that index.
 */

//Takes no extra space other than the result product array
//Another approach is to use logarithms. It is O(1) space
vector<int> productArrayPuzzle(vector<int> &arr) {
    int n = arr.size();
    //contains leftProduct[i] = a[0].a[1].a[2].....a[i-1]
    vector<int> result(n, 1);
    for (int i = 1; i < n; ++i) {
        result[i] = arr[i - 1] * result[i - 1];
    }
    int rightProduct = arr[n - 1];
    for (int i = n - 2; i >= 0; --i) {
        result[i] = result[i] * rightProduct;
        rightProduct = rightProduct * arr[i];
    }
    return result;
}