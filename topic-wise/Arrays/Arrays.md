## ARRAYS
Tips = 
- If numbers in array are between 0 to n or 1 to n then we can use indexes to do some stuff.

- For median there should n/2 elements less than it.
- equal number of 0s and 1s => replacing 0 as -1 and prefix sum

- think of modifying structure or using array itself for getting O(1) space (especially in index questions)

- 2nd largest and 1st largest in subarrays
let arr[i] is 2nd largest then 1st largest will be next greater on right and also next greater on left
so you got 2 subarrays where arr[i] is 2nd largest
similarily do the same thing for all elements and it is next greater on right not max on right since if we take
max on right arr[i] may not be 2nd largest

- subarrays = sliding winodw

- (**IMP**)count exactly K = count atmost K - count atmost (K - 1)
            To count for a given range like [low, high] use [ atmostK(high) - atmostK(low - 1) ]
## MATRIX
- Min operations in array then try to do that with median
- (i, j) => i * Cols + j (1D index) [Not i * Rows + j]

## SORTING
- If standard NlogN approach isn't good then try to think of O(N) approach using something like Bucket Sort. Maybe using indexes. Think
- Counting some inequality in arrays then try to use modified **MERGE METHOD** while doing merge sort especially for i < j type
- Sometimes you can **MERGE METHOD** only for combining arrays (Given two sorted arrays)
(Sorting squared array. Leetcode 997)
- https://www.geeksforgeeks.org/when-to-use-each-sorting-algorithms/?ref=leftbar-rightbar


# Functions 
- *max_element(heights.begin(), heights.end()) = max element


# BUCKETS
- implementation, greedy, sorting, two pointers, interval based, index based, sliding window, deque, prefix sum, hashing, difference array

# TODO
https://www.geeksforgeeks.org/maximize-arrj-arri-arrl-arrk-such-that-i-j-k-l/
same as buy and sell stocks exactly twice. but the approach given is powerful and can be expanded for other exps also



https://www.geeksforgeeks.org/maximum-size-rectangle-binary-sub-matrix-1s/
all approaches
