#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-maximum-product-of-a-triplet-in-array/
 * 1. Three loops. O(N^3) Time | O(1) Space
 * 2. Use sorting and find max(last 3 elements product, first two elements and last element product)
 *      O(NlogN) Time | O(1) Space
 * 3. You only need largest, 2nd largest, 3rd largest and 1st min and 2nd min. You don't need to sort entire array for that
 *      O(N) Time | O(1) Space
 * Why 1st two elements?
 * We take two -ve eles product (which will be +ve) since it might be high
 */

int maxProduct(vector<int> arr, int n) {
    sort(arr.begin(), arr.end());
    return max(
        arr[n - 1] * arr[n - 2] * arr[n - 3],
        arr[n - 1] * arr[0] * arr[1]);
}
