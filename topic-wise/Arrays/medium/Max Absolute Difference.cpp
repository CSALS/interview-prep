#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/maximum-absolute-difference/
 * You are given an array of N integers, A1, A2 ,…, AN. 
 * Return maximum value of f(i, j) for all 1 ≤ i, j ≤ N.
 * f(i, j) is defined as |A[i] - A[j]| + |i - j|
 * 
 * 
 */

/*
1. (A[i] + i) - (A[j] + j)
2. -(A[i] - i) + (A[j] - j) 
3. (A[i] - i) - (A[j] - j) 
4. (-A[i] - i) + (A[j] + j) = -(A[i] + i) + (A[j] + j)

1&4 are similar. 2&3 are similar
need to compute a[i]+i for all elemnts and also a[i]-i for all elements
*/

int maxArr(vector<int> &arr) {
    int n = arr.size();
    //arr1=>arr+i   arr2=>arr-i
    vector<int> arr1(n), arr2(n);
    int max1, min1, max2, min2;
    for (int i = 0; i < n; ++i) {
        arr1[i] = arr[i] + i;
        max1 = max(max1, arr1[i]);
        min1 = min(min1, arr1[i]);
    }
    for (int i = 0; i < n; ++i) {
        arr2[i] = arr[i] - i;
        max2 = max(max2, arr2[i]);
        min2 = min(min2, arr2[i]);
    }
    return max(
        max1 - min1, max2 - min2);
}
