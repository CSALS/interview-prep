#include <bits/stdc++.h>
using namespace std;
/*
https://www.interviewbit.com/problems/balance-array/

Given an integer array A of size N. You need to count the number of special elements in the given array.

A element is special if removal of that element make the array balanced.

Array will be balanced if sum of even index element equal to sum of odd index element.
*/
int solve(vector<int> &A) {
    if (A.empty()) return 0;
#define arr A
    int N = A.size();
    //sums are calc. excluding ith element
    vector<int> left_odd_sum(N, 0), left_even_sum(N, 0);
    vector<int> right_odd_sum(N, 0), right_even_sum(N, 0);

    left_odd_sum[0] = left_even_sum[0] = 0;
    for (int i = 1; i < N; ++i) {
        if (i & 1) {
            //this is odd
            //[0...i - 1]
            left_even_sum[i] = left_even_sum[i - 1] + arr[i - 1];
            left_odd_sum[i] = (i - 2 >= 0) ? left_odd_sum[i - 2] + arr[i - 2] : 0;
        } else {
            left_odd_sum[i] = left_odd_sum[i - 1] + arr[i - 1];
            left_even_sum[i] = (i - 2 >= 0) ? left_even_sum[i - 2] + arr[i - 2] : 0;
        }
    }

    right_odd_sum[N - 1] = right_even_sum[N - 1] = 0;
    for (int i = N - 2; i >= 0; --i) {
        if (i & 1) {
            right_even_sum[i] = right_even_sum[i + 1] + arr[i + 1];
            right_odd_sum[i] = (i + 2 < N) ? right_odd_sum[i + 2] + arr[i + 2] : 0;
        } else {
            right_odd_sum[i] = right_odd_sum[i + 1] + arr[i + 1];
            right_even_sum[i] = (i + 2 < N) ? right_even_sum[i + 2] + arr[i + 2] : 0;
        }
    }

    int count = 0;
    for (int i = 0; i < N; ++i) {
        // cout << left_even_sum[i] << " " << right_even_sum[i] << " EVEN\n";
        // cout << left_odd_sum[i] << " " << right_odd_sum[i] << " ODD\n";
        // cout << endl;
        count += (left_odd_sum[i] + right_even_sum[i]) == (left_even_sum[i] + right_odd_sum[i]);
    }
    return count;
}