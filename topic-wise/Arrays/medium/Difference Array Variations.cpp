#include <bits/stdc++.h>
using namespace std;

/*
    You will be given some ranges [L, R]. Need to do something
    All these problems have same trick. Placing +1 on L and -1 on (R+1) and taking prefix sum
*/

/**
 * https://www.geeksforgeeks.org/maximum-occurred-integer-n-ranges/
 * Given n ranges of the form L and R, the task is to find the maximum occurred integer in all the ranges. 
 * If more than one such integer exists, print the smallest one.
 */
int solution() {
    int n;
    cin >> n;
    int MIN_L = INT_MAX, MAX_R = INT_MIN;
    vector<int> left(n), right(n);
    for (int &x : left) {
        cin >> x;
        MIN_L = min(MIN_L, x);
    }
    for (int &x : right) {
        cin >> x;
        MAX_R = max(MAX_R, x);
    }
    //
    vector<int> arr(MAX_R + 100, 0);
    for (int i = 0; i < n; ++i) {
        int l = left[i], r = right[i];
        arr[l] += 1;
        arr[r + 1] -= 1;
    }
    for (int i = MIN_L + 1; i <= MAX_R; ++i) {
        arr[i] = arr[i] + arr[i - 1];
    }
    int max_integer = INT_MIN;
    int max_freq = INT_MIN;
    for (int i = MIN_L; i <= MAX_R; ++i) {
        if (arr[i] > max_freq) {
            max_freq = arr[i];
            max_integer = i;
        }
    }
    return max_integer;
}

/**
 * https://www.hackerrank.com/challenges/crush/problem
 * Starting with a 1-indexed array of zeros and a list of operations,
 * for each operation add a value to each of the array element between two given indices, inclusive.
 * Once all operations have been performed, return the maximum value in your array. 
 */
long arrayManipulation(int n, vector<vector<int>> queries) {
    vector<long long> arr(n, 0);
    for (vector<int> query : queries) {
        int l = query[0] - 1, r = query[1] - 1, amount = query[2];
        arr[l] += amount;
        if (r + 1 < n) arr[r + 1] -= amount;
    }
    long long answer = INT_MIN;
    for (int i = 1; i < n; ++i) {
        arr[i] = arr[i - 1] + arr[i];
        answer = max(answer, arr[i]);
    }
    return answer;
}