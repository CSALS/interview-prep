#include <bits/stdc++.h>
using namespace std;

/**
 * @question
 * Majority Element in an array = element occuring > N/2 Times.
 * Input : {3, 3, 4, 2, 4, 4, 2, 4, 4}
 * Output : 4
 * 
 * @solution
 * There can be only atmost one element like that.
 * 1) Find the candidate for majority element
 * 2) Check if it actually the majority element
 * 
 * Distributed solution https://gregable.com/2013/10/majority-vote-algorithm-find-majority.html
 */

int majortiyElement(vector<int> arr) {
    //Initial candidate is arr[0] so ct is 1
    int maj_ind = 0;
    int ct = 1;
    int N = arr.size();

    for (int i = 1; i < N; ++i) {
        if (arr[i] == arr[maj_ind]) {
            ct++;
        } else {
            //Idea is to cancel one occurence of majority element with all other elements and still majority ele should exist till end
            ct--;
        }

        //If ct is 0 then we have not assumed any candidate
        if (ct == 0) {
            maj_ind = i;
            ct = 1;
        }
    }
    //Even if there is no maj. element we might get some index as candidate so verify
    ct = 0;
    for (int i = 0; i < N; ++i)
        if (arr[i] == arr[maj_ind])
            ct++;

    if (ct > N / 2)
        return arr[maj_ind];
    else
        return -1;
}

/**
 * Variation = Find all elements occuring > N/3 Times
 * There can be atmost 2 elements like this
 * 1) So take two candidates and cancel out occurences
 * 2) Then find if their count is > N/3
 */
int majorityElementVariation(vector<int> arr) {
    int N = arr.size();

    //Not assuming any candidates initially.
    int e1 = INT_MAX;
    int ct1 = 0;
    int e2 = INT_MAX;
    int ct2 = 0;

    for (int i = 0; i < N; ++i) {
        //ct1==0 and ct2==0 are mutually exclusive conditions since e1 and e2 shouldn't point to same element
        //these conditions order is also imp. first check if curr element is either e1 or e2 then only check if ct1 or ct2 are 0
        if (e1 == arr[i])
            ct1++;
        else if (e2 == arr[i])
            ct2++;
        else if (ct1 == 0) {
            e1 = arr[i];
            ct1 = 1;
        } else if (ct2 == 0) {
            e2 = arr[i];
            ct2 = 1;
        } else {
            ct1--;
            ct2--;
        }
    }

    ct1 = 0;
    ct2 = 0;
    for (int i = 0; i < N; ++i) {
        if (arr[i] == e1)
            ct1++;
        else if (arr[i] == e2)
            ct2++;
    }

    if (ct1 > N / 3) {
        //e1 is majority element
    }

    if (ct2 > N / 3) {
        //e2 is also majority elemnet
    }

    //None of them are majority elements
}