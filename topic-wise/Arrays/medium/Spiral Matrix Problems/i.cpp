#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/spiral-matrix/
 * Printing matrix in spiral order
 * Input:
 * [[ 1, 2, 3 ],
 * [ 4, 5, 6 ],
 * [ 7, 8, 9 ]
 * ]
 * Output: [1,2,3,6,9,8,7,4,5]
 */
class Solution {
   public:
    vector<int> spiralOrder(vector<vector<int>> &matrix) {
        if (matrix.empty() || matrix[0].empty()) {
            return {};
        }
        int rows = matrix.size(), cols = matrix[0].size();
        int rS = 0, cS = 0, rE = rows - 1, cE = cols - 1;
        vector<int> result;
        /**
         * 0. Left to right
         * 1. top to bottom
         * 2. right to left
         * 3. bottom to top
         */
        int direction = 0;
        while (rS <= rE && cS <= cE) {
            if (direction == 0) {
                for (int col = cS; col <= cE; ++col)
                    result.push_back(matrix[rS][col]);
                rS++;
                // direction = 1;
            } else if (direction == 1) {
                for (int row = rS; row <= rE; ++row)
                    result.push_back(matrix[row][cE]);
                cE--;
                // direction = 2;
            } else if (direction == 2) {
                for (int col = cE; col >= cS; --col)
                    result.push_back(matrix[rE][col]);
                rE--;
                // direction = 3;
            } else if (direction == 3) {
                for (int row = rE; row >= rS; --row)
                    result.push_back(matrix[row][cS]);
                cS++;
                // direction = 0;
            }
            direction = (direction + 1) % 4;
        }
        return result;
    }
};