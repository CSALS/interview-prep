#include <bits/stdc++.h>
using namespace std;

/**
 * IMP
 * https://www.geeksforgeeks.org/find-number-of-triangles-possible/
 * Given an unsorted array of positive integers, find the number of triangles that can be formed with three different array elements as three sides of triangles.
 * For a triangle to be possible from 3 values, the sum of any of the two values (or sides) must be greater than the third value (or third side).
 * 1. Use three loops for checking all possible values. O(N^3) Time
 * 2. Sort array. Use two pointers to fix a and b. Find c in the later portion after b. Since c will be > a and b (sorted) the properties a+c>b and b+c>a will hold
 *    Only need to check a+b>c. Use binary srch to find first position at which a+b>c will fail and use that to count all possible counts. O(N^2 logN) Time
 * 3. Same as above approach. Instead of binary srch you can maintain separate pointer for c.
 *    If a[i] + a[j] > c for a range then a[i] + a[j + 1] > c will hold for the same range.
 *    O(N^2) Time
 */
int nTriang(vector<int> &arr) {
    int n = arr.size();
    sort(arr.begin(), arr.end());
    const int MOD = 1e9 + 7;
    int result = 0;
    for (int i = 0; i < n; ++i) {
        int k = i + 2;
        for (int j = i + 1; j < n; ++j) {
            while (k < n && arr[i] + arr[j] > arr[k])
                ++k;
            //k points to the first point where arr[i] + arr[j] is not > arr[k]
            result = (result + k - 1 - j) % MOD;
        }
    }
    return result;
}

/**
 * https://www.spoj.com/problems/NOTATRI/
 * Find number of triplets which can't form triangle
 */
int solve(int n, vector<int> arr) {
    sort(arr.begin(), arr.end());
    int result = 0;  //number of triplets which dont form triangles
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            int a = arr[i];
            int b = arr[j];
            //a + c will be > b since c > a and b
            //Same with b + c > a
            //We need to check for a + b < c (the first c where this happens)
            //we are not using a+b<=c since in question it is given dengenerate triangles (for which a+b==c) will be counted as triangle
            int c_ind = upper_bound(arr.begin() + j + 1, arr.end(), a + b) - arr.begin();  // first c > a + b
            result += (n - c_ind);
        }
    }
    return result;
}
