#include <bits/stdc++.h>
#define deb(x) cout << #x << " " << x << endl;
using namespace std;

/**
 * Next Smaller Palindrome
 * https://www.spoj.com/problems/PALIN/
 * 
 * 23545 = 23632
 * 999 = 1001
 * 808 = 818
 * 2133 = 2222
 * 1234 = 1331
 * 
 * Many cases
 * 0. String has all 9s. Then answer is (string + 2) => 1 + (N-1) 0s + 1
 * 1. Even length String
 *       a) rev(1st half) <= 2nd half then str[mid1]++ (propagate carry if any towards left) and then copy the rev(1st half) into 2nd half
 *       b) rev(1st half) > 2nd half then directly copy the rev(1st half) into 2nd half
 * 2. Odd length String
 *       a) rev(1st half) <= 2nd half then str[mid]++ here mid is n/2 (propagate carry if any towards left) and then copy the rev(1st half) into 2nd half
 *       b) rev(1st half) > 2nd half then directly copy the rev(1st half) into 2nd half
 */
int all_nine(string str)  //check if all digits are '9'
{
    for (int i = 0; i < str.length(); i++) {
        if (str[i] != '9')
            return 0;
    }
    return 1;
}

//Place revLeft in str from index 'ind'
void stringCopy(string &revLeft, string &str, int ind) {
    int k = 0;
    for (int i = ind; i < str.length(); ++i) {
        str[i] = revLeft[k++];
    }
}

string reverseString(string &str) {
    string rev = str;
    reverse(rev.begin(), rev.end());
    return rev;
}

void generateNextPalindromeUtil(string &str, int n) {
    int mid1, mid2;
    if (n & 1) {
        mid1 = n / 2 - 1;
        mid2 = n / 2 + 1;
    } else {
        mid1 = n / 2 - 1;
        mid2 = n / 2;
    }
    string left = str.substr(0, mid1 + 1);
    string right = str.substr(mid2);
    string revLeft = reverseString(left);
    if (n % 2 == 0) {
        if (revLeft <= right) {
            while (left[mid1] == '9') {
                left[mid1] = '0';
                str[mid1] = '0';
                mid1--;
            }
            left[mid1]++;
            str[mid1]++;
            revLeft = reverseString(left);
            stringCopy(revLeft, str, mid2);
        } else {
            stringCopy(revLeft, str, mid2);
        }
    } else {
        int mid = n / 2;
        if (revLeft <= right) {
            while (str[mid] == '9') {
                str[mid--] = '0';
            }
            str[mid]++;
            left = str.substr(0, mid1 + 1);
            revLeft = reverseString(left);
            stringCopy(revLeft, str, mid2);
        } else {
            stringCopy(revLeft, str, mid2);
        }
    }
}

int main() {
    int t;
    cin >> t;

    while (t--) {
        string str;
        cin >> str;
        int i, j, num;
        char ans[str.length() + 3];
        int n = str.length();

        if (all_nine(str)) {  //if all 9
            ans[0] = '1';
            for (i = 0; i < n; i++) {
                ans[i + 1] = '0';
            }
            ans[n] = '1';
            ans[n + 1] = '\0';
            cout << ans << endl;
        }

        else {
            generateNextPalindromeUtil(str, str.length());
            cout << str << endl;
        }
    }
    return 0;
}

/*

Closest Palindrome (Can be next smaller or previous largest)

https://leetcode.com/problems/find-the-closest-palindrome

    many cases

    0. single digit => it should be the digit before this

    There are 5 candidates for being closest palindrome
    1. palindrome is one digit more => 100001
    2. palindrome is one digit less => 99999
    3. palindrome has same digits =>
        a) middle digit(s) same and then copy revLeft into right
        b) middle digit(s) increment by 1 and then copy revLeft into right
        c) middle digit(s) decrement by 1 and then copy revLeft into right
        COPYING revRight into left will increase the difference so not needed
*/
class Solution {
   private:
    void getCandidates(vector<string> &candidates, int mid1, int mid2, int n) {
        //middle digit remain same
        string left = candidates[2].substr(0, mid1 + 1);
        stringCopy(left, reverseString(left), candidates[2]);
        //middle digit increment by 1
        int mid;
        if (n & 1)
            mid = mid1 + 1;
        else
            mid = mid1;

        while (mid >= 0 && candidates[3][mid] == '9') {
            candidates[3][mid--] = '0';
        }
        if (mid < 0)
            candidates[3] = "1" + candidates[3];
        else
            candidates[3][mid]++;
        left = candidates[3].substr(0, mid1 + 1);
        stringCopy(left, reverseString(left), candidates[3]);
        //middle digit decrement by 1
        if (n & 1)
            mid = mid1 + 1;
        else
            mid = mid1;

        while (mid >= 0 && candidates[4][mid] == '0') {
            candidates[4][mid--] = '9';
        }
        if (mid < 0)
            candidates[4] = "1" + candidates[4];
        else
            candidates[4][mid]--;
        left = candidates[4].substr(0, mid1 + 1);
        stringCopy(left, reverseString(left), candidates[4]);
    }

    //copy mirror of left half into right portion
    void stringCopy(string left, string revLeft, string &str) {
        int n = str.length();
        if (n & 1) {
            str = left + str[n / 2] + revLeft;
        } else {
            str = left + revLeft;
        }
    }

    string reverseString(string str) {
        string rev = str;
        reverse(rev.begin(), rev.end());
        return rev;
    }

   public:
    string nearestPalindromic(string str) {
        int n = str.length();
        if (n == 1) {
            if (str == "0") return "1";
            str[0]--;
            return str;
        }

        vector<string> candidates(5, str);

        candidates[0] = "1";
        int iters = n - 1;
        while (iters--) candidates[0] += "0";
        candidates[0] += "1";

        candidates[1] = "";
        iters = n - 1;
        while (iters--) candidates[1] += "9";

        //only one middle digit
        if (n & 1) {
            getCandidates(candidates, n / 2 - 1, n / 2 + 1, n);
        }
        //two middle digits
        else {
            getCandidates(candidates, n / 2 - 1, n / 2, n);
        }
        auto compCandidates = [&](string cA, string cB) {
            //return true if cA should come before cB
            long long diffA = abs(stoll(cA) - stoll(str));
            long long diffB = abs(stoll(cB) - stoll(str));
            if (diffA < diffB) return true;
            if (diffA > diffB) return false;
            return stoll(cA) < stoll(cB);
        };
        sort(candidates.begin(), candidates.end(), compCandidates);
        for (auto c : candidates) {
            if (c != str) return c;
        }
        return "";
    }
};