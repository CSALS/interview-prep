1. https://www.geeksforgeeks.org/largest-subarray-having-sum-greater-than-k/
2. https://www.geeksforgeeks.org/longest-subarray-having-average-greater-than-or-equal-to-x/
3. https://www.geeksforgeeks.org/longest-subarray-having-average-greater-than-or-equal-to-x-set-2/
4. https://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/

4. https://www.geeksforgeeks.org/find-maximum-sum-triplets-array-j-k-ai-aj-ak/?ref=rp
5. https://www.geeksforgeeks.org/find-number-of-triplets-in-array-such-that-aiajak-and-ijk//


1. https://www.geeksforgeeks.org/maximum-lcm-among-all-pairs-i-j-from-the-given-array/?ref=leftbar-rightbar




1. Max (arr[j] - arr[i]) s.t j > i
2. Max (j - i) s.t. arr[j] > arr[i]



uses suffix max/min, prefix max/min


https://www.interviewbit.com/problems/leaders-in-an-array/

/**
 * @Question
 * https://www.interviewbit.com/problems/perfect-peak-of-array/
 * Given an integer array A of size N.
 * You need to check that whether there exist a element which is strictly greater than all the elements on left of it and 
 * strictly smaller than all the elements on right of it.
 
 * @Solution
 * Here we use leftMax[] and rightMin[] arrays
 */
int perfectPeak(vector<int>& A) {
    int N = A.size();
    vector<int> leftMax(N), rightMax(N);  //excluding that element

    rightMax[N - 1] = INT_MAX;
    for (int i = N - 2; i >= 0; --i) {
        rightMax[i] = min(rightMax[i + 1], A[i + 1]);
    }
    leftMax[0] = INT_MIN;
    for (int i = 1; i < N; ++i) {
        leftMax[i] = max(leftMax[i - 1], A[i - 1]);
    }

    //exclude 1st and last element
    for (int i = 1; i < N - 1; ++i) {
        if (A[i] < rightMax[i] && A[i] > leftMax[i]) return 1;
    }
    return 0;
}