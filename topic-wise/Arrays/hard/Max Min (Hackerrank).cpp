#include <bits/stdc++.h>
using namespace std;

/**
 * Problem reduces to choosing K elements from array (need not be consecutive)
 * such that the max of those - min of those must be minimized
 * Solution =
 * 1. Sort the array
 * 2. Use a window of fixed size K and find min of (max - min in that window).
 *    It makes sense to use subarrays only not subsequence (since sorted)
 */

//O(NlogN) T | O(1) S
int maxMin(int K, vector<int> arr) {
    sort(arr.begin(), arr.end());

    //Maintain sliding window of size K
    int low = 0, high = K - 1;
    int minDiff = INT_MAX;
    while (high < arr.size()) {
        minDiff = min(minDiff, arr[high++] - arr[low++]);
    }
    return minDiff;
}