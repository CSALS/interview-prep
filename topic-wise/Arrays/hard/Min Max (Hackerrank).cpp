#include <bits/stdc++.h>
using namespace std;

/**
 * Find maximum of minimum for every window size (1 to n) in a given array. 
 * Given below is O(N^2) Time | Space
 * The O(N) Time | O(N) Space is very hard & confusing. Check it
 * https://www.geeksforgeeks.org/find-the-maximum-of-minimums-for-every-window-size-in-a-given-array/
 * https://www.hackerrank.com/challenges/min-max-riddle/editorial
 */
vector<int> riddle(vector<int> arr) {
    int n = arr.size();
    int dp[n][n];
    //dp[i][j] = min value b/w i...j

    for (int i = 0; i < n; ++i) {
        dp[i][i] = arr[i];
    }
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            //dp[i...j]
            dp[i][j] = min(dp[i][j - 1], arr[j]);
        }
    }

    vector<int> result(n);
    //O(N^2) Time
    for (int length = 1; length <= n; ++length) {  //length of window
        int maxValue = INT_MIN;
        for (int i = 0; i < n; ++i) {
            int j = i + length - 1;
            if (j >= n) break;
            //Iterate over arr[i...j] to find min value
            //And maintain max of all these min values
            //Instead of iterating, pre-compute min(i..j)
            maxValue = max(maxValue, dp[i][j]);
        }
        result[length - 1] = maxValue;
    }
    return result;
}