#include <bits/stdc++.h>
using namespace std;

/**
 * Suffix-array
 * MaxRight[i] = max element on [i...N] = DECREASING
 * MinRight[i] = min element on [i...N] = INCREASING
 * 
 * Prefix-array
 * MaxLeft[i] = max element on [0...i] =  INCREASING
 * Minleft[i] = min element on [0...i] = DECREASING
 */

/**
 * @question
 * Max (arr[j] - arr[i]) s.t j > i
 * 
 * @solution
 * 1. Compute MaxOnRight[i] for each element. Max of [i....N-1]
 * 2. Then for each element i find max of MaxOnRight[i]-arr[i]
 * Used in max profit atmost 1 transaction allowed question (buy and sell stock)
 */
int maxProfit_atmost_1_transaction(vector<int> &prices) {
    //O(N) Time | O(N) Space
    int n = prices.size();
    if (n == 0 || n == 1)
        return 0;
    int maxOnRight[n];
    maxOnRight[n - 1] = prices[n - 1];
    for (int i = n - 2; i >= 0; --i) {
        maxOnRight[i] = max(prices[i], maxOnRight[i + 1]);
    }

    int max_profit = 0;
    for (int i = 0; i < n - 1; ++i) {
        max_profit = max(max_profit, maxOnRight[i] - prices[i]);
    }
    return max_profit;

    //O(N) Time | O(1) Space
    int n = prices.size();
    if (n == 0) return 0;

    int maxProfit = 0;
    int rightMax = prices[n - 1];
    for (int i = n - 2; i >= 0; --i) {
        rightMax = max(rightMax, prices[i]);
        maxProfit = max(maxProfit, rightMax - prices[i]);
    }
    return maxProfit;
}

/**
 * @question IMP
 * https://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/
 * Max (j - i) s.t. arr[j] > arr[i]
 * 
 * @solution
 * 1. Brute force
 * For each index i, iterate from j = n -1 down to i and find max(j - i) s.t arr[j] > arr[i]
 * O(N^2) Time | O(N) Space
 * 2. (??) Can use hashing to store index and then sort the array. Then use two pointers i = 0 and j  = n - 1
 * O(NlogN) TIme | O(N) Space
 * 3. EFFICIENT
 *      a) Compute LeftMin[] and RightMax[] for each index. Both will be decreasing in nature
 *      b) Traverse both arrays from left to right to obtain max(j - i)
 */

int maxIndexDiff(int arr[], int n) {
    int maxDiff;
    int i, j;

    vector<int> LMin(n, 0), RMax(n, 0);

    LMin[0] = arr[0];
    for (i = 1; i < n; ++i)
        LMin[i] = min(arr[i], LMin[i - 1]);

    RMax[n - 1] = arr[n - 1];
    for (j = n - 2; j >= 0; --j)
        RMax[j] = max(arr[j], RMax[j + 1]);

    i = 0, j = 0, maxDiff = -1;
    while (j < n && i < n) {
        // i.......j and when leftmin(i) < rightmax(j) then arr[i] < arr[j] so store max(j - i) and increment j to find better j - i
        if (LMin[i] < RMax[j]) {
            maxDiff = max(maxDiff, j - i);
            j = j + 1;
        } else {
            // 0....i....j and when leftmin(i) > rightmax(j) then need to increment i since [0...i] is decreasing
            i = i + 1;
        }
    }

    return maxDiff;
}