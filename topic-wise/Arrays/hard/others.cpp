/**
 * Max XOR of max. and 2nd max in subarrays
 * @question
 * 1. https://www.geeksforgeeks.org/maximum-xor-value-of-maximum-and-second-maximum-element-among-all-possible-subarrays/
 * 2. shortest subarray with max. xor of max and second max in the sub-array
 * 
 * @solution-1
 * Let arr[i] be 2nd largest element. Then the 1st largest will be next greater element in the right.
 * Compute XOR and store max.
 * 1st largest can also be the next greater element in the left
 * Compute XOR and store max.
 * NGR and NGL can be computed using stacks in O(N) T
 * Now run our algo for each element
 * 
 * @solution-2
 * O(N) Time | O(N) Space Solution
 * Since need smallest subarray = the 2nd largest & largest must be at corners of subarrays
 * Compute NGR and NGL for each element
 * For each element arr[i] assume it is 2nd largest element in a subarray towards right. It's NGR[i] is the largest in the subarray
 * If you take the largest on right that doesn't work since arr[i] may not be 2nd largest in that case
 * Similarily it's NGL[i] is the largest in the subarray towards left.
 * Do this for each element and get the maximum.
 * You can use method for XOR, AND, OR (doesn't depend on it)
 */

/**
 * https://www.geeksforgeeks.org/minimize-the-maximum-difference-between-adjacent-elements-in-an-array/
 * Given a non-decreasing array arr[] and an integer K, 
 * the task is to remove K elements from the array such that maximum difference between adjacent element is minimum.
 * 
 * main problem -> finding the subarray of size N-K whose max diff b/w adjcanet eles is min.
 *              -> finding max. of every window in difference array of size N-K-1 -> using deque for finding max in every window (Sliding Window.cpp)
 *                  and it will be min. for sure since we are not removing adjacent eles, we are only considering subarray
 */