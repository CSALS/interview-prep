#include <bits/stdc++.h>
using namespace std;

const int mod = 1e9 + 7;

void dfsCalculateDP(int u, int p, vector<int> &values, vector<int> graph[], vector<int> &dp, long long &maxProduct) {
    dp[u] = values[u];
    for (int v : graph[u]) {
        if (v == p) continue;
        dfsCalculateDP(v, u, values, graph, dp, maxProduct);
        dp[u] += dp[v];
        //remove u----v edge
        long long currentProduct = (dp[v] % mod * (dp[u] - dp[v]) % mod) % mod;
        maxProduct = max(maxProduct, currentProduct);
    }
}

int deleteEdge(vector<int> values, vector<vector<int> > B) {
    int N = values.size();
    vector<int> graph[N];
    for (vector<int> edge : B) {
        graph[edge[0] - 1].push_back(edge[1] - 1);
        graph[edge[1] - 1].push_back(edge[0] - 1);
    }

    vector<int> dp(N, 0);  //dp[u] = sum of values of its subtrees including it

    long long maxProduct = 0;
    dfsCalculateDP(0, -1, values, graph, dp, maxProduct);
    for (int u = 0; u < N; ++u) {
        cout << u + 1 << " " << dp[u] << endl;
    }
    return maxProduct;
}
int main() {
    deleteEdge({10, 5, 12, 6, 10, 100, 8},
               {{1, 2}, {1, 4}, {4, 3}, {4, 5}, {2, 6}, {2, 7}});
}

/*

offset=11

  0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
[ 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0,  1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0 ]

  0  1  2  3  4  5
[ 1, 1, 0, 0, 1, 1 ]

*/