#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/maximal-square
class LargestAreaSquare {
   public:
    int maximalSquare(vector<vector<char>>& matrix) {
        if (matrix.empty() || matrix[0].empty())
            return 0;
        int rows = matrix.size();
        int cols = matrix[0].size();
        int dp[rows][cols];
        memset(dp, 0, sizeof(dp));
        //dp[i][j] = max length of square with (i, j) as bottom-right corner

        //Init
        dp[0][0] = (matrix[0][0] == '1') ? 1 : 0;
        for (int col = 1; col < cols; ++col) {
            if (matrix[0][col] == '1')
                dp[0][col] = 1;
        }
        for (int row = 1; row < rows; ++row) {
            if (matrix[row][0] == '1')
                dp[row][0] = 1;
        }

        //Recurrence
        for (int row = 1; row < rows; ++row) {
            for (int col = 1; col < cols; ++col) {
                if (matrix[row][col] == '1') {
                    dp[row][col] = 1 + min({dp[row - 1][col], dp[row][col - 1], dp[row - 1][col - 1]});
                }
            }
        }

        int maxLengthSquare = 0;
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                maxLengthSquare = max(maxLengthSquare, dp[row][col]);
                cout << dp[row][col] << " ";
            }
            cout << endl;
        }
        return maxLengthSquare * maxLengthSquare;
    }
};

//https://leetcode.com/problems/maximal-rectangle/
class LargestAreaRectangle {
   public:
    /**
     * 1. Find NSR and NSL on each index
     * 2. For each index the area is (NSL[i] - NSR[i] - 1) * heights[i]
     */
    int largestRectangleArea(vector<int>& heights) {
        int n = heights.size();
        //Find NSL for each index
        stack<int> st;
        int NSL[n];
        for (int i = 0; i < n; ++i) {
            while (!st.empty() && heights[i] <= heights[st.top()]) st.pop();
            NSL[i] = (st.empty()) ? -1 : st.top();
            st.push(i);
        }
        while (!st.empty()) st.pop();

        //Find NSR for each index
        int NSR[n];
        for (int i = n - 1; i >= 0; --i) {
            while (!st.empty() && heights[i] <= heights[st.top()]) st.pop();
            NSR[i] = (st.empty()) ? n : st.top();
            st.push(i);
        }

        //Calculate areas
        int maxArea = 0;
        for (int i = 0; i < n; ++i) {
            maxArea = max(maxArea, heights[i] * (NSR[i] - NSL[i] - 1));
        }
        return maxArea;
    }
    /**
     * O(rows*col) Time | O(cols) space
     * if cols are really large than rows then you can do this for rows instead of cols.
     * So if rows are larger than cols then use leftRow & rightRow bounds and calculate col-sum.
     */

    int maximalRectangle(vector<vector<char>>& matrix) {
        if (matrix.empty() || matrix[0].empty())
            return 0;
        int rows = matrix.size();
        int cols = matrix[0].size();
        vector<int> heights(cols, 0);
        int maxArea = 0;
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                if (matrix[row][col] == '0')
                    heights[col] = 0;
                else
                    heights[col]++;
            }
            //We get one level of heights
            maxArea = max(maxArea, largestRectangleArea(heights));
        }
        return maxArea;
    }
};

/**
 * Important variation of LargestAreaRectangle
 * https://www.interviewbit.com/problems/largest-area-of-rectangle-with-permutations/?ref=random-problem
 * https://www.geeksforgeeks.org/find-the-largest-rectangle-of-1s-with-swapping-of-columns-allowed/
 */
int largestAreaRectangleWithColumnsPermutations(vector<vector<int>> matrix) {
    if (matrix.empty() || matrix[0].empty()) return 0;
    int rows = matrix.size(), cols = matrix[0].size();
    vector<vector<int>> count(rows, vector<int>(cols, 0));
    //count[i][j] = in jth column -> number of consecutive ones ending at (i, j)
    for (int j = 0; j < cols; ++j) {
        count[0][j] = matrix[0][j];
    }

    for (int i = 1; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (matrix[i][j] == 0)
                count[i][j] = 0;
            else
                count[i][j] = 1 + count[i - 1][j];
        }
    }

    int max_area = 0;
    for (int row = 0; row < rows; ++row) {
        vector<int> row_count = count[row];
        sort(row_count.begin(), row_count.end());
        //Traverse from descending order
        for (int col = cols - 1; col >= 0; --col) {
            int curr_area = (cols - col) * row_count[col];
            max_area = max(max_area, curr_area);
        }
    }

    return max_area;
}
