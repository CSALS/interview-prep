#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-the-minimum-distance-between-two-numbers/
 * Given an unsorted array arr[] and two numbers x and y, find the minimum distance between x and y in arr[]. 
 * The array might also contain duplicates. You may assume that both x and y are different and present in arr[]
 */
int minDist(int a[], int n, int x, int y) {
    int prev_x = -1, prev_y = -1;  //prev occ of x and y
    int min_diff = INT_MAX;
    //basically checking consecutive (x, y) | (y, x) pairs. we only need to check them
    for (int i = 0; i < n; ++i) {
        if (a[i] == x) prev_x = i;
        if (a[i] == y) prev_y = i;

        if (prev_x != -1 && prev_y != -1) {
            //found (x, y) pair
            min_diff = min(min_diff, abs(prev_y - prev_x));
        }
    }
    if (min_diff == INT_MAX)
        return -1;
    else
        return min_diff;
}