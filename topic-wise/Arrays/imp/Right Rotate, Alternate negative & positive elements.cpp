#include <bits/stdc++.h>
using namespace std;

//In-place rotation
void rightRotateByOne(vector<int> &arr, int i, int j) {
    //right rotate [i...j] by one meaning result => [j i i+1 i+2 ....j-1]
    int temp = arr[j];
    for (int index = j - 1; index >= i; --index) {
        arr[index + 1] = arr[index];
    }
    arr[i] = temp;
}
//If you want to rotate by K time
void rightRotate(vector<int> &arr, int K) {
    //rotate in place by K positions
    //If k = 2 then [i......j] = [j-1 j-2 i......j-3]
    int size = arr.size() - 1;
    for (int ind = 0; ind < K; ++ind) {
        swap(arr[ind], arr[size - K + ind + 1]);
    }
    reverse(arr.begin() + K + 1, arr.end());
    reverse(arr.begin() + K, arr.end());
}

/**
 * IMP
 * https://www.geeksforgeeks.org/rearrange-array-alternating-positive-negative-items-o1-extra-space/
 * Alternate negative and positive elements (maintain the order of apperance)
 *  Input:  arr[] = {1, 2, 3, -4, -1, 4}
    Output: arr[] = {-4, 1, -1, 2, 3, 4}

    Input:  arr[] = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8}
    output: arr[] = {-5, 5, -2, 2, -8, 4, 7, 1, 8, 0} 
    
 * 1. Using extra space is simple (linear time & space)
 * 2. Constant space is difficult (quadratic time & constant space)
 * Variation = If order need not be maintained then solve in O(N) Time | O(1) Space
 * https://www.geeksforgeeks.org/rearrange-array-in-alternating-positive-negative-items-with-o1-extra-space-set-2/
 * In this move all negative elements at the end and then it is easy to place them in alternate fashion
 */
vector<int> solve1(vector<int> &A) {
    int N = A.size();
    vector<int> odds, evens;

    for (int num : A) {
        //non negative will come at odd position
        if (num >= 0)
            odds.push_back(num);
        //negative will come at even position
        else
            evens.push_back(num);
    }
    if (odds.empty())
        return evens;
    if (evens.empty())
        return odds;
    int oddPtr = 0;
    int evenPtr = 0;
    vector<int> result;
    while (oddPtr < odds.size() && evenPtr < evens.size()) {
        result.push_back(evens[evenPtr++]);
        result.push_back(odds[oddPtr++]);
    }
    while (evenPtr < evens.size()) {
        result.push_back(evens[evenPtr++]);
    }
    while (oddPtr < odds.size()) {
        result.push_back(odds[oddPtr++]);
    }
    return result;
}

//Uses rotation
vector<int> solve2(vector<int> &arr) {
    //Modify in place
    //Negative should be at even and positive at odd
    int N = arr.size();
    for (int i = 0; i < N; ++i) {
        //Check for out of order element
        int num = arr[i];
        if (num < 0 && i & 1) {
            //Find first opposite sign element and then right rotate [i...j] by one rotation to maintain order
            int j = i + 1;
            while (j < N && arr[j] < 0)
                j++;
            if (j >= N) break;
            rightRotateByOne(arr, i, j);
        } else if (num >= 0 && i % 2 == 0) {
            //Find first opposite sign element and then right rotate [i...j] by one rotation to maintain order
            int j = i + 1;
            while (j < N && arr[j] >= 0)
                j++;
            if (j >= N) break;
            rightRotateByOne(arr, i, j);
        }
    }
    return arr;
}
