#include <bits/stdc++.h>
using namespace std;

//For segregate questions maintain head, tail of the halfs

/**
 * https://www.geeksforgeeks.org/segregate-0s-and-1s-in-an-array-by-traversing-array-once/
 * Segregate 0s and 1s in an array (Sort Binary Array)
 * 1. Using two pointers one at beginning, one at end
 * 2. One pointer to oneHead and another iterator [Maintains ordering of 1st half elements. 2nd half elemnets may not be in same order]
 * These methods will work for sorting array where there are only two types of elements (0s, 1s or even, odd)
 */

//Doesnt Maintians order of left half elemnets
void sortBinaryArray1(vector<int> &arr) {
    int left = 0, right = arr.size() - 1;
    while (left < right) {
        //arr[left] = 1 & arr[right] = 0 is mismatch so need to swap
        //find that pair
        while (left < right && arr[left] == 0)
            left++;
        while (left < right && arr[right] == 1)
            right--;
        if (left < right) {
            arr[left++] = 0;
            arr[right--] = 1;
        }
    }
    printArray(arr);
}

//Maintians order of left half elemnets
void sortBinaryArray2(vector<int> &arr) {
    if (arr.empty() || arr.size() == 1) {
        printArray(arr);
        return;
    }

    //0000000001111110
    //'i' is at last zero and zeroTail will be at first 1
    //after swapping them we can move zeroTail to next pos.
    int zeroTail = 0;
    for (int i = 0; i < arr.size(); ++i) {
        if (arr[i] == 0) {
            swap(arr[i], arr[zeroTail]);
            zeroTail++;
        }
    }
    printArray(arr);
}

/**
 * https://www.interviewbit.com/problems/remove-element-from-array/
 * Variation of two types of elements
    Given an array and a value, remove all the instances of that value in the array.
    Also return the number of elements left in the array after the operation.
    It does not matter what is left beyond the expected length.

    [4, 1, 1, 2, 1, 3] => [4, 2, 3, 1, 1, 1]
 */
int removeElement(vector<int> &A, int B) {
    int N = A.size();
    int diffTail = 0;  //points to B value where we will swap any eles other than B to maintain order
    for (int i = 0; i < N; ++i) {
        if (A[i] != B) {
            swap(A[i], A[diffTail++]);
        }
    }
    return diffTail;
}

/**
 * https://leetcode.com/problems/move-zeroes/
 * Move Zeroes To The end while maintaing relative order of non-zero elements
 * Use zeroHead pointer
 */

void moveZeroes(vector<int> &nums) {
    int n = nums.size();
    if (n == 0 || n == 1) return;

    int zeroHead;  //points to first zero available where we will swap non-zero
    for (zeroHead = 0; zeroHead < n; ++zeroHead)
        if (nums[zeroHead] == 0) break;

    for (int i = zeroHead + 1; i < n; ++i) {
        if (nums[i] != 0) {
            swap(nums[i], nums[zeroHead++]);
        }
    }
}

/**
 * https://www.geeksforgeeks.org/segregate-even-and-odd-numbers/
 * Segregate Even and Odd numbers
 * Using oddHead pointer
 */
void segregrateEvenOdd(vector<int> &arr) {
    int n = arr.size();

    //will point to first odd. the position in which we will swap even number
    int evenTail = 0;
    //arr[0.....evenTail - 1] are even so don't bother about them
    for (int i = 0; i < n; ++i) {
        if (arr[i] % 2 == 0) {
            swap(arr[i], arr[evenTail++]);
        }
    }
    printArray(arr);
}

/** IMPPPPPP
 * https://leetcode.com/problems/sort-colors
 * Segregrate 0s, 1s, 2s (DUTCH NATIONAL FLAG PROBLEM)
 * @throw Invalid input exception
 */
void sortColors(vector<int> &nums) {
    int n = nums.size();
    if (n <= 1) return;

    int low = 0;       //where we will keep 0s
    int high = n - 1;  //where we will keep 2s

    //iterator to find out mismatches
    //stop traversing if mid > high
    for (int mid = 0; mid <= high;) {
        if (nums[mid] == 1) {
            //we can just skip since we want 1s in the middle only
            mid++;
        } else if (nums[mid] == 0) {
            swap(nums[low++], nums[mid++]);
        } else if (nums[mid] == 2) {
            //we might swap 0 or 1 to nums[mid]
            //so don't increment it yet
            // If it is 1 then we will increase anyway in the next iteration
            //but if it is 0 then need to swap
            swap(nums[mid], nums[high--]);
        } else {
            throw "Invalid input. Should contain only 0, 1, 2";
        }
    }
}

/*
Variation => removing duplicates in sorted array
*/

/* SORTED ARRAYS  === IMP*/

/**
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-array/
 * Given a sorted array, remove the duplicates in place such that each element appears only once and return the new length.
 */
int removeDuplicates1(vector<int> &A) {
    if (A.empty()) return 0;
    int N = (int)A.size();

    //1. Two traversals

    //replace all duplicates with dummy value (which won't occur in the array)
    int dummy = A[0] - 1;
    int prev = A[0];
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev)
            A[i] = dummy;
        else
            prev = A[i];
    }

    //segregrate dummy values to the end
    int uniqueTail = 0;  //where we will put the unique values
    for (int i = 0; i < N; ++i) {
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }

    return uniqueTail;

    //2. Only one traversal
    int dummy = A[0] - 1;
    int prev = A[0];
    int uniqueTail = 0;

    for (int i = 0; i < N; ++i) {
        if (i != 0) {
            if (A[i] == prev)
                A[i] = dummy;
            else
                prev = A[i];
        }
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }
    return uniqueTail;

    //3. Only one traversal + no modification
    int prev = A[0];
    int uniqueTail = 1;
    for (int i = 1; i < N; ++i) {
        // if (i != 0) {
        if (A[i] == prev) {
            continue;
        } else {
            prev = A[i];
            swap(A[i], A[uniqueTail++]);
        }
        // }
    }
    return uniqueTail;
}
/**
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-array-ii/
 * Given a sorted array, remove the duplicates in place such that each element appears atmost twice and return the new length.
 */
int removeDuplicates(vector<int> &A) {
    if (A.empty()) {
        return 0;
    }

    int N = (int)A.size();

    /* Two Traversals */

    //replace all duplicates with dummy value (which won't occur in the array)
    int dummy = A[0] - 1;
    int prev = A[0];
    int freq = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev) {
            if (freq >= 2)
                A[i] = dummy;
            else
                freq++;
        } else {
            prev = A[i];
            freq = 1;
        }
    }

    //segregrate dummy values to the end
    int uniqueTail = 0;  //where we will put the unique values
    for (int i = 0; i < N; ++i) {
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }

    return uniqueTail;

    /* One traversal + no modify. converting above code to this. 
        just check when you are not making A[i] as dummy. in those cases swap */
    int uniqueTail = 1;  //where we will put the unique values
    int prev = A[0];
    int freq = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev) {
            if (freq >= 2)
                continue;
            else
                freq++;
        } else {
            prev = A[i];
            freq = 1;
        }
        swap(A[i], A[uniqueTail++]);
    }

    return uniqueTail;
}

void printArray(vector<int> &arr) {
    for (int num : arr) cout << num << " ";
    cout << endl;
}
