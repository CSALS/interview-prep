#include <bits/stdc++.h>
using namespace std;

/**
 * IMP
 * https://leetcode.com/problems/find-the-duplicate-number/
 * Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), 
 * prove that at least one duplicate number must exist. Assume that there is only one duplicate number, find the duplicate one.
 * 1) Using Hashing. O(N) Time | O(N) Space
 * 2) Works for positive integers only =>
 *    Modifying array => marking the arr[arr[i]] as -1 and if arr[arr[i]] is already -1 then arr[i] is duplicate
 * 3) O(N) Time | O(1) Space. No modifying array. Uses linked - list cycle detection
 *    Traverse linked list using index of array and if there is a cycle that means there is a number repeating. Start node of cycle is duplicate
 */
int findDuplicate2(vector<int>& nums) {
    //Modifying array
    int index = 0;
    while (true) {
        if (nums[nums[index]] == -1)
            return nums[index];
        int temp = nums[nums[index]];
        nums[nums[index]] = -1;
        nums[index] = temp;
    }
    return -1;
}

//imagine linked list of the actual values and find start of the cycle
int findDuplicate3(vector<int>& nums) {
    int slow = nums[0];
    int fast = nums[0];
    do {
        slow = nums[slow];
        fast = nums[nums[fast]];
    } while (fast != slow);

    slow = nums[0];
    while (slow != fast) {
        slow = nums[slow];
        fast = nums[fast];
    }
    return slow;
}

/**
 * https://www.interviewbit.com/problems/max-chunks-to-make-sorted/
 * Whenever elements are in 0 to N-1 then there is some signifcance for that (Think of index in array)
 * Given an array of integers A of size N that is a permutation of [0, 1, 2, …, (N-1)],
 * if we split the array into some number of “chunks” (partitions), and individually sort each chunk.
 * After concatenating them, the result equals the sorted array.
 * What is the most number of chunks we could have made?
 * 
 * TODO: https://leetcode.com/problems/max-chunks-to-make-sorted-ii/
 * Input = 2 1 3 4 4     
 * Sort the array = 1 2 3 4 4
 * Keep computing prefix sum and if they are equal then partition there
 * Will work because numbers are in 0....N-1 permutations
 */

int solve(vector<int>& arr) {
    int result = 0;
    int maxNo = -1;
    for (int i = 0; i < arr.size(); ++i) {
        maxNo = max(maxNo, arr[i]);
        if (maxNo == i) {
            result++;    //found a chunk we can sort since left side eles will be < maxNo or else we would got maxNo a diff value
            maxNo = -1;  //unecessary since next numbers will be greater anyway
        }
    }
    return result;
}

//----------------------------------------------------
/**
 * IMP
 * Count frequency of elements in the range of 1 to N
 * https://www.geeksforgeeks.org/count-frequencies-elements-array-o1-extra-space-time/
 * Can use this method in many other problems
 * 
 *  Input: arr[] = {2, 3, 3, 2, 5}
    Output: Below are frequencies of all elements
            1 -> 0
            2 -> 2
            3 -> 2
            4 -> 0
            5 -> 1
 */

//Marking frequency count as non-positive (-ve or 0)
void printFrequency(vector<int> arr, int n) {
    for (int i = 0; i < n;) {
        if (arr[i] <= 0) {
            i++;
            continue;
        }

        int element_index = arr[i] - 1;  //Since [1...N] range and indexes are in [0...N-1]

        if (arr[element_index] <= 0) {
            //this element presence is already marked
            //Not the first occurence so just increment the freq count
            arr[element_index]--;
            arr[i] = 0;
            i++;
        } else {
            //first presence of the element
            arr[i] = arr[element_index];
            arr[element_index] = -1;
        }
    }

    for (int i = 0; i < n; ++i) {
        cout << abs(arr[i]) << " ";
    }
    cout << endl;
}

/**
 * https://www.geeksforgeeks.org/check-array-elements-consecutive-time-o1-space-handles-positive-negative-numbers/
 * Given an unsorted array of distinct numbers, write a function that returns true if array consists of consecutive numbers.
 * 1. Using sorting O(NlogN) Time | O(1) Space
 * 2. Find min of the array and subtract all elements with min and then use frequency sort method and check if all guys freq. is 1. If 0 or > 1 then not consecutive
 *   O(N) Time | O(1) Space
 */

/**
 * https://practice.geeksforgeeks.org/problems/find-duplicates-in-an-array/1
 * Array has elements in [0....N - 1] order
 * Since in above function we mark 0 for frequency here 0 is itself element
 * So convert numbers into [1...N] order
 */

/**
 * https://leetcode.com/problems/first-missing-positive
 * Given array containing both positive and negative numbers find the first missing positive number
 * We don't care about -ve and 0 and +ve eles > N 
 * We only care about numbers in [1...N]
 * Whenever you encounter that number mark the presence in (arr[i]-1)th index by using -1.
 * After this first first index having value as not -1 that is missing.
 * If every number from 1....N is present then ans is N+1
 */
int firstMissingPositive(vector<int>& arr) {
    int n = arr.size();
    for (int& x : arr) {
        if (x < 0) x = 0;
        if (x > n) x = 0;
    }
    //now we only have elements in [1....N] range and 0s
    for (int i = 0; i < n;) {
        if (arr[i] <= 0) {
            ++i;
        } else {
            //mark presence of 1....N in (value-1)index
            int element_index = arr[i] - 1;
            if (arr[element_index] == 0) {
                arr[element_index] = -1;
                arr[i] = 0;
                ++i;
            } else if (arr[element_index] == -1) {
                //already marked the presence so skip
                ++i;
            } else {
                arr[i] = arr[element_index];
                arr[element_index] = -1;
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        // cout << arr[i] << " ";
        if (arr[i] != -1) return i + 1;
    }
    return n + 1;
}
//----------------------------------------------------
/**
 * IMP IMP
 * https://www.interviewbit.com/problems/rearrange-array/
 * Rearrange a given array so that Arr[i] becomes Arr[Arr[i]] with O(1) extra space.
 * Elements will be range of 0 to N-1 since we need to use arr[arr[i]].
 * With extra space you can use pairs to store prev and new value.
 * Without extra space we just store both values in one cell
 */
void arrange(vector<int>& A) {
    //To represent (a, b) using one number use T = a * N + b
    //a = T/N & b = T%N
    //a= prev value, b = new value
    //we can do this because a and b will be < N
    int N = A.size();
    for (int i = 0; i < N; ++i) {
        int prevVal = A[i];
        int newVal;
        //We process from left to right. So [0...i-1] are already storing 2 elements
        if (A[i] < i)
            newVal = A[A[i]] / N;
        else
            newVal = A[A[i]];
        A[i] = prevVal * N + newVal;
    }

    for (int i = 0; i < N; ++i) {
        A[i] = A[i] % N;
    }
}

int main() {
    printFrequency({2, 3, 2, 3, 5}, 5);
    printFrequency({3, 3, 3, 3}, 4);
    printFrequency({11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, 11);
}