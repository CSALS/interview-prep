#include <bits/stdc++.h>
#define deb(x) cout << #x << " " << x << endl;
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-next-greater-number-set-digits/
 * Works on string permutations also
 * Variation = https://practice.geeksforgeeks.org/problems/next-greater-even-number/0 (Generate next permutations till you get even number)
 */
vector<int> nextPermutation(vector<int> &A) {
    int N = A.size();
    int startIndex = N - 1;  // starting index of decreasing subarray
    while (startIndex >= 1 && A[startIndex - 1] > A[startIndex]) {
        startIndex--;
    }
    if (startIndex != 0) {
        // find next greater element to A[startIndex-1] in [startIndex...N-1]
        // subarray
        int greaterIndex;
        for (int i = N - 1; i >= startIndex; --i) {
            if (A[startIndex - 1] < A[i]) {
                greaterIndex = i;
                break;
            }
        }
        // swap A[sI] & A[gI]
        int temp = A[greaterIndex];
        A[greaterIndex] = A[startIndex - 1];
        A[startIndex - 1] = temp;
    }
    // reverse [startIndex...N-1] subarray
    reverse(A.begin() + startIndex, A.end());
    return A;
}
