#include <bits/stdc++.h>
using namespace std;

//TODO: https://www.geeksforgeeks.org/minimum-incrementdecrement-to-make-array-non-increasing/

/**
 * @Question
 * Minimum operation to make all elements equal in array
 * Given an array with n positive integers. We need to find the minimum number of operation to make all elements equal.
 * We can perform addition, subtraction with any element on any array element
 * 
 * @Solution
 * Basically convert all elements into mode of array by using just 1 addition/subtraction
 */

/**
 * @Question
 * https://www.geeksforgeeks.org/minimum-number-of-increment-decrement-operations-such-that-array-contains-all-elements-from-1-to-n/?ref=rp
 * Minimum number of increment/decrement operations such that array contains all elements from 1 to N exactly once
 * [4, 1, 1] => 2 ([3, 1, 2])
 * 
 * @Solution
 * Simply sort the array
 * Find how many operations needed to convert arr[i] into (i + 1) [0-based indexing]
 */

/**
 * @Question
 * Minimum Increment / decrement to make array elements equal
 * Given an array of integers. In one operation you can either Increment/Decrement any element by 1. 
 * The task is to find the minimum operations needed to be performed on the array elements to make all array elements equal.
 * 
 * @Solution
 * Median minimizes the sum of absolute deviation.
 * So convert all elements into median.
 * If there are two medians (in case of even length array) then use both of them and find min operations out of them
 */

/**
 * @Question
 * https://www.interviewbit.com/problems/minimum-operations-of-given-type-to-make-all-elements-of-a-matrix-equal/
 * Minimum operations of given type to make all elements of a matrix equal
 * where opeartion => subtract / add K to element
 * 
 * @Solution
 * The median minimizes the the sum of absolute deviations. So make all elements equal to median
 * Median = x + count * K
 * count = abs(Median - X)/K (if not divisible then it is not possible to make all elements equal)
 * If even length matrix then use both middle elements and find the min ops req.
 */
int solve(vector<vector<int> > &A, int B) {
    int N = A.size();
    int M = A[0].size();
    vector<int> arr;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            arr.push_back(A[i][j]);
        }
    }
    sort(arr.begin(), arr.end());
    int median = arr[(N * M) / 2];
    int ops1 = 0;
    for (int i = 0; i < N * M; ++i) {
        if (abs(arr[i] - median) % B != 0)
            return -1;
        ops1 += abs(arr[i] - median) / B;
    }

    //If number of elements are even then consider both the medians and take min of them
    int ops2 = INT_MAX;
    if ((N * M) % 2 == 0) {
        median = arr[(N * M) / 2 - 1];
        for (int i = 0; i < N * M; ++i) {
            if (abs(arr[i] - median) % B != 0)
                return -1;
            if (ops2 == INT_MAX) {
                ops2 = abs(arr[i] - median) / B;
            } else {
                ops2 += abs(arr[i] - median) / B;
            }
        }
    }
    return min(ops1, ops2);
}

/**
 * @Question
 * Min. operators to make array sorted
 */

/*  @Solution

https://www.geeksforgeeks.org/minimum-increment-or-decrement-operations-required-to-make-the-array-sorted/

Given an array arr[] of N integers, the task is to sort the array in non-decreasing order by performing the minimum number of operations. 
In a single operation, an element of the array can either be incremented or decremented by 1. 
Print the minimum number of operations required.

    IMP Observation => Any number shouldn't go below min. number and also shouldn't go above max. number

    dp[0.....N-1][minEle.....maxEle]

    dp[i][j] = making [0....i] sorted and last element will be j

    //Base Case
    for(int j = minEle; j <= maxEle; ++j)
        dp[0][j] = abs(j - arr[0]) //making first element as j

    //Recurrence
    for(int i = 1; i < n; ++i) {
        for(int j = minEle; j <= maxEle; ++j) {
            dp[i][j] = INF
            for(int k = 1; k <= j; ++k) {
                dp[i][j] = min(dp[i][j], dp[i - 1][k] + abs(arr[i] - j))
            }
        }
    }

    //Recurrence-Optimized
    for(int i = 1; i < n; ++i) {
        //maintain minimum of dp[i - 1][j] till now and use that for dp[i][j]
        int min = INF
        for(int j = minEle; j <= maxEle; ++j) {
            min = min(min, dp[i - 1][j])
            dp[i][j] = min + abs(arr[i] - j) //min stores minimum of dp[i-1][k] where k is 1...j
        }
    }

    //Answer
    int ans = INF
    for(int j = minEle; j <= maxEle; ++j) 
        ans = min(ans, dp[N - 1][j])
*/
