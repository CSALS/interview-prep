#include <bits/stdc++.h>
using namespace std;

/**
 * Prints the Largest length subarray with sum K (general version)
 * O(N) Time | O(N) Space
 */
vector<int> largestLengthSubarray(vector<int> arr, int K) {
    long long sum = 0;
    int left = 0, right = -1;
    unordered_map<int, int> sumIndex;  //subarray sum -> ending index of subarray
    for (int i = 0; i < arr.size(); ++i) {
        sum += arr[i];
        if (sum == K) {
            int size = i + 1;
            if (size > right - left + 1) {
                left = 0;
                right = i;
            }
        } else {
            if (sumIndex.find(sum - K) == sumIndex.end()) {
                sumIndex[sum] = i;
            } else {
                //[l+1 ..... r] has sum as K
                int l = sumIndex[sum - K] + 1;
                int r = i;
                if (r - l + 1 > right - left + 1) {
                    left = l;
                    right = r;
                }
            }
        }
    }
    vector<int> result;
    for (int i = left; i <= right; ++i)
        result.push_back(arr[i]);
    return result;
}

/**
 * @Variation https://www.geeksforgeeks.org/longest-span-sum-two-binary-arrays/ 
 * Given two binary arrays arr1[] and arr2[] of same size n. 
 * Find length of the longest common span (i, j) where j >= i such that arr1[i] + arr1[i+1] + …. + arr1[j] = arr2[i] + arr2[i+1] + …. + arr2[j].
 * 
 * @Solution
 * Subtract two arrays and find largest subarray with zero sum
 * Why? since a1[i]+a1[i+1]+....+a1[j] = (j-i+1)/2 = a2[i]+a2[i+1]+...+a2[j] => (a1[i] - a2[i]) + (a1[i+1] - a2[i+1]) +...+ (a1[j] - a2[j]) = 0.
 */

/**
 * Number of subarrays with sum K (general version)
 * O(N) Time | O(N) Space
 */
int countSubarrayWithSumK(vector<int> arr, int K) {
    long long sum = 0;
    unordered_map<int, int> sumCount;  //subarray sum -> count of how many times it has occured
    int result = 0;
    for (int i = 0; i < arr.size(); ++i) {
        sum += arr[i];
        if (sum == K) {
            result++;
        }

        if (sumCount.find(sum - K) != sumCount.end()) {
            //[l+1 ..... r] has sum as K
            result += sumCount[sum - K];
        }

        sumCount[sum]++;
    }
    return result;
}
/**
 * @Variation Given a binary array count number of subarrays with equal no. of 0s and 1s in it.
 * @Solution 
 * Replace 0 with -1 and find number of subarrays with sum as 0. It is the answer. Can be extended to binary matrix also
 */

/**
 * @Variation = count subarrays with even sum
 * @Solution
 *      we want to find (s2 - s1) % 2 == 0
 *      A number divides Difference when it has remainder when it divides them separtely
 *      i.e. s1%2 == s2%2
 *      There are only two remainders possible. So store the remainderCount of 0 and 1
 *      and for a new prefix sum add count of same remainder
 */
int countSubarraysWithEvenSum(vector<int> arr, int n) {
    //[0] => count of prefix sums with 0 remainder
    //[1] => count of prefix sums with 1 remainder
    vector<int> remainderCount(2, 0);

    int sum = 0;
    int count = 0;  //count of even sum subarrays
    for (int i = 0; i < n; ++i) {
        sum += arr[i];
        if (sum % 2 == 0) count++;
        count += remainderCount[sum % 2];
        remainderCount[sum % 2]++;
    }
    return count;
}

/**
 * @varation = find if subarray exists whose sum is multiple of k
 * Generalized version of above solution
 * @solution
 *      basically we want to find subarray sum divisible by k
 *      (sum - s)%k == 0 when sum%k == s%k
 *      So store seen remainders
 */
bool checkSubarraySum(vector<int>& nums, int k) {
    if (nums.empty()) return false;

    //find contiguous subarray of size 2 with all 0s
    bool subarray0sSize2Exists = false;
    for (int i = 0; i < nums.size() - 1; ++i) {
        if (nums[i] == 0 && nums[i + 1] == 0) {
            subarray0sSize2Exists = true;
            break;
        }
    }
    //0 sum divisible by any K
    if (subarray0sSize2Exists) {
        return true;
    }
    if (k == 0) {
        return false;
    }

    unordered_map<int, int> index;  //remainder -> index
    int sum = 0;
    for (int i = 0; i < nums.size(); ++i) {
        int num = nums[i];
        sum += num;
        if (sum % k == 0 && i >= 1) {
            return true;
        }

        if (index.find(sum % k) != index.end() && i - index[sum % k] >= 2) {
            return true;
        }

        index[sum % k] = i;
    }
    return false;
}

/**
 * @Question Count submatrices with sum K
 * O(col*col*row) Time | O(row) Space
 * So if rows are larger than cols then use leftRow & rightRow bounds and calculate col-sum.
 * You can also find largest area with sum K
 */
int countSubmatrixWithSumK(vector<vector<int>> matrix, int K) {
    int rows = matrix.size();
    int cols = matrix[0].size();
    int result = 0;
    for (int leftCol = 0; leftCol < cols; ++leftCol) {
        vector<int> rowSum(rows, 0);
        for (int rightCol = leftCol; rightCol < cols; ++rightCol) {
            //Calc rowSum
            for (int row = 0; row < rows; ++row)
                rowSum[row] += matrix[row][rightCol];

            //Find number of subarrays in rowSum with sum as K
            result += countSubarrayWithSumK(rowSum, K);
        }
    }
    return result;
}

/**
 * @Variation Variation = https://www.geeksforgeeks.org/largest-area-rectangular-sub-matrix-equal-number-1s-0s/
 * @Solution
 * Replacing 0 with -1.
 * And find largest submatrix with 0 sum
 */

/**
 * Count number of subarrays containing all values as K
 * All elements in subarray should be K only
 */
int countIn1DArray(vector<int>& arr, int K) {
    //dp[i] = number of subarrays ending at ith position with all 1s

    int n = arr.size();
    vector<int> dp(n, 0);
    dp[0] = arr[0] == K;
    for (int i = 1; i < n; ++i) {
        if (arr[i] == K) {
            dp[i] = 1 + (arr[i - 1] == K ? dp[i - 1] : 0);
        }
    }

    int count = 0;
    for (int i = 0; i < n; ++i)
        count += dp[i];
    return count;
}

/**
 * Count number of submatrices having all values as one
 */
int countIn2DArray(vector<vector<int>>& mat) {
    if (mat.empty() || mat[0].empty())
        return 0;
    int R = mat.size();
    int C = mat[0].size();

    int count = 0;
    for (int leftCol = 0; leftCol < C; ++leftCol) {
        vector<int> rowSum(R, 0);
        for (int rightCol = leftCol; rightCol < C; ++rightCol) {
            for (int i = 0; i < R; ++i) {
                if (mat[i][rightCol] == 1) {
                    rowSum[i]++;
                }
            }
            count += countIn1DArray(rowSum, rightCol - leftCol + 1);
        }
    }
    return count;
}

int main() {
    cout << countSubarrayWithSumK({10, 2, -2, -20, 10}, -10) << endl;  //3
    cout << countSubarrayWithSumK({9, 4, 20, 3, 10, 5}, 33) << endl;   //2
}