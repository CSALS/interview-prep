#include <bits/stdc++.h>
using namespace std;

int maxSubArray(vector<int> a) {
    if (a.empty())
        return 0;

    int max_so_far = a[0];
    int curr_max = a[0];

    for (int i = 1; i < a.size(); i++) {
        curr_max = max(a[i], curr_max + a[i]);
        max_so_far = max(max_so_far, curr_max);
    }
    return max_so_far;
}

/**
 * @variation get min. sum subarray
 * @solution
 * 1. Follow kadane's algo for min_so_far and curr_min
 * 2. Do maxSumSubarray on negative of this array
 */
int minSubArray(vector<int> a) {
    vector<int> negative_a;
    for (int num : a)
        negative_a.push_back(-1 * num);

    return -1 * maxSubArray(negative_a);
}

/**
 * https://www.geeksforgeeks.org/maximum-contiguous-circular-sum/
 * 1. Use extra space. Repeat the same array after the end of the array. Find max sum subarray in this 2 * N array of max window size N
 * 2. maximum of (normal max subarry sum) and (total sum - minimum subarray sum). 
 * 1st case for no circular subarray
 * 2nd case for circular subarrays
 * 2nd value might be 0. That is for empty subarray
 */
int maxCircularSum(vector<int> a) {
    int noWrappingMaxSum = maxSubArray(a);

    int totalSum = 0;
    for (int num : a)
        totalSum += num;

    //[.....XXXX.....] thoses Xs form min sum. remove them from overall sum
    int wrappingSum = totalSum - minSubArray(a);
    if (wrappingSum == 0) {
        //Don't consider empty subarray case
        return noWrappingMaxSum;
    }
    return max(wrappingSum, noWrappingMaxSum);
}

//O(cols*cols*rows) T
//if cols >>> rows then use leftRow and rightRow bounds appraoch
int maxSubMatrix(vector<vector<int>> a) {
    int rows = a.size();
    int cols = a[0].size();
    int ans = INT_MIN;
    for (int leftCol = 0; leftCol < cols; ++leftCol) {
        vector<int> rowSum(rows, 0);
        for (int rightCol = leftCol; rightCol < cols; ++rightCol) {
            //Calculate Current Row Sum Bounded by leftCol --- rightCol
            for (int i = 0; i < rows; ++i) {
                rowSum[i] += a[i][rightCol];
            }

            //Now Do Kadane's Algo On This Array
            ans = max(ans, maxSubArray(rowSum));
        }
    }
    return ans;
}

/*
@variation https://www.interviewbit.com/problems/maximum-sum-square-submatrix/
Given a 2D integer matrix A of size N x N find a B x B submatrix where B<= N and B>= 1, 
such that sum of all the elements in submatrix is maximum.
*/

//O(N^3) Solution. We can optimize by pre-computing prefix sum
int solve(vector<vector<int>> &A, int B) {
    int N = A.size();

    int max_sum = INT_MIN;
    for (int leftCol = 0; leftCol < N; ++leftCol) {
        int rightCol = leftCol + (B - 1);
        if (rightCol >= N) break;

        vector<int> rowSum(N, 0);
        for (int row = 0; row < N; ++row) {
            for (int col = leftCol; col <= rightCol; ++col) {
                rowSum[row] += A[row][col];
            }
        }

        //find max. sum of every window of size K
        int i = 0, j = 0;
        int sum = 0;
        while (j < N) {
            sum += rowSum[j];
            if (j - i + 1 == B) {
                max_sum = max(max_sum, sum);
                sum -= rowSum[i++];
            }
            ++j;
        }
    }
    return max_sum;
}

//O(N^2) Time
int solveOptimized(vector<vector<int>> &A, int B) {
    int N = A.size();

    int max_sum = INT_MIN;

    vector<vector<int>> prefixSum(N, vector<int>(N, 0));
    for (int row = 0; row < N; ++row) {
        prefixSum[row][0] = A[row][0];
    }

    for (int col = 1; col < N; ++col) {
        for (int row = 0; row < N; ++row) {
            prefixSum[row][col] = A[row][col] + prefixSum[row][col - 1];
        }
    }
    for (int leftCol = 0; leftCol < N; ++leftCol) {
        int rightCol = leftCol + (B - 1);
        if (rightCol >= N) break;

        vector<int> rowSum(N, 0);
        // for(int row = 0; row < N; ++row) {
        //     for(int col = leftCol; col <= rightCol; ++col) {
        //         rowSum[row] += A[row][col];
        //     }
        // }
        for (int row = 0; row < N; ++row) {
            //we want A[row][leftCol...rightCol]
            if (leftCol == 0)
                rowSum[row] = prefixSum[row][rightCol];
            else
                rowSum[row] = prefixSum[row][rightCol] - prefixSum[row][leftCol - 1];
        }

        //find max. sum of every window of size K
        int i = 0, j = 0;
        int sum = 0;
        while (j < N) {
            sum += rowSum[j];
            if (j - i + 1 == B) {
                max_sum = max(max_sum, sum);
                sum -= rowSum[i++];
            }
            ++j;
        }
    }
    return max_sum;
}

//1D Kadane's Variation

//https://leetcode.com/problems/maximum-subarray-sum-with-one-deletion/
#define INF 100000000
int maximumSum(vector<int> &arr) {
    if (arr.empty()) return 0;

    int n = (int)arr.size();

    // dp[i][0] = max subarray ending at ith element with no deletion in [0...i]
    // dp[i][1] = max subarray ending at ith element with 1 deletion in [0...i]
    int dp[n][2];
    dp[0][0] = arr[0];
    dp[0][1] = -INF;
    int max_sum = arr[0];

    for (int i = 1; i < n; ++i) {
        dp[i][0] = max(arr[i], arr[i] + dp[i - 1][0]);
        dp[i][1] = max(dp[i - 1][0], arr[i] + dp[i - 1][1]);
        max_sum = max({max_sum, dp[i][0], dp[i][1]});
    }
    return max_sum;
}

/*
https://www.youtube.com/watch?v=CImnzpLRX90

Maximum subarray where you need to square exactly one element

    dp[i][0] = max subarray ending at ith element with no element squared in [0...i]
    dp[i][1] = max subarray ending at ith element with exactly one element squared in [0...i]

    dp[i][0] = max(arr[i], arr[i] + dp[i - 1][0])
    dp[i][1] = max(arr[i] * arr[i] + dp[i - 1][0], arr[i] * arr[i], dp[i - 1][1])
*/

//TODO: CF Beautiful Array