#include <bits/stdc++.h>
using namespace std;

/**
 * Many methods to solve
 * 1. Using two arrays.
 * 2. Two Pointers Approach (O(1) Space)
 * 
 * TODO: https://leetcode.com/problems/trapping-rain-water-ii (A more harder version)
 */

//Using two arrays
int trap(vector<int>& heights) {
    int N = heights.size();
    if (N == 0) return 0;
    int maxOnRight[N];  //maxOnRight[i] = max heights[i....N-1]
    maxOnRight[N - 1] = heights[N - 1];
    for (int i = N - 2; i >= 0; --i) {
        maxOnRight[i] = max(maxOnRight[i + 1], heights[i]);
    }

    int maxOnLeft[N];  //maxOnLeft[i] = max heights[0...i]
    maxOnLeft[0] = heights[0];
    for (int i = 1; i < N; ++i) {
        maxOnLeft[i] = max(maxOnLeft[i - 1], heights[i]);
    }
    //maxOnRight[i] will be either some value >heights[i] or it will be heights[i] only. lly for maxOnLeft[i]
    //that's why when we do min(left, right) it will be same as heights[i] if there's no bound for that building
    //why? let's say for i there's no greater on left and right so left[i]=right[i]=height[i]
    //say for i there's no greater on left but there is some greater on right. now right[i] will be > left[i]
    //so min(left, right) will be left which is same as height.
    int totalArea = 0;
    int width = 1;
    for (int i = 0; i < N; ++i) {
        int heightBound = min(maxOnLeft[i], maxOnRight[i]);
        //this heightBound will be either heights[i] or something greater than it
        totalArea += (heightBound - heights[i]);
    }
    return totalArea;
}

//Constant space. Two pointers
int trap2(vector<int>& heights) {
    int n = heights.size();
    int totalArea = 0;
    int leftMax = heights[0];
    int rightMax = heights[n - 1];
    int left = 1, right = n - 2;
    while (left <= right) {
        /**
         * leftMax.....left.....right.....rightMax
         * if leftMax <= rightMax then we found min height bound for arr[left]
         * beacause the maxOnRight for arr[left] will be either rightMax or something larger than it but for sure it won't be smaller
         * so that's why maxonright for left is rightMax and maxonleft for left is leftmax and out of them min. is leftMax so calc. area
         * But we didn't still find for right since in between [left.....right] we can still get a leftMax for arr[right].
         * So we compute area for arr[left]. 
         * If (leftMax - arr[left]) was negative then arr[left] is the new leftMax
         * Similarily for other case
         */
        if (leftMax <= rightMax) {
            //maxOnRight[left] will be atleast rightMax so out of them min will be leftMax
            int diff = leftMax - heights[left];
            totalArea += (diff > 0) ? diff : 0;
            if (diff < 0) {
                //we found new left max
                leftMax = heights[left];
            }
            left++;
        } else {
            int diff = rightMax - heights[right];
            totalArea += (diff > 0) ? diff : 0;
            if (diff < 0) {
                rightMax = heights[right];
            }
            right--;
        }
    }
    return totalArea;
}
