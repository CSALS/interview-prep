#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

/*

https://www.geeksforgeeks.org/minimum-number-swaps-required-sort-array/

Variation => https://www.geeksforgeeks.org/minimum-swap-required-convert-binary-tree-binary-search-tree/?ref=lbp
    
    use inorder of binary tree and find min. swaps to convert it into sorted array (since for bst inorder is sorted)
    if only adjacent nodes can be swapped in the BT then use below answer (min adjacent swaps)

similar question => min. adjacent swaps to sort array. 
    Input  : arr[] = {3, 2, 1}
    Output : 3
    We need to do following swaps
    (3, 2), (3, 1) and (1, 2)

    Input  : arr[] = {1, 20, 6, 4, 5}
    Output : 5
        
    answer : number of inversions

*/

//O(NlogN) T | O(2N)S
//Forms cycle. Need to swap (cycleLength - 1) for all cycles
int minSwapsUtil(vector<int> arr) {
    int N = arr.size();
    vector<pair<int, int>> sortedArr(N);

    for (int i = 0; i < N; ++i) {
        sortedArr[i] = {arr[i], i};
    }
    sort(sortedArr.begin(), sortedArr.end());
    int swaps = 0;
    vector<bool> visited(N, false);
    for (int i = 0; i < N; ++i) {
        if (visited[i] || arr[i] == sortedArr[i].first) continue;
        visited[i] = true;

        int v = sortedArr[i].second;
        int nodes_count = 0;
        while (v != i) {
            visited[v] = true;
            nodes_count++;
            v = sortedArr[v].second;
        }
        swaps += nodes_count;
    }
    return swaps;
}

/*
https://www.hackerrank.com/challenges/minimum-swaps-2/problem
Min Swaps to sort the array containg [1...N] no duplicates
*/
int minimumSwaps(vector<int> arr) {
    int i, c = 0, n = arr.size();
    for (i = 0; i < n; i++) {
        if (arr[i] == (i + 1))
            continue;

        swap(arr[i], arr[arr[i] - 1]);
        c++;
        i--;
    }
    return c;
}

int main() {
    cout << minSwapsUtil({1, 5, 4, 3, 2});
    return 0;
}