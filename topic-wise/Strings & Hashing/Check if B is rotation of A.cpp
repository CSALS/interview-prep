/*

https://www.geeksforgeeks.org/a-program-to-check-if-strings-are-rotations-of-each-other/

A = abcdef
B = cdefab
So B is rotation of A


    Algo:-
        1. string temp = A + A
        2. if B is substring of A then rotations of each other
        3. else false