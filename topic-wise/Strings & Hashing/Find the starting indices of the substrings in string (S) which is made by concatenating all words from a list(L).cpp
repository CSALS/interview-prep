#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/substring-with-concatenation-of-all-words/
 * You are given a string, s, and a list of words, words, that are all of the same length. 
 * Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.
 * Input:
 * s = "barfoothefoobarman",
 * words = ["foo","bar"]
 * Output: [0,9]
 * Explanation: Substrings starting at index 0 and 9 are "barfoo" and "foobar" respectively.
 * The output order does not matter, returning [9,0] is fine too.
 */

vector<int> findSubstring(string text, vector<string> &words) {
    if (text.empty() || words.empty())
        return {};
    unordered_map<string, int> wordMap;
    for (string word : words)
        wordMap[word]++;
    int wordLength = words[0].length();
    int offset = words.size() * wordLength;  //each word has same length
    vector<int> result;
    for (int startIndex = 0; startIndex < text.length(); ++startIndex) {
        int endIndex = startIndex + offset - 1;
        if (endIndex >= text.length())
            break;
        //Check if all words of length wordLength in [startIndex.....endIndex] present in map
        bool isSubstring = true;
        unordered_map<string, int> alreadyMapped;
        for (int i = startIndex; i <= endIndex; i += wordLength) {
            string word = text.substr(i, wordLength);
            if (wordMap.find(word) == wordMap.end()) {
                isSubstring = false;
                break;
            } else if (alreadyMapped[word] + 1 > wordMap[word]) {
                isSubstring = false;
                break;
            } else {
                alreadyMapped[word]++;
            }
        }
        if (isSubstring)
            result.push_back(startIndex);
    }
    return result;
}
