## Implementations
- unordered_map, unordered_set, unordered_multiset are implemented using hash table
- map, set, multiset are implemented using self-balancing BST (red-black trees, avl trees)
- Implemented unordered_map with pairs
```
struct hash_pair { 
    template <class T1, class T2> 
    size_t operator()(const pair<T1, T2>& p) const
    { 
        auto hash1 = hash<T1>{}(p.first); 
        auto hash2 = hash<T2>{}(p.second); 
        return hash1 ^ hash2; 
    } 
}; 
unordered_map<pair<int, int>, bool, hash_pair> um; 
```
- Hashing on strings = This analysis(O(1) lookup) does not count the amount of time that it takes to compute the hash function itself. For strings, it typically requires O(k) to calculate the hash value, where k is the length of the string. Storing n strings in a hash table with total length m (potentially much larger than n) takes O(m) time, not O(n), because a hash value needs to be computed for all the strings. So sometimes using TRIE instead of hashing strings is preferred.

## Problems

TODO: (add to library)
- Cloning Questions =
1. Cloning an undirected graph (Hashing)
2. Cloning a linked list with random pointers (Hashing / O(1) Space)
3. Cloning a binary tree with random pointers (Hashing / O(1) Space)
```
Practical Application for the 3rd =
A practical analogue of this problem : IDM site grabber, or any other tool for website downloading (for offline browsing).
In site grabbing, you have html (and other script) files in a file system (which resembles a tree), and each html file could contain hyperlink to any other file on the website (resembles to random pointer).
So, the site grabber has to basically do the same, copy a tree (not necessarily binary), with each node having any number of random pointers to any other node in the tree.
```


## HASHING THEORY

Why Hashing? <br>
- For O(1) searching of elements
- Since using binary search O(logN)

How ? <br>
Ideal Hashing & Modulus Hashing =
- Keys = 8,3,6,10,15,18,4
- B/W key space & hash table we use a function for mapping.
- Ideally better to use one-one function which is H(x) = x (storing key ele in that same index)
- This function is IDEAL hashing function since time for searching is constant.
- The Drawback of IDEAL hashing is lot of space consumption.

- Fix hash table size. Say size is 10
- H(x) = x % 10 (modulus hash function)
- Drawback = This is many-one function
- Two keys mapped to same location => called COLLISIONS
- hash function should be good else all keys might fall in one bucket

How To Resolve Collisions? <br>

1. Open hashing (we will consume extra space)
- Chaining
2. Closed Hashing  (we won't extra space)
- Open Addressing (we will store the key at some other place need not be x % 10). 
- The following methods dictate where we store that key
    1. linear probing
    2. quadractic probing.
    3. double hashing

Chaining <br>
- insert, search, delete (main operations in any DS)

INSERT(x)
- h(x) = x%size
- hash table is array of linked list(chains)
- In case of collision add to the linked list and maintain each linked list in sorted order for better searching.

SEARCH(x)
- search in linked list of h(x) index in hash table

DELETE(X)
- search & delete if found

loading factor = N/Size (LF)
N = no of keys, size = size of hash table

on average, assuming uniform distribution each index has LF number of keys
success search => 1 (hashing) + LF/2 (on avergae serching in LF keys)
unsuccess search => 1 + LF


Linear Probing <br>
INSERT(x)
- H(x) = (x%10 + i) % 10 where i = (0, 1, 2.....)
- when there is collision try to store in next space
- compute H(x) for all i till you get a free space and store it there

SEARCH(X)
- search X at x%10 then at (x%10+1)%10 and so on...
- once you get a free space that means element is not there so stop

DELETE(X)
- search for X and delete it and ALSO shift the below elements to the vacant spaces
- this is costly. so linear probing is not good for deleting

Quadrtic Probing <br>
- H(X) = (x%10 + i*i) % 10

Double Hashing <br>
- two hash functions
- h1(x) = x % 10
- h2(x) = R - (x % R) where R is a nearest prime number less than size of hash table
- H(x) = (h1(x) + i * h2(x)) % 10 where i = 0, 1, 2....