#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/window-string/
 * Given Text and Pattern find the minimum substring in text where pattern is present.
 */

//1. Getting TLE with this approach. Everytime calling contains() which compares entire hash table.
// Instead of this maintain just a variable to keep track of number of matching characters
bool contains(unordered_map<char, int> patternMap, unordered_map<char, int> textMap) {
    for (auto it : patternMap) {
        if (textMap[it.first] < it.second)
            return false;
    }
    return true;
}
string minWindow(string text, string pattern) {
    unordered_map<char, int> patternMap;
    for (char c : pattern)
        patternMap[c]++;

    unordered_map<char, int> textMap;
    int finalLeft = 0, finalRight = -1;
    for (int left = 0, right = 0; right < text.length(); ++right) {
        textMap[text[right]]++;
        if (contains(patternMap, textMap)) {
            while (left <= right && contains(patternMap, textMap)) {
                textMap[text[left++]]--;
            }
            if (finalRight == -1 || finalRight - finalLeft + 1 > right - left + 2) {
                finalLeft = left - 1;
                finalRight = right;
            }
        }
    }
    return text.substr(finalLeft, finalRight - finalLeft + 1);
}

//2. Maintaining a variable called currentMatchings will be increased if current character matched in pattern. if currentMatchings equals to pattern length
//then we got a window so try to minimze it.
string minWindow(string text, string pattern) {
    unordered_map<char, int> patternMap;
    for (char c : pattern)
        patternMap[c]++;

    int expectedCount = pattern.length();
    unordered_map<char, int> textMap;
    int currentMatchings = 0;
    int finalLeft = 0, finalRight = -1;
    for (int left = 0, right = 0; right < text.length(); ++right) {
        char c = text[right];
        textMap[c]++;
        if (patternMap.find(c) != patternMap.end() && textMap[c] <= patternMap[c])
            currentMatchings++;
        if (currentMatchings == expectedCount) {
            //[left....right] contains the pattern. But check if we can minimze the window.
            //How to minimize? 1st cond. is if char is occuring more times than needed. 2nd cond. is if it is useless char (not present in pattern map)
            while (textMap[text[left]] > patternMap[text[left]] || patternMap[text[left]] == 0) {
                textMap[text[left]]--;
                left++;
            }
            if (finalRight == -1 || finalRight - finalLeft + 1 > right - left + 1) {
                finalLeft = left;
                finalRight = right;
            }
        }
    }
    return text.substr(finalLeft, finalRight - finalLeft + 1);
}
