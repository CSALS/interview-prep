#include <bits/stdc++.h>
using namespace std;

string fractionToDecimal(int a, int b) {
    long long A = a, B = b;
    bool isResNeg = false;
    if (A < 0 && B > 0)
        isResNeg = true;
    else if (A > 0 && B < 0)
        isResNeg = true;
    A = abs(A);
    B = abs(B);
    string real = "";
    real = real + to_string(A / B);
    A = A % B;
    // No Decimal Part
    if (A == 0) return real;
    // Compute Decimal Part
    string decimal = "";
    unordered_map<int, int> rem_hash;
    rem_hash[A] = 0;
    A = A * 10;

    while (true) {
        decimal = decimal + to_string(A / B);
        if (A % B == 0) break;
        A = A % B;
        if (rem_hash.find(A) != rem_hash.end()) {
            int ind = rem_hash[A];
            decimal.insert(ind, "(");
            decimal += ")";
            break;
        } else {
            rem_hash[A] = decimal.length();
        }
        A = A * 10;
    }
    string res = real + "." + decimal;
    return (isResNeg) ? "-" + res : res;
}