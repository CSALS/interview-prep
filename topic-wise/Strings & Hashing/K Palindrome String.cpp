#include <bits/stdc++.h>
using namespace std;

/**
 * Can you make a string palindrome with atmost K deletions
 * https://www.geeksforgeeks.org/find-if-string-is-k-palindrome-or-not/
 * 1. recursive approach with 2^n time (every char has two choices in the worst case of distinct chars string)
 * 2. cache results due to overlapping subproblems
 * 3. Find min. no of deletions to make a string into palindrome. (length of string - length of Longest palindromic subsequence)
 *    If that is <= k then true else false
 */

int dp[1001][1001];

bool isKPalindromeRecursive(string &str, int low, int high, int k) {
    if (low > high) {
        return false;
    }
    if (low == high) {
        return true;
    }

    if (dp[low][high] != -1) {
        return dp[low][high];
    }

    if (str[low] == str[high]) {
        return dp[low][high] =
                   isKPalindromeRecursive(str, low + 1, high - 1, k);
    }
    if (str[low] != str[high]) {
        if (k == 0) return false;
        //delete str[low] or delete str[high]
        return dp[low][high] =
                   isKPalindromeRecursive(str, low, high - 1, k - 1) ||
                   isKPalindromeRecursive(str, low + 1, high, k - 1);
    }
}

int isKPalindrome(string s, int n, int k) {
    memset(dp, -1, sizeof(dp));
    return isKPalindromeRecursive(s, 0, n - 1, k);
}