#include <bits/stdc++.h>
using namespace std;

/**
 * Given a text(length n) and a pattern(length m). Find all indices in text where pattern occurs
 * Naive Algo => For every window of size m check if it is same as pattern. O(m * (n - m))
 * Better Algo If pattern has no repeating characters => Can skip many characters at a time. O(n)
 * If pattern has no repeating characters then we know the starting character won't occur anywhere else
 * so we can skip the window to (i + j)
 * TODO: Advanced Algorithms =
 * 1. Preprocess pattern (use this when text changes but pattern is fixed)
 * Rabin Karp = O(m * (n - m)) worst case. On average it is better
 * KMP = O(n) worst case.
 * 2. Preprocess text (use this when pattern is fixed but text changes)
 * Suffix Tree = O(m) worst case
 */

void naive(string text, string pattern) {
    int n = text.length(), m = pattern.length();
    for (int i = 0; i <= n - m; ++i) {
        int j;
        for (j = 0; j < m; ++j)
            if (text[i + j] != pattern[j])
                break;
        if (j == m)
            cout << i << " ";
    }
    cout << endl;
}

void naiveNoRepeating(string text, string pattern) {
    int n = text.length(), m = pattern.length();
    for (int i = 0; i <= n - m;) {
        int j;
        for (j = 0; j < m; ++j)
            if (text[i + j] != pattern[j])
                break;
        if (j == m)
            cout << i << " ";
        if (j == 0)
            ++i;
        else
            i = i + j;
    }
}

/**
 * Calculating lps[] array. Longest proper prefix which is also suffix
 * 1. Naive Algo O(N^3)
 * 2. Tricky Algo O(N)
 */
//Naive = O(N^3)
void fillLPS(string str) {
    int longestProperPrefixSuffix(string str, int N) {
        //Shouldn't check for len = N since we need proper prefix
        for (int len = N - 1; len >= 1; --len) {
            bool flag = true;
            for (int i = 0; i < len; ++i) {
                int j = N - len + i;
                if (str[i] != str[j]) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                return len;
        }
        return 0;
    }
    int n = str.length();
    vector<int> lps(n);
    for (int i = 0; i < n; ++i)
        lps[i] = longestProperPrefixSuffix(str, i + 1);
}

//TODO: Tricky Algo. To use already computed lps values

int main() {
    naive("ababab", "abab");    //[0, 2]
    naive("abababab", "abab");  // [0, 2, 4]
}