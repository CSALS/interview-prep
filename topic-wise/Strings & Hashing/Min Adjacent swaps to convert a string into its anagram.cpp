//https://www.geeksforgeeks.org/minimum-number-of-adjacent-swaps-to-convert-a-string-into-its-given-anagram/

#include <bits/stdc++.h>
using namespace std;

// Function that returns true if s1
// and s2 are anagrams of each other
bool isAnagram(string s1, string s2) {
    sort(s1.begin(), s1.end());
    sort(s2.begin(), s2.end());
    if (s1 == s2)
        return 1;
    return 0;
}

void swap(string &s, int i, int j, int &swapCount) {
    char temp = s[i];
    s[i] = s[j];
    s[j] = temp;
    swapCount++;
}

// Function to return the minimum adjcanet swaps required
int CountSteps(string s1, string s2) {
    int swapCount = 0;
    for (int i = 0; i < s1.length(); ++i) {
        if (s1[i] == s2[i])
            continue;

        //Find nearest s2[i] in s1 and keep swapping
        int j = i + 1;
        //We will def. find since they are anagrams of each (we call this funciton only if they are)
        while (s1[j] != s2[i])
            ++j;

        for (int k = i + 1; k <= j; ++k) {
            swap(s1, k, k - 1, swapCount);
        }
    }
    return swapCount;
}
int main() {
    string s1 = "abcd";
    string s2 = "cdab";

    // If both the strings are anagrams
    // of each other then only they
    // can be made equal
    if (isAnagram(s1, s2))
        cout << CountSteps(s1, s2);
    else
        cout << -1;

    return 0;
}
