#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-a-pair-swapping-which-makes-sum-of-two-arrays-same/
 * sum1 - X + Y = sum2 - Y + X
 * diff = sum1 - sum2
 * 2x - 2y = diff [diff should be even else no pair exists] 
 * Assume arrays are unsorted so using hashing
 * If arrays are sorted then use two pointers
 */

bool isPairExists(vector<int> arr1, vector<int> arr2) {
    int sum1 = 0, sum2 = 0;
    for (int num : arr1)
        sum1 += num;
    for (int num : arr2)
        sum2 += num;
    int diff = sum1 - sum2;
    if (diff & 1) return false;
    int K = diff / 2;

    // Hashing Approach
    unordered_set<int> m;
    for (int num : arr2)
        m.insert(num + K);
    for (int num : arr1)
        if (m.find(num) != m.end())
            return true;
    return false;

    //Two Pointers Approach
    sort(arr1.begin(), arr1.end());
    sort(arr2.begin(), arr2.end());
    int i = 0, j = 0;
    while (i < n && j < m1) {
        diff = arr1[i] - arr2[j];
        if (diff == K) return 1;
        if (diff < K)
            i++;
        else if (diff > K)
            j++;
    }
    return -1;
}