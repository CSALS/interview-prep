#include "Tree.h"

/**
 * https://www.geeksforgeeks.org/connect-nodes-at-same-level
 * https://www.interviewbit.com/problems/populate-next-right-pointers-tree/
 * Two methods to do level order. One is to use NULL delimiter. Another one is to have nested loop inside the main loop
 * These use extra space (queue)
 */
void connect1(Node* p) {
    if (p == NULL) return;
    queue<Node*> q;
    q.push(p);
    while (!q.empty()) {
        q.push(NULL);
        int size = q.size();
        while (size--) {
            Node* node = q.front();
            q.pop();
            if (node == NULL) break;
            node->nextRight = q.front();
            if (node->left) q.push(node->left);
            if (node->right) q.push(node->right);
        }
    }
}
void connect2(Node* p) {
    if (p == NULL) return;
    queue<Node*> q;
    q.push(p);
    q.push(NULL);
    while (q.size() > 1) {
        Node* node = q.front();
        q.pop();
        if (node == NULL) {
            q.push(NULL);
            continue;
        }
        node->nextRight = q.front();
        if (node->left != NULL) q.push(node->left);
        if (node->right != NULL) q.push(node->right);
    }
}

/**
 * IMP
 * https://www.geeksforgeeks.org/connect-nodes-at-same-level-with-o1-extra-space/?ref=lbp
 * 1. Preorder Recursive Solution (which takes some space but we can convert this into iterative version)
 *      best explanation = https://www.youtube.com/watch?v=dDE4gGRx2ZY
 * 2. Iterative version of the preorder recursive
 */

//returns leftmost node in the next level in the curr->next chain
Node* getNextRight(Node* curr) {
    curr = curr->next;
    while (curr != NULL) {
        if (curr->left != NULL)
            return curr->left;
        if (curr->right != NULL)
            return curr->right;
        curr = curr->next;
    }
    return NULL;
}

void connectRecursivePreorder(Node* root) {
    if (!root) return;

    //At this point we have already set root->next
    //We use that to set next pointers for its children
    //For left child its next will be the root->right (if it exists)
    //If root->right doesn't exist then use next node in the root->next chain

    if (root->left != NULL) {
        if (root->right != NULL) {
            root->left->next = root->right;
        } else {
            root->left->next = getNextRight(root);
        }
    }
    if (root->right != NULL) {
        root->right->next = getNextRight(root);
    }

    connectRecursivePreorder(root->left);
    connectRecursivePreorder(root->right);
}

void connectIterative(Node* root) {
    if (!root) return;

    while (root != NULL) {
        Node* curr = root;
        //Connect the children nodes of curr and then connect children nodes of all other nodes at same level as curr
        while (curr != NULL) {
            if (curr->left != NULL) {
                if (curr->right != NULL) {
                    curr->left->next = curr->right;
                } else {
                    curr->left->next = getNextRight(curr);
                }
            }
            if (curr->right != NULL) {
                curr->right->next = getNextRight(curr);
            }
            curr = curr->next;
        }
        //root should point to leftmost node of next level
        if (root->left)
            root = root->left;
        else if (root->right)
            root = root->right;
        else
            root = getNextRight(root);
    }
}
