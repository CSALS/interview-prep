#include "../../Tree.h"

unordered_set<Node*> result;

/*
https://www.geeksforgeeks.org/print-nodes-distance-k-leaf-node/
    can optimize this instead of using vector send pair? (https://thecodingsimplified.com/print-elements-at-k-distance-from-leaf-node-in-binary-tree/)
*/
vector<int> findKDistFromLeafUtil(Node* root, int K) {
    if (!root) return {};
    if (root->left == NULL && root->right == NULL) return {0};

    vector<int> leftLeafDist = findKDistFromLeafUtil(root->left, K);
    vector<int> rightLeafDist = findKDistFromLeafUtil(root->right, K);

    vector<int> leafDist = {};
    for (int num : leftLeafDist) leafDist.push_back(num + 1);
    for (int num : rightLeafDist) leafDist.push_back(num + 1);

    for (int num : leafDist) {
        if (num == K) {
            result.insert(root);
            break;
        }
    }
    return leafDist;
}

int printKDistantfromLeaf(Node* root, int k) {
    if (!root) return 0;
    result.clear();
    findKDistFromLeafUtil(root, k);
    return result.size();
}