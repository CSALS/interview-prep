#include "../../Tree.h"

/** 
 * Solve here https://leetcode.com/problems/all-nodes-distance-k-in-binary-tree/
 * Two approaches
 * 1. Use hashmap to find parent of each node
 *    And then just do iterative bfs from target node. 
 *    only difference is along with node->left, node->right we also push node->parent.
 *    https://tinyurl.com/k-dist-from-node-2
 * 2. Print nodes at k distance down in the subtree and above 
 *    https://tinyurl.com/k-dist-from-node-1
 */
class Solution {
    vector<int> result;  //all nodes at K distance from target node

    //record all nodes in the subtree of root at distance K
    void KDistanceNodesDown(Node* root, int K) {
        if (!root) return;
        if (K == 0) {
            result.push_back(root->data);
            return;
        }

        KDistanceNodesDown(root->left, K - 1);
        KDistanceNodesDown(root->right, K - 1);
    }

    //returns the distance of root to target, -1 if not present
    int distanceKHelper(Node* root, Node* target, int K) {
        if (!root) return -1;

        if (root == target) {
            KDistanceNodesDown(root, K);
            return 0;
        }

        //distance of left node to the target
        int left = distanceKHelper(root->left, target, K);
        //distance of right node to the target
        int right = distanceKHelper(root->right, target, K);

        if (left != -1) {
            //found in left subtree. so search in right subtree
            if (left + 1 == K) {
                result.push_back(root->data);
            } else {
                KDistanceNodesDown(root->right, K - (left + 2));
            }
            return left + 1;
        } else if (right != -1) {
            //found in right subtree. so search in left subtree
            if (right + 1 == K) {
                result.push_back(root->data);
            } else {
                KDistanceNodesDown(root->left, K - (right + 2));
            }
            return right + 1;
        } else {
            //didn't find in either subtree
            return -1;
        }
    }

   public:
    vector<int> distanceK(Node* root, Node* target, int K) {
        result.clear();
        distanceKHelper(root, target, K);
        return result;
    }
};