/*
    O(N^2) TIME
    
    bool isSameTree(TreeNode* s, TreeNode* t) {
        if(s == NULL && t == NULL) return true;
        if(!s || !t) return false;
        
        if(s->val != t->val) return false;
        
        return isSameTree(s->left, t->left) && isSameTree(s->right, t->right);
    }
    
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if(s == NULL && t == NULL) return true;
        if(!s || !t) return false;
        
        if(isSameTree(s, t)) return true;
        return isSubtree(s->left, t) || isSubtree(s->right, t);
    }


    O(N) TIME

    https://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/
    
    inorder + preorder uniquely identify a tree

    so inorder of subtree should be subarray of inorder of main tree
    same with preorder

    to check if it is subarray convert array into string and check substring

    when you encounter non-leaf nodes and one child is NULL add special char for null
*/