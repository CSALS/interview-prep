#include "Tree.h"

Node* invertTree(Node* root) {
    if (!root) {
        return NULL;
    }

    Node* leftRoot = root->left;
    Node* rightRoot = root->right;
    root->left = invertTree(rightRoot);
    root->right = invertTree(leftRoot);
    return root;
}