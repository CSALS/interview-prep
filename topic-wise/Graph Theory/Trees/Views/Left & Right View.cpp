#include "Tree.h"

/**
 * https://www.geeksforgeeks.org/print-left-view-binary-tree/
 * Left View = first node in every level
 * Right View = last node in every level. Same code as left view except you go to node->right first and then node->left
 */

//BFS
void leftviewIterative(Node* root) {
    if (!root)
        return;

    queue<Node*> q;
    q.push(root);

    while (!q.empty()) {
        int size = q.size();
        bool isFirst = true;  //Checks if first node of the current level is printed
        while (size--) {
            Node* node = q.front();
            q.pop();
            if (isFirst) {
                isFirst = false;
                cout << node->data << " ";
            }
            if (node->left != NULL)
                q.push(node->left);
            if (node->right != NULL)
                q.push(node->right);
        }
    }
}

//DFS
int previousLevelPrinted = 0;  //The level already printed
void leftViewRecursive(Node* root, int currentLevel) {
    if (!root)
        return;

    if (currentLevel > previousLevelPrinted) {
        previousLevelPrinted = currentLevel;
        cout << root->data << " ";
    }

    leftViewRecursive(root->left, currentLevel + 1);
    leftViewRecursive(root->right, currentLevel + 1);
}