#include "Tree.h"

//https://www.hackerrank.com/challenges/tree-top-view/problem

/**
 * All these questions have similar approach. 
 * Need to use horizontal distances of each node from root.
 * Top View, Bttom View, Vertical order traversal, Vertical Width, Vertical Sum
 */

/**
 * Top View. Need to visit in level order fashion (NOT INORDER or PREORDER)
 * We need to do a level order traversal so that the topmost node of a horizontal distance
 * is visited before any other node of same horizontal distance below it.
 * In bottom view we need not check ranks.find(rank) condition we can replace it.
 */

#define pp pair<Node *, int>
map<int, int> ranks;  //rank->node
void levelOrder(Node *root) {
    if (!root)
        return;
    queue<pp> q;
    q.push({root, 0});
    while (!q.empty()) {
        Node *node = q.front().first;
        int rank = q.front().second;
        q.pop();
        //
        //1. Top View
        if (ranks.find(rank) == ranks.end()) {
            ranks[rank] = node->data;
        }
        //2. Bottom View
        ranks[rank] = node->data;
        //3. Vertical Traversal (Printing all nodes in vertical order)
        ranks[rank].push_back(node->data);
        //4. Vertical Width = Ans is number of different ranks after entire level order
        ranks.insert(rank);
        int vertical_width = ranks.size();
        if (node->left != NULL) {
            q.push({node->left, rank - 1});
        }
        if (node->right != NULL) {
            q.push({node->right, rank + 1});
        }
    }
}
void topView(struct Node *root) {
    ranks.clear();
    levelOrder(root);
    for (auto it : ranks) {
        cout << it.second << " ";
    }
}