#include "Tree.h"

/**
 * https://www.interviewbit.com/problems/flatten-binary-tree-to-linked-list/
 * Tree should be flattened to right askwed tree. All nodes will be having left as NULL
 */
Node* flatten(Node* root) {
    if (!root)
        return NULL;
    if (root->left == NULL && root->right == NULL)
        return root;
    if (root->left == NULL) {
        root->right = flatten(root->right);
        return root;
    }
    Node* newRight = flatten(root->left);
    Node* tail = newRight;
    while (tail->right != NULL)
        tail = tail->right;
    tail->right = flatten(root->right);
    root->left = NULL;
    root->right = newRight;
    return root;
}
