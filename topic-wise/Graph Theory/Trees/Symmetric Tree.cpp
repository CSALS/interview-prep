#include "Tree.h"

int isSameTree(Node* root1, Node* root2) {
    if (!root1 && !root2)
        return true;
    if (root1 && !root2)
        return false;
    if (!root1 && root2)
        return false;

    if (root1->data != root2->data)
        return false;

    return isSameTree(root1->left, root2->right) && isSameTree(root1->left, root2->right);
}

int isSymmetric(Node* A) {
    return isSameTree(A->left, A->right);
}
