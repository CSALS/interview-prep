
# TIPS
- Two types => top-down, bottom-up
in bottom-up we pass information from bottom to top
it may be int, Node*, or some pair of nodes, or some struct of info like node, max, min, sum, size etc..

- always ask if we can modify the tree?


- For tree recursion carefully think about base cases and then write them. work it out on paper and then write. dont just write blindly.
- Trees = two types of recursion (top-down , bottom-up)
Bottom-up = Mostly optimization problems on binary trees use this
- First calculating answer of leaves then going up
- If answer can be found in subtrees itself not always at root then use bottom-up recursion. 
- Storing answer as ref. variable but passing appropriate results to above parent.
- This recursion is basically bottom -up traversal (passing info about subtrees to parent). 

- Two recursions = Top-down and bottom-up
Use bottom-up for optimization over top-down approach

```
- trees & BST => additional info for each node in bottom-up
- bst => {min, max}
- augmenting tree => storing some extra info in struct Node
```

# introduction
- Tree is undirected graph G(V, V - 1). No cycles. 
- Any two nodes are connected by exactly one path (if they are connected by more than one path then they form cycle)
- https://stackoverflow.com/questions/2603692/what-is-the-difference-between-tree-depth-and-height

- height of node = number of edges in longest path from node to a leaf node
- depth of node = number of edges from node to the root
- height of tree = height of root node / depth of deepest node
- diameter of tree = number of nodes in the longest path b/w any two nodes (they should obv. be leaf nodes since we want max. nodes)

# problems

## Check if graph is tree
 - if graph is connected and has no cycle

-------------------------------------------------------------------------------------------------------------------------

## types of binary tree
1. Full binary tree => if every node has either 0 or 2 children
        - Number of leaves = number of nodes + 1

2. complete binary tree => A Binary Tree is a complete Binary Tree if all the levels are completely filled 
                        except possibly the last level and the last level has all keys as left as possible
        - it is sometimes called almost complete binary tree if last level is not completely filled
        - A node at 'i' has left child at (2i+1) and right child at (2i+2)

3. Perfect Binary Tree => full + complete binary tree.
                          All nodes have 2 children and all leaves on same level.
        - if height is 'h' then nodes are (2^h - 1)

4. Balanced Binary Tree => if height is O(logN)

5. degenerate tree => every node has only one child. same performance as linked list

### questions
1. check if given tree is perfect binary tree
        - find depth of leftmost node (d)
        - Now every node should have 2 child and all leaves should be at depth of 'd'

2. check if given tree is complete binary tree
        - do level order traversal. push NULL children also
        - if you encounter NULL first time then mark a flag
        - after encountering first NULL if we encounter non-NULL node then it is not complete

3. check if tree is height balanced
        - bottom up recursion

-------------------------------------------------------------------------------------------------------------------------

## Constructing Tree
1. From parent array (1st create all value nodes and then link all the nodes)
2. From Traversals

without constructing tree we can solve these
1. https://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals
2. https://www.geeksforgeeks.org/check-if-given-preorder-inorder-and-postorder-traversals-are-of-same-tree
        - construct postorder like above question
        - then compare given postorder with the constructed one

-------------------------------------------------------------------------------------------------------------------------

## Views =
1. Left View Of Tree, Right View Of Tree
2. Vertical Traversal, Top View, Bottom View Of Tree

-------------------------------------------------------------------------------------------------------------------------

## Traversals =
1. Traversals Without Recursion
2. Inorder & Preorder Traversal Without Recursion & Stack = Morris Traversal
3. Level Order Spiral
4. Reverse Level Order
5. Reverse Level Order (Spiral)
6. Boundary Traversal

-------------------------------------------------------------------------------------------------------------------------

## Bottom-up = (Essentialy a post-order type. left, right and then compute at root. Also we might send answer as ref. variable in recursive call since the function need not necessarily return the answer)

- In bottom-up we return either Node* or int or struct of lot of attributes required to the parent
- all bottom-up approaches are optimized version of top-down

- https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
- Diamater of binary tree / Diameter of N-Ary tree
- Max Path Sum from any node to any node
- Max Path Sum from leaf to leaf
- check if tree is height balanced

(**IMP**)Similar questions (returning some data. that data might have many attributes so use some struct)
- https://www.geeksforgeeks.org/find-the-largest-subtree-in-a-tree-that-is-also-a-bst/
- https://www.geeksforgeeks.org/count-the-number-of-binary-search-trees-present-in-a-binary-tree/ (Same Like Above)
- (**IMPPPPPPPP**)https://www.geeksforgeeks.org/minimum-time-to-burn-a-tree-starting-from-a-leaf-node/?ref=leftbar-rightbar

## distance based
1. https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/
2. https://www.geeksforgeeks.org/print-nodes-distance-k-leaf-node/
3. (**IMPP**)https://tinyurl.com/k-dist-from-node-1 (Nodes at K dist from given node) (something similar to in-out concept)

-------------------------------------------------------------------------------------------------------------------------

## Misc
- https://www.geeksforgeeks.org/minimum-swap-required-convert-binary-tree-binary-search-tree/?ref=lbp
    Same as min. swaps in an array to sort it in increasing order (check graphs/misc)
- https://www.interviewbit.com/problems/populate-next-right-pointers-tree/  (IMP without extra space)
- https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/

-------------------------------------------------------------------------------------------------------------------------

## N-ary trees
        representation =>
                1. using adjacency list like graph (vector<int>[])              
                2. using vector<Node*> child in struct Node {}                  

1. https://www.geeksforgeeks.org/print-levels-with-odd-number-of-nodes-and-even-number-of-nodes/
2. diameter in N-ary trees / longest path in undirected tree

### DP on trees

whenevre tree is rep using adj_list & not linked list and it is optimization then it is tree-dp (bottom-up recursion)

Type A => 
maximum path sum from root to leaf node [sum = sum of value of all nodes]
        - greedy wont work (picking larger child)
        - so go from bottom to top

Type B => IN-OUT DP (see notes) =
        - in recursion = bottom-up [for in[u] you need to compute from bottom till u]
        - out recursion = top-down [for out[u] you need out values of parent of u]
1. https://www.interviewbit.com/problems/height-of-every-node-in-a-tree/ (standard)
2. https://www.interviewbit.com/problems/trees-and-sum-of-distances/ = beautiful problem on in-out concept


# TODO

serialize

- https://www.geeksforgeeks.org/reverse-tree-path/
- https://www.geeksforgeeks.org/create-balanced-binary-tree-using-its-leaf-nodes-without-using-extra-space/?ref=leftbar-rightbar
- LOCKS in BT
- https://www.dailycodingproblem.com/blog/lockable-binary-trees/
- https://www.geeksforgeeks.org/nutanix-interview-experience-set-3-campus-internship/
- vertical width vs max width (check gfg questions) vs Maximum Width of Binary Tree (leetcode)



- https://www.geeksforgeeks.org/flip-binary-tree/?ref=lbp (can't understand the question)
- https://www.geeksforgeeks.org/find-if-a-degree-sequence-can-form-a-simple-graph-havel-hakimi-algorithm/


