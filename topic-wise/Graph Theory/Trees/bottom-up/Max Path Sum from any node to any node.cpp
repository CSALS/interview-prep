#include "Tree.h"

int maxPathSum(Node* root, int& res) {
    if (!root) return 0;

    int l = maxPathSum(root->left, res);
    int r = maxPathSum(root->right, res);

    res = max({res, root->data, root->data + l + r, root->data + max(l, r)});

    return max({root->data, root->data + max(l, r)});
}