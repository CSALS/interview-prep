#include "Tree.h"

/*
    diameter of tree = max{ (lheight + rheight+1) for each and every node}
    so our function actually returns the height of tree rooted at 'root'
    res stores the max(lheight+rheight+1) as we traverse every node
*/

int heightOfCurrent(Node* current, int& diameterOfTree) {
    if (!current) return 0;
    int l = heightOfCurrent(current->left, diameterOfTree);
    int r = heightOfCurrent(current->right, diameterOfTree);

    diameterOfTree = max(diameterOfTree, l + r + 1);  //l + r + 1 is diameter of tree at current
    return 1 + max(l, r);
}

int diameter(Node* root) {
    int diameterOfTree = 0;
    heightOfCurrent(root, diameterOfTree);
    return diameterOfTree;
}

//number of edges along the longest path from root to leaf
//leaf node height is 0. that's why returning -1 for empty tree
int height(Node* root) {
    if (!root) return -1;
    return 1 + max(height(root->left), height(root->right));
}

int heightIterative(Node* root) {
    if (!root) return 0;

    queue<Node*> q;
    q.push(root);
    int heightOfTree = 0;
    while (!q.empty()) {
        int size = q.size();
        while (size--) {
            Node* node = q.front();
            q.pop();
            if (node->left) q.push(node->left);
            if (node->right) q.push(node->right);
        }
        if (q.empty()) break;
        heightOfTree++;
    }
    return heightOfTree;
}