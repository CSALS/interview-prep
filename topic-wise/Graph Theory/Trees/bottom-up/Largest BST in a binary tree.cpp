#include "Tree.h"

//Additional information stored at each node
struct Info {
    bool isBST;              //is the binary tree at this node is binary search tree?
    int size;                //size of the binary tree
    int min_data, max_data;  // min & max value in this binary tree (useful for BST detection)
    Info(bool a, int b, int c, int d) {
        isBST = a;
        size = b;
        min_data = c;
        max_data = d;
    }
};

Info largestBSTRecursive(Node* root, int& result) {
    if (!root) {
        return Info(true, 0, INF, -INF);
    }

    if (root->left == NULL && root->right == NULL) {
        return Info(true, 1, root->data, root->data);
    }

    Info left = largestBSTRecursive(root->left, result);
    Info right = largestBSTRecursive(root->right, result);

    //If both subtrees are BST then use another condition to check if this tree is BST
    if (left.isBST && right.isBST) {
        if (left.max_data < root->data && root->data < right.min_data) {
            int size = left.size + right.size + 1;
            result = max(result, size);
            int min_data = left.min_data, max_data = right.max_data;
            if (min_data == INF)
                min_data = root->data;
            if (max_data == -INF)
                max_data = root->data;
            return Info(true, size, min_data, max_data);
        }
    }
    //If it is not BST then it doesn't matter what values we return as min & max & size since we won't use them
    //Actual min of binary tree = min({root->data, left.min_data, right.min_data})
    return Info(false, -1, -1, -1);
}

int solve(Node* root) {
    int result = 1;
    largestBSTRecursive(root, result);
    return result;
}
