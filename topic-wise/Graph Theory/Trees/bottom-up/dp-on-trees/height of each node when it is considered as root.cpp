#include <bits/stdc++.h>
using namespace std;

/*
    https://www.geeksforgeeks.org/dynamic-programming-trees-set-2/

    IN-OUT DP CONCEPT

    Refer the image in this folder for more clarity
*/

int V = 11;
vector<int> graph[12];  //nodes from 1,2,3...11
int height[12];         //height[u] = height of tree when u is considered as root

int in[12];   //in[u] = height of subtree rooted at u
int out[12];  //out[u] = height of u by neglecting its subtree

void dfs_in(int u, int par) {
    in[u] = 0;
    for (int v : graph[u]) {
        if (v == par) continue;
        dfs_in(v, u);
        in[u] = max(in[u], 1 + in[v]);
    }
}

//out[u] is already known. use that to calculate out[children of u]
void dfs_out(int u, int par) {
    int mx1(-1), mx2(-1);
    for (int v : graph[u]) {
        if (v == par) continue;
        if (in[v] >= mx1) {
            mx2 = mx1;
            mx1 = in[v];
        } else if (in[v] > mx2) {
            mx2 = in[v];
        }
    }

    for (int v : graph[u]) {
        if (v == par) continue;
        if (in[v] == mx1) {
            out[v] = 1 + max(out[u], 1 + mx2);
        } else {
            out[v] = 1 + max(out[u], 1 + mx1);
        }
        dfs_out(v, u);  //since out[v] is calculated now calc. for its children
    }
}

void printHeights() {
    dfs_in(1, 0);
    out[1] = 0;  //current root has out value 0
    dfs_out(1, 0);

    for (int u = 1; u <= V; ++u) {
        height[u] = max(in[u], out[u]);
        cout << u << " " << height[u] << endl;
    }
}

int main() {
    graph[1].push_back(2), graph[2].push_back(1);
    graph[1].push_back(3), graph[3].push_back(1);
    graph[1].push_back(4), graph[4].push_back(1);
    graph[2].push_back(5), graph[5].push_back(2);
    graph[2].push_back(6), graph[6].push_back(2);
    graph[3].push_back(7), graph[7].push_back(3);
    graph[7].push_back(10), graph[10].push_back(7);
    graph[7].push_back(11), graph[11].push_back(7);
    graph[4].push_back(8), graph[8].push_back(4);
    graph[4].push_back(9), graph[9].push_back(4);
    printHeights();
    // 1 3
    // 2 4
    // 3 3
    // 4 4
    // 5 5
    // 6 5
    // 7 4
    // 8 5
    // 9 5
    // 10 5
    // 11 5
}