#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/delete-edge/

Given a undirected tree with N nodes labeled from 1 to N.

Each node has a certain weight assigned to it given by an integer array A of size N.

You need to delete an edge in such a way that Product between sum of weight of nodes in one subtree with sum of weight of nodes in other subtree is maximized.

 A = [10, 5, 12, 6]
 B = [

        [1, 2]
        [1, 4]
        [4, 3]
     ]


    APPROACH =>

        For every edge u--->v
        if we remove it the subtree values sums are dp[v] and totalValueSum-dp[v]

*/
const int mod = 1e9 + 7;

void dfsCalculateDP(int u, int p, vector<int> &values, vector<int> graph[], vector<int> &dp, long long &maxProduct, long long totalSum) {
    dp[u] = values[u];
    for (int v : graph[u]) {
        if (v == p) continue;
        dfsCalculateDP(v, u, values, graph, dp, maxProduct, totalSum);
        dp[u] += dp[v];
        //remove u---->v edge
        //MISTAKE: I used dp[u] - dp[v] before but that's wrong. wont give other subtree sum
        long long currentProduct = (dp[v] % mod * (totalSum - dp[v]) % mod) % mod;
        maxProduct = max(maxProduct, currentProduct);
    }
}

int deleteEdge(vector<int> &values, vector<vector<int> > &B) {
    int N = values.size();
    vector<int> graph[N];
    for (vector<int> edge : B) {
        graph[edge[0] - 1].push_back(edge[1] - 1);
        graph[edge[1] - 1].push_back(edge[0] - 1);
    }

    vector<int> dp(N, 0);  //dp[u] = sum of values of its subtrees including it

    long long maxProduct = 0;
    long long totalSum = 0;
    for (int value : values) totalSum += value;
    dfsCalculateDP(0, -1, values, graph, dp, maxProduct, totalSum);
    return maxProduct;
}