#include <bits/stdc++.h>
using namespace std;

int V = 1000;
int E = V - 1;

//nodes are 1,2,3...,V
vector<int> g[V + 1];  //adjacency list
int value[V + 1];      //value of each node
int dp[V + 1] = {0};   //dp[i] = max. path sum from i to leaf

//Find max. path sum from root to leaf

void dfs(int u, int par) {
    dp[u] = value[u];

    int maxSum = 0;
    for (int v : graph[u]) {
        if (v == par) continue;
        dfs(v, u);
        maxSum = max(maxSum, dp[v]);
    }
    dp[u] += maxSum;
    return dp[u];
}

int maxPathSumRootToLeaf() {
    dfs(1, 0);
    return dp[1];
}