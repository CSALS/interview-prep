#include "Tree.h"

int solve(Node* root, int& res) {
    if (!root)
        return 0;

    //If it is leaf node return it's value
    if (root->left == NULL && root->right == NULL)
        return root->data;

    int l = solve(root->left, res);
    int r = solve(root->right, res);

    if (root->left && root->right) {
        res = max(res, l + r + root->data);
        return root->data + max(l, r);
    }

    if (root->left != NULL) {
        return root->data + l;
    } else {
        return root->data + r;
    }
}