#include "Tree.h"

/**
 * 1. Using normal level order function and reversing the result
 * 2. Using stack
 */
vector<int> reverseLevelOrder(Node* root) {
    queue<Node*> q;
    q.push(root);
    stack<int> st;
    //Every level go from right to left order and push them in stack in that order
    while (!q.empty()) {
        int size = q.size();
        // vector<int> levelNodes;
        while (size--) {
            Node* node = q.front();
            q.pop();
            st.push(node->data);
            if (node->right != NULL) {
                q.push(node->right);
            }
            if (node->left != NULL) {
                q.push(node->left);
            }
        }
    }
    vector<int> reverseLevelOrder;
    while (!st.empty()) {
        reverseLevelOrder.push_back(st.top());
        st.pop();
    }
    return reverseLevelOrder;
}
