#include <bits/stdc++.h>
using namespace std;

vector<int> result;
void dfsLeftBoundary(Node* root) {
    if (!root) return;

    //leaf node (leftmost node)
    if (root->left == NULL && root->right == NULL) {
        return;
    }

    result.push_back(root->data);
    if (root->left)
        dfsLeftBoundary(root->left);
    else
        dfsLeftBoundary(root->right);
}
void addLeavesInorder(Node* root) {
    //inorder traversal. since we need leaves from left to right order
    //this is WRONG since we need from left to right order which means inorder not levelorder
    // queue<Node*> q;
    // q.push(root);
    // while(!q.empty()) {
    //     Node* node = q.front(); q.pop();
    //     if(node->left == NULL && node->right == NULL) {
    //         result.push_back(node->data);
    //     } else {
    //         if(node->left)
    //             q.push(node->left);
    //         if(node->right)
    //             q.push(node->right);
    //     }
    // }
    if (!root) return;
    addLeavesInorder(root->left);
    if (root->left == NULL && root->right == NULL) {
        result.push_back(root->data);
    }
    addLeavesInorder(root->right);
}
void dfsRightBoundary(Node* root) {
    if (!root) return;

    //leaf node (rightmost node)
    if (root->left == NULL && root->right == NULL) {
        return;
    }
    if (root->right)
        dfsRightBoundary(root->right);
    else
        dfsRightBoundary(root->left);
    result.push_back(root->data);
}
vector<int> printBoundary(Node* root) {
    if (!root) return {};
    result.clear();
    /**
     * first go from root->left to leftmost node and store nodes in top-down manner
     * next store all leaves
     * next go from root->left to rightmost node and store nodes in bottom-up manner
     * print in such a way that nodes don't repeat
     * in 1st leftmost boundary the leftmost node is also leaf so don't print it in dfsLeftBoundary()
     * what if left subtree is NULL? what is the leftmost node? 
     * in that case the left boundary will be just leftmost node which will be added in the leaves traversal anyway
     */
    result.push_back(root->data);
    dfsLeftBoundary(root->left);
    addLeavesInorder(root);
    dfsRightBoundary(root->right);
    return result;
}