#include "Tree.h"

//try dfs, bfs and lot of other approaches also

void levelOrderDeque(Node* root) {
    deque<Node*> q;
    int dir = 0;  //dir = 0 (right to left), 1 (left to right)
    q.push_front(root);

    while (!q.empty()) {
        int size = q.size();
        while (size--) {
            //In this level we go from right to left. So keep popping from back and add to front
            if (dir == 0) {
                Node* node = q.back();
                q.pop_back();
                cout << node->data << " ";
                if (node->right)
                    q.push_front(node->right);
                if (node->left)
                    ;
                q.push_front(node->left);
            }
            //In this level we go from left to right. So keep popping from front and add to back
            else {
                Node* node = q.front();
                q.pop_front();
                cout << node->data << " ";
                if (node->left)
                    q.push_back(node->left);
                if (node->right)
                    q.push_back(node->right);
            }
        }
        cout << endl;
        dir = 1 - dir;
    }
}