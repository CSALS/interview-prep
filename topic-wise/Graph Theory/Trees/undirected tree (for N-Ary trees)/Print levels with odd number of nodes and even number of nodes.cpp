#include <bits/stdc++.h>
using namespace std;

// https://www.geeksforgeeks.org/print-levels-with-odd-number-of-nodes-and-even-number-of-nodes/

void solution(vector<int> tree[], int N) {
    enum Parity { EVEN,
                  ODD };
    //result[0] = list of levels which have even nodes
    //result[1] = list of levels which have odd nodes
    vector<int> result[2];
    vector<int> visited(N + 1, false);
    queue<int> q;
    q.push(1);
    visited[1] = true;

    int level = 1;
    while (!q.empty()) {
        int size = q.size();  //current level nodes count
        if (size & 1) {
            result[ODD].push_back(level);
        } else {
            result[EVEN].push_back(level);
        }
        while (size--) {
            int u = q.front();
            q.pop();

            for (int v : tree[u]) {
                if (!visited[v]) {
                    q.push(v);
                    visited[v] = true;
                }
            }
        }
        ++level;
    }

    cout << "Odd nodes levels ";
    for (int num : result[ODD]) {
        cout << num << " ";
    }
    cout << endl;

    cout << "Even nodes levels ";
    for (int num : result[EVEN]) {
        cout << num << " ";
    }
    cout << endl;
}

void insertEdges(int x, int y, vector<int> tree[]) {
    tree[x].push_back(y);
    tree[y].push_back(x);
}
int main() {
    // Construct the tree

    /*   1 
       /   \ 
      2     3 
     / \     \ 
    4    5    6 
        / \  / 
       7   8 9  */

    const int N = 9;

    vector<int> tree[N + 1];

    insertEdges(1, 2, tree);
    insertEdges(1, 3, tree);
    insertEdges(2, 4, tree);
    insertEdges(2, 5, tree);
    insertEdges(5, 7, tree);
    insertEdges(5, 8, tree);
    insertEdges(3, 6, tree);
    insertEdges(6, 9, tree);

    solution(tree, N);

    return 0;
}
