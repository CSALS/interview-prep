#include "Tree.h"

int maxSumRecursive(Node *root, int include) {
    if (!root) return 0;

    //Skip this node
    if (include == 0) {
        return max(maxSumRecursive(root->left, 0), maxSumRecursive(root->left, 1)) +
               max(maxSumRecursive(root->right, 0), maxSumRecursive(root->right, 1));
    }

    return root->data + maxSumRecursive(root->left, 0) + maxSumRecursive(root->right, 0);
}

int maxSum(Node *root) {
    return max(maxSumRecursive(root, 0), maxSumRecursive(root, 1));
}