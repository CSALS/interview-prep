#include "../Tree.h"

/*
https://www.geeksforgeeks.org/construct-a-special-tree-from-given-preorder-traversal/
Construct a special tree from given preorder traversal


Given an array ‘pre[]’ that represents Preorder traversal of a spacial binary tree where every node has either 0 or 2 children.
One more array ‘preLN[]’ is given which has only two possible values ‘L’ and ‘N’.
The value ‘L’ in ‘preLN[]’ indicates that the corresponding node in Binary Tree is a leaf node and value ‘N’ indicates that the 
corresponding node is non-leaf node.

Basically we want FULL BINARY TREE

Input:  pre[] = {10, 30, 20, 5, 15},  preLN[] = {'N', 'N', 'L', 'L', 'L'}
Output: Root of following tree
          10
         /  \
        30   15
       /  \
      20   5
*/

Node *constructTreeRecursive(int pre[], char preLN[], int &preIndex, int n) {
    if (preIndex >= n) return NULL;

    Node *root = new Node(pre[preIndex]);
    if (preLN[preIndex++] == 'N') {
        root->left = constructTreeRecursive(pre, preLN, preIndex, n);
        root->right = constructTreeRecursive(pre, preLN, preIndex, n);
    }
    return root;
}

//construct root node and if it is 'N' then recurse for left and right subtrees else just return it
Node *constructTree(int pre[], char preLN[], int n) {
    int preIndex = 0;
    return constructTreeRecursive(pre, preLN, preIndex, n);
}

void printInorder(Node *node) {
    if (node == NULL) return;
    printInorder(node->left);
    printf("%d ", node->data);
    printInorder(node->right);
}

int main() {
    Node *root = NULL;
    int pre[] = {10, 30, 20, 5, 15};
    char preLN[] = {'N', 'N', 'L', 'L', 'L'};
    int n = sizeof(pre) / sizeof(pre[0]);
    root = constructTree(pre, preLN, n);
    printInorder(root);  //20 30 5 10 15

    return 0;
}