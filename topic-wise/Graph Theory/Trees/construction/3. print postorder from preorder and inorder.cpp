#include <bits/stdc++.h>
using namespace std;
/*
https://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals

    1. construct binary tree from inorder and preorder. and find postorder of that tree and compare with given postorder

    2. without constructing binary tree

Variation => https://www.geeksforgeeks.org/check-if-given-preorder-inorder-and-postorder-traversals-are-of-same-tree
    1. construct postorder using given inorder and preorder using 2nd method
    and compare with given postorder. 
    2. don't need to construct it explicitly. do comparision that's it
*/

/**
 * In postorder first print left subtree first and then print right subtree and then root
 * Time Complexity O(N^2)
 * Can be improved using unordered_map to return index in O(1)
 */

//in vectors we use indices
void printPostorder(int inorder[], int preorder[], int N) {
    //preorder[0] is root. Search its index in inorder

    if (N == 0)
        return;
    if (N == 1)
        cout << preorder[0] << " ";

    int index;
    for (index = 0; index < N; ++index)
        if (inorder[index] == preorder[0])
            break;

    printPostorder(inorder, preorder + 1, index);
    printPostorder(inorder + index + 1, preorder + 1 + index, N - 1 - index);
    cout << preorder[0] << " ";
}

//O(N) approach (use map of inorder values to index)
vector<int> preorder;
vector<int> inorder;
vector<int> postorder;

//constructing postorder array from backwards, first root then right subtree then left subtree
//other method is first reucrse for left subtree and right subtree and then add root. then postIndex will start from 0
void construct(int &postIndex,
               int inorderLeft,
               int inorderRight,
               int preLeft,
               int preRight) {
    if (postIndex < 0 || inorderLeft > inorderRight || preLeft > preRight) return;

    int rootValue = preorder[preLeft];
    postorder[postIndex--] = rootValue;

    int inorderIndex;
    //can use map to optimize this
    for (inorderIndex = inorderLeft; inorderIndex <= inorderRight; ++inorderIndex) {
        if (inorder[inorderIndex] == rootValue) break;
    }

    int leftSize = inorderIndex - inorderLeft;
    //recurse for right subtree
    construct(postIndex, inorderIndex + 1, inorderRight, preLeft + leftSize + 1, preRight);
    //recurse for left subtree
    construct(postIndex, inorderLeft, inorderIndex - 1, preLeft + 1, preLeft + leftSize);
}
void solution() {
    int n;
    cin >> n;
    preorder.resize(n);
    for (int &x : preorder) cin >> x;
    //
    inorder = preorder;
    sort(inorder.begin(), inorder.end());

    postorder.resize(n);
    int postIndex = n - 1;
    construct(postIndex, 0, n - 1, 0, n - 1);
    for (int num : postorder) cout << num << " ";
}
