#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int data;
    struct ListNode *next;

    ListNode(int x) {
        data = x;
        next = NULL;
    }
};
struct TreeNode {
    int data;
    TreeNode *left;
    TreeNode *right;

    TreeNode(int x) {
        data = x;
        left = NULL;
        right = NULL;
    }
};

/*
Make Complete Binary Tree from given list/array

LINKED LIST
https://practice.geeksforgeeks.org/problems/make-binary-tree/1
    Just do level order traversal

ARRAY
https://www.geeksforgeeks.org/construct-complete-binary-tree-given-array/
    Same procedure in arrays also
    Just keep index pointer
*/
void convert(ListNode *head, TreeNode *&root) {
    if (!head) {
        root = NULL;
        return;
    }

    queue<TreeNode *> q;  //push nodes and when we pop them we will assign left and right child
    root = new TreeNode(head->data);
    q.push(root);
    head = head->next;
    while (head != NULL && !q.empty()) {
        TreeNode *node = q.front();
        q.pop();
        node->left = new TreeNode(head->data);
        q.push(node->left);
        head = head->next;
        if (head != NULL) {
            node->right = new TreeNode(head->data);
            q.push(node->right);
            head = head->next;
        } else {
            break;
        }
    }
    return;
}
