#include "Tree.h"
/**
 * In these contructions using traversals, atleast one of them should be inorder traversal for constructing unique binary tree.
 * Using preorder + postorder we may get many binary trees
 * Time Complexity O(N^2) in the worst case when skewed tree. But we can make improve it to O(N) by using unordered_map to store
 * all the indexes of inorder values (this eliminates the for loop in each function)
 */

/**
 * Constructing from preorder & inorder traversal
 */
int N;  //size of tree
int preorderIndex = 0;
Node* buildTree(int inorder[], int preorder[], int inorderStart = 0, int inorderEnd = N - 1) {
    if (inorderStart > inorderEnd)
        return NULL;

    int data = preorder[preorderIndex++];
    Node* node = new Node(data);

    if (inorderStart == inorderEnd)
        return node;

    int inorderIndex = -1;
    //Instead of this use unordered_map to do this in O(N)
    for (int i = inorderStart; i <= inorderEnd; ++i) {
        if (inorder[i] == data) {
            inorderIndex = i;
            break;
        }
    }
    node->left = buildTree(inorder, preorder, inorderStart, inorderIndex - 1);
    node->right = buildTree(inorder, preorder, inorderIndex + 1, inorderEnd);
    return node;
}

/**
 * Constructing from postorder & inorder traversal
 * Postorder is just opposite of preorder
 * In Preorder you maintain index go up from 0
 * In postorder you maintain index go down from N - 1
 */
int N;  //size of tree
int postorderIndex = N - 1;
Node* buildTree(int inorder[], int postorder[], int inorderStart = 0, int inorderEnd = N - 1) {
    if (inorderStart > inorderEnd)
        return NULL;

    int data = postorder[postorderIndex--];
    Node* node = new Node(data);

    if (inorderStart == inorderEnd)
        return node;

    int inorderIndex = -1;
    for (int i = inorderStart; i <= inorderEnd; ++i) {
        if (inorder[i] == data) {
            inorderIndex = i;
            break;
        }
    }
    node->right = buildTree(inorder, postorder, inorderIndex + 1, inorderEnd);
    node->left = buildTree(inorder, postorder, inorderStart, inorderIndex - 1);
    return node;
}

/**
 * @question
 * https://www.geeksforgeeks.org/construct-tree-inorder-level-order-traversals/
 * Constructing from inorder & level order
 * 
 * @solution
 * Here it is different. In level order the left & right subtree nodes are not in consecutive order like in preorder/postorder
 * So what you can do is to separate them out for a node and call recursively
 */

Node* buildTree(int inorder[], int levelOrder[], int inorderStart, int inorderEnd, int N) {
    if (inorderStart > inorderEnd)
        return NULL;

    //First node in levelOrder is root
    int data = levelOrder[0];
    Node* root = new Node(data);

    if (levelOrder.size() == 1)
        return root;

    int inorderIndex;
    for (int i = inorderStart; i <= inorderEnd; ++i) {
        if (inorder[i] == data) {
            inorderIndex = i;
            break;
        }
    }

    unordered_set<int> leftSubtreeSet;  //will contain all the left subtree nodes of the root
    for (int i = inorderStart; i < inorderIndex; ++i)
        leftSubtreeSet.insert(inorder[i]);

    //will store the level order nodes of left subtree & right subtree of root
    int leftSubtree[leftSubtreeSet.size()], rightSubtree[N - leftSubtreeSet.size() - 1];
    int index1 = 0, index2 = 0;
    int levelOrderSize = sizeof(levelOrder) / sizeof(int);
    for (int i = 1; i < levelOrderSize; ++i) {
        if (leftSubtreeSet.find(levelOrder[i]) != leftSubtreeSet.end())
            leftSubtree[index1++] = levelOrder[i];
        else
            rightSubtree[index2++] = levelOrder[i];
    }

    root->left = buildTree(inorder, leftSubtree, inorderStart, inorderIndex - 1, index1);
    root->right = buildTree(inorder, rightSubtree, inorderIndex + 1, inorderEnd, index2);

    return root;
}

/**
 * Constructing any binary tree from preorder & postorder
 * Nice explanation => https://leetcode.com/problems/construct-binary-tree-from-preorder-and-postorder-traversal/discuss/161286/C%2B%2B-O(N)-recursive-solution
 */
unordered_map<int, int> postIndexMap;
vector<int> pre, post;
Node* construct(int preStart, int preEnd, int postStart, int postEnd) {
    if (preStart > preEnd || postStart > postEnd)
        return NULL;

    Node* root = new Node(pre[preStart]);
    if (preStart == preEnd)
        return root;

    int index = postIndexMap[pre[preStart + 1]];

    int lSize = index - postStart + 1;
    root->left = construct(preStart + 1, preStart + lSize, postStart, index);
    root->right = construct(preStart + lSize + 1, preEnd, index + 1, postEnd - 1);
    return root;
}
Node* constructFromPrePost(vector<int>& pre, vector<int>& post) {
    pre = pre;
    post = post;
    postIndexMap.clear();
    int N = pre.size();
    for (int i = 0; i < N; ++i)
        postIndexMap[post[i]] = i;
    return construct(0, N - 1, 0, N - 1);
}