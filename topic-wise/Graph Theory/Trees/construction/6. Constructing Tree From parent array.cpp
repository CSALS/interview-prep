#include "../Tree.h"

/*

https://www.geeksforgeeks.org/construct-a-binary-tree-from-parent-array-representation/

    1. first construct node corresponding to each index and store in map
    2. then link parent to node
*/

Node* createTree(int parent[], int n) {
    unordered_map<int, Node*> ptr;
    for (int i = 0; i < n; ++i) {
        Node* temp = new Node(i);
        ptr[i] = temp;
    }
    Node* root = NULL;
    for (int i = 0; i < n; ++i) {
        Node* current = ptr[i];
        if (parent[i] == -1) {
            root = current;
            continue;
        }
        Node* par = ptr[parent[i]];  //parent node for the current node
        if (par->left == NULL) {
            par->left = current;
        } else {
            par->right = current;
        }
    }
    return root;
}