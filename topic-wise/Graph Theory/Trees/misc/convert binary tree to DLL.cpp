#include "../Tree.h"

/*
https://practice.geeksforgeeks.org/problems/binary-tree-to-dll/1
    
    1. Make DLL as a separate list
        Here we just need to do inorder can be done using morris and construct the DLL

    2. Convert the given tree itself into DLL
        bottom-up recursion.
        for a node get the DLL head,tail of left subtree and right subtree
        tail of left subtree -> next = current node
        current->prev = tail of left subtree
        current->next = head of right subtree
        head of right subtree -> prev = current
        return head of left subtree to its parent



Variation =>
    1. check if inorder of tree is palindrome if modification of tree is allowed
        convert given BT to DLL and then check. use 2nd method since we finally get head and tail

    2. https://www.geeksforgeeks.org/convert-a-binary-tree-to-a-circular-doubly-link-list/
        do 2nd method and finally link tail and head of entire tree
*/

//1st
#define next right
#define prev left
void inorder(Node* root, Node*& head, Node*& tail) {
    if (root) {
        inorder(root->left, head, tail);
        Node* current = new Node(root->data);
        if (head == NULL) {
            head = current;
            tail = head;
        } else {
            tail->next = current;
            tail->next->prev = tail;
            tail = tail->next;
        }
        inorder(root->right, head, tail);
    }
}

Node* bToDLL(Node* root) {
    Node *head = NULL, *tail = NULL;
    inorder(root, head, tail);
    return head;
}

//2nd
#define next right
#define prev left
#define Info pair<Node*, Node*>  //head and tail of subtree DLL

Info construct(Node* root) {
    if (!root) return {NULL, NULL};

    Info left = construct(root->left);
    Info right = construct(root->right);

    Node* leftHead = left.first;
    Node* leftTail = left.second;
    Node* rightHead = right.first;
    Node* rightTail = right.second;

    //change links
    root->prev = leftTail;
    if (leftTail) leftTail->next = root;
    root->next = rightHead;
    if (rightHead) rightHead->prev = root;

    Node *head = NULL, *tail = NULL;
    if (leftTail == NULL)
        head = root;
    else
        head = leftHead;

    if (rightHead == NULL)
        tail = root;
    else
        tail = rightTail;
    return {head, tail};
}

Node* bToDLL(Node* root) {
    return construct(root).first;
}
