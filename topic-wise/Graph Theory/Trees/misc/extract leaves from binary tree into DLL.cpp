#include "../Tree.h"

/*
https://www.geeksforgeeks.org/connect-leaves-doubly-linked-list/

Let the following be input binary tree
        1
     /     \
    2       3
   / \       \
  4   5       6
 / \         / \
7   8       9   10


Output:
Doubly Linked List
785910

Modified Tree:
        1
     /     \
    2       3
   /         \
  4           6

*/
#define next right
#define prev left
Node* convertToDLLRecursive(Node* root, Node*& head, Node*& tail) {
    if (!root) return NULL;

    // leaf node
    if (root->left == NULL && root->right == NULL) {
        if (head == NULL) {
            head = root;
            tail = root;
        } else {
            tail->next = root;
            root->prev = tail;
            tail = tail->next;
        }
        //since in the modified tree there should not be leafs present return null to its parent
        return NULL;
    }

    root->left = convertToDLLRecursive(root->left, head, tail);
    root->right = convertToDLLRecursive(root->right, head, tail);
    return root;
}

Node* convertToDLL(Node* root) {
    Node *head = NULL, *tail = NULL;
    Node* modifiedRoot = convertToDLLRecursive(root, head, tail);
    return head;
}