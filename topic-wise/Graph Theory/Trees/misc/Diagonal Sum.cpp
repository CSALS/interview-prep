#include "../Tree.h"

//https://www.geeksforgeeks.org/diagonal-sum-binary-tree/
unordered_map<int, int> diagonalSumMap;  //diagonalNumber -> sum of all nodes across that diagonal
void diagonalSumRecursive(Node* root, int diagonalNumber) {
    if (!root) return;

    diagonalSumMap[diagonalNumber] += root->data;
    diagonalSumRecursive(root->left, diagonalNumber + 1);  //left node will be on the next diagonal
    diagonalSumRecursive(root->right, diagonalNumber);     //right node will be on the same diagonal
}

void diagonalSum(struct Node* root) {
    diagonalSumMap.clear();
    diagonalSumRecursive(root, 0);
    for (auto it : diagonalSumMap) {
        cout << it.first << " " << it.second << endl;
    }
}

int main() {
    struct Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(9);
    root->left->right = new Node(6);
    root->right->left = new Node(4);
    root->right->right = new Node(5);
    root->right->left->right = new Node(7);
    root->right->left->left = new Node(12);
    root->left->right->left = new Node(11);
    root->left->left->right = new Node(10);

    diagonalSum(root);

    return 0;
}