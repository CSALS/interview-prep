#include <bits/stdc++.h>
using namespace std;
struct Node {
    char data;
    struct Node *left;
    struct Node *right;

    Node(char x) {
        data = x;
        left = NULL;
        right = NULL;
    }
};

/*
    https://practice.geeksforgeeks.org/problems/convert-ternary-expression-to-binary-tree/1#_=_
    solution in editorial seems wrong. although preorder will be correct the constructed tree is not
*/
Node *convertExpressionRecursive(string str, int start, int end) {
    int n = (int)str.length();
    if (start >= n) {
        return NULL;
    }

    Node *root = new Node(str[start]);

    int questionCount = 0;
    for (int i = start; i <= end; ++i) {
        if (str[i] == '?') {
            questionCount++;
        } else if (str[i] == ':') {
            questionCount--;
            if (questionCount == 0) {
                root->left = convertExpressionRecursive(str, start + 2, i - 1);
                root->right = convertExpressionRecursive(str, i + 1, end);
                break;
            }
        }
    }
    return root;
}

Node *convertExpression(string str, int i) {
    Node *root = convertExpressionRecursive(str, i, str.length() - 1);
    return root;
}