/*
https://www.geeksforgeeks.org/check-if-inorder-traversal-of-a-binary-tree-is-palindrome-or-not/?ref=leftbar-rightbar

    1. do inorder traversal and store it in array and then check
        O(N) T | O(N) S for storing the inorder array

    2. Using forward and backward iterators. just one traversal is enough
        O(N) T | O(Height) S

    3. if modification of tree is allowed
        convert binary tree into DLL and then check
        O(N) T  | O(1) S since we are converting given binary tree itself into DLL

*/
