#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/find-height-binary-tree-represented-parent-array/

    1. construct the binary tree from parent array and then find height
        constructing has two approaches => O(N^2) T and O(N) T,S approach
    
    2. find depth of each node. don't need to construct the tree
        Time => every node depth is calculated only once so it is O(N)
*/

void calculateDepth(int u, vector<int> &parent, vector<int> &depth) {
    int par = parent[u];
    //u is root
    if (par == -1) {
        depth[u] = 0;
        return;
    }

    if (depth[par] == -1)
        calculateDepth(par, parent, depth);

    depth[u] = 1 + depth[par];
}

//nodes are from 0...N-1
//parent[i] is parent of ith node
int findHeight(vector<int> parent) {
    int n = parent.size();
    //depth[i] = depth of node i. -1 if not calculated
    vector<int> depth(n, -1);

    int maxDepthOfNode = -1;
    for (int u = 0; u < n; ++u) {
        if (depth[u] == -1) {
            calculateDepth(u, parent, depth);
        }
        maxDepthOfNode = max(maxDepthOfNode, depth[u]);
    }
    return 1 + maxDepthOfNode;
}

// Driver program to test above functions
int main() {
    // int parent[] = {1, 5, 5, 2, 2, -1, 3};
    vector<int> parent = {-1, 0, 0, 1, 1, 3, 5};

    cout << findHeight(parent) << endl;  //5
    parent = {1, 5, 5, 2, 2, -1, 3};
    cout << findHeight(parent) << endl;  //4
    return 0;
}
