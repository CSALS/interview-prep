#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/count-smaller-elements-right-side-using-set-c-stl/
https://www.geeksforgeeks.org/count-smaller-elements-on-right-side/

https://leetcode.com/problems/count-of-smaller-numbers-after-self/

You are given an integer array nums and you have to return a new counts array. 
The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

Example:

Input: [5,2,6,1]
Output: [2,1,1,0] 


    Avg case => O(NlogN) if BST is balanced
    Worst case => O(N^2) if it is skewed towards one direction

    Thought process =>
        obvious approach is O(N^2)
        how to optimize futher?
        it doesn't look like can be solved in O(N)
        so we will try the next best which is O(NlogN)
        what data structure support logN operators?
        Balanced BST. and it also has property that all eles in left subtree are less than current node and  current less than right subtree
        so we might use this
        since we need elements which are smaller towards right
        start from right to left and keep finding answer for each index
*/

struct SpecialBST {
    int data;
    SpecialBST *left;
    SpecialBST *right;
    int leftSize;  //size of left subtree

    //every time a new node is inserted. it is inserted as leaf so leftSize will be 0
    SpecialBST(int data) : data(data), left(NULL), right(NULL), leftSize(0) {}
};

vector<int> result;
SpecialBST *root;
void insertBST(int data, int resultIndex) {
    result[resultIndex] = 0;

    SpecialBST *current = root, *previous = NULL;
    while (current != NULL) {
        previous = current;
        if (current->data < data) {
            //will go into right subtree
            result[resultIndex] += (1 + current->leftSize);
            current = current->right;
        } else {
            //will go into left subtree
            (current->leftSize)++;
            current = current->left;
        }
    }
    if (previous == NULL) {
        //root node
        root = new SpecialBST(data);
    } else if (previous->data < data) {
        previous->right = new SpecialBST(data);
    } else {
        previous->left = new SpecialBST(data);
    }
}

vector<int> rightSmallerThan(vector<int> nums) {
    if (nums.empty()) return {};
    result.clear();
    result.resize(nums.size(), 0);
    root = NULL;

    int n = nums.size();
    for (int i = n - 1; i >= 0; --i) {
        insertBST(nums[i], i);
    }
    return result;
}