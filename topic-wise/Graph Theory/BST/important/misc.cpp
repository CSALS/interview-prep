#include "Tree.h"

/*
https://www.geeksforgeeks.org/convert-bst-min-heap/
The tree which is BST should be converted into min heap such that for every node
the left subtree values < right subtree values

    1. Do inorder traversal of BST and store it in arr[]
    2. That arr[] itself is min-heap but we need left subtree values < right subtree values also
    3. Do preorder traversal on the bst and copy arr[i++] where is global


https://www.geeksforgeeks.org/binary-tree-binary-search-tree-conversion-using-stl-set/?ref=lbp
The tree which is BT should have values which follow BST

    O(N)     1. get inorder traversal of BT and store it in arr[]
    O(NlogN) 2. since inorder of bst should be sort it, sort(arr[])
    O(N)     3. then again do inorder of BT and copy arr[i++] where i is global

    Instead of array use set since set will be sorted
    time complexity will be same since insertion and deletion is O(logn)
    but we don't need to sort

https://www.geeksforgeeks.org/duplicates-removal-in-array-using-bst/?ref=leftbar-rightbar
    
    Keep adding inserting array elements into bst.
    O(NlogN) T | O(N) S
*/