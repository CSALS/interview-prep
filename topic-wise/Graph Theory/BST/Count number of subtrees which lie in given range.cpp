#include "Tree.h"

/*
https://www.geeksforgeeks.org/count-bst-subtrees-that-lie-in-given-range/
    we will do bottom-up recursion.
    if both subtrees are in range and current node also in range then we increment count and send true to parent, else send false
*/
bool getCountRecursive(Node* root, int X, int Y, int& count) {
    if (!root) return true;

    bool left = getCountRecursive(root->left, X, Y, count);
    bool right = getCountRecursive(root->right, X, Y, count);

    bool result = false;
    if (left && right) {
        int key = root->data;
        if (X <= key && key <= Y) {
            ++count;
            result = true;
        }
    }
    return result;
}

int getCount(Node* root, int X, int Y) {
    int count = 0;
    getCountRecursive(root, X, Y, count);
    return count;
}

int main() {
    // Let us construct the BST shown in the above figure
    Node* root = new Node(10);
    root->left = new Node(5);
    root->right = new Node(50);
    root->left->left = new Node(1);
    root->right->left = new Node(40);
    root->right->right = new Node(100);
    /* Let us constructed BST shown in above example 
          10 
        /    \ 
      5       50 
     /       /  \ 
    1       40   100   */
    cout << getCount(root, 5, 45) << endl;   //1
    cout << getCount(root, 1, 45) << endl;   //3
    cout << getCount(root, 1, 100) << endl;  //6
    cout << getCount(root, 1, 50) << endl;   //3
    return 0;
}