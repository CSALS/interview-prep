#include "Tree.h"

/*
https://www.geeksforgeeks.org/remove-bst-keys-outside-the-given-range/

    1. for every node which is out of range delete it using standard deleteBST() method
        O(NlogN) in the worst case

    2. if we carefully observe if current value is X
            if X < L then entire left subtree of X is also out of range so delete current and its left (return current->right to parent)
            similarly for x > R case
        so for every node out of range we just return opposite subtree pointer O(1)
        O(N) TIME
*/

//bottom-up
//delete if needed in left subtree and right subtree and then delete current node if needed
Node* removeOutsideRange(Node* root, int L, int R) {
    if (!root) return NULL;

    root->left = removeOutsideRange(root->left, L, R);
    root->right = removeOutsideRange(root->right, L, R);
    int key = root->data;
    if (L <= key && key <= R) {
        //in-range so don't delete anything
        return root;
    }

    if (key < L) {
        //entire left subtree out of range so return current.right to parent
        Node* right = root->right;
        delete root;
        return right;
    } else {
        Node* left = root->left;
        delete root;
        return left;
    }
}

void inorderTraversal(Node* root) {
    if (root) {
        inorderTraversal(root->left);
        cout << root->key << " ";
        inorderTraversal(root->right);
    }
}
Node* insert(Node* root, int key) {
    if (root == NULL)
        return new Node(key);
    if (root->data > key)
        root->left = insert(root->left, key);
    else
        root->right = insert(root->right, key);
    return root;
}
int main() {
    Node* root = NULL;
    root = insert(root, 6);
    root = insert(root, -13);
    root = insert(root, 14);
    root = insert(root, -8);
    root = insert(root, 15);
    root = insert(root, 13);
    root = insert(root, 7);

    inorderTraversal(root);  // -13 -8 6 7 13 14 15
    cout << endl;
    root = removeOutsideRange(root, -10, 13);
    inorderTraversal(root);  //-8 6 7 13
    cout << endl;
    return 0;
}