#include "Tree.h"

/*
    https://www.geeksforgeeks.org/count-bst-nodes-that-are-in-a-given-range/?ref=lbp

    1. We can do morris traversal and keep counting. Worst case O(RightBound)
        But not using binary search tree property.
    2. Eliminate subtrees based on the current value. worst case O(height + K)
*/
void getCountOfNodeHelper(Node *root, int A, int B, int &count) {
    if (!root) return;

    int key = root->data;

    if (A <= key && key <= B) {
        ++count;
        getCountOfNodeHelper(root->left, A, B, count);
        getCountOfNodeHelper(root->right, A, B, count);
    } else if (key < A) {
        getCountOfNodeHelper(root->right, A, B, count);
    } else {
        getCountOfNodeHelper(root->left, A, B, count);
    }
}

int getCountOfNode(Node *root, int A, int B) {
    int count = 0;
    getCountOfNodeHelper(root, A, B, count);
    return count;
}