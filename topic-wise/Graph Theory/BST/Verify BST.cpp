#include "Tree.h"

/*
    1. using the min, max bounds method
       O(N) Time | O(N) recursion stack space 
    2. using inorder array and check if it is sorted
       O(N) Time | O(N) space [Inorder array can obtained in constant space using morris traversal]
    3. In the 1st approach we are essentialy doing pre-order recursion.
      Can do preorder using stack itself no need of recursion
      O(N) Time | O(N) space
      Queue of <Info> where Info = {Node* ptr, int min, int max} and for each node check if it in bounds
*/

bool isValidBST(Node* root) {
    return isBSTRecursive(root, LONG_MIN, LONG_MAX);
}

bool isBSTRecursive(Node* root, long long min, long long max) {
    if (!root) {
        return true;
    }

    if (min < root->data && root->data < max) {
        bool isLeftBST = isBSTRecursive(root->left, min, root->data);
        bool isRightBST = isBSTRecursive(root->right, root->data, max);
        return isLeftBST && isRightBST;
    } else {
        return false;
    }
}
