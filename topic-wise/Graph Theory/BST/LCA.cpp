#include "Tree.h"

Node* lcaBST(Node* root, int x, int y) {
    if (!root) return NULL;

    int key = root->data;

    if (key < x && key < y) {
        return lcaBST(root->right, x, y);
    } else if (key > x && key > y) {
        return lcaBST(root->left, x, y);
    } else {
        //if key is x or y or it is b/w x and y then this is the lca
        return root;
    }
}
/*
    O(logN) Time. Assumes both n1 & n2 exist. If it is not guarenteed then do a search after calling LCA()
    Since this is tail-call recursion function just use iteration to do it in O(1) space without involving any stack
*/
Node* lcaIterative(Node* root, int x, int y) {
    if (!root) return NULL;

    Node* current = root;
    while (current) {
        int key = current->data;

        if (key < x && key < y) {
            current = current->right;
        } else if (key > x && key > y) {
            current = current->left;
        } else {
            return current;
        }
    }
    return NULL;
}