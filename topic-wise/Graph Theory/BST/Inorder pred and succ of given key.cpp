#include "Tree.h"

//TODO:: IMP DO ONCE. DRY RUN

//https://practice.geeksforgeeks.org/problems/predecessor-and-successor/1

//Tail Recursive function
void findPreSucRecursive(Node* root, Node*& pre, Node*& suc, int key) {
    if(!root) {
        return;
    }
    
    if(root->data == key) {
        //Find pred in left subtree rightmost node
        //Find succ in right subtree leftmost node
        if(root->left != NULL) {
            pre = root->left;
            while(pre->right != NULL) {
                pre = pre->right;
            }
        }
        if(root->right != NULL) {
            suc = root->right;
            while(suc->left != NULL) {
                suc = suc->left;
            }
        }
    }
    else if(key < root->data) {
        //In the left subtree
        suc = root;
        findPreSucRecursive(root->left, pre, suc, key);
    } else {
        //In the right subtree
        pre = root;
        findPreSucRecursive(root->right, pre, suc, key);
    }
}
//Above recursion function is tail-recursion so can easily convert to iteration
void findPreSucIterative(Node* root, Node*& pre, Node*& suc, int key) {
    //TODO: complete it
}