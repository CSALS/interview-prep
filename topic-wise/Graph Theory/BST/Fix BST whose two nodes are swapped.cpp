#include "Tree.h"

/*
https://leetcode.com/problems/recover-binary-search-tree

Lot of approaches.
1. Do a inorder traversal and use that array to find the two nodes which are swapped
2. Instead of searching for swapped nodes after doing entire inorder traversal, do it while doing inorder traversal
   Maintain previous variable node to compare current node while visiting.
    a) Inorder using Recursion
    b) Inorder using Forward iterator
    c) Inorder using morris (This solution builds from a). So first do a) and then this)
        O(N) Time | O(1) Space
*/

//inorder using recursion
void inorderRecursive(Node* current, Node*& previous, Node*& inversion1, Node*& inversion2) {
    if (!current) {
        return;
    }

    inorderRecursive(current->left, previous, inversion1, inversion2);
    visitNode(current, previous, inversion1, inversion2);  //After visiting node we go to right subtree
    inorderRecursive(current->right, previous, inversion1, inversion2);
}
//inorder using morris
void visitNode(Node* current, Node*& previous, Node*& inversion1, Node*& inversion2) {
    if (previous != NULL && current->data < previous->data) {
        if (inversion1 == NULL) {
            inversion1 = previous;
            inversion2 = current;
        } else {
            inversion2 = current;
        }
    }
    previous = current;  //After visitNode() we go into right subtree so the previous will be the current node
}

void inorderIterative(Node* current, Node*& inversion1, Node*& inversion2) {
    //Morris inorder to get O(N) T | O(1) S solution
    auto getInorderPredecessor = [&](Node* current) {
        //In left subtree the rightmost node
        Node* predecessor = current->left;
        while (predecessor != NULL && predecessor->right != NULL && predecessor->right != current) {
            predecessor = predecessor->right;
        }
        return predecessor;
    };

    Node* previous = NULL;
    while (current != NULL) {
        if (current->left == NULL) {
            //Visit this node and move to right subtree
            visitNode(current, previous, inversion1, inversion2);
            current = current->right;
        } else {
            Node* predecessor = getInorderPredecessor(current);
            if (predecessor->right == NULL) {
                //Visit the left subtree
                predecessor->right = current;
                current = current->left;
            } else {
                //Left subtree is visited. Visit this node and move to right subtree
                predecessor->right = NULL;
                visitNode(current, previous, inversion1, inversion2);
                current = current->right;
            }
        }
    }
}

void recoverTree(Node* root) {
    Node *inversion1 = NULL, *inversion2 = NULL, *previous = NULL;
    // inorderRecursive(root, previous, inversion1, inversion2);
    inorderIterative(root, inversion1, inversion2);
    int temp = inversion1->data;
    inversion1->data = inversion2->data;
    inversion2->data = temp;
}