# Tricks
- in bsts try to use inorder array (it will be sorted) to solve some problems
- bst distnace b/w two node in order is just diff of indexes
- Understand difference b/w => Inorder predecessor & successor | Forward & Backward BST Iterator
- Mostly we assume unique values
- lot of times we store some extra information about node like min, max bounds of that node's subtrees, size of that node's subtree etc.
- prev, curr while doing inorder

- **think of using {min, max} of subtrees**

- Two recursions = Top-down and bottom-up
Use bottom-up for optimization over top-down approach

# Important
1. delete node from bst
2. bst from preorder (without using inorder)
3. bst from level order (Only one possible if BST. If BT then many)
4. common nodes in 2 bst

# Problems
* Find Two Node From Same BST which add up to a sum K = https://tinyurl.com/yazkgbg5
    1. Inorder Traversal & Reduces to sorted array and target sum. Two pointer. one from 0 and one from n-1.
        Inorder can be done using Morris-Traversal (constant space)
        O(N) Time & Space
    2. Inorder Traversal + Hashing
        O(N) Time & Space
    3. Using Fwd & Bwd BST Iterator as two pointer technique
* Find Two Nodes From 2 BSTs which add up to a sum K =
    1. Inorder Traversal & Reduces to two sorted arrays and target sum. Two pointer. one from 0 and one from n-1.
        Inorder can be done using Morris-Traversal (constant space)
        O(N + M) Time & Space

# Implementation

- In std::map c++ = self-balancing BST is used (red-black or AVL)
- Red-black has O(1) re-balancing whereas AVL has O(logn) so red-black is most used
- https://www.geeksforgeeks.org/hash-table-vs-stl-map/

bsts implementing =>
height balanced = red-black, avl
if 3-level or 4-level hashmap doesn't give good results use hashmap + BST (use balanced one meaning use red-black trees)
https://www.youtube.com/watch?v=mTNkqMDCasI  what Java 8 has done with HashMap implementation which now converts the bucket into a Red Black Tree after reaching the threshold of 8 elements and converts it back to a linked list after reaching 6 elements with better performance. (when number of elements in bucket cross that threshold)

how self-balancing-bst (set, map) implemented in cpp?