#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/kth-smallest-element-in-a-bst/
 * https://www.geeksforgeeks.org/find-k-th-smallest-element-in-bst-order-statistics-in-bst/
 * https://www.geeksforgeeks.org/kth-smallest-element-in-bst-using-o1-extra-space/
 * These approaches are same if you want find kth element in binary tree
 * 
 * 1. Using inorder array and find kth element which is kth smallest element since sorted
 *    O(N) Time | O(h) Space
 * 2. Can do inorder traversal using morris traversal
 *    O(N) Time | O(1) Space
 * 3. Augmented BST. Modify node structure. Store count of nodes in the left-subtree
 *    O(height) Time | O(height) Space
 *   Whenever we want to optimize futher then we can modify structure
 */

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct SpecialBST {
    int data;
    SpecialBST *left, *right;
    //size of left subtree for this node which is same as number of elements less than current node
    int leftSize;

    //whenever a new node is inserted in BST it will be added as leaf node so leftSize will be 0 initially
    SpecialBST(int data) : data(data), left(NULL), right(NULL), leftSize(0) {}
};

class Solution {
   public:
    int kthSmallest(TreeNode *root, int k) {
        vector<int> keys;
        queue<TreeNode *> q;
        q.push(root);
        while (!q.empty()) {
            TreeNode *current = q.front();
            q.pop();
            keys.push_back(current->val);
            if (current->left) q.push(current->left);
            if (current->right) q.push(current->right);
        }

        return kthSmallestHelper(keys, k);
    }

    //out of the given keys we need to find kth smallest element
    int kthSmallestHelper(vector<int> keys, int k) {
        SpecialBST *root = NULL;
        //insert keys into the augmented bst
        for (int key : keys) {
            insertSpecialBST(root, key);
        }
        return kthSmallestRecursive(root, k);
    }

    int kthSmallestRecursive(SpecialBST *root, int K) {
        int rootPosition = root->leftSize + 1;  //root position in the inorder array
        if (rootPosition == K) {
            return root->data;
        } else if (K < rootPosition) {
            //go search in left subtree the Kth element
            return kthSmallestRecursive(root->left, K);
        } else {
            //go search in right subtree the (K - rootPosition)th element
            return kthSmallestRecursive(root->right, K - rootPosition);
        }
    }

    void insertSpecialBST(SpecialBST *&root, int key) {
        SpecialBST *current = root, *previous = NULL;
        while (current != NULL) {
            previous = current;
            if (key > current->data) {
                //will go into right subtree
                current = current->right;
            } else {
                //will go into left subtree
                (current->leftSize)++;
                current = current->left;
            }
        }
        if (previous == NULL) {
            root = new SpecialBST(key);
        } else if (key < previous->data) {
            previous->left = new SpecialBST(key);
        } else {
            previous->right = new SpecialBST(key);
        }
    }
};