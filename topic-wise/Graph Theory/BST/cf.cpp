#include <bits/stdc++.h>
using namespace std;

#define INF 1000000000
#define int long long
#define endl "\n"
#define deb(x) cout << #x << " " << x << endl;

void solution() {
    int N, L;
    cin >> N >> L;
    vector<int> costs(N);
    for (int &x : costs) cin >> x;
    //
    // vector<vector<int>> dp(N + 1, vector<int>(L + 1));  //dp[n][l] = min cost when you have n bottles and need atleast l litres

    // for (int n = 0; n <= N; ++n) dp[n][0] = 0;
    // for (int l = 1; l <= L; ++l) dp[0][l] = INF;

    // for (int n = 1; n <= N; ++n) {
    //     for (int l = 1; l <= L; ++l) {
    //         int curr_lit = (int)(pow(2, n - 1) + 0.5);
    //         if (curr_lit > l)
    //             dp[n][l] = min(dp[n - 1][l], costs[n - 1]);
    //         else if (l >= curr_lit)
    //             dp[n][l] = min(dp[n - 1][l], costs[n - 1] + dp[n][l - curr_lit]);
    //     }
    // }
    // cout << dp[N][L] << endl;

    // vector<int> dp_n(L + 1, INF);  //initially 0 bottles
    // dp_n[0] = 0;

    // for (int n = 1; n <= N; ++n) {
    //     for (int l = 1; l <= L; ++l) {
    //         int curr_lit = (int)(pow(2, n - 1) + 0.5);
    //         if (curr_lit > l)
    //             dp_n[l] = min(dp_n[l], costs[n - 1]);
    //         else if (l >= curr_lit)
    //             dp_n[l] = min(dp_n[l], costs[n - 1] + dp_n[l - curr_lit]);
    //     }
    // }
    // cout << dp_n[L] << endl;
}
int32_t main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solution();
    return 0;
}