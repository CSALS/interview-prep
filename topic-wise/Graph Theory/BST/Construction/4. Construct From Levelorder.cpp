#include "../Tree.h"

/*
https://www.geeksforgeeks.org/construct-bst-from-its-given-level-order-traversal-set-2/


Input : {7, 4, 12, 3, 6, 8, 1, 5, 10}
Output : BST: 
        7        
       / \       
      4   12      
     / \  /     
    3  6 8    
   /  /   \
  1   5   10

    1st appraoch is to construct iteratively.
    The idea is to use the Recursion:-
    We know that the first element will always be the root of tree and second element will be the left child 
    and third element will be the right child (if fall in the range), and so on for all the remaining elements.

    1) First pick the first element of the array and make it root.
    2) Pick the second element, if it’s value is smaller than root node value make it left child,
    3) Else make it right child
    4) Now recursively call the step (2) and step (3) to make a BST from its level Order Traversal.

    This is O(N^2) approach

    2nd approach is to maintain extra information for each node (min, max) bounds
    These bounds can be used to check if we can insert the next node in its subtree
    This is O(N) approach and we also queue O(N) space
*/

#define INF INT_MAX

struct Info {
    Node *ptr;
    int min, max;
    Info(Node *ptr, int min, int max) : ptr(ptr), min(min), max(max) {}
};

/**
 * @param arr is level order array of the BST
 * @param n is the number of nodes in the BST
 */
Node *constructBst(int arr[], int n) {
    if (n == 0) {
        return NULL;
    }
    queue<Info> q;
    Node *root = new Node(arr[0]);
    Info rootInfo(root, -INF, INF);
    q.push(rootInfo);
    int i = 1;  //level order index
    while (i < n) {
        Node *node = q.front().ptr;
        int min = q.front().min;
        int max = q.front().max;
        q.pop();
        //If the current node value is the less than node value and it is the bounds then insert in left
        if (arr[i] < node->data && min < arr[i] && arr[i] < max) {
            //in left subtree
            Node *temp = new Node(arr[i++]);
            node->left = temp;
            Info tempDetails(temp, min, node->data);
            q.push(tempDetails);
        }
        //in above body we are doing i++ so that's why we need i < n condition here
        if (i < n && arr[i] > node->data && min < arr[i] && arr[i] < max) {
            //in right subtree
            Node *temp = new Node(arr[i++]);
            node->right = temp;
            Info tempDetails(temp, node->data, max);
            q.push(tempDetails);
        }
    }
    return root;
}