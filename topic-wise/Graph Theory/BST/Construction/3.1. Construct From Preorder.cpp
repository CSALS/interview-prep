#include "../Tree.h"

/**
 * https://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversa/

Given preorder traversal of a binary search tree, construct the BST.

For example, if the given traversal is {10, 5, 1, 7, 40, 50}, then the output should be root of following tree.

     10
   /   \
  5     40
 /  \      \
1    7      50


CAN BE APPLIED FOR POSTORDER ALSO (Using postorder we can use similar method. Coming from back in the array)

 * 1. To get inorder array you can sort preorder. And then construct binary tree using preorder & inorder.
 * O(NlogN) Time | O(1) Space
 * 2. Only using preorder and stack 
 * O(N) Time | O(h) Space
 */
Node* bstFromPreorder(vector<int>& preorder) {
    if (preorder.empty()) {
        return NULL;
    }

    int preorderIndex = 0;
    Node* root = new Node(preorder[preorderIndex++]);
    //To keep track of nodes whose right subtree has yet to be computed but left subtree has already been computed
    stack<Node*> st;
    Node* current = root;
    while (preorderIndex < preorder.size()) {
        Node* node = new Node(preorder[preorderIndex]);

        //node will go in the left subtree of current
        if (node->data < current->data) {
            current->left = node;
            st.push(current);  //current left is assigned but not right
            current = current->left;
            preorderIndex++;
        } else {
            //Although node->data > current->data it may not lie on the right subtree of current.
            //That depends on parent of current which is on the top of stack
            //if empty stack then thee current is root so we can add it to the right subtree
            if (st.empty() || st.top()->data > node->data) {
                //Then node lies on left subtree of st.top() and to the right of current
                current->right = node;  //current right is also assigned so its job is done. It is useless now
                current = current->right;
                preorderIndex++;
            } else {
                //Node won't lie on right of current so go back to its parent which is on top of stack
                current = st.top();
                st.pop();
                delete node;
            }
        }
    }
    return root;
}