/*

https://www.geeksforgeeks.org/binary-tree-to-binary-search-tree-conversion/


1. get inorder
2. sort it
3. do inorder traversal on the tree and replace sorted inorder

O(N + NlogN + N) Time | O(N) Space
*/