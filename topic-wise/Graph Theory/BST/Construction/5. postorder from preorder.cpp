#include "../Tree.h"

/*
https://www.geeksforgeeks.org/find-postorder-traversal-of-bst-from-preorder-traversal/
https://practice.geeksforgeeks.org/problems/preorder-to-postorder/0

40 30 35 80 100 = 35 30 100 80 40
40 30 32 35 80 90 100 120 = 35 32 30 120 100 90 80 40

    1. get the inroder array by sort preorder and find postorder using preorder & inorder [without.constructing.tree]
    2. without consturcting any tree

    POSTORDER = left....right...root
*/
vector<int> preorder;
vector<int> postorder;

//construct left subtree and then right subtree and then add root
//maintain {min, max} to check for appropriate left and right subtree positioning
void construct(int &postIndex, int min, int max, int &preIndex, int n) {
    if (preIndex >= n || postIndex >= n) return;

    int rootVal = preorder[preIndex];

    //if root value not in bounds then this can't be root
    if (rootVal < min || rootVal > max) return;

    preIndex++;
    //for left subtree
    construct(postIndex, min, rootVal, preIndex, n);
    //for right subtree
    construct(postIndex, rootVal, max, preIndex, n);
    postorder[postIndex++] = rootVal;
}

void solution() {
    int n;
    cin >> n;
    preorder.resize(n);
    for (int &x : preorder) cin >> x;
    //
    postorder.resize(n);
    int postIndex = 0;
    int preIndex = 0;
    construct(postIndex, -INF, INF, preIndex, n);
    for (int num : postorder) cout << num << " ";
}