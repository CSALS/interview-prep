#include "../Tree.h"

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

/**
 * https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree
 * Convert sorted linked list to balanced BST
 * 1. Unlike arrays finding here takes linear time. Time complexity for that approach is similar to merge sort
 *    O(NlogN) Time | O(logN) Space
 * 2. Use hashmap to store index->number map. This essentialy gives O(1) access to middle element 
 *    O(N) Time | O(N) Space
 * 3. Since sorted list is the inorder of BST. Try to simulate the behaviour of inorder traversal.
 *    Construct BST from bottom to top.
 *    O(N) Time | O(h) Space
 */
class Solution {
    int getLengthList(ListNode* head) {
        ListNode* trav = head;
        int count = 0;
        while (trav != NULL) {
            ++count;
            trav = trav->next;
        }
        return count;
    }
    TreeNode* inorderSimulation(ListNode*& head, int left, int right) {
        if (left > right || head == NULL) {
            return NULL;
        }
        int mid = (left + right) / 2;

        TreeNode* leftSubtree = inorderSimulation(head, left, mid - 1);

        TreeNode* current = new TreeNode(head->val);
        head = head->next;

        TreeNode* rightSubtree = inorderSimulation(head, mid + 1, right);

        current->left = leftSubtree;
        current->right = rightSubtree;
        return current;
    }

   public:
    TreeNode* sortedListToBST(ListNode* head) {
        int length = getLengthList(head);
        TreeNode* root = inorderSimulation(head, 0, length - 1);
        return root;
    }
};