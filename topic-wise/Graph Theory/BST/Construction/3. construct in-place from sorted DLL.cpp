#include <bits/stdc++.h>
using namespace std;

/* A Doubly Linked List node that 
will also be used as a tree node */
class Node {
   public:
    int data;
    Node *right;
    Node *left;
};

// A utility function to count nodes in a Linked List
int countNodes(Node *head);

/*
https://www.geeksforgeeks.org/in-place-conversion-of-sorted-dll-to-balanced-bst/
    same as single-linked list. 
    inorder simulation.
    but there we created new nodes for the tree.
    here we just change links.
*/

#define prev left
#define next right
Node *sortedListToBSTRecursive(Node *&head, int L, int R) {
    if (!head || L > R) return NULL;

    int mid = (L + R) / 2;

    Node *left = sortedListToBSTRecursive(head, L, mid - 1);
    Node *current = head;
    current->prev = left;
    head = head->next;
    Node *right = sortedListToBSTRecursive(head, mid + 1, R);
    current->next = right;
    return current;
}

Node *sortedListToBST(Node *head) {
    int N = countNodes(head);
    return sortedListToBSTRecursive(head, 1, N);
}

//----------------------------------------
/* UTILITY FUNCTIONS */
int countNodes(Node *head) {
    int count = 0;
    Node *temp = head;
    while (temp) {
        temp = temp->right;
        count++;
    }
    return count;
}
void push(Node **head_ref, int new_data) {
    Node *new_node = new Node();
    new_node->data = new_data;
    new_node->left = NULL;
    new_node->right = (*head_ref);
    if ((*head_ref) != NULL)
        (*head_ref)->left = new_node;
    (*head_ref) = new_node;
}

void printList(Node *node) {
    while (node != NULL) {
        cout << node->data << " ";
        node = node->right;
    }
    cout << endl;
}
void preOrder(Node *node) {
    if (node == NULL)
        return;
    cout << node->data << " ";
    preOrder(node->left);
    preOrder(node->right);
}

int main() {
    /* Start with the empty list */
    Node *head = NULL;

    /* Let us create a sorted linked list to test the functions  
    Created linked list will be 7->6->5->4->3->2->1 */
    push(&head, 7);
    push(&head, 6);
    push(&head, 5);
    push(&head, 4);
    push(&head, 3);
    push(&head, 2);
    push(&head, 1);

    printList(head);                     //1 2 3 4 5 6 7
    Node *root = sortedListToBST(head);  //4 2 1 3 6 5 7
    preOrder(root);
    cout << endl;
    return 0;
}
