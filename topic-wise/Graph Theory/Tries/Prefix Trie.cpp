#include <bits/stdc++.h>
using namespace std;

/**
 * PREFIX TRIE.
 * Useful in implemeting dictionary, autosearch feature
*/

const int MAX_CHARS = 26;

class Trie {
   private:
    bool isEnd;
    //We can use unordered_map<char, Trie*> for space efficiency. Since we will be storing only required characters
    Trie* child[MAX_CHARS];

   public:
    Trie() {
        isEnd = false;
        for (int i = 0; i < MAX_CHARS; ++i) {
            child[i] = NULL;
        }
    }

    /** Inserts a word into the trie. */
    void insert(string word) {
        if (word.empty()) {
            return;
        }

        Trie* current = this;
        for (char c : word) {
            if (current->child[c - 'a'] == NULL) {
                current->child[c - 'a'] = new Trie();
            }
            current = current->child[c - 'a'];
        }
        current->isEnd = true;
    }

    /** Returns if the word is in the trie. */
    bool search(string word) {
        Trie* current = this;
        for (char c : word) {
            if (current->child[c - 'a'] == NULL) {
                return false;
            }
            current = current->child[c - 'a'];
        }
        return (current->isEnd);
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) {
        Trie* current = this;
        for (char c : prefix) {
            if (current->child[c - 'a'] == NULL) {
                return false;
            }
            current = current->child[c - 'a'];
        }
        return true;
    }

    void deleteWord(string word) {
        if (!search(word)) {
            return;
        }
        this->deleteWordUtil(word, 0);
    }

   private:
    //isEnd = T doesn't mean leaf node. if all children is null then it is leaf node
    bool isLeafNode() {
        for (int i = 0; i < MAX_CHARS; ++i) {
            if (this->child[i] != NULL) {
                return false;
            }
        }
        return true;
    }

    Trie* deleteWordUtil(string word, int index) {
        if (this == NULL) {
            return NULL;
        }
        if (index == word.length()) {
            bool isLeaf = this->isLeafNode();
            if (isLeaf) {
                delete this;
                return NULL;
            } else {
                this->isEnd = false;
                return this;
            }
        }
        int key = word[index] - 'a';
        this->child[key] = this->child[key]->deleteWordUtil(word, index + 1);
        //If current node has no child and also not ending node for any other word
        if (this->isLeafNode() && this->isEnd == false) {
            return NULL;
        } else {
            return this;
        }
    }
};

int main() {
    Trie* obj = new Trie();
    obj->insert("zoo");
    obj->deleteWord("zoo");

    obj->insert("geek");
    obj->insert("geeks");
    obj->insert("bat");
    obj->insert("bad");

    obj->deleteWord("geeks");
    obj->deleteWord("bat");

    cout << obj->search("bat") << endl;
    cout << obj->search("bad") << endl;
    cout << obj->startsWith("bat") << endl;
    cout << obj->startsWith("ba") << endl;

    cout << obj->search("geeks") << endl;     // 0
    cout << obj->search("geek") << endl;      // 1
    cout << obj->startsWith("geek") << endl;  // 1
}