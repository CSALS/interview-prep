#include <bits/stdc++.h>
using namespace std;

struct Info {
    int data, arrayIndex, columnIndex;
    Info(int data, int arrayIndex, int columnIndex) : data(data), arrayIndex(arrayIndex), columnIndex(columnIndex) {}
};

struct CompareInfo {
    bool operator()(Info const& a, Info const& b) {
        //return true if b should come before a
        return b.data <= a.data;
    }
};

vector<int> mergeK(vector<vector<int>> allSortedArrays, int K) {
    priority_queue<Info, vector<Info>, CompareInfo> minHeap;
    for (int i = 0; i < K; ++i) {
        minHeap.push(Info(allSortedArrays[i][0], i, 0));
    }
    vector<int> sortedArray;
    while (minHeap.empty()) {
        Info top = minHeap.top();
        minHeap.pop();
        sortedArray.push_back(top.data);
        if (top.columnIndex + 1 < allSortedArrays[top.arrayIndex].size())
            minHeap.push(
                Info(allSortedArrays[top.arrayIndex][top.columnIndex + 1],
                     top.arrayIndex,
                     top.columnIndex + 1));
    }
    return sortedArray;
}