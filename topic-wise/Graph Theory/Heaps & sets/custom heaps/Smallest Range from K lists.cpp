#include <bits/stdc++.h>
using namespace std;
#define N 100

/*
https://www.geeksforgeeks.org/find-smallest-range-containing-elements-from-k-lists/

    1. Maintain K pointers (one for each array) [initially pointing to 0]
    2. Find min and max of elements pointed by those K pointers.
    3. Update answer
    4. since we want better answer for MAX-MIN+1 we need to increment min value or decrease max value
        we can't decrease max value since it is sorted in increasing order and we can go only forward
        so increment the pointer which points to min value
        if it any point we exhausted one list that means we can stop since then we can only increment max values
        we cant min value anymore

    Normal approach => O(K) for finding min and max and we traverse every element so O(NK * K) T | O(K) S for storing K pointers
    
    optimized =>
        since we always need min value we can use min heap
        O(1) for finding min and maintain a separate max variable and keep updating it whenever we increment min value and push it to queue
        O(logK) for popping and pushing
        O(NK * logK) since we do those operators for atmost every element
*/

struct MinHeapNode {
    int element;       //element based on which ordering is done
    int arrayIndex;    //index of the array to which this element belongs
    int elementIndex;  //index of the element in the array

    MinHeapNode(int element, int arrayIndex, int elementIndex) : element(element), arrayIndex(arrayIndex), elementIndex(elementIndex) {}
};

struct CompareElement {
    //return true if b should come before a
    bool operator()(const MinHeapNode &a, const MinHeapNode &b) {
        return b.element <= a.element;
    }
};

void findSmallestRange(int arr[][N], int n, int K) {
    priority_queue<MinHeapNode, vector<MinHeapNode>, CompareElement> minHeap;
    //O(NlogK)
    int maxElement = INT_MIN;
    for (int i = 0; i < K; ++i) {
        minHeap.push(MinHeapNode(arr[i][0], i, 0));
        maxElement = max(maxElement, arr[i][0]);
    }
    //O(NK * logK). NK because we will traverse all the elements
    bool stop = false;
    int rangeL = -1, rangeR = -1;
    while (!stop) {
        MinHeapNode minElement = minHeap.top();  //O(logK)
        minHeap.pop();                           //O(logK)
        if (rangeR == -1 || maxElement - minElement.element < rangeR - rangeL) {
            rangeL = minElement.element;
            rangeR = maxElement;
        }

        //if exhausted one list then we can stop
        if (minElement.elementIndex == n - 1) {
            stop = true;
        } else {
            int newArrayIndex = minElement.arrayIndex;
            int newElementIndex = minElement.elementIndex + 1;
            int newElement = arr[newArrayIndex][newElementIndex];
            if (newElement > maxElement) {
                maxElement = newElement;
            }

            minHeap.push(MinHeapNode(newElement, newArrayIndex, newElementIndex));
        }
    }
    cout << rangeL << " " << rangeR << endl;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n, k;
        cin >> n >> k;
        int arr[N][N];
        for (int i = 0; i < k; i++)
            for (int j = 0; j < n; j++)
                cin >> arr[i][j];
        findSmallestRange(arr, n, k);
    }
    return 0;
}