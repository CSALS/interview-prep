#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/n-max-pair-combinations/
 * a[]={1,4,2,3}
 * b[]={2,5,1,6}
 * 4 3 2 1
 * 6 5 2 1
 * Maximum 4 elements of combinations sum are
 * 10   (4+6), 9    (3+6), 9    (4+5), 8    (2+6)
 * Using set for ordering by sum of pairs and also to eliminate duplicates.
 * For every {L, R} insert {L - 1, R} and {L, R - 1} in the set
 * You can just the run the for loop for K times to get K max combinations.
 */

//O(NlogN)
vector<int> solve(vector<int> &A, vector<int> &B) {
    sort(A.begin(), A.end());
    sort(B.begin(), B.end());
    set<pair<int, pair<int, int>>> s;  //use set instead of priority_queue to remove duplicates
    int n = A.size();
    s.insert({A[n - 1] + B[n - 1], {n - 1, n - 1}});
    vector<int> result;
    for (int i = 0; i < n; ++i) {
        auto it = prev(s.end());
        auto p = *it;
        s.erase(it);
        result.push_back(p.first);
        s.insert({A[p.second.first - 1] + B[p.second.second], {p.second.first - 1, p.second.second}});
        s.insert({A[p.second.first] + B[p.second.second - 1], {p.second.first, p.second.second - 1}});
    }
    return result;
}
