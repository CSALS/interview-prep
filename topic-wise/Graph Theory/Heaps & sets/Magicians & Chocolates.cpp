#include <bits/stdc++.h>
using namespace std;

/**
 * Given bag of chocolates. Choose a random arr[i] and consume it and replace it with arr[i]/2
 * In 'D' days how much maximum chocolates you can consume?
 */

int nchoc(int days, vector<int> &arr) {
    const int mod = 1e9 + 7;
    priority_queue<int> maxHeap;
    for (int num : arr) {
        maxHeap.push(num);
    }
    long long sum = 0;
    while (days--) {
        sum += maxHeap.top();
        sum %= mod;
        maxHeap.push(maxHeap.top() / 2);
        maxHeap.pop();
    }
    return sum;
}
