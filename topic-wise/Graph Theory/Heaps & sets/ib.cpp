/*
      @author: Charan Sai
*/
#include <bits/stdc++.h>
using namespace std;
#define IOS                           \
    ios_base::sync_with_stdio(false); \
    cin.tie(0);                       \
    cout.tie(0)
#define int long long
#define endl "\n"
#define deb(x) cout << #x << " " << x << endl;

#define element pair<int, char>
string getString(char c) {
    string s(1, c);
    return s;
}
int solution() {
    string str;
    cin >> str;
    //
    unordered_map<char, int> freq;
    for (char c : str) freq[c]++;

    priority_queue<element> maxHeap;
    for (auto it : freq) {
        maxHeap.push({it.second, it.first});
    }

    string result = "";
    while (!maxHeap.empty()) {
        element one = maxHeap.top();
        maxHeap.pop();
        if (one.first != 1 && maxHeap.empty()) break;
        if (one.first == 1 && maxHeap.empty()) {
            result = result + getString(one.second);
            break;
        }
        element two = maxHeap.top();
        maxHeap.pop();
        result = result + getString(one.second);
        result = result + getString(two.second);
        one.first--;
        two.first--;
        if (one.first != 0) maxHeap.push(one);
        if (two.first != 0) maxHeap.push(two);
    }
    cout << result << endl;
    return (str.length() == result.length());
}
int32_t main() {
    IOS;
    int t = 1;
    cin >> t;
    while (t--) {
        // solution();
        cout << solution() << endl;
    }
    return 0;
}