#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/find-k-closest-elements/
 * Given a sorted array arr, two integers k and x, find the k closest elements to x in the array. 
 * The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.
    Example 1:

    Input: arr = [1,2,3,4,5], k = 4, x = 3
    Output: [1,2,3,4]

    Example 2:

    Input: arr = [1,2,3,4,5], k = 4, x = -1
    Output: [1,2,3,4]

 * Find absolute difference of each element with the given and then use K smallest numbers logic
 * Time O(NlogK) | Space O(K)
 * Heap approach works for any given array.
 * But it is mentioned that array is sorted but we are not using it. So think of binary search
 * TODO: do using BS (https://leetcode.com/problems/find-k-closest-elements/discuss/106426/JavaC%2B%2BPython-Binary-Search-O(log(N-K)-%2B-K))
 */

vector<int> KClosestNumbers(vector<int> arr, int K, int value) {
    if (K > arr.size()) {
        cout << "K should be less than number of elements\n";
        return {};
    }
    if (K == arr.size()) {
        //If we can return in any order the result
        return arr;
    }
    vector<int> result;                      //stores k smallest elements
    priority_queue<pair<int, int>> maxHeap;  // {absolute difference, original value}
    for (int i = 0; i < K; ++i) {
        maxHeap.push({abs(arr[i] - value), arr[i]});
    }

    for (int i = K; i < arr.size(); ++i) {
        if (abs(arr[i] - value) < maxHeap.top().first) {
            maxHeap.push({abs(arr[i] - value), arr[i]});
            maxHeap.pop();
        }
    }

    while (!maxHeap.empty()) {
        result.push_back(maxHeap.top().second);
        maxHeap.pop();
    }
    reverse(result.begin(), result.end());
    return result;
}