#include <bits/stdc++.h>
using namespace std;

/**
 * Maintain heap of K+1 elements
 * For index 0 answer will be at top. Pop it & push arr[K+1] and continue
 * Time O(NlogK) | Space O(K)
 */

vector<int> sort(vector<int> arr, int K) {
    if (K >= arr.size()) {
        cout << "K should be less than array size\n";
        return {};
    }
    priority_queue<int, vector<int>, greater<int>> minHeap;
    for (int i = 0; i <= K; ++i) {
        minHeap.push(arr[i]);
    }
    vector<int> result(arr.size());
    int index = 0;
    for (int i = K + 1; i < arr.size(); ++i) {
        result[index++] = minHeap.top();
        minHeap.pop();
        minHeap.push(arr[i]);
    }
    while (!minHeap.empty()) {
        result[index++] = minHeap.top();
        minHeap.pop();
    }
    return result;
}