#include <bits/stdc++.h>
using namespace std;

//The most efficient solution is to use QUICK SELECT ALGORITHM not heaps
//we can use binary search O(NlogN) but O(1) Space solution

//K smallest elements/K largest elements in an infinite sequence => heap only

/**
 * For smallest you are using max heap
 * std::priority_queueTime O(NlogK) | Space O(k)
 * Using custom heap => O(N) constructing min heap + O(KlogN) for extracting top K elements
 * In case of k = N just find the maximum element (linear time)
 */

int KthSmallest(vector<int> arr, int K) {
    if (K > arr.size()) {
        cout << "K should be less than size of array\n";
        return -1;
    }
    if (K == arr.size()) {
        int maxEle = INT_MIN;
        for (int num : arr) maxEle = max(maxEle, num);
        return maxEle;
    }
    priority_queue<int> maxHeap;

    //1. Keep pushing all elements and pop when size > K. O(NlogK)
    for (int i = 0; i < arr.size(); ++i) {
        maxHeap.push(arr[i]);
        if (maxHeap.size() > K)
            maxHeap.pop();
    }
    //Heap contains Kth Smallest, K-1th, K-2th ....
    return maxHeap.top();

    //2. Push first K elements. Then for the rest elements push that element if it is less than top. O(K + (N - K)logK)
    for (int i = 0; i < K; ++i) {
        maxHeap.push(arr[i]);
    }
    for (int i = K; i < arr.size(); ++i) {
        if (arr[i] < maxHeap.top()) {
            maxHeap.pop();
            maxHeap.push(arr[i]);
        }
    }
    return maxHeap.top();
}

/**
 * For largest element use min heap
 * 
 */

int KthLargest(vector<int> arr, int K) {
    if (K > arr.size()) {
        cout << "K should be less than size of array\n";
        return -1;
    }
    if (K == arr.size()) {
        int minEle = INT_MAX;
        for (int num : arr) minEle = min(minEle, num);
        return minEle;
    }
    priority_queue<int, vector<int>, greater<int>> minHeap;

    //1. Keep pushing all elements and pop when size > K. O(NlogK)
    for (int i = 0; i < arr.size(); ++i) {
        minHeap.push(arr[i]);
        if (minHeap.size() > K)
            minHeap.pop();
    }
    //Heap contains Kth largest, K-1th, K-2th ....
    return minHeap.top();

    //2. Push first K elements. Then for the rest elements push that element if it is less than top. O(K + (N - K)logK)
    for (int i = 0; i < K; ++i) {
        minHeap.push(arr[i]);
    }
    for (int i = K; i < arr.size(); ++i) {
        if (arr[i] > minHeap.top()) {
            minHeap.pop();
            minHeap.push(arr[i]);
        }
    }
    return minHeap.top();
}

int main() {
    // cout << KthSmallest({2, 3, 1, 5, 4}, 1) << endl;  //1
    // cout << KthSmallest({2, 3, 1, 5, 4}, 2) << endl;  //2
    // cout << KthSmallest({2, 3, 1, 5, 4}, 3) << endl;  //3
    // cout << KthSmallest({2, 3, 1, 5, 4}, 4) << endl;  //4
    // cout << KthSmallest({2, 3, 1, 5, 4}, 5) << endl;  //5

    cout << KthLargest({2, 3, 1, 5, 4}, 1) << endl;  //5
    cout << KthLargest({2, 3, 1, 5, 4}, 2) << endl;  //4
    cout << KthLargest({2, 3, 1, 5, 4}, 3) << endl;  //3
    cout << KthLargest({2, 3, 1, 5, 4}, 4) << endl;  //2
    cout << KthLargest({2, 3, 1, 5, 4}, 5) << endl;  //1
}