#include <bits/stdc++.h>
using namespace std;

void print(vector<int> arr) {
    for (int num : arr) cout << num << " ";
    cout << endl;
}
/**
 * https://www.geeksforgeeks.org/sort-elements-by-frequency/
 * Sort elements by decreasing frequencey
 * If two elements have same frequency then print which one comes first.
 * Lot of approaches. Check GFG.
 * Time O(N + MlogM) | Space O(M)
 * If two elements having same frequency the smaller element should come first then directly use (-1 * num) as second in pair of heap no need of index
 */

void frequencySort(vector<int> arr) {
    if (arr.empty())
        return;
    unordered_map<int, int> freqMap;   // num -> freq
    unordered_map<int, int> indexMap;  //num -> firstIndex
    //O(N)
    int N = arr.size();
    for (int i = 0; i < N; ++i) {
        int num = arr[i];
        freqMap[num]++;
        if (indexMap.find(num) == indexMap.end()) {
            indexMap[num] = i;
        }
    }

    //O(MlogM) where M is distinct elements
    priority_queue<pair<int, int>> maxHeap;
    for (auto it : freqMap) {
        int num = it.first;
        int freq = it.second;
        int firstIndex = indexMap[num];
        //storing N - firstIndex because if freq is same then we need lesser index to come first so to avoid custom comparator you can use this
        maxHeap.push({freq, N - firstIndex});
    }

    vector<int> result;
    //O(N + MlogM)
    while (!maxHeap.empty()) {
        int index = N - maxHeap.top().second;
        int num = arr[index];
        int freq = maxHeap.top().first;
        while (freq--) {
            result.push_back(num);
        }
        maxHeap.pop();
    }

    print(result);
}

int main() {
    frequencySort({2, 5, 2, 8, 5, 6, 8, 8});               // [8, 8, 8, 2, 2, 5, 5, 6]
    frequencySort({2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8});  // [8, 8, 8, 2, 2, 5, 5, 6, -1, 9999999]
}