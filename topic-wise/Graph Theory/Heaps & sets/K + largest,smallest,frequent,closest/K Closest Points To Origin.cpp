#include <bits/stdc++.h>
using namespace std;

#define PAIR pair<int, int>
class Solution {
   public:
    //O(NlogK) Time | O(K) Space
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        priority_queue<PAIR> maxHeap;
        //O(NlogK)
        for (int i = 0; i < points.size(); ++i) {
            vector<int> point = points[i];
            int dist = point[0] * point[0] + point[1] * point[1];
            maxHeap.push({dist, i});
            if (maxHeap.size() > K)
                maxHeap.pop();
        }
        //O(KlogK)
        vector<vector<int>> result;
        while (!maxHeap.empty()) {
            result.push_back(points[maxHeap.top().second]);
            maxHeap.pop();
        }
        return result;
    }
};