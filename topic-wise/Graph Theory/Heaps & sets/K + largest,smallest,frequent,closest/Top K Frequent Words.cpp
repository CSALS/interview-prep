#include <bits/stdc++.h>
using namespace std;

/**
 * If having same frequency then print the lexicographic smallest word.
 */

#define PAIR pair<string, int>

//1. Using custom comparator in heap O(N + MlogM + KlogM) Time
struct Compare {
    bool operator()(const PAIR &a, const PAIR &b) {
        //return true if b should come before a
        if (b.second > a.second)
            return true;
        if (b.second < a.second)
            return false;
        return (b.first < a.first);
    }
};
vector<string> topKFrequent1(vector<string> &words, int K) {
    //O(N)
    unordered_map<string, int> freqMap;
    for (string word : words) {
        freqMap[word]++;
    }

    //O(MlogM) where M is distinct words
    priority_queue<PAIR, vector<PAIR>, Compare> maxHeap(freqMap.begin(), freqMap.end());

    //O(KlogM)
    vector<string> result;
    while (!maxHeap.empty() && result.size() < K) {
        string word = maxHeap.top().first;
        int freq = maxHeap.top().second;
        result.push_back(word);
        maxHeap.pop();
    }
    return result;
}

//2. No custom comparator in heap O(N + MlogM + KlogM) Time
#define PAIR pair<int, string>
vector<string> topKFrequent2(vector<string> &words, int K) {
    //O(N)
    unordered_map<string, int> freqMap;
    for (string word : words) {
        freqMap[word]++;
    }

    //O(MlogM) where M is distinct words
    priority_queue<PAIR, vector<PAIR>, greater<PAIR>> maxHeap;
    for (auto it : freqMap) {
        maxHeap.push({-1 * it.second, it.first});
    }

    //O(KlogM)
    vector<string> result;
    while (!maxHeap.empty() && result.size() < K) {
        string word = maxHeap.top().second;
        int freq = -1 * maxHeap.top().first;
        result.push_back(word);
        maxHeap.pop();
    }
    return result;
}

//3. Most efficient. Since you need K largest use min heap of size K
// O(N + MlogK + KlogK) Time
#define PAIR pair<string, int>

struct Compare {
    bool operator()(const PAIR &a, const PAIR &b) {
        //return true if b should come before a
        if (b.second > a.second)
            return false;
        if (b.second < a.second)
            return true;
        return (b.first > a.first);
    }
};

vector<string> topKFrequent3(vector<string> &words, int K) {
    //O(N)
    unordered_map<string, int> freqMap;
    for (string word : words) {
        freqMap[word]++;
    }

    //O(MlogK) where M is distinct words
    priority_queue<PAIR, vector<PAIR>, Compare> minHeap;
    for (auto it : freqMap) {
        minHeap.push({it.first, it.second});
        if (minHeap.size() > K)
            minHeap.pop();
    }

    //O(KlogK)
    vector<string> result;
    while (!minHeap.empty()) {
        string word = minHeap.top().first;
        int freq = minHeap.top().second;
        result.push_back(word);
        minHeap.pop();
    }
    reverse(result.begin(), result.end());
    return result;
}