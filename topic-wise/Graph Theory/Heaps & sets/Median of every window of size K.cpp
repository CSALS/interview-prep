#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/sliding-window-median/

    1. Running median. There we used two heaps since we only need to add elements
    2. Here we need to remove element also but heaps doesn't support remove so we use sets
*/
class Solution {
   public:
    vector<double> medianSlidingWindow(vector<int> &array, int K) {
        if (array.empty()) return {};

        vector<double> medians;
        multiset<int> leftHalf, rightHalf;

        for (int i = 0; i < array.size(); ++i) {
            //remove array[i - K]
            if (i - K >= 0) {
                int removeElement = array[i - K];
                if (leftHalf.find(removeElement) != leftHalf.end()) {
                    leftHalf.erase(leftHalf.find(removeElement));
                } else {
                    rightHalf.erase(rightHalf.find(removeElement));
                }
                balance(leftHalf, rightHalf);
            }

            //insert array[i]
            if (leftHalf.empty() || array[i] <= *(leftHalf.rbegin())) {
                leftHalf.insert(array[i]);
            } else {
                rightHalf.insert(array[i]);
            }
            balance(leftHalf, rightHalf);

            if (i >= K - 1) {
                //add median
                medians.push_back(getMedian(leftHalf, rightHalf, K));
            }
        }
        // medians.push_back(getMedian(leftHalf, rightHalf, K));
        return medians;
    }

    void balance(multiset<int> &leftHalf, multiset<int> &rightHalf) {
        if (leftHalf.size() > rightHalf.size() + 1) {
            auto it = (leftHalf.end());
            it--;
            int middle = *it;
            leftHalf.erase(it);
            rightHalf.insert(middle);
        } else if (rightHalf.size() > leftHalf.size() + 1) {
            auto it = (rightHalf.begin());
            int middle = *it;
            rightHalf.erase(it);
            leftHalf.insert(middle);
        }
    }

    double getMedian(multiset<int> &leftHalf, multiset<int> &rightHalf, int K) {
        double middle1 = *(leftHalf.rbegin());
        double middle2 = *(rightHalf.begin());
        if (K & 1) {
            if (leftHalf.size() > rightHalf.size())
                return middle1;
            else
                return middle2;
        } else {
            return (middle1 + middle2) / 2;
        }
    }
};