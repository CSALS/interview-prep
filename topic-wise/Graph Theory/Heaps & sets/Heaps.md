# Identification =
2. K + Smallest/Largest/Large/Frequent/Closest/Lowest
3. K + Top/Largest = MIN HEAP
4. K + Smallest = MAX HEAP
5. Heap size will be K
6. K + sort array = Heap
7. If instead of sorting all 'N' elements we can get result by sorting 'K' 
8. Freq + Sort = Heap 
9. NlogK = heap
Sorting Based = NlogN
Heap Based = NlogK.

```
min-heap and set are used interchangeably
set doesnt allow duplicates but priority-queue does
but set can remove any element in O(logN) but priority-queue doesn't support delete of any ele except top
```

# Priority Queue (STL) =

1. Max Heap : priority_queue<type> max_heap;
2. Min Heap : priority_queue<type, vector<type>, greater<type>> min_heap;
    1. If pair is used as type the sorting is done based on pair.first and if equal then done on pair.second
    2. Using struct/class objects then we will get error
    Custom comparators =>
    struct Person {
        int age; float height;
    };
    struct CompareHeight {
        bool operator()(Person const& p1, Person const& p2)
        {
            //True if p2 is ordered before p1 (READ THISSS. TRUE if p2 should come before p1)
            return p1.height >= p2.height;
        }
    };
    priority_queue<Person, vector<Person>, CompareHeight> pq;
3. Populating heap
priority_queue<int> pq(arr.begin(), arr.end());


# SET (STL) =

- custom comparator
struct compare {
    bool compare()(int a, int b) {
        //return true if a should come before b
    }
}
set<int, compare> s;

- s.begin() => iterator to first element
- s.rebegin() => iterator to last element


- set vs heap => https://stackoverflow.com/questions/10141841/difference-between-stdset-and-stdpriority-queue

# Problems = 

- For the below questions if sorting is used then we get NlogN. But using heap we get NlogK.
- To ensure NlogK is always less than NlogN, solve separately for the case K = N. Keep that as base case. Sometimes the array itself is the answer
- **Some problems can be solved using QUICK SELECT which gives linear time on average**

0. Mostly will be used in greedy problems. 

1. Merge K Sorted Lists
2. Merge K Sorted Arrays

3. https://www.interviewbit.com/problems/magician-and-chocolates/
4. https://www.interviewbit.com/problems/n-max-pair-combinations/


5. Kth Smallest/Largest and First K large/small elements [Most efficient is not using heaps but rather using QUICK SELECT algorithm]
6. Sort K-Sorted Array/Nearly-Sorted Array

7. K Closest Numbers To Given Value
- Max Heap({abs(arr[i] - value), arr[i]})
8. Top K Frequent Numbers
- Construct FreqMap first
- Min Heap({Frequency, Actual Value})
9. Frequency Sort (Array/String)
M is distinct elements
- std::priority_queue: O(N) freq_map + O(MlogM) constructing max heap {freq, value} + O(N + MlogM) extracting elements
- In case of strings max possible characters are 256. (26 if only lowercase). So it will be O(N) + O(N + 256*log256) => O(N)
If you want to preserve order of elements then store index in heap.
10. Top K frequent words. If same frequency then which ever is lexicographically smaller print it.
11. K Closest Points To Origin