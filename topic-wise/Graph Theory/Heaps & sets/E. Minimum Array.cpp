//https://codeforces.com/problemset/problem/1157/E

#include <bits/stdc++.h>
using namespace std;

#define int long long
#define deb(x) cout << #x << " " << x << endl;

void solution() {
    int n;
    cin >> n;
    vector<int> a(n), b(n);
    multiset<int> ms;
    for (int &x : a) cin >> x;
    for (int &x : b) {
        cin >> x;
        ms.insert(x);
    }
    //
    vector<int> c(n);
    //for every ai we will use first element >= n-ai if found else we will use the min present
    for (int i = 0; i < n; ++i) {
        auto it = ms.lower_bound(n - a[i]);
        //let's take min element if not found
        if (it == ms.end()) it = ms.begin();
        c[i] = (a[i] + *it) % n;
        ms.erase(it);
        cout << c[i] << " ";
    }
    cout << endl;
}

int32_t main() {
    int t = 1;
    // cin >> t;
    while (t--) {
        solution();
        // cout << solution() << endl;
    }
    return 0;
}