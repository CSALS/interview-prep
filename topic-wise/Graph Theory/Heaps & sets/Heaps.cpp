#include <bits/stdc++.h>
using namespace std;

/**
 * Heap is complete binary tree. All levels except last are filled completely and last level all nodes are towards left.
 * Max Heap => root key >= all its children and this is true for all nodes
 * Min Heap => root key <= all its children and this is true for all nodes
 * Represented using array (Basically the level order traversal)
 * parent (i), left child (2i + 1), right child (2i + 2), parent[i] = (i - 1)/2
 * for leafs => i >= N/2
 */

class MaxHeap {
   private:
    vector<int> heap;  //Basically the level order traversal
    int heapSize;

    int parent(int i) {
        return (i - 1) / 2;
    }
    int left(int i) {
        return 2 * i + 1;
    }
    int right(int i) {
        return 2 * i + 2;
    }
    void swap(int i, int j) {
        int temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }

   public:
    MaxHeap(vector<int> arr) {
        heap = arr;
        heapSize = heap.size();
    }
    void insert(int data);      //O(H)
    int getMaximum();           //O(1)
    void deleteTop();           //O(H)
    void constructHeap();       //O(N)
    void MaxHeapify(int root);  //O(H) height of subtree at root
    void printHeap();           //O(N)

    void updateValue1(int index, int newData);  //O(H)
    void updateValue2(int data, int newData);   //O(H) if we have a map of data to indexes. Else it will be O(N) since we need to first get the index
};

void MaxHeap::printHeap() {
    for (int i = 0; i < heapSize; ++i) {
        cout << heap[i] << " ";
    }
    cout << endl;
}

//Insert element at the end and reverse-heapify
void MaxHeap::insert(int data) {
    //Increase size of heap and data to the end of array
    heapSize++;
    heap.push_back(data);

    //Reverse-Heapify
    int index = heapSize - 1;
    while (index > 0 && heap[index] >= heap[parent(index)]) {
        swap(index, parent(index));
        index = parent(index);
    }
}

//In heap we can only delete top element
void MaxHeap::deleteTop() {
    if (heapSize == 1) {
        return;
    }
    if (heapSize == 0) {
        heap = {};
        return;
    }
    //Replace last element on the top
    swap(0, heapSize - 1);
    heapSize--;
    //Recursively compare the node with its children and swap with max. children => called Heapify
    MaxHeapify(0);
}

//Most important function.MaxHeapify compares the root with children and replace with max. child and recursively calls for that child
//O(logN)/O(Height) time
void MaxHeap::MaxHeapify(int root) {
    //If Lead Node skip it
    if (root >= heapSize / 2)
        return;

    int leftIndex = left(root);
    int rightIndex = right(root);
    int largestIndex = root;
    if (leftIndex < heapSize && heap[leftIndex] > heap[largestIndex]) {
        largestIndex = leftIndex;
    }
    if (rightIndex < heapSize && heap[rightIndex] > heap[largestIndex]) {
        largestIndex = rightIndex;
    }
    if (largestIndex != root) {
        swap(largestIndex, root);
        MaxHeapify(largestIndex);
    }
}

int MaxHeap::getMaximum() {
    if (heap.empty()) {
        return -1;
    }
    return heap[0];
}
//Given some random order in the heap. Make it a max heap. O(N)
void MaxHeap::constructHeap() {
    //Do MaxHeapify from last non-leaf node which is actually the parent of last node
    int lastNonLeafNode = heapSize / 2 - 1;
    for (int i = lastNonLeafNode; i >= 0; --i) {
        MaxHeapify(i);
    }
}

/////////

//This update only if the updated value is more. THat's why reverse-heapify

//Update the value directly and then reverse-heapify
void MaxHeap::updateValue1(int index, int newData) {
    heap[index] = newData;
    //Reverse-Heapify
    while (index != 0 && heap[index] > heap[parent(index)]) {
        swap(index, parent(index));
        index = parent(index);
    }
}

/**
 * If heap has unique values then maintain mappings for data to index
 * In this way in O(1) you can get index of data and then use updateValue1(index, newData)
*/
unordered_map<int, int> dataToIndex;
void MaxHeap::updateValue2(int data, int newData) {
    updateValue1(dataToIndex[data], newData);
}

int main() {
    vector<int> arr = {5, 4, 2, 1, 3, 0, 1};
    sort(arr.begin(), arr.end());
    MaxHeap heap(arr);
    heap.constructHeap();
    heap.printHeap();
    heap.updateValue1(2, 6);
    heap.printHeap();
}