#include <bits/stdc++.h>
using namespace std;
#define element pair<int, char>

//https://leetcode.com/problems/reorganize-string
class Solution {
   public:
    string reorganizeString(string str) {
        unordered_map<char, int> freq;
        for (char c : str) freq[c]++;

        priority_queue<element> maxHeap;
        for (auto it : freq) {
            maxHeap.push({it.second, it.first});
        }

        string result = "";
        while (!maxHeap.empty()) {
            element one = maxHeap.top();
            maxHeap.pop();
            if (one.first != 1 && maxHeap.empty()) break;
            if (one.first == 1 && maxHeap.empty()) {
                result = result + (one.second);
                break;
            }
            element two = maxHeap.top();
            maxHeap.pop();
            result = result + (one.second);
            result = result + (two.second);
            one.first--;
            two.first--;
            if (one.first != 0) maxHeap.push(one);
            if (two.first != 0) maxHeap.push(two);
        }
        if (str.length() == result.length())
            return result;
        else
            return "";
    }
};