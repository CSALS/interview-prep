#include <bits/stdc++.h>
using namespace std;

/*
    If we are at a node u and we have an edge from u to v but v has been entered but
    not exited (that means it is currently in recursion stack) which means u->v is
    back-edge and cycle is present

    Undirected graph method doesn't work.
    0 <-- 1 --> 2
    first we visit 0.
    Then at 1 we find that there is edge to 0 which is not parent of 1 so we print cycle exists
    but there is no cycle
*/

//recSt[u] = true then u is currently present in recursion stack
bool dfs(int u, vector<int> adj[], vector<bool> &vis, vector<bool> &recSt) {
    // u is entered
    vis[u] = recSt[u] = true;

    for (int v : adj[u]) {
        if (!vis[v] && dfs(v, adj, vis, recSt))
            return true;
        else if (recSt[v] == true)  // back edge present
            return true;
    }
    // u is exited
    recSt[u] = false;
    return false;
}