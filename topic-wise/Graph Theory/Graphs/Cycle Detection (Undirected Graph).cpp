// Cycle Detection in an undirected graph. (BFS, DFS)

#include <bits/stdc++.h>
using namespace std;

const int INF = INT_MAX;

// DFS
bool dfs(int src, vector<int> adj[], vector<bool> &visited, int parent) {
    // cout << parent << " " << src << endl;
    visited[src] = true;
    for (int child : adj[src]) {
        if (visited[child] == false) {
            if (dfs(child, adj, visited, src)) return true;
        } else if (child != parent) {
            return true;
        }
    }
    return false;
}

int main() {
    int V, E;
    cin >> V >> E;
    int edge_ct = E;
    vector<int> adj[V];
    while (edge_ct--) {
        int a, b;
        cin >> a >> b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    vector<bool> visited(V, false);
    bool flag = true;
    for (int i = 0; i < V; ++i) {
        if (visited[i] == false) {
            if (dfs(i, adj, visited, -1)) {
                cout << "YES" << endl;
                flag = false;
                break;
            }
        }
    }
    if (flag) cout << "NO" << endl;
}

// BFS
bool bfs(int src, vector<int> graph[], vector<int> &vis, vector<int> &par) {
    vis[src] = true;
    par[src] = -1;
    queue<int> q;
    q.push(src);

    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v : graph[u]) {
            if (!vis[v]) {
                vis[v] = true;
                par[v] = u;
                q.push(v);
            } else if (v != par[u]) {
                return true;
            }
        }
    }
    return false;
}

bool isCyclic(vector<int> graph[], int V) {
    vector<int> vis(V, false);
    vector<int> par(V, false);
    for (int i = 0; i < V; ++i) {
        if (!vis[i]) {
            if (!bfs(i, graph, vis, par)) continue;
            return true;
        }
    }
    return false;
}