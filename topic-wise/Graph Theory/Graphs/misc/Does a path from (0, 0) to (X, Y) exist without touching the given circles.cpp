#include <bits/stdc++.h>
using namespace std;

#define Cell pair<int, int>

//https://www.interviewbit.com/problems/valid-path/
class Solution {
   public:
    /**
     * We will do BFS with each node having 8 childs in the possible directions
     * While generating children we need to check if they are present inside any circle.
     * If (X - xc)^2 + (Y - yc)^2 <= R then (X, Y) is present inside circle with center at (xc, yc)
     * @param (destX, destY) are the destination point we need to reach from (0, 0)
     * @param circlesCount is the number of circles whose center is present inside rectangle
     * @param circlesRadius is the radius of all the circles
     * @param (centersX, centersY) are the list of centers of the circles
     * @return YES if it is possible to reach destination from origin, else NO
     */
    string solve(int destX,
                 int destY,
                 int circlesCount,
                 int circlesRadius,
                 vector<int> &centersX,
                 vector<int> &centersY) {
        Cell destination = {destX, destY};
        queue<Cell> q;
        q.push({0, 0});
        vector<vector<bool>> visited(destX + 1, vector<bool>(destY + 1, false));
        visited[0][0] = true;

        vector<int> rowOffset = {0, 0, 1, -1, 1, 1, -1, -1};
        vector<int> colOffset = {1, -1, 0, 0, 1, -1, 1, -1};

        while (!q.empty()) {
            Cell current = q.front();
            q.pop();

            if (current == destination) {
                return "YES";
            }

            int size = rowOffset.size();
            for (int k = 0; k < size; ++k) {
                Cell neighbour = {current.first + rowOffset[k],
                                  current.second + colOffset[k]};

                if (isValid(neighbour, destination, centersX, centersY, circlesRadius) && !visited[neighbour.first][neighbour.second]) {
                    visited[neighbour.first][neighbour.second] = true;
                    q.push(neighbour);
                }
            }
        }
        return "NO";
    }

    /**
     * Checks if the given neighbour cell is valid.
     * It is valid if it doesn't go out of bounds from the rectangle and also not present inside any circles
     * since then the path will touch circle
     * @param neighbour is the cell we are checking validity for
     */
    bool isValid(Cell neighbour,
                 Cell destination,
                 vector<int> &centersX,
                 vector<int> &centersY,
                 int radius) {
        if (neighbour.first < 0 || neighbour.second < 0 || neighbour.first > destination.first ||
            neighbour.second > destination.second) {
            return false;
        }

        for (int i = 0; i < centersX.size(); ++i) {
            int centerX = centersX[i];
            int centerY = centersY[i];

            int value = (neighbour.first - centerX) * (neighbour.first - centerX) + (neighbour.second - centerY) * (neighbour.second - centerY);

            // neighbor is inside this circle
            if (value <= radius * radius) {
                return false;
            }
        }
        return true;
    }
};