#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/check-if-a-tree-can-be-split-into-k-equal-connected-components/?ref=leftbar-rightbar

K equal connected components (all should contain same number of nodes)

We need to check if we can form K components of each size N/K
Bottom-up recursion

at a node we recieve all avilable nodes from children.
if avilalbe is 0 that means that child has been choose in one component
total sum == N/K then we just formed one more component so send 0 to parent
total sum > N/K then it is not possible send 0 to parent
total sum < N/K send it to parent 

keep count of components taken. if it is == K then YES else NO
*/

int DFS(int u, int par, vector<int> adj[], int targetNodes, int &count) {
    int currentNodes = 1;
    for (int v : adj[u]) {
        // if (v != par) //if it was undirected tree then use par to prevent from infinite looping
        currentNodes += DFS(v, u, adj, targetNodes, count);
    }

    if (currentNodes == targetNodes) {
        count++;
        return 0;  //returning 0 to parent so that it won't choose this components nodes further
    }

    else if (currentNodes < targetNodes) {
        return currentNodes;  //hopefully this comp. nodes might be choosen by parent
    }

    else if (currentNodes > targetNodes) {
        return 0;  //obv. can't split the tree
    }
}

bool canSplitTreeIntoKEqualConnectedComponents(int N, vector<int> adj[], int K) {
    if (N % K != 0) {
        return false;
    }

    int count = 0;                  //number of connected components formed with N/K nodes in it
    DFS(1, -1, adj, N / K, count);  //1 is root
    return (count == K);
}

void addEdge(vector<int> adj[], int u, int v) {
    adj[u].push_back(v);
    // adj[v].push_back(u);
}

int main() {
    int N = 15, K = 5;
    vector<int> adj[N + 1];
    addEdge(adj, 1, 2);
    addEdge(adj, 2, 3);
    addEdge(adj, 2, 4);
    addEdge(adj, 4, 5);
    addEdge(adj, 5, 6);
    addEdge(adj, 5, 7);
    addEdge(adj, 4, 8);
    addEdge(adj, 4, 9);
    addEdge(adj, 8, 11);
    addEdge(adj, 11, 10);
    addEdge(adj, 11, 14);
    addEdge(adj, 9, 12);
    addEdge(adj, 12, 15);
    addEdge(adj, 12, 13);
    cout << canSplitTreeIntoKEqualConnectedComponents(N, adj, K) << endl;  //true
}