#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/stepping-numbers/

Given A and B you have to find all stepping numbers in range A to B.

The stepping number:

A number is called as a stepping number if the adjacent digits have a difference of 1.

e.g. 123 is stepping number, but 358 is not a stepping number
*/
vector<int> stepnum(int N, int M) {
    queue<int> q;
    for (int i = 0; i <= 9; ++i) {
        q.push(i);
    }

    vector<int> result;
    while (!q.empty()) {
        int node = q.front();
        q.pop();
        if (node >= N && node <= M) {
            result.push_back(node);
        }
        if (node > M) break;

        //generate the two child. 3 => 32, 34
        if (node % 10 != 0)
            q.push(node * 10 + (node % 10 - 1));
        if (node % 10 != 9)
            q.push(node * 10 + (node % 10 + 1));
    }
    return result;
}

/*
https://www.interviewbit.com/problems/smallest-multiple-with-0-and-1/ (https://www.spoj.com/problems/ONEZERO/)

You are given an integer N. You have to find smallest multiple of N which consists of digits 0 and 1 only. 
Since this multiple could be large, return it in form of a string.

For N = 55, 110 is smallest multiple consisting of digits 0 and 1.
For N = 2, 10 is the answer.

    ALGO =>
        Since multiple can only have 1s or 0s, construct it incremently till you get multiple
        
        1 -> 10, 11 -> 100, 101, 110, 111 ....

        This is like bfs. When you get first multiple which is divisible by N then print it

        But since strings can be really huge storing entire strings in the queue isn't optimal

        So we will store remainder of that node when divided by N
    
        but how to construct child?

        parent_node = q * N + rem
        parent_node * 10 = q * 10 * N + (rem * 10) % N
        parent_node * 10 + 1 = q * 10 + (rem * 10 + 1) % N
        parent_node * 10 + 0 = q * 10 + (rem * 10) % N

        so new remainder is (10*rem)%N and (10*rem + 1)%N

        but finally how to construct the multiple?

        for every node => keep track of its parent remainder node and what digit (0 or 1) we used to construct it
*/
string multiple(int N) {
    if (N == 0) return "0";
    if (N == 1) return "1";
    queue<int> q;
    q.push(1);
    /*
        since we are using only remainders instead of unordered_map we can use vector(N) 
        since remainder lies b/w 0 and N-1. getting TLE on interviewbit if using unordereed_map
    */
    // unordered_set<int> visited;
    // unordered_map<int, int> parent;
    // unordered_map<int, int> digit;  //digit[x] is digit (0 or 1) used to construct it from its parent parent[x]
    vector<bool> visited(N, false);
    vector<int> parent(N, -1);
    vector<int> digit(N, -1);
    visited[1] = true;
    while (!q.empty()) {
        int node = q.front();
        q.pop();

        if (node == 0) {
            //Found multiple. now construct it
            string multiple = "";
            while (node != 1) {
                multiple = to_string(digit[node]) + multiple;
                node = parent[node];
            }
            multiple = "1" + multiple;
            return multiple;
        }

        int child1 = (10 * node) % N;
        if (visited[child1] == false) {
            q.push(child1);
            parent[child1] = node;
            digit[child1] = 0;
            visited[child1] = true;
        }
        int child2 = (10 * node + 1) % N;
        if (visited[child2] == false) {
            q.push(child2);
            parent[child2] = node;
            digit[child2] = 1;
            visited[child2] = true;
        }
        // cout << node << " " << child1 << " " << child2 << endl;
    }
    return "-1";
}

int main() {
    cout << multiple(3) << endl;   //111
    cout << multiple(55) << endl;  //110
    cout << multiple(2) << endl;   //10
}
