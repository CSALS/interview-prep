#include <bits/stdc++.h>
using namespace std;

/*

https://www.geeksforgeeks.org/find-a-mother-vertex-in-a-graph/

What is a Mother Vertex?
A mother vertex in a graph G = (V,E) is a vertex v such that all other vertices in G 
can be reached by a path from v.

There are can be several mother vertices. We are interested in one of them

    GRAPH is
        undirected, connected = then every vertex is mother vertex
        undirected, disconnected = no mother vertex
        directed = there can be several mother vertices

    O(V*(V+E)) ALGO =>
        for every vertex do dfs/bfs and check if it is mother vertex

    O(V+E) ALGO =>
        1. Like kosaraju's algo we first do DFS and 
            find the vertex which will be on top of the stack
            (which exits last. we don't need entire stack, we only need the last vertex)
        2. Then from that last vertex do bfs/dfs and count reachable vertices.
            if that count is V-1 then it is mother vertex

    int last=-1
    for(int u = 0; u < V; ++u) {
        if(vis[u]==false) {
            dfs(u, last)
        }
    }
    void dfs(int u, int& last) {
        vis[u]=true
        for(int v : adj[u]) {
            dfs(v, last)
        }
        last=u;
    }

    using that "last" do dfs/bfs for count of reachable vertices
*/