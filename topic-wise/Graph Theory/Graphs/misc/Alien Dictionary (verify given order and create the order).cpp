#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

/*
https://leetcode.com/problems/verifying-an-alien-dictionary/
    verify whether given list of words are in lexicographic order acc. to given order of alphabet

https://www.geeksforgeeks.org/given-sorted-dictionary-find-precedence-characters/
    given lexicographic order find the the order of alphabet
*/

class Graph {
    int V;
    vector<int> *adj;
    stack<int> st;  //stores nodes in descending order of exit time
    vector<bool> visited;

   public:
    Graph(int V) {
        this->V = V;
        adj = new vector<int>[26];
        visited.resize(26, false);
    }
    void addEdge(char u, char v) {
        //directed edge from u--->v
        adj[u - 'a'].push_back(v - 'a');
    }

   private:
    void dfs(int u) {
        visited[u] = true;
        for (int v : adj[u]) {
            if (!visited[v])
                dfs(v);
        }
        st.push(u);  //push when exiting
    }

   public:
    string topologicalSort() {
        for (int u = 0; u < V; ++u) {
            if (!visited[u]) {
                dfs(u);
            }
        }
        string order = "";
        while (!st.empty()) {
            order += (st.top() + 'a');
            st.pop();
        }
        return order;
    }
};

string findOrder(string dict[], int N, int K) {
    Graph g(K);

    // Construct the graph
    for (int i = 0; i < N - 1; ++i) {
        string word1 = dict[i];
        string word2 = dict[i + 1];
        for (int k = 0; k < min(word1.length(), word2.length()); ++k) {
            if (word1[k] == word2[k]) continue;
            //word1[k] comes before word2[k]
            g.addEdge(word1[k], word2[k]);
            break;
        }
    }
    // Do topological sort
    string order = g.topologicalSort();
    return order;
}

int main() {
    string arr[] = {"baa", "abcd", "abca", "cab", "cad"};
    cout << findOrder(arr, 5, 4);
    return 0;
}