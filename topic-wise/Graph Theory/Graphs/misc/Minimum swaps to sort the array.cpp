#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

/*

https://www.geeksforgeeks.org/minimum-number-swaps-required-sort-array/

Variation => https://www.geeksforgeeks.org/minimum-swap-required-convert-binary-tree-binary-search-tree/?ref=lbp
use inorder of binary tree and find min. swaps to convert it into sorted array (since for bst inorder is sorted)

similar question => min. adjacent swaps to sort array. answer is number of inversions
*/

//O(NlogN) T | O(2N)S
int minSwapsUtil(vector<int> arr) {
    int N = arr.size();
    vector<pair<int, int>> sortedArr(N);

    for (int i = 0; i < N; ++i) {
        sortedArr[i] = {arr[i], i};
    }
    sort(sortedArr.begin(), sortedArr.end());
    int swaps = 0;
    vector<bool> visited(N, false);
    for (int i = 0; i < N; ++i) {
        if (visited[i] || arr[i] == sortedArr[i].first) continue;
        visited[i] = true;

        int v = sortedArr[i].second;
        int nodes_count = 0;
        while (v != i) {
            visited[v] = true;
            nodes_count++;
            v = sortedArr[v].second;
        }
        swaps += nodes_count;
    }
    return swaps;
}

int main() {
    cout << minSwapsUtil({1, 5, 4, 3, 2});
    return 0;
}