#include <bits/stdc++.h>
using namespace std;
#define INF LONG_MAX
#define int long long
#define deb(x) cout << #x << " " << x << endl;

/*
https://codeforces.com/problemset/problem/229/B

    Basically it is dijkstra's
    But when relaxing edges you need to consider time of other arriving guys also
*/

struct Edge {
    int v, wt;
    Edge(int x, int y) {
        v = x;
        wt = y;
    }
};

int solution() {
    int N;  //planets
    int E;  //edges
    cin >> N >> E;
    vector<Edge> adj[N + 1];
    while (E--) {
        int u, v, wt;
        cin >> u >> v >> wt;
        adj[u].push_back(Edge(v, wt));
        adj[v].push_back(Edge(u, wt));
    }
    vector<int> others[N + 1];
    for (int i = 1; i <= N; ++i) {
        int size;
        cin >> size;
        while (size--) {
            int time;
            cin >> time;
            others[i].push_back(time);
        }
    }
    //modified dijkstra's
    vector<int> dist(N + 1, INF);
    dist[1] = 0;
    set<pair<int, int>> q;  //{dist, node}
    q.insert({0, 1});
    while (!q.empty()) {
        pair<int, int> p = *(q.begin());
        int node = p.second;
        int time = p.first;
        q.erase(q.begin());

        //relax all adjacent vertices
        for (Edge e : adj[node]) {
            int v = e.v;
            while (binary_search(others[node].begin(), others[node].end(), time)) {
                ++time;
            }
            if (dist[v] > time + e.wt) {
                q.erase({dist[v], v});
                dist[v] = time + e.wt;
                q.insert({dist[v], v});
            }
        }
    }
    return (dist[N] >= INF) ? -1 : dist[N];
}

int32_t main() {
    int t = 1;
    // cin >> t;
    while (t--) {
        // solution();
        cout << solution() << endl;
    }
    return 0;
}