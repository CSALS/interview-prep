#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

#define Cell pair<int, int>

/*
https://leetcode.com/problems/regions-cut-by-slashes

Explanation = https://leetcode.com/problems/regions-cut-by-slashes/discuss/205719/Mark-the-boundary-and-then-the-problem-become-Number-of-Islands-(dfs-or-bfs)

convert this problem into number of islands

/ => 0 0 1
     0 1 0
     1 0 0

why not 2d + diagonal support in number of islands? 
since / has 2 components
/ => 0 1   (0 and 0 are connected since we are considering diagonal also)
     1 0

So convert the given string array into [N*3][N*3] matrix

Finding number of connected components => BFS O(N^2) T (we can use DSU also we get O(N^2) T)
*/

class Solution {
   public:
    void fillMatrix(vector<string> &grid, vector<vector<int>> &matrix, int N) {
        for (int i = 0; i < N; ++i) {
            string symbol = grid[i];
            for (int j = 0; j < (int)symbol.length(); ++j) {
                char c = symbol[j];
                if (c == '/') {
                    matrix[i * 3][j * 3 + 2] = 1;
                    matrix[i * 3 + 1][j * 3 + 1] = 1;
                    matrix[i * 3 + 2][j * 3] = 1;
                } else if (c == '\\') {
                    matrix[i * 3][j * 3] = 1;
                    matrix[i * 3 + 1][j * 3 + 1] = 1;
                    matrix[i * 3 + 2][j * 3 + 2] = 1;
                }
            }
        }
        // for (int i = 0; i < 3 * N; ++i) {
        //     for (int j = 0; j < 3 * N; ++j) {
        //         cout << matrix[i][j] << " ";
        //     }
        //     cout << endl;
        // }
    }

    bool isValid(Cell cell,
                 vector<vector<int>> &matrix,
                 vector<vector<bool>> &visited,
                 int N) {
        return cell.first >= 0 && cell.first < N && cell.second >= 0 && cell.second < N && !visited[cell.first][cell.second] && matrix[cell.first][cell.second] == 0;
    }

    void bfs(int sourceX,
             int sourceY,
             vector<vector<bool>> &visited,
             vector<vector<int>> &matrix,
             int N) {
        queue<Cell> q;
        q.push({sourceX, sourceY});
        visited[sourceX][sourceY] = true;

        vector<int> rowOffset = {0, 0, 1, -1};
        vector<int> colOffset = {1, -1, 0, 0};

        while (!q.empty()) {
            Cell current = q.front();
            q.pop();
            for (int k = 0; k < rowOffset.size(); ++k) {
                Cell newCell = {current.first + rowOffset[k],
                                current.second + colOffset[k]};

                if (isValid(newCell, matrix, visited, N)) {
                    visited[newCell.first][newCell.second] = true;
                    q.push(newCell);
                }
            }
        }
    }

    int getNumberOfConnectedComponents(vector<vector<int>> &matrix) {
        int N = matrix.size();
        int connectedComponents = 0;
        vector<vector<bool>> visited(N, vector<bool>(N, false));
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (!visited[i][j] && matrix[i][j] == 0) {
                    connectedComponents++;
                    bfs(i, j, visited, matrix, N);
                }
            }
        }

        return connectedComponents;
    }

    int regionsBySlashes(vector<string> &grid) {
        int N = grid.size();
        vector<vector<int>> matrix(N * 3, vector<int>(N * 3, 0));
        // construct matrix
        fillMatrix(grid, matrix, N);
        // find number of connected comp. (edge from 0 to 0 only)
        return getNumberOfConnectedComponents(matrix);
    }
};

int main() {
    Solution s;
    cout << s.regionsBySlashes({" /", "/ "}) << endl;    //2
    cout << s.regionsBySlashes({" /", "  "}) << endl;    //1
    cout << s.regionsBySlashes({"\\/", "/\\"}) << endl;  //4
    cout << s.regionsBySlashes({"/\\", "\\/"}) << endl;  //5
    cout << s.regionsBySlashes({"//", "/ "}) << endl;    //3
    return 0;
}