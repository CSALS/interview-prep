#include <bits/stdc++.h>
using namespace std;

/*

https://www.interviewbit.com/problems/snake-ladder-problem/

The board is always 10 x 10 with squares numbered from 1 to 100

Find min moves to reach from 1 to 100

    Each cell has 6 neighbors, use BFS
*/

int snakeLadder(vector<vector<int>> &ladders,
                vector<vector<int>> &snakes) {
    vector<int> move(101, -1);  //move[i] = -1 then no snake or ladder, else tail of snake or end of latter

    for (vector<int> ladder : ladders) {
        move[ladder[0]] = ladder[1];
    }
    for (vector<int> snake : snakes) {
        move[snake[0]] = snake[1];
    }

    vector<bool> visited(101, false);

    queue<pair<int, int>> q;
    visited[1] = true;
    q.push({1, 0});

    while (!q.empty()) {
        int u = q.front().first;
        int dist = q.front().second;
        q.pop();
        if (u == 100) return dist;
        for (int v = u + 1; v <= min(u + 6, 100); ++v) {
            if (!visited[v]) {
                //snake or ladder present here
                if (move[v] != -1) {
                    if (!visited[move[v]]) {
                        q.push({move[v], dist + 1});
                        visited[move[v]] = true;
                    }
                } else {
                    q.push({v, 1 + dist});
                }
                visited[v] = true;
            }
        }
    }
    return -1;  //if not possible
}
