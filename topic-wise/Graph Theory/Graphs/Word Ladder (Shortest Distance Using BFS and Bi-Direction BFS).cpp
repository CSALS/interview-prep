#include <bits/stdc++.h>
using namespace std;

/**
 * Shortest Path Using BFS
 * N = number of words in wordList and N is the number of nodes present in the graph
 * M = length of each word (all words have same length)
 * O(N*N + M*N) T | O(N*M) S (wordList, queue, visited can store O(|V|) and here V is N but each string has length M so N*M)
 * What if strings are very huge to store??
 * Instead of storing strings directly store their index
 */
int ladderLength(string beginWord, string endWord, vector<string> &wordList) {
    dict.clear();
    //O(N * M) T
    for (string word : wordList) dict.insert(word);

    //if endWord itself isn't present in the dictionary then we can't reach it anyway
    if (dict.find(endWord) == dict.end()) return 0;

    // bfs(beginWord, endWord)
    queue<string> q;
    unordered_set<string> visited;

    q.push(beginWord);
    visited.insert(beginWord);
    int level = 0;
    //O(e1 + M) + O(e2 + M) + ... + O(eN + M)
    //Each node has O(N) edges
    //O(N*N + M*N) T
    while (!q.empty()) {
        int size = q.size();
        while (size--) {
            string node = q.front();
            q.pop();
            if (node == endWord) return level + 1;

            vector<string> children = generateChildren(node);
            for (string child : children) {
                if (visited.find(child) == visited.end()) {
                    visited.insert(child);
                    q.push(child);
                }
            }
        }
        level++;
    }

    return 0;
}
/**
 * Shortest Path Using Bi-Directional BFS
 */
unordered_set<string> dict;

//O(M) T
vector<string> generateChildren(string s) {
    vector<string> children;
    for (int i = 0; i < s.length(); ++i) {
        string temp = s;
        for (char c = 'a'; c <= 'z'; ++c) {
            if (s[i] == c) continue;
            temp[i] = c;
            if (dict.find(temp) != dict.end()) children.push_back(temp);
        }
    }
    return children;
}

void BFS(queue<string> &q, unordered_set<string> &visited,
         unordered_map<string, int> &dist) {
    string node = q.front();
    q.pop();
    vector<string> children = generateChildren(node);
    for (string child : children) {
        if (visited.find(child) == visited.end()) {
            visited.insert(child);
            q.push(child);
            dist[child] = dist[node] + 1;
        }
    }
}
int ladderLength(string beginWord, string endWord, vector<string> &wordList) {
    dict.clear();
    for (string word : wordList) dict.insert(word);

    // Base Case
    if (dict.find(endWord) == dict.end()) return 0;
    if (beginWord == endWord) return 1;

    queue<string> f, b;
    unordered_set<string> visitedF, visitedB;
    unordered_map<string, int> distF, distB;

    f.push(beginWord);
    distF[beginWord] = 0;
    b.push(endWord);
    distB[endWord] = 0;
    visitedF.insert(beginWord);
    visitedB.insert(endWord);

    while (!f.empty() && !b.empty()) {
        BFS(f, visitedF, distF);
        BFS(b, visitedB, distB);

        //check for intersection
        int ans_inter = INT_MAX;   //if we reach from forward nodes to (intermediate) and backward nodes also reach that (intermediate) node
        int ans_direct = INT_MAX;  //if we reached from forward nodes directly to destination
        for (auto it : visitedF) {
            if (it == endWord) {
                ans_direct = distF[it] + 1;  //+1 since we need number of nodes not length
            }
            if (visitedB.find(it) != visitedB.end()) {
                ans_inter = distF[it] + distB[it] + 1;  //+1 since we need number of nodes not length
            }
        }
        if (!(ans_inter == INT_MAX && ans_direct == INT_MAX)) {
            return min(ans_inter, ans_direct);
        }
    }

    return 0;
}