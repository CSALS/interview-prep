#include <bits/stdc++.h>
using namespace std;

int V;                       // number of vertices
vector<vector<int>> adj[V];  // graph

vector<bool> visited(V, false);
vector<bool> entry(V), low(V);
int timer = 0;

vector<int> cut_vertices;
vector<pair<int, int>> bridges;
/**
* Exploring DFS TREE.
* entry[node] = entry time of that node
* low[node] = lowest entry time of all nodes reachable from that node (tree edges, back edges)
* Procedure = (Articulation Points)
* 1. if it is root node and having > 1 children then it is cut vertex
* 2. if it is not root node but having for any child => low[child] >= entry[node] then it is cut vertex since child subtree can't reach ancestors of node
* Procedure = (Bridges)
0. same for both root and non-root nodes
* 1. low[child] > entry[node] then node----child edge is a bridge
*/
void dfs(int u, int parent) {
    visited[u] = true;
    entry[u] = low[u] = timer++;
    int children = 0;
    for (int v : adj[u]) {
        if (v == parent) continue;

        if (visited[v])  // u--->v is a back edge
            low[u] = min(low[u], entry[v]);
        else {
            dfs(v, u);
            low[u] = min(low[u], low[v]);

            // Cut Vertices (for non root nodes only)
            // v can't reach ancestors of u so u is cut vertex
            if (low[v] >= entry[u] && parent != -1) {
                cut_vertices.push_back(u);
            }
            ++children;

            // Bridges
            // v can't reach ancestors of u and also shouldn't reach u through one
            // its descendants
            if (low[v] > entry[u]) {
                bridges.push_back({u, v});
            }
        }
    }
    // root node
    if (children > 1 && parent == -1) cut_vertices.push_back(u);
}

void find_cutvertices() { dfs(0, -1); }