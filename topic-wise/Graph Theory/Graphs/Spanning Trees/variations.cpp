#include <bits/stdc++.h>
using namespace std;


/**
 * https://www.geeksforgeeks.org/minimum-spanning-tree-cost-of-given-graphs/
 * https://www.geeksforgeeks.org/spanning-tree-with-maximum-degree-using-kruskals-algorithm/
 *      Find the max. degree vertex. Include all its edges in MST. And then do regular MST for rest of edges
 * TODO: https://www.codechef.com/problems/MSTQS
 * TODO: https://tinyurl.com/bitmask-dp (HARD)
*/