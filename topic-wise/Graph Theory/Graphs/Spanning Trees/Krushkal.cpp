#include <bits/stdc++.h>
using namespace std;

struct Edge {
    int u;
    int v;
    int weight;
    Edge(int u, int v, int wt) : u(u), v(v), weight(wt) {}
};

bool comp(Edge a, Edge b) {
    // return true if a should come before b
    return a.weight <= b.weight;
}

// O(ElogE + VE)
void krushkal() {
    int V;
    vector<Edge> edges;
    vector<Edge> MST_Edges;
    int cost = 0;

    // dsu[i] == dsu[j] then they are already connected
    vector<int> dsu(V);
    for (int i = 0; i < V; ++i) dsu[i] = i;

    // 1. Sort all edges in increasing order of weights O(ElogE)
    sort(edges.begin(), edges.end(), comp);

    // 2. Pick each edge and include in MST if adding that edge doesn't make the graph cyclic
    for (Edge e : edges) {
        if (dsu[e.u] == dsu[e.v]) {
            // including this edge u-v will give cycle so skip
            continue;
        }

        cost += e.weight;
        MST_Edges.push_back(e);

        // Replace all guys with value dsu[u] with dsu[v]
        int old_value = dsu[e.u], new_value = dsu[e.v];
        //O(V)
        for (int i = 0; i < V; ++i) {
            if (dsu[i] == old_value) dsu[i] = new_value;
        }
    }
}