#include <bits/stdc++.h>
using namespace std;

/**
 * Disjoint-Set Data Structure
 * Each element belongs to a set. A set is represented using one element
 * find(x) = returns representative of the set in which x belongs to
 * union(x, y) = combines sets of x and y
 * 
 * 1. Naive implementation (done in krushkal.cpp)
 *      find(x) =     O(1) 
 *      union(x, y) = O(N)
 * 2. Another naive implementation (using parent array)
 *      union(x, y) = find(x) = O(N) [union uses find]
 * 3. Same as 2nd but we use union by rank (using rank array)
 *      union(x, y) = find(x) = O(logN) [union by rank] since we make tree balanced
 * 4. union(x, y) = find(x) = O(logN) worst case | O(1) amortized time 
 * [if there are m operations then total time for them is O(m * alpha(n)) where alpha(n) is inverse Ackermann function
 *                                               and it is very slowly growing function and practically it is <= 4]
 */
class DSU {
   public:
    vector<int> rank, parent;  // rank = height of tree [we can use size of subtree also]
    DSU(int V) {
        rank.resize(V, 0);
        parent.resize(V);
        for (int i = 0; i < V; ++i) parent[i] = i;
    }

    // Path By Compression. O(logN) worst case
    int findDSU(int x) {
        if (x == parent[x]) return x;

        return (parent[x] = findDSU(parent[x]));
    }

    // Union By Rank. O(logN) worst case
    void unionDSU(int x, int y) {
        int x_rep = findDSU(x);
        int y_rep = findDSU(y);

        if (x_rep == y_rep) {
            // already x and y in the same set
            return;
        }

        // attach smaller tree to larger
        // make small height node rep. as child of longer height node rep.
        if (rank[x_rep] < rank[y_rep])
            parent[x_rep] = y_rep;

        else if (rank[y_rep] < rank[x_rep])
            parent[y_rep] = x_rep;

        else {
            // same ranks
            parent[y_rep] = x_rep;
            rank[x_rep]++;
        }
    }
};

struct Edge {
    int u;
    int v;
    int weight;
    Edge(int u, int v, int wt) : u(u), v(v), weight(wt) {}
};

bool comp(Edge a, Edge b) {
    // return true if a should come before b
    return a.weight <= b.weight;
}

// O(ElogE + ElogV) Using DSU
void krushkal() {
    int V;
    vector<Edge> edges;
    vector<Edge> MST_Edges;
    int cost = 0;
    DSU dsu(V);
    // 1. Sort all edges in increasing order of weights O(ElogE)
    sort(edges.begin(), edges.end(), comp);

    // 2. Pick each edge and include in MST if they don't give cycle
    for (Edge e : edges) {
        int u = e.u, v = e.v, wt = e.weight;
        if (dsu.findDSU(u) == dsu.findDSU(v)) {
            // there's already a path b/w u and v so adding this edge makes a cycle
            continue;
        }

        // make an edge b/w u and v in dsu
        dsu.unionDSU(u, v);

        MST_Edges.push_back(e);
        cost += wt;
    }
}



/*
Using DSU in directed graphs.
https://www.interviewbit.com/problems/cycle-in-directed-graph/ (check solution)
*/