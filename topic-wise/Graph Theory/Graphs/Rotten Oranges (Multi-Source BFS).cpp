#include <bits/stdc++.h>
using namespace std;

int rottenOranges(vector<vector<int> > &grid) {
#define ppi pair<int, int>
    queue<ppi> q;
    int n = grid.size(), m = grid[0].size();
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (grid[i][j] == 2) {
                q.push({i, j});
            }
        }
    }

    int level = 0;
    int row[4] = {0, 0, 1, -1};
    int col[4] = {1, -1, 0, 0};
    while (!q.empty()) {
        int size = q.size();
        while (size--) {
            ppi cell = q.front();
            q.pop();
            for (int k = 0; k < 4; ++k) {
                int new_x = cell.first + row[k];
                int new_y = cell.second + col[k];
                if (new_x >= 0 && new_y >= 0 && new_x < n && new_y < m &&
                    grid[new_x][new_y] == 1) {
                    grid[new_x][new_y] = 2;
                    q.push({new_x, new_y});
                }
            }
        }
        if (!q.empty())
            level++;
    }
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
            if (grid[i][j] == 1)
                return -1;

    return level;
}
