#include <bits/stdc++.h>
using namespace std;

int V;
vector<vector<int>> adj;
bool isBipartite = false;

/**
 * Do a BFS. Assign the unvisited neighbor opposite color. If current node has a
 * visited neighbor and same color as this node then graph is not 2-colorable (not bipartite)
 * Same thing you can do in DFS also
 */
void bipartite() {
    // color[i] = -1 then unvisited, color[i] = 0 or 1
    vector<int> color(V, -1);
    for (int src = 0; src < V; ++src) {
        if (color[src] == -1) {
            queue<int> q;
            q.push(src);
            color[src] = 0;

            while (!q.empty()) {
                int u = q.front();
                q.pop();
                for (int v : adj[u]) {
                    if (color[v] == -1) {
                        color[v] = 1 - color[u];
                        q.push(v);
                    } else if (color[v] == color[u]) {
                        isBipartite = false;
                        break;
                    }
                }
            }
        }
        if (isBipartite == false) break;
    }
    cout << isBipartite;
}

/**
 * Application https://www.geeksforgeeks.org/check-if-there-is-a-cycle-with-odd-weight-sum-in-an-undirected-graph/?ref=rp
 * Check if there is a cycle with odd weight sum in an undirected graph
 * 
 * Bipartite graph = undirected graph with no odd length cycle
 * Here we can convert -
 *  even edge into two units of each weight 1
 *  odd edge into one unit of weight 1
 * 1---2 (with weight even) => 1 -- 2 -- (dummyNode) [each weight 1]
 * After adding the extra edges for even length weights then do bipartite check
 * if modified graph is bipartite => no cycle with odd weight sum
 * 
 * 
 * Application https://www.geeksforgeeks.org/convert-the-undirected-graph-into-directed-graph-such-that-there-is-no-path-of-length-greater-than-1/?ref=rp
 * Path length b/w any 2 nodes shouldn't be more than one
 * Answer is to simply check if it is bipartite by 2-coloring method
 * if it is not bipartite then can't convert the graph
 * but if it is bipartite then it can be divided into vertieces set V1 and V2 where edges are always b/w V1 and V2
 * so make all edges go from V1 -> V2 (0 color vertex is u and 1 color vertex is v then u --> v is the directed edge for u -- v)
 */