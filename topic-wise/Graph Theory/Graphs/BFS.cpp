#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> adj;  // adjacency list representation
int n;                    // number of nodes
vector<bool> visited(n);
#define INF 1000000

/**
 * 1. For getting any one of the shortest path store for every node one of its parent
 * 2. For getting all the shortest paths store for every node all its parent and then use recursion to trace all the paths
 * 3. For getting lexicographic first shortest path use min heap (Is it required if adj list is sorted?)
 */
void bfsEntireGraph() {
    for (int i = 0; i < n; ++i) {
        if (!visited[i]) {
            bfs(i);
        }
    }
}
void bfs(int source) {
    queue<int> q;

    vector<int> dist(n, INF), parent(n);

    q.push(source);
    visited[source] = true, dist[source] = 0, parent[source] = -1;
    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v : adj[u]) {
            if (!visited[v]) {
                visited[v] = true;
                dist[v] = dist[u] + 1;
                parent[v] = u;
            }
        }
    }
    //if dist[i] = INF then no path from source to that node 'i'

    // Path Tracking of source to some vertex say u
    int u;
    if (dist[u] == INF) return {};
    vector<int> path;
    for (int v = u; v != -1; v = parent[v]) {
        path.push_back(v);
    }
    reverse(path.begin(), path.end());
}

//multiple SHORTEST paths
void bfsMultipltPaths(int source) {
    queue<int> q;

    vector<int> dist(n, INF);
    vector<int> parent[n];  //each node can be reached through multiple nodes

    q.push(source);
    visited[source] = true, dist[source] = 0, parent[source] = {};
    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v : adj[u]) {
            if (!visited[v]) {
                visited[v] = true;
                dist[v] = dist[u] + 1;
                parent[v] = {u};

            } else if (dist[u] + 1 == dist[v]) {
                //if v is already visited but you can reach v through u using same distance means it is another shortest path
                parent[v].push_back(u);
            }
        }
    }
}
