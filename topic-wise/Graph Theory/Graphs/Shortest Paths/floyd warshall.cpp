#include <bits/stdc++.h>
using namespace std;

/**
 * Finds shortest path distance b/w all pairs in a graph
 * graph[i][j] = INF if no edge b/w them
 * graph[i][j] = 0 if i == j
 * graph[i][j] = weight of the edge
 * 
 * Variations = finding all pairs of vertices (i,j) which don't have a shortest path between them
 */

const int INF = 10000000;

int dist[4][4] = {
    {0, 5, INF, 10}, {INF, 0, 3, INF}, {INF, INF, 0, 1}, {INF, INF, INF, 0}};

/**
* For path construction. next(i, j) = next node after i in the shortest path between i and j
*/
int next[4][4] = {-1};

int V = 4;

void floyd_warshall() {
    for (int u = 0; u < V; ++u) {
        for (int v = 0; v < V; ++v) {
            if (dist[u][v] == INF) continue;
            next[u][v] = v;  // initially
        }
    }

    for (int k = 0; k < V; ++k)
        for (int i = 0; i < V; ++i)
            for (int j = 0; j < V; ++j) {
                if (dist[i][k] != INF && dist[j][k] != INF) {
                    if (dist[i][j] > dist[i][k] + dist[j][k]) {
                        dist[i][j] = dist[i][k] + dist[j][k];
                        // if k should be intermediate b/w i & j in their
                        // shortest path next(i, j) wont be k but rather it will
                        // be next(i, k)
                        // since there might be some intermediate node b/w i & k
                        // in their shortest path
                        next[i][j] = next[i][k];
                    }
                }
            }

    //-ve cycle detection = if dist from a node to itself is negative then there is -ve cycle
    for (int i = 0; i < V; ++i) {
        if (dist[i][i] != 0)
            printf(
                "-ve cycle present since dist of a node to itself should be 0 "
                "unless there is -ve cycle in which case we will get -ve "
                "dist\n");
    }
}

void pathTracing(int u, int v) {
    if (next[u][v] == -1) {
        printf("No Path\n");
    }
    vector<int> path;
    for (int i = u; i != v; i = next[i][v]) {
        path.push_back(i);
    }
    path.push_back(v);
}
