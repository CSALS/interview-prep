#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/minimum-cost-path-left-right-bottom-moves-allowed/

If only right/bottom moves allowed then we can use DP.
But here we can't use DP.

Need to use dijkstra's since it is weighted graph (N^2 nodes, O(4*N^2) edges)

(0, 0) --------> (N, N)
Need to find shortest path from (0, 0) to (N, N)

Use dijkstra's algo with dist[x][y] = shortest distance from (0, 0) to (x, y)
Answer = dist[N][N]

Only change here is, in dijkstra's dist[src] = 0 (distance of a node to itself is 0)
but here to pass through (0, 0) you incur matrix[0][0] cost
so dist[0][0] will be matrix[0][0]
*/