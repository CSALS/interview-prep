#include <bits/stdc++.h>
using namespace std;

struct edge {
    int a, b, cost;
};

int V;
vector<edge> edges;
const int INF = 10000000;

/**
 * DP Solution
 * ith iteration => shortest distances with atmost 'i' edges are calculated
 * Any simple path can't contain more than V-1 edges.
 * If there is -ve weight cycle then for some nodes the shortest path distance
 * isn't defined since we can keep on cycling in the -ve weight cycle and get -INF shortest path dist.
 * 
 */
void bellmanford(int src, int dest) {
    vector<int> dist(V, INF);
    vector<int> parent(V, -1);

    dist[src] = 0;
    parent[src] = -1;
    for (int count = 0; count < V - 1; ++count) {
        bool isDone = true;  // if no relaxing is done in this iteration then it
                             // wont be done in any more iterations so break
        for (edge e : edges) {
            int u = e.a, v = e.b, wt = e.cost;
            if (dist[u] != INF && dist[v] > dist[u] + wt) {
                // first condition is needed because of -ve weights. if
                // dist[u]&dist[v] = INF and wt < 0 we get INF > INF - c so we
                // update the distance. it's better not do it
                dist[v] = dist[u] + wt;
                parent[v] = u;
                isDone = false;
            }
        }

        if (isDone)
            break;
    }

    // shortest path tracing of src->dest
    if (dist[dest] == INF) printf("No path from src to dest\n");

    vector<int> path;
    for (int v = dest; v != -1; v = parent[v]) path.push_back(v);
    reverse(path.begin(), path.end());

    /**
     * negative weight cycle detection & printing
     * if after the (V-1) phases if we run one more relaxtion on all edges and
     * if we find any improvement that means there is a negative weight cycle
     * since in case of that we can endlessly improve the dist. of cycle nodes
     * and nodes reachable from those nodes.
    */
    int x = -1;  // x will be store last relaxed node (if any) in the Vth phase
    // this node will be either in the cycle or is reachable node from cycle
    for (edge e : edges) {
        int u = e.a, v = e.b, wt = e.cost;
        if (dist[v] > dist[u] + wt) {
            dist[v] = dist[u] + wt;
            parent[v] = u;
            x = v;
        }
    }
    if (x == -1)
        printf("No -ve weight cycle\n");
    else {
        // x will be either in the cycle or is reachable node from cycle
        // starting from the vertex x, pass through to the predecessors V times
        // You will get a node def. which is present in the cycle since cycle length can't be more than V
        // Then, we have to go from this vertex, through the predecessors,
        // until we get back to the same vertex y (and it will happen, because relaxation
        // in a negative weight cycle occur in a circular manner).
        int y = x;
        for (int i = 0; i < V; ++i) y = parent[y];

        // y will be in the cycle
        vector<int> cycleNodes;
        bool firstIteration = true;
        for (int node = y;; node = parent[node]) {
            cycleNodes.push_back(node);
            if (node == y && firstIteration == false) break;
            firstIteration = false;
        }
    }
}

/**
 * Negative weight cycle detection.
 * Extending for disconnected graphs also
 * Maintain visited array. For each node do bellman-ford using it as source and 
 * after that mark all nodes whose dist != INF as visited
 */