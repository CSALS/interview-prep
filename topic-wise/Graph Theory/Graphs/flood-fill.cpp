#include <bits/stdc++.h>
using namespace std;

/*
Just a fancy term for DFS/BFS on matrix


https://www.geeksforgeeks.org/given-matrix-o-x-replace-o-x-surrounded-x/
Given a matrix where every element is either ‘O’ or ‘X’, replace ‘O’ with ‘X’ 
if surrounded by ‘X’. A ‘O’ (or a set of ‘O’) is considered to be by surrounded by ‘X’ 
if there are ‘X’ at locations just below, just above, just left and just right of it. 

    ALGO=> from all O present at boundaries (they are obv. not surrounded by X) do bfs to all reachable Os.
    All the non-reachable Os will be converted to Xs since they will be surrounded
*/