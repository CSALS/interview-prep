#include <bits/stdc++.h>
using namespace std;

/**
 * Intuitive Solution
 * 1. Find NSR and NSL on each index
 * 2. For each index the area is (NSL[i] - NSR[i] - 1) * heights[i]
 */
int largestRectangleArea(vector<int>& heights) {
    int n = heights.size();
    //Find NSL for each index
    stack<int> st;
    int NSL[n];
    for (int i = 0; i < n; ++i) {
        while (!st.empty() && heights[i] <= heights[st.top()]) st.pop();
        NSL[i] = (st.empty()) ? -1 : st.top();
        st.push(i);
    }
    while (!st.empty()) st.pop();

    //Find NSR for each index
    int NSR[n];
    for (int i = n - 1; i >= 0; --i) {
        while (!st.empty() && heights[i] <= heights[st.top()]) st.pop();
        NSR[i] = (st.empty()) ? n : st.top();
        st.push(i);
    }

    //Calculate areas
    int maxArea = 0;
    for (int i = 0; i < n; ++i) {
        maxArea = max(maxArea, heights[i] * (NSR[i] - NSL[i] - 1));
    }
    return maxArea;
}

/*
    Only one stack question
    https://www.youtube.com/watch?v=ZmnqCZp9bBs&t=340s
*/
int largestRectangleArea(vector<int>& heights) {
    int n = heights.size();

    stack<int> st;

    /*
            For each bar as height we will calc. area
            When you pop from stack for that index calc. its area
            since we pop only when we find lesser height
        */

    int maxArea = 0;
    int i;
    for (i = 0; i < n; ++i) {
        int currentHeight = heights[i];
        while (!st.empty() && currentHeight < heights[st.top()]) {
            int top = st.top();
            st.pop();
            int area = 0;
            if (st.empty()) {
                area = i * heights[top];
            } else {
                area = (i - 1 - st.top()) * heights[top];
            }
            maxArea = max(maxArea, area);
        }
        st.push(i);
    }

    while (!st.empty()) {
        int top = st.top();
        st.pop();
        int area = 0;
        if (st.empty()) {
            area = i * heights[top];
        } else {
            area = (i - 1 - st.top()) * heights[top];
        }
        maxArea = max(maxArea, area);
    }

    return maxArea;
}
