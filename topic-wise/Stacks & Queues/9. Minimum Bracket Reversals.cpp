#include <bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/minimum-number-of-bracket-reversals-needed-to-make-an-expression-balanced/

//Thought of graph approach with bfs for getting min reversals.... but that gives exponential complexity.
//so whenever expression of (, ) + balanced is given = try to just think of using stacks somehow even if it is min or max etc.

/*
remove all matchings brackets
in the end you will have )))))((((
reverse pair-wise
*/
int solve(string expr) {
    int len = expr.length();
    if (len & 1)
        return -1;

    stack<char> st;
    for (char c : expr) {
        if (c == '(')
            st.push(c);
        else {
            if (!st.empty() && st.top() == '(')
                st.pop();
            else
                st.push(c);
        }
    }

    string res = "";
    while (!st.empty()) {
        res += st.top();
        st.pop();
    }
    if (res.empty())
        return 0;
    reverse(res.begin(), res.end());
    int ind;
    for (ind = 0; ind < res.length(); ++ind) {
        if (res[ind] == '(')
            break;
    }
    int closing_ct = ind;
    int ending_ct = res.length() - ind;
    if (closing_ct & 1) {
        return closing_ct / 2 + ending_ct / 2 + 2;
    } else {
        return closing_ct / 2 + ending_ct / 2;
    }
}
