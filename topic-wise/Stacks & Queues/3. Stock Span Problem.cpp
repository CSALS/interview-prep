#include <bits/stdc++.h>
using namespace std;

/**
 * Questions boils down to finding index of nearest greater element on the left.
 * O(N) Time | O(N) Space
 */

void stockSpan(int arr[], int n) {
    stack<int> st;
    int res[n];
    for (int i = 0; i < n; ++i) {
        while (!st.empty() && arr[i] >= arr[st.top()])
            st.pop();
        res[i] = st.empty() ? i + 1 : i - st.top();
        st.push(i);
    }
}