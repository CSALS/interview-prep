#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/implement-stack-using-priority-queue-or-heap/

    In the priority queue along with value we push current index and order according to that index
    (0, A) (1, B) (2, C) ...


https://www.geeksforgeeks.org/stack-set-2-infix-to-postfix/

    Check Notes

TODO:
    https://www.geeksforgeeks.org/prefix-infix-conversion/?ref=rp
    https://www.geeksforgeeks.org/convert-infix-prefix-notation/?ref=rp
    https://www.geeksforgeeks.org/postfix-prefix-conversion/?ref=rp
*/

/*
Balanced paranthesis
    1. stack based
    2. no stack
*/
int stackBased(string A) {
    stack<char> st;
    for (char c : A) {
        if (c == '(') {
            st.push(c);
        } else if (c == ')') {
            if (st.empty()) return false;
            st.pop();
        }
    }
    return st.empty();
}

int noStackBased(string A) {
    int openCount = 0;
    for (char c : A) {
        if (c == '(') {
            openCount++;
        } else if (c == ')') {
            if (openCount == 0) return false;
            openCount--;
        }
    }
    return openCount == 0;
}
