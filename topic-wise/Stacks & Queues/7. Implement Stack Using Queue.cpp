#include <bits/stdc++.h>
using namespace std;

// Stack using Two Queues
queue<int> q1,q2;

/*
Method 1 :

q1 will hold all items in LIFO order, q2 will be empty
Make push operation costly.

push(x) : O(N)
enqueue x to q2
dequeue all elements of q1 to q2
swap q1 & q2
pop() : O(1)
dequeue item from q1 and return it

Method 2 :

q1 will hold all items in FIFO order, q2 will be empty
Make pop operation costly.

push(x) : O(1)
enqueu x to q1
pop() : O(N)
dequeue all elements except last element in q1 to q2
dequeue last element from q1
swap q1 and q2
return the last element
*/

// Stack using Single Queue
// All you need to do is either maintain LIFO order in the queue or 
// maintain FIFO order and pop the last element in the queue and maintain the order
/*
Method 1 :
Make push operation costly

push(x) : O(N)
s = size of queue
enqueu x to queue
dequeue s elements from queue and enqueue them

pop() : O(1)
dequeue first element and return it

Method 2 :
Make pop operation costly

push(x) : O(1)
enqueue x to queue

pop() : O(N)
s = size of queue
dequeue (s-1) elements from queue and enqueue them
dequeue the top element and return it
*/