#include <bits/stdc++.h>
using namespace std;

/*

https://www.geeksforgeeks.org/next-greater-element/

1 3 0 0 1 2 4
3 4 1 1 2 4 -1

O(N) Time | O(N) Space
*/

void NearestGreaterToRight(vector<int> arr) {
    int NGR[arr.size()];
    stack<int> st;
    for (int i = arr.size() - 1; i >= 0; --i) {
        while (!st.empty() && arr[i] >= st.top())
            st.pop();
        NGR[i] = st.empty() ? -1 : st.top();
        st.push(arr[i]);
    }
}