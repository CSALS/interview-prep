#include <bits/stdc++.h>
using namespace std;

/**
 * @question
 * https://leetcode.com/problems/min-stack/
 * 
 * @approachs
 * 1. Using One Additional Stack store minimum element at that level
 * 2. No Extra Space
 */

stack<int> st1;  //Holds the actual elements
stack<int> st2;  //Holds minimum element at each level
void push(int x) {
    st1.push(x);
    if (st2.empty())
        st2.push(x);
    else {
        if (x < st2.top()) st2.push(x);
    }
}
void pop() {
    if (st1.empty() || st2.empty()) return;
    int y = st1.top();
    st1.pop();
    if (y == st2.top()) st2.pop();
}
int getMin() {
    if (st1.empty() || st2.empty()) return -1;
    return st2.top();
}

/**
 * No Extra Space =>
 * Use global variable minElement to store the current minimum element in the stack.
 * Push(x) =
 * If x > minEle then push(x)
 * If x < minEle then push(2 * x - minEle) and make minEle = x
 * Pop() =
 * If y > minEle then y isn't minEle so we can just pop()
 * If y < minEle then y is the minEle which we are trying to remove
 * Why? y = x + (x - minEle) since x < minEle we get y = x + (some -ve value) so y < x where x was the new min value
 * So retrieve the old minimum value using 2 * minEle - y and pop the element
 */

stack<int> st;
int minEle;

void push(int x) {
    if (st.empty()) {
        minEle = x;
        st.push(x);
    } else if (x < minEle) {
        st.push(2 * x - minEle);
        minEle = x;
    } else {
        st.push(x);
    }
}

void pop() {
    if (st.empty()) {
        cout << "Stack is empty\n";
        return;
    }
    int y = st.top();
    st.pop();

    if (y < minEle) {
        minEle = 2 * minEle - y;
    }
}

int getMin() {
    if (st.empty()) {
        cout << "Stack is empty\n";
        return -1;
    } else
        return minEle;
}

int top() {
    if (st.empty()) {
        return -1;
    }

    int y = st.top();
    if (y < minEle) {
        return minEle;
    } else {
        return y;
    }
}