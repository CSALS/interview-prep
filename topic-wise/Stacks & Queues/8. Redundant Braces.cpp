#include <bits/stdc++.h>
using namespace std;

/**
 * Returns true if there are redundant braces
 * 1. stack<string> Not space efficient. If expression is very large then we might end up storing huge data
 * 2. Instead can use only stack<char>
 */

int braces1(string str) {
    stack<string> st;
    for (char c : str) {
        if (c != ')') {
            string str = "";
            str += c;
            st.push(str);
        } else {
            int pop_count = 0;
            string expr = "";
            while (st.top() != "(") {
                pop_count++;
                expr = st.top() + expr;
                st.pop();
            }
            st.pop();
            if (pop_count == 1) {
                if (expr.length() == 1 || expr[0] == '(')
                    return 1;
            }
            expr = "(" + expr + ")";
            st.push(expr);
        }
    }
    return 0;
}

int braces2(string str) {
    stack<char> st;
    for (char c : str) {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'B')) {
            continue;
        }

        if (c == '(') {
            st.push(c);
        } else if (c == '+' || c == '-' || c == '*' || c == '/') {
            st.push(c);
        } else if (c == ')') {
            //If top itself is ( then we are placing redundant braces around an expression which was already popped
            if (st.top() == '(') return true;
            //pop all operators
            while (st.top() != '(') st.pop();
            st.pop();  //pop the (
        }
    }
    return false;
}