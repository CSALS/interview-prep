#include <bits/stdc++.h>
using namespace std;

//EVERY ELEMENT OCCURS TWICE EXCEPT ONE
int singleNumber1(vector<int>& nums) {
    //1. O(N) time & space
    //         unordered_map<int, int> freq;
    //         for(int num : nums) freq[num]++;

    //         for(auto it = freq.begin(); it != freq.end(); ++it)
    //             if(it->second == 1) return it->first;

    //2. O(N) time & O(1) space
    int result = 0;
    for (int num : nums) result ^= num;
    return result;
}

//EVERY ELEMENT OCCURS TWICE EXCEPT TWO
vector<int> singleNumber3(vector<int>& nums) {
    /*
            We xor all elements
            we get a ^ b which will be like 010001010
            find the first bit where it is 1 that means
            either a or b at that bit will have 1 the other
            will have 0
            so you can separate them into two groups
            you will have (x1,x1,x2,x2,...a)
            and (y1,y1,y2,y2.....,b) and you can xor those groups
        */
    int allXOR = 0;
    for (int num : nums) allXOR ^= num;

    for (int i = 0; i <= 31; ++i) {
        //if ith bit is set
        if (allXOR & (1 << i)) {
            int a = 0, b = 0;
            for (int num : nums) {
                //if ith bit is set then xor in a else b
                if (num & (1 << i)) {
                    a ^= num;
                } else {
                    b ^= num;
                }
            }
            return {a, b};
        }
    }
    return {};
}

//EVERY NUMBER OCCURS THRICE EXCEPT ONE
int singleNumber2(vector<int>& nums) {
    int result = 0;
    //Sum of ith bit of all numbers = 3 * x + y
    //where y is the ith bit for the number we want
    for (int i = 0; i <= 31; ++i) {
        int sum = 0;
        for (int num : nums) {
            if (num & (1 << i)) ++sum;
        }
        if (sum % 3 == 1) {
            result = result | (1 << i);
        }
    }
    return result;
}

/*
MISSING NUMBER https://www.geeksforgeeks.org/find-the-missing-number/
Given n-1 integers in the range of 1 to n
no duplicates. so one is missing. find that one

    1. XOR of (1 to n) and (array elements) will give the answer
    2. can use sum of 1 to n formula
*/

/*
REPEATING & MISSING https://www.geeksforgeeks.org/find-a-repeating-and-a-missing-number/

Given n integers in the range of 1 to n
where 1 number occurs twice. That means one number is missing
Find the missing number and repeating number

    1. Can maintain hash of array[1 to n]
    2. modifying array. marking presence of arr[i] in its index
    3. 1 + 2 + 3 + ... n = n(n+1)/2 = S
       1^2 + 2^2 + .... = n(n+1)(2n+1)/6 = S^2
       S - (array of elements sum) = X (missing) - Y (repeating)
       S^2 - (array of elements sum squares) = X^2 - Y^2
       after this you get X+Y and solve the equations

    4. xor of (1,2,3...N) and (array elements) => X ^ Y
       in X^Y find the bit where it is 1 and you can separe out elements based on that bit
       from both (1,2,3,...N) and (array elements)
       and take xor of both buckets you will get A, B
       You can travel the array again to find the repeating number
*/