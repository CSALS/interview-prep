- Number of set bits
```
while(n > 0) {
    set_bits += (n & 1);
    n = n >> 1;
}
```

- Important Bitmasking Functions

```
i power of 2 ->>>  (1 << i)
get ith bit ->>>>  return n & (1 << i)
set ith bit ->>>>  n = n | (1 << i)
clear ith bit ->>> n = n & ~(1 << i)
```

Identify =
Powers of 2, bits, xor, traversing 32 bits of integer which is constant time.

Problems on number occuring twice/thrice/missing like that =

x ^ 0 = x
x ^ x = 0
- Every number occurs twice except one number. (Use XOR. linear time | constant space) <br>
```General = every number occurs even times except one number which occurs odd number of times```
- Every number occurs twice except two distinct numbers (XOR + Bit Masking. linear time | constant space) <br>
```General = every number occurs even times except two distinct number which occurs odd number of times```
- Every number occurs thrice except one number. (Bit Masking.  N*32 time | constant space)
- https://www.geeksforgeeks.org/find-the-missing-number/ (XOR. Linear Time | Constant Space)



Other Problems = 
- How many min. jumps needed from 0th floor to nth floor if jumps can be done in powers of 2?