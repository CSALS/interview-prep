#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-the-element-that-appears-once-in-a-sorted-array/
 * https://www.interviewbit.com/problems/single-element-in-a-sorted-array/
 * In sorted array all elements occur twice except one. Find that element
 * 1. Linear approach = Take XOR of all elements. O(N) Time. But we are not using the fact that array is sorted
 * 2. Binary search = O(logN) 
 */
int findElementOccuringOnce(vector<int> &arr, int n) {
    int low = 0, high = n - 1;
    /*
    IMP Observation->
        To the left of that element all first occureneces are in even index
        To the right of that element all first occureneces are in odd index
        We will use this to reduce search space
    */
    while (low <= high) {
        int mid = low + (high - low) / 2;
        int firstInd = -1;
        if (mid != n - 1 && arr[mid] == arr[mid + 1]) {
            firstInd = mid;
        } else if (mid != 0 && arr[mid - 1] == arr[mid]) {
            firstInd = mid - 1;
        }
        //arr[mid] has no duplicate on left or right side, so it is the required element
        if (firstInd == -1)
            return arr[mid];

        if (firstInd & 1) {
            //We are in right portion so go left
            high = mid - 1;
        } else {
            //We are in left portion so go right
            low = mid + 1;
        }
    }
    return -1;
}
