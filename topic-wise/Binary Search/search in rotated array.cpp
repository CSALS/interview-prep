#include <bits/stdc++.h>
using namespace std;

//TODO: Try to do them only in one binary search

//https://leetcode.com/problems/search-in-rotated-sorted-array/
class WithoutDuplicates {
   public:
    int binarySearch(int lo, int hi, vector<int>& nums, int target) {
        while (lo < hi) {
            int mid = lo + (hi - lo + 1) / 2;
            if (nums[mid] <= target)
                lo = mid;
            else
                hi = mid - 1;
        }
        return (nums[lo] == target) ? lo : -1;
    }

    int search(vector<int>& nums, int target) {
        int n = nums.size();
        if (n == 0) return -1;
        int lo = 0, hi = n - 1;

        if (nums[0] <= nums[n - 1]) return binarySearch(0, n - 1, nums, target);  //unrotated array / single element array
        while (lo < hi) {
            int mid = lo + (hi - lo + 1) / 2;
            if (nums[mid] < nums[0])
                hi = mid - 1;
            else
                lo = mid;
        }
        //lo -> contains last F
        int lastF = lo, firstT = lo + 1;
        if (target > nums[n - 1]) {
            //search in F
            return (binarySearch(0, lastF, nums, target));
        } else {
            // search in T
            return (binarySearch(firstT, n - 1, nums, target));
        }
    }

    //Only one binary search
    int search2(vector<int>& nums, int target) {
        int n = nums.size();
        int low = 0, high = n - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            /*
                At mid
                either [low...mid] will be increasing 
                or [mid...high] will be increasing
                So find that strictly increasing subarray to reduce search space
            */
            if (nums[low] <= nums[mid]) {  //nums[low...mid] is sorted
                if (nums[low] <= target && target <= nums[mid]) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            } else {  //nums[mid...high] is sorted
                if (nums[mid] <= target && target <= nums[high]) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
            }
        }
        return -1;
    }
};

//https://leetcode.com/problems/search-in-rotated-sorted-array-ii
//After removing duplicates from back we can use only single binary search to get answer
class WithDuplicates {
   public:
    //In the worst case O(N) Time (due to duplicates)
    bool search(vector<int>& nums, int target) {
        /*
            Our predicate when there were no duplicates was arr[mid] < arr[0]
            This will give us FFFFTTTT and search for last F
            But due to duplicates we might get FFFTTTFFF when last set of elements are same first element
            So skip all those duplicates and then do the search
        */
        int n = nums.size();
        if (n == 0)
            return false;
        if (n == 1)
            return nums[0] == target;
        if (n == 2)
            return nums[0] == target || nums[1] == target;

        //Unrotated array
        if (nums[0] < nums[n - 1])
            return binary_search(nums.begin(), nums.end(), target);

        int low = 0, high = n - 1;
        //Skip duplicates of first element in the rightmost portion
        for (; high >= 0; --high) {
            if (nums[high] != nums[0])
                break;
        }
        //Search for pivot in [0...high]
        if (high == 0)
            return nums[0] == target;

        while (low < high) {
            int mid = low + (high - low + 1) / 2;
            if (nums[mid] < nums[0])
                high = mid - 1;
            else
                low = mid;
        }

        int pivot = low;
        //FFFFFTTTTTT
        if (target < nums[0]) {
            //Search in TTTTT
            return binary_search(nums.begin() + pivot + 1, nums.end(), target);
        } else {
            //Search in FFFFFF
            return binary_search(nums.begin(), nums.begin() + pivot + 1, target);
        }
    }
};