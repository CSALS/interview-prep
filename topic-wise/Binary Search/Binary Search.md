# Identify
- For median there should n/2 elements less than it.
- If search space is sorted and you are using linear search then use binary search
- If given 'sorted' keyword in the problem then think of binary search. 

## if we know the range of answer then binary search on that range
- some dp problems can be done using BS-answer if range is known
- Binary Search On Answer = Either unsorted array or no array will be given (The answer search space will be sorted)
- Discrete Binary Search For Optimization Problems = **Maximize X on some constraint based on Y or minimize the maximum**
- Ex = Max X keeping Y as min
- Ex = Min Weight such that each day sum of weights can't exceed that weight which means it is maximum weight possible on each day. So minmized based on constraint so BS (Leetcode 1011)
- For some dp problems we can BS on answer if answer range is known


https://www.geeksforgeeks.org/search-an-element-in-a-sorted-and-pivoted-array/
https://www.geeksforgeeks.org/median-of-two-sorted-arrays/
https://www.geeksforgeeks.org/find-rotation-count-rotated-sorted-array/

https://www.geeksforgeeks.org/find-bitonic-point-given-bitonic-sequence/(finding pivot)

https://www.geeksforgeeks.org/find-the-row-with-maximum-number-1s/ (search in 2d type variation)

search in row-wise sorted matrix (2 variations)
median in row-wise sorted matrix


# IMP
- power(x, n), square root, nth root
- kth smallest element, median of array, median in matrix
- Book allocation, Painters partition, Split array, min. no of items to be delieverd
- Aggressive Cows, Max. Height of staircase
- Maximum Median


# MODELS
## 1st Type

1. Sorted Rotated Array - Without Duplicates & With Duplicates
2. https://www.interviewbit.com/problems/maximum-depth/?ref=random-problem

## 2nd Type

1. https://www.geeksforgeeks.org/square-root-of-an-integer/ (No Array Given)
2. https://leetcode.com/problems/find-peak-element/ (Unsorted Array)

## 3rd Type (Binary Search on the answer)

1. Book Allocation Problem, Aggressive Cows, Painters Partition
2. https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/
3. https://www.interviewbit.com/problems/maximum-height-of-the-staircase/ (Binary Searching On Quadractic Equation Solution)
4. https://www.interviewbit.com/problems/maximum-median/ (Awesome Application Of Binary Search)
5. https://www.interviewbit.com/problems/fruit-packets/ (Optimization Based On Constraint)
6. https://www.geeksforgeeks.org/minimum-number-of-items-to-be-delivered/ (Optimization Based On Constraint)
7. https://codeforces.com/problemset/problem/807/C (really good example on discrete binary search)


# todo
https://www.youtube.com/watch?v=JMHL9geRAKI&list=PLJtzaiEpVo2wrUwkvexbC-vbUqVIy7qC-&index=2&t=0s

# Functions =
1. lower_bound(arr.begin(), arr.end(), val) = 1st index value >= val
if no element in arr which is >= val then returns length of array as the result
2. upper_bound(arr.begin(), arr.end(), val) = 1st index value > val
if no element in arr which is > val then returns length of array as the result
3. equal_range(arr.begin(), arr.end(), val) = returns pair of lower_bound(val), upper_bound(val). That range whose values are "val"
4. binary_search(arr.begin(), arr.end(), value) = true or false

# Templates =

1. Normal predicate based binary search
If predicates makes search space as TTTTFFFFF or FFFFTTTT then we can apply BS
```
//Search space = TTTTFFFF. We can search for last T or first F
while (low < high) {
    int mid = low + (high - low + 1)/2;
    if(predicate(mid)) {
        //We want last T
        low = mid
    } else {
        high = mid - 1 //when you subtract here then make sure to add 1 to high-low in mid to avoid infinite loop (check for 2 element case)
    }
}
if(predicate(low))
    return low
else
    return high
```

2. Binary Search on answer / discrete binary search
Finding max/min based on a constraint
```
//Say we want to find largest possible answer. The search space is the values possible for answer
int ans = INT_MIN
while(low <= high) {
    int mid = (low + high)/2 //always same. No need to add 1 if you subtract 1 in high calculation like above case

    //Write boolean function to check if "mid" can be the answere (should follow the constraint in the problem) 
    
    //For getting maximum case
    if(isPossible(mid, input_array)) {
        ans = max(ans, mid)
        low = mid + 1
    } else {
        high = mid - 1
    }

    //For getting minimum case
    if(isPossible(mid, input_array)) {
        ans = min(ans, mid)
        high = mid - 1
    } else {
        low = mid + 1
    }
}
return ans
```

3. Binary Search on floating point values
```
double low = ..., high = ...
double epsilon = 1e-6 //since low == high for floating values doesn't make sense due to precision issues
while(high - low >= epsilon) {
    double mid = (low + high)/2

    if(isPossible(mid, input_array)) {
        low = mid
    } else {
        high = mid
    }
}
```


