//https://www.spoj.com/problems/SUBS/

#include <cstring>
#include <iostream>
using namespace std;

//Is X^M a subsequence of Y?
bool isPossible(int M, string X, string Y) {
    int x_ind = 0;
    int M_ct = M;
    for (char c : Y) {
        if (X[x_ind] == c) {
            M_ct--;
            if (M_ct == 0) {
                x_ind++;
                M_ct = M;
            }
        }
    }
    return x_ind == X.length();
}

int maximumMValue(string X, string Y) {
    //Maximum M such that X^M is subsequence of Y
    //1. Go from 0, 1, 2, 3... and check the maximum M
    //2. Instead of linear, do binary search.
    int low = 1;                             //least M value is 0. If from 1 to high there's no answer found then it is 0
    int high = Y.length() / X.length() + 1;  //maximum M value
    int ans = 0;
    while (low <= high) {
        int mid = low + (high - low) / 2;
        if (isPossible(mid, X, Y)) {
            ans = mid;
            low = mid + 1;
        } else {
            high = mid - 1;
        }
    }
    return ans;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        string X, Y;
        cin >> X >> Y;
        cout << maximumMValue(X, Y) << endl;
    }
}