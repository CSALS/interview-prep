//https://www.interviewbit.com/problems/leftover/
#include <bits/stdc++.h>
using namespace std;

//Is it possible to make all array elements as "value"?
bool isPossible(int value, int B, vector<int> &A) {
    int incr = 0, decr = 0;
    for (int x : A) {
        if (x > value) {
            decr += (x - value);
        } else {
            incr += (value - x);
        }
    }
    int incrPossible = decr - decr * B / 100;
    return incr <= incrPossible;
}

//Binary search on "x" where x will be the final element in array
//Find maximum "x"
int solve(vector<int> &A, int B) {
    int low = INT_MAX, high = INT_MIN;
    for (int num : A) {
        low = min(low, num);
        high = max(high, num);
    }

    int ans = -1;
    while (low <= high) {
        int mid = (low + high) / 2;
        if (isPossible(mid, B, A)) {
            ans = mid;
            low = mid + 1;
        } else {
            high = mid - 1;
        }
    }
    return ans;
}
