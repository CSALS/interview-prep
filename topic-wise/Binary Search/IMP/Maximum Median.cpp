#include <bits/stdc++.h>
using namespace std;

/*
    https://codeforces.com/problemset/problem/1201/C
    You are given a SORTED array A of N integers, where N is odd. You can make the following operation with it:
        Choose one of the elements of the array (for example ai) and increase it by 1
    Return the largest possible median of the array using at most B operations.
*/
#define ll long long

//Can make all elements in 2nd half which are < median => equal to median with only these many "operations"
bool isMedian(vector<int> &A, ll operations, ll median) {
    ll requiredOperations = 0;
    for (int i = A.size() / 2; i < A.size(); ++i) {
        requiredOperations += max((ll)0, median - A[i]);
    }
    return requiredOperations <= operations;
}

string solve(vector<int> &A, string B) {
    sort(A.begin(), A.end());
    ll b = stoll(B);
    //Only change [n/2 ..... n -1] since changing 1st half of elements is not needed (waste of opeartions, order might change)
    ll low = A[A.size() / 2];       //min median we can get is the original median
    ll high = A[A.size() - 1] + b;  //max median we can get is b + last element
    ll ans = 0;
    while (low <= high) {
        ll mid = low + (high - low) / 2;
        if (isMedian(A, b, mid)) {
            ans = max(ans, mid);
            low = mid + 1;
        } else {
            high = mid - 1;
        }
    }
    return to_string(ans);
}
