

/*
https://codeforces.com/problemset/problem/807/C

    Identifying it as BS problem is difficult

    optimization problem (need to find smallest no. of submissions)
    based on a constraint (need to finally make p/q ratio)

    Really good example of discrete binary search and on how the isSafe() should be
*/

#include <bits/stdc++.h>
using namespace std;
#define INF LONG_MAX
#define int long long
#define deb(x) cout << #x << " " << x << endl;

bool isSafe(int t, int x, int y, int p, int q) {
    if (x <= p * t && y - x <= q * t - p * t) return true;
    return false;
}

int solution() {
    int x, y;
    cin >> x >> y;
    int p, q;
    cin >> p >> q;

    int g1 = __gcd(x, y);
    if (x / g1 == p && y / g1 == q) return 0;

    /*
        we want pt success, qt-pt failure
        we need to find least t sucht that x<=pt and y-x<=qt-pt 
        this will give us TTTTTTTT...FFFFF we want last T
    */
    int low = 0;
    int high = 1e10;
    while (low < high) {
        int mid = low + (high - low) / 2;
        if (isSafe(mid, x, y, p, q)) {
            high = mid;
        } else {
            low = mid + 1;
        }
    }
    // cout << low << endl;
    if (low == 1e10) return -1;
    // if (x == p * low && y - x == q * low - p * low) {
    return q * low - y;
    // }
    // return -1;
}

int32_t main() {
    int t = 1;
    cin >> t;
    while (t--) {
        // solution();
        cout << solution() << endl;
    }
    return 0;
}