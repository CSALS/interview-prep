#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/
 * Minimize the maximim weight capacity => BS-ANSWER
 */
class Solution {
   public:
    int shipWithinDays(vector<int>& weights, int D) {
        int low = 1, high = 0;
        for (int wt : weights) high += wt;

        int answer = INT_MAX;
        while (low <= high) {
            int mid = (low + high) / 2;

            int daysNeeded = calculateDaysNeeded(weights, mid, D);

            //if can ship within D days decrease capacity to get lesser answer
            if (daysNeeded <= D) {
                answer = min(answer, mid);
                high = mid - 1;
            }
            //if can't ship within D days increase capacity
            else {
                low = mid + 1;
            }
        }
        return answer;
    }

   private:
    //return the number of days needed to ship all packages if maxWeight is the
    //maximum possible weight for each day
    int calculateDaysNeeded(vector<int>& weights, int maxWeight, int D) {
        int days = 1;
        int currWeight = weights[0];
        if (currWeight > maxWeight) {
            return D + 1;
        }

        for (int i = 1; i < weights.size(); ++i) {
            //try to include as much weight as possible on one day
            if (currWeight + weights[i] <= maxWeight) {
                currWeight += weights[i];
            } else {
                days++;
                currWeight = weights[i];
                if (currWeight > maxWeight) {
                    return D + 1;
                }
            }
        }
        return days;
    }
};