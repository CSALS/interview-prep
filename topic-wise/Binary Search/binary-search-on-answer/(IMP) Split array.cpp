#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/split-array-largest-sum/
 * 
 * Given an array which consists of non-negative integers and an integer m, 
 * you can split the array into m non-empty continuous subarrays. 
 * Write an algorithm to minimize the largest sum among these m subarrays. 
 * 
 * 1. There's a DP Approach
 * 2. Since answer range is fixed [1.......sum of array] it won't go outside these so try BS on answer
 *    Minimze the max. => BS on answer
 */

class DynamicProgramming {
    /*
        Before tried dp[left][right][m]
        but let's say you split at some K then calling two separate subproblems with what m value?
        doesn't make sense.
    */
   public:
    //O(N*N*M) T | O(N*M) S
    int splitArray(vector<int>& nums, int m) {
        if (nums.empty()) return 0;

        prefixSum.push_back(nums[0]);
        for (int i = 1; i < nums.size(); ++i) {
            prefixSum.push_back(nums[i] + prefixSum[i - 1]);
        }

        dp.resize(nums.size(), vector<long long>(m + 1, -1));
        return splitArrayRecursive(nums, m, 0);
    }

   private:
    vector<long long> prefixSum;
    vector<vector<long long>> dp;
    //dp[start][m] = min. of max. sum by splitting arr[start...N - 1] into m subarrays
    long long splitArrayRecursive(vector<int>& nums,
                                  int m,
                                  int start) {
        int N = nums.size();
        if (start >= N) return 0;

        //just want one subarray
        if (m == 1) {
            return sum(start, N - 1);
        }

        if (dp[start][m] != -1) {
            return dp[start][m];
        }

        dp[start][m] = LONG_MAX;
        //choosing which index to split the array[start...N-1]
        for (int splitIndex = start; splitIndex <= N - 1; ++splitIndex) {
            //[start...splitIndex] is one subarray and iterate for [splitIndex+1...N-1] for m-1 subarrays
            long long currSum = max(sum(start, splitIndex), splitArrayRecursive(nums, m - 1, splitIndex + 1));
            dp[start][m] = min(dp[start][m], currSum);
        }
        return dp[start][m];
    }

    long long sum(int left, int right) {
        return prefixSum[right] - (left != 0 ? prefixSum[left - 1] : 0);
    }
};

class BinarySearch {
   public:
    int splitArray(vector<int>& nums, int m) {
        if (nums.empty()) return 0;

        long long low = 1, high = 0;
        for (int num : nums) high += num;

        long long answer = INT_MAX;
        while (low <= high) {
            long long mid = (low + high) / 2;

            int subarraysNeeded = calculateSubarraysWithMaximumSumFixed(nums, mid, m);

            /* IMP
                We persist result even if subarraysNeeded < m
                say we want 5 subarrays with mid as maxSum 
                we can get 2 subarrays in the case when we try to accomadate as much sum as possible
                So that 2 is in worst-case. we can easily expand that 2 subarrays to 5 without violating the constraint
            */
            if (subarraysNeeded <= m) {
                answer = min(answer, mid);
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return answer;
    }

   private:
    int calculateSubarraysWithMaximumSumFixed(vector<int>& nums, int maxSum, int m) {
        int subarraysNeeded = 1;
        long long currSum = nums[0];

        if (currSum > maxSum) {
            return m + 1;
        }

        for (int i = 1; i < nums.size(); ++i) {
            //try to include as much sum as possible in one subarray
            if (currSum + nums[i] <= maxSum) {
                currSum += nums[i];
            }
            //we need new subarray
            else {
                subarraysNeeded++;
                currSum = nums[i];
                if (currSum > maxSum) {
                    return m + 1;
                }
            }
        }
        return subarraysNeeded;
    }
};