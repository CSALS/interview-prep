#include <bits/stdc++.h>
using namespace std;

void nthRootRealNumber(long double x, int n) {
    long double low = 1, high = x;
    long double eplison = 1e-6;
    //say we want error range 0.001 then use epsilon as 0.001
    while (high - low >= eplison) {
        long double mid = (low + high) / 2;
        if (pow(mid, n) <= x) {
            low = mid;
        } else {
            high = mid;
        }
    }
    cout << low << setprecision(5) << endl;
}

//Unit tests
int main() {
    nthRootRealNumber(5, 3);
    nthRootRealNumber(5, 2);
    nthRootRealNumber(10, 2);
    nthRootRealNumber(10, 4);
}