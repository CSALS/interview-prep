#include <bits/stdc++.h>
using namespace std;
#define ll long long

/*
    https://www.interviewbit.com/problems/maximum-height-of-the-staircase/
    Given an integer A representing the square blocks. The height of each square block is 1.
    The task is to create a staircase of max height using these blocks.
    The first stair would require only one block, the second stair would require two blocks and so on.
    Find and return the maximum height of the staircase.
*/
int solve(int blocks) {
    //Binary search on "max. height"
    ll low = 1, high = blocks;
    ll ans = 0;
    while (low <= high) {
        ll mid = low + (high - low) / 2;
        //mid is the max. height we want
        if (1LL * mid * mid + mid <= 1LL * blocks * 2) {
            ans = max(ans, mid);
            low = mid + 1;
        } else {
            high = mid - 1;
        }
    }
    return ans;
}