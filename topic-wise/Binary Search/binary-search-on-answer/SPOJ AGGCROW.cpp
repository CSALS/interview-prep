#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

//https://www.spoj.com/problems/AGGRCOW/
bool isPossible(vector<int> &pos, int cows, int minDist) {
    int prev = pos[0];
    int cowsAssigned = 1;  //1 cow assigned to 0th stall
    for (int i = 1; i < pos.size(); ++i) {
        if (pos[i] - prev >= minDist) {
            cowsAssigned++;
            prev = pos[i];
            //If assigned all cows
            if (cowsAssigned == cows)
                return true;
        }
    }
    return false;
}

int solution() {
    int n, c;
    scanf("%d %d", &n, &c);
    vector<int> pos(n);
    for (int &x : pos)
        scanf("%d", &x);
    sort(pos.begin(), pos.end());
    int lo = 0, hi = pos[pos.size() - 1] - pos[0];
    int ans = -1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (isPossible(pos, c, mid)) {
            ans = max(ans, mid);
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }
    }
    return ans;
}

int main() {
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);
    // cout.tie(0);
    int t;
    scanf("%d", &t);
    while (t--) {
        printf("%d\n", solution());
    }
    return 0;
}