#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
#define int long long

/**
 * https://www.spoj.com/problems/SUBSUMS/
 * Count number of subsets whose sum is in range of [A, B]
 * Efficient = meet in middle (somewhat similar to divide and conquer)
 * Divide the array into 2 halfs. Generate all subset sums and store it in a vector for each half of the given array.
 * We are doing this because 2*2^17 is much better than 2^34 (Note: n<=34) 
 * Now using binary search we have to find all subsets that will provide a sum in the range [A,B]
 */

//1. Brute-Force O(2^N) Time | O(1) Space
int countSubsetSumInRange1(vector<int> &arr, int n, int A, int B) {
    int result = 0;
    //includedMap = 1101 then 0, 2, 3 element included in subset but not 1st
    for (int includedMap = 0; includedMap < (1 << n); ++includedMap) {
        int subsetSum = 0;
        for (int index = 0; index < n; ++index) {
            //if index bit is set in includedMap then this element is included
            if (includedMap & (1 << index)) {
                subsetSum += arr[index];
            }
        }
        if (subsetSum >= A && subsetSum <= B)
            result++;
    }
    return result;
}

vector<int> getSubsetSums(vector<int> &arr, int n, bool firstHalf) {
    vector<int> result;
    // cout << "subsetsStart\n";
    for (int includedMap = 0; includedMap < (1 << n); ++includedMap) {
        int subsetSum = 0;
        for (int index = 0; index < n; ++index) {
            //if index bit is set in includedMap then this element is included
            if (includedMap & (1 << index)) {
                if (firstHalf)
                    subsetSum += arr[index];
                else
                    subsetSum += arr[index + arr.size() / 2];
            }
        }
        // cout << endl;
        result.push_back(subsetSum);
    }
    // cout << "subsetsEnd\n";
    return result;
}

//O(2^N/2 + 2^N/2) Time which is much smaller than O(2^N) for large inputs
int countSubsetSumInRange2(vector<int> &arr, int n, int A, int B) {
    int result = 0;

    //Divide array into two halfs and compute subset sum for both of them separately
    int n1 = n / 2;
    int n2 = n - n / 2;

    vector<int> subsetSum1 = getSubsetSums(arr, n1, true);
    vector<int> subsetSum2 = getSubsetSums(arr, n2, false);

    sort(subsetSum1.begin(), subsetSum1.end());

    //We want to find y in subsetSum1 s.t x + y is in [A, B]
    //y <= B - x and y >= x - A
    for (int x : subsetSum2) {
        int upperInd = upper_bound(subsetSum1.begin(), subsetSum1.end(), B - x) - subsetSum1.begin();
        int lowerInd = lower_bound(subsetSum1.begin(), subsetSum1.end(), A - x) - subsetSum1.begin();
        result += (upperInd - lowerInd);
    }
    return result;
}

int32_t main() {
    int n;
    cin >> n;
    int a, b;
    cin >> a >> b;
    vector<int> arr(n);
    for (int &x : arr) cin >> x;
    //
    cout << countSubsetSumInRange2(arr, n, a, b);
}