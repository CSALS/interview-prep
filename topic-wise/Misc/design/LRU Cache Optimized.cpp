#include <bits/stdc++.h>
using namespace std;

//you need to maintain doubly linked list since you need to remove arbitrary node from it. With plain linked list, you wont be able to do it.

class DLLNode {
   public:
    int key;
    int value;
    DLLNode* prev;
    DLLNode* next;

    DLLNode(int key, int value) {
        this->key = key;
        this->value = value;
        prev = next = NULL;
    }
};

class DLL {
    DLLNode* head;  //dummyNode
    DLLNode* tail;  //dummyNode

   public:
    DLL() {
        head = tail = new DLLNode(-1, -1);
        head->next = tail;
        tail->prev = head;
    }

    void addNodeAtLast(int key, int value) {
        DLLNode* newNode = new DLLNode(key, value);
        tail->prev->next = newNode;
        newNode->prev = tail->prev;
        tail->prev = newNode;
        newNode->next = tail;
    }

    void detatchNode(DLLNode* node) {
        DLLNode* prev = node->prev;
        DLLNode* next = node->next;
        prev->next = next;
        next->prev = prev;
    }

    DLLNode* getHead() {
        return head;
    }

    void setHead(DLLNode* head) {
        this->head = head;
    }

    DLLNode* getTail() {
        return tail;
    }

    void setTail(DLLNode* tail) {
        this->tail = tail;
    }
};

class LRUCache {
   private:
    int capacity;
    unordered_map<int, DLLNode*> nodeMap;  //key -> node pointer
    DLL dll;                               //list in the order of access time. recently accessed node at the end

   public:
    LRUCache(int capacity) {
        this->capacity = capacity;
    }

    //O(1) Time
    int get(int key) {
        if (nodeMap.find(key) == nodeMap.end()) {
            return -1;
        }

        int value = nodeMap[key]->value;
        dll.detatchNode(nodeMap[key]);
        dll.addNodeAtLast(key, value);
        nodeMap[key] = dll.getTail()->prev;

        return value;
    }

    //O(1) Time
    void put(int key, int value) {
        if (nodeMap.find(key) != nodeMap.end()) {
            dll.detatchNode(nodeMap[key]);
        }

        dll.addNodeAtLast(key, value);
        nodeMap[key] = dll.getTail()->prev;

        if (nodeMap.size() > capacity) {
            DLLNode* head = dll.getHead();
            DLLNode* deleteNode = head->next;
            head->next = deleteNode->next;
            deleteNode->prev = head;
            nodeMap.erase(deleteNode->key);
        }
    }
};

int main() {
    LRUCache cache(2);

    cache.put(1, 1);
    cache.put(2, 2);
    cout << cache.get(1) << endl;  // returns 1
    cache.put(3, 3);               // evicts key 2
    cout << cache.get(2) << endl;  // returns -1 (not found)
    cache.put(4, 4);               // evicts key 1
    cout << cache.get(1) << endl;  // returns -1 (not found)
    cout << cache.get(3) << endl;  // returns 3
    cout << cache.get(4) << endl;  // returns 4
}