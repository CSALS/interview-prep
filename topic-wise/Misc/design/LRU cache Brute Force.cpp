#include <bits/stdc++.h>
using namespace std;

//https://medium.com/@ankitkp88534/implement-your-own-lru-cache-cb80dcb4b6f3
//https://www.interviewbit.com/problems/lru-cache/

class LRUCache {
   private:
    int timer;
    unordered_map<int, int> times;  //latest time when accessed
    unordered_map<int, int> values;
    int cap;

   public:
    LRUCache(int capacity) {
        timer = 0;
        times.clear();
        values.clear();
        cap = capacity;
    }

    int get(int key) {
        if (values.find(key) != values.end()) {
            times[key] = ++timer;
            return values[key];
        }
        return -1;
    }

    void set(int key, int value) {
        times[key] = ++timer;
        values[key] = value;
        if (values.size() <= cap) {
            //cap won't exceed
            return;
        }
        //cap exceeded. remove least recently used item
        int min_time = INT_MAX;
        auto min_it = times.begin();
        for (auto it = times.begin(); it != times.end(); ++it) {
            if (min_time > it->second) {
                min_time = it->second;
                min_it = it;
            }
        }
        int key1 = min_it->first;
        times.erase(min_it);
        auto iter = values.begin();
        for (auto it = values.begin(); it != values.end(); ++it) {
            if (it->first == key1) {
                iter = it;
                break;
            }
        }
        values.erase(iter);
    }
};