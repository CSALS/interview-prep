/*

https://www.geeksforgeeks.org/design-a-stack-with-find-middle-operation/


    Basic idea for Stack design is to use
        1. stl stack
        2. implement it using array
        3. implement it using linked list (SLL, DLL)

    Here you need to findMiddle(), deleteMiddle() in constant time

    1 2 3 4
    currently 2 is middle
    when you psuh 5 our middle pointer should go to 3 (can be done by SLL, array)
    when you pop 5 our middle pointer should go back to 2 (can be done by DLL, array and not SLL since you can't go back)

    delete in middle in array is not O(1) but any arbitary node can be deleted in DLL in O(1)

    so use DLL

*/