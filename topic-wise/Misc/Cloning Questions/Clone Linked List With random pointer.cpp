#include <bits/stdc++.h>
using namespace std;

struct Node {
    int data;
    Node* next;
    Node* random;
    Node(int x) {
        data = x;
        next = random = NULL;
    }
};

/**
 * 1st approach would be to use hashing
 * OriginalNode -> ClonedNode
 * Firstly create clone and keep mapping.
 * After that assign random pointers
 * 2nd approach would be to modify linked list itself
 */

// O(N) time | O(1) space
Node* copyList(Node* head) {
    if (!head) return head;

    // Insert clone nodes
    for (Node* curr = head; curr != NULL;) {
        Node* next = curr->next;
        curr->next = new Node(curr->data);
        curr->next->next = next;
        curr = next;
    }
    // Set the random ptr of clone nodes
    for (Node* curr = head; curr != NULL; curr = curr->next->next) {
        curr->next->random = (curr->random == NULL) ? NULL : curr->random->next;
    }
    Node *clone = NULL, *last = NULL;
    // Remove clone nodes from original list and make new clone list
    for (Node* curr = head; curr != NULL; curr = curr->next) {
        if (clone == NULL) {
            clone = curr->next;
            last = clone;
        } else {
            last->next = curr->next;
            last = last->next;
        }
        curr->next = curr->next->next;
    }
    last->next = NULL;
    return clone;
}