#include <bits/stdc++.h>
using namespace std;


/*
https://leetcode.com/problems/battleships-in-a-board/

    Although we can do dfs/bfs but we are not using the fact that Xs will be either horizoontal or vertical
*/

class Solution {
public:
    int countBattleships(vector<vector<char>>& board) {
        int count = 0; //will count all upper-left corners
        
        for(int r = 0; r < board.size(); ++r) {
            for(int c = 0; c < board[0].size(); ++c) {
                if(board[r][c] == 'X') {
                    //if this is upper-left then there shouldn't be
                    //X or left side or upper side
                    if(   (r == 0 || board[r - 1][c] != 'X')
                       && (c == 0 || board[r][c - 1] != 'X')
                       ) count++;
                }
            }
        }
        return count;
    }
};