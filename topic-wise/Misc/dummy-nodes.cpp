#include <bits/stdc++.h>
using namespace std;


//https://algorithms.tutorialhorizon.com/double-threaded-binary-tree-complete-implementation/dummy-node/

/*
dummy head and dummy tail - can be done in DLL LRU Cache
*/

/*
MST - Dummy node

https://stackoverflow.com/questions/43483548/minimum-spanninjg-tree-with-vertex-weight-and-edge-weight

Each city is a node. The cost b/w building a road b/w two nodes is given.
Find min. cost to connect all cities using road

    Answer is simple MST


Tricky Variation =
we can build an airport in each city. we can connect two cities if they have airport

    Answer is to add dummy node in the graph called SKY
    cost of building airport in a city = cost of building road from that city node to SKY (edge)

    Now after getting this modified graph we can do MST

*/