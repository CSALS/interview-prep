/*

number of binary numbers without consecutive ones

_ _ _ _ i

at ith bit there are no choices either it is 0 or 1
if it is 1 then previous should be 0

_ _ _ _ 1 => f(n - 1)
_ _ _ 0 1 => f(n - 2)

f(n) = f(n - 1) + f(n - 2)

*/

/*
ATLASSIAN - IMP

Find the number of lists that exist and satisfy the following:
    1.  list contains exactly 'N' elements
    2.  all elements of the list are between 'low' and 'high'
    3.  sum of all elements is even

Return answer modulo 10^9+7

Constraints
1 <= n <= 10^6
1 <= low <= high <= 10^6

Sample
(n=3,low=1,high=2) -> 4
(n=2,low=1,high=10) -> 50


@solution https://www.youtube.com/watch?v=QGJXQAaDs3I

MAKE USE OF THIS
even + even = even
odd + odd = even
even + odd = odd
*/