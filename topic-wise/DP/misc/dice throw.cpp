#include <bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/dice-throw-dp-30/
//https://leetcode.com/problems/number-of-dice-rolls-with-target-sum

#define ll long long
class Solution {
    const int mod = 1e9 + 7;
    int F;
    ll dp[35][1001];

   public:
    ll numRollsToTargetRecursive(int d, int target) {
        if (d == 0) return 0;
        if (d == 1) return 1 <= target && target <= F;

        if (dp[d][target] != -1) return dp[d][target] % mod;
        ll ways = 0;
        for (int f = 1; f <= F; ++f) {
            if (f > target) break;

            ways = (ways % mod + numRollsToTargetRecursive(d - 1, target - f) % mod) % mod;
        }
        return dp[d][target] = ways % mod;
    }
    int numRollsToTarget(int d, int f, int target) {
        this->F = f;
        for (int i = 0; i < 35; ++i)
            for (int j = 0; j < 1001; ++j)
                dp[i][j] = -1;
        return numRollsToTargetRecursive(d, target) % mod;
    }
};