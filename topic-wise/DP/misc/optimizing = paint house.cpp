#include <bits/stdc++.h>
using namespace std;

/*
https://www.lintcode.com/problem/paint-house-ii/description


dp[i][j] = min cost to color [0...i] and ith house with jth color

we will get O(NK^2) solution
*/
int minCostII(vector<vector<int>> &costs) {
    vector<vector<int>> dp;
    int n = costs.size();
    int k = costs[0].size();
    dp.resize(n, vector<int>(k, -1));

    //dp[i][j] = min cost to color [0...i] and ith house with jth color
    for (int j = 0; j < k; ++j) {
        dp[0][j] = costs[0][j];
    }

    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < k; ++j) {
            //dp[i][j] = costs[i][j] + min(dp[i - 1][color] where color is 0...k-1 except j)
            int minCost = INT_MAX;
            for (int color = 0; color < k; ++color) {
                if (color != j) {
                    minCost = min(minCost, dp[i - 1][color]);
                }
            }
            dp[i][j] = minCost + costs[i][j];
        }
    }

    int result = INT_MAX;
    for (int j = 0; j < k; ++j) {
        result = min(result, dp[n - 1][j]);
    }
    return result;
}

/*
http://buttercola.blogspot.com/2015/09/leetcode-paint-house-ii.html

            //This loop is bottleneck
            for (int color = 0; color < k; ++color) {
                if (color != j) {
                    minCost = min(minCost, dp[i - 1][color]);
                }
            }


what we need is minimum of dp[i - 1][0....k-1] and we don't get consider same color cost
so we can maintain two variables min1, min2 (1st two minimum in dp[i - 1][0...k - 1])
and use those values for dp[i][0...k - 1] and after that update them for next row
*/
int minCostII(vector<vector<int>> &costs) {
    if (costs.empty() || costs[0].empty()) {
        return 0;
    }

    vector<vector<int>> dp;
    int n = costs.size();
    int k = costs[0].size();
    dp.resize(n, vector<int>(k, -1));

    //dp[i][j] = min cost to color [0...i] and ith house with jth color
    int min1 = INT_MAX, min2 = INT_MAX, min1Color = -1;
    for (int j = 0; j < k; ++j) {
        dp[0][j] = costs[0][j];
        if (dp[0][j] <= min1) {
            min2 = min1;
            min1 = dp[0][j];
            min1Color = j;
        } else if (dp[0][j] < min2) {
            min2 = dp[0][j];
        }
    }

    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < k; ++j) {
            //dp[i][j] = costs[i][j] + min(dp[i - 1][color] where color is 0...k-1 except j)
            int minCost = INT_MAX;
            // for(int color = 0; color < k; ++color) {
            //     if(color != j) {
            //         minCost = min(minCost, dp[i - 1][color]);
            //     }
            // }
            if (j != min1Color) {
                minCost = min1;
            } else {
                minCost = min2;
            }
            dp[i][j] = minCost + costs[i][j];
        }
        min1 = INT_MAX;
        min2 = INT_MAX;
        for (int j = 0; j < k; ++j) {
            if (dp[i][j] <= min1) {
                min2 = min1;
                min1 = dp[i][j];
                min1Color = j;
            } else if (dp[i][j] < min2) {
                min2 = dp[i][j];
            }
        }
    }

    int result = INT_MAX;
    for (int j = 0; j < k; ++j) {
        result = min(result, dp[n - 1][j]);
    }
    return result;
}

//TODO: https://leetcode.com/problems/paint-house-iii/