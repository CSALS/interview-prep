#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/program-nth-catalan-number/
    1. dp[N] = Σ (i = 1 to N) dp[i - 1] * dp[N - i];
       DP Approach = O(N^2) T | O(N) S

    2. C(n) = 1/(n + 1) * (2n)C(n) => O(N) T | O(1) S
        https://www.geeksforgeeks.org/space-and-time-efficient-binomial-coefficient/ O(r) T | O(1) S to find nCr


Applications
    1. Count unique structured BSTs from 1 to N
            Ans = Catalan(N)
    2. Count Binary trees with labels from 1 to N
            Ans = Catalan(N) * N! [catalan(N) will give unique structured trees and placing 1 to N them is N!]
    3. Count unlabelled binary trees 
            Ans = Catalan(N) (since only structure matters)
    4. Count BTs which can be constructed from given preorder array of length N  
            Ans = catalan(N)

    5. Given a number n, return the number of ways you can draw n chords in a circle with 2 x n points such that no 2 chords intersect.
    6. Count the number of expressions containing n pairs of parentheses which are correctly matched.
    For n = 3, possible expressions are ((())), ()(()), ()()(), (())(), (()()).
        Ans = catalan(N)
        Starting bracket should be opening. Corresponding to that there will be closing somewhere to the right.
        Inside this pair there might be some balanced pairs, outside also there might be some balanced pairs.
        Inside this pair there might be 0, 1, 2, 3... N - 1 pairs and outside will be N - 1, N - 2, N - 3, ... , 0 pairs
        So it is dp[N] = Σ (i = 1 to N) dp[i - 1] * dp[N - i];

    7. intersecting chords in a circle (https://www.interviewbit.com/problems/intersecting-chords-in-a-circle/)
    And many more = https://www.geeksforgeeks.org/applications-of-catalan-numbers/
*/

//count number of unique structured BSTs from 1 to N
int numberOfUniqueBST(int N) {
    int dp[N + 1];
    dp[0] = 1;
    dp[1] = 1;
    for (int n = 2; n <= N; ++n) {
        dp[n] = 0;
        for (int i = 1; i <= n; ++i) {
            //consider i as the root. (i - 1) will be on left side and (n - i) will be on right side since 123..N is inorder for BST (left root right)
            dp[n] += dp[i - 1] * dp[n - i];
        }
    }
    return dp[N];
}

//construct those BSTs
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
vector<TreeNode*> generateTreeRecursive(int L, int R) {
    if (L == 0 || L > R) {
        return {NULL};
    }
    if (L == R) {
        return {new TreeNode(L)};
    }

    vector<TreeNode*> trees;
    for (int rootVal = L; rootVal <= R; ++rootVal) {
        vector<TreeNode*> leftTrees = generateTreeRecursive(L, rootVal - 1);
        vector<TreeNode*> rightTrees = generateTreeRecursive(rootVal + 1, R);
        for (TreeNode* left : leftTrees) {
            for (TreeNode* right : rightTrees) {
                TreeNode* root = new TreeNode(rootVal);
                root->left = left;
                root->right = right;
                trees.push_back(root);
            }
        }
    }
    return trees;
}
vector<TreeNode*> generateTrees(int N) {
    return generateTreeRecursive(1, N);
}