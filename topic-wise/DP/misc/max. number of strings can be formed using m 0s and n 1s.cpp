#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/ones-and-zeroes/

Greedy = sort by smaller length and choose strings
it fails for [111, 1100, 11000] m=5 and n=4
greedy chooses only 111 but actual answer is [1100, 11000]

Use DP since greedy is failing
*/

#define INF 10000000
class Solution {
   public:
    //M 0s and N 1s
    //O(array_size * M * N) T | O(array_size * M * N) S
    int findMaxForm(vector<string>& strs, int M, int N) {
        int maxSize = strs.size();

        //dp[m][n][size] = max strings from 1st size strings in the array using m 0s and n 1s
        int dp[M + 1][N + 1][maxSize + 1];

        //Base Case
        for (int m = 0; m <= M; ++m) {
            for (int n = 0; n <= N; ++n) {
                dp[m][n][0] = 0;
            }
        }

        //recurrence of size depends only on size - 1
        //so optimize is it using only two arrays. knapsack opti type
        for (int size = 1; size <= maxSize; ++size) {
            int one_count = 0, zero_count = 0;
            for (char c : strs[size - 1]) {
                if (c == '0')
                    zero_count++;
                else
                    one_count++;
            }
            for (int m = 0; m <= M; ++m) {
                for (int n = 0; n <= N; ++n) {
                    int temp = (m < zero_count || n < one_count) ? dp[m][n][size - 1] : dp[m - zero_count][n - one_count][size - 1] + 1;
                    dp[m][n][size] = max(dp[m][n][size - 1],
                                         temp);
                }
            }
        }
        return dp[M][N][maxSize];
    }
};