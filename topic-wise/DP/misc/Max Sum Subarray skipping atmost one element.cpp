#define INF 1e9 + 7

int maxSumSubarray(int A[], int n) {
    int dp[n][2];
    // dp[i][0] = max sum subarray ending at ith position and no element skipped
    // dp[i][1] = max sum subarray in [0.....i] and exactly 1 element skipped =
    // can be ith element or some element b/w 0 and i also
    dp[0][0] = A[0];
    dp[0][1] = -INF;  // since we need non-empty subarrays

    int max_sum = max(A[0], -INF);
    for (int i = 1; i < n; ++i) {
        dp[i][0] = max(A[i], A[i] + dp[i - 1][0]);
        dp[i][1] = max(dp[i - 1][0],        //either skip this element
                       A[i] + dp[i - 1][1]  //consider this element and skip some element in the left subarray
        );
        max_sum = max({max_sum, dp[i][0], dp[i][1]});
    }
    return max_sum;
}