#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/wildcard-matching/
Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for '?' and '*'.
'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).

        Input:          
        s = "adceb"
        p = "*a*b"
        Output: true

        Input:
        s = "acdcb"
        p = "a*c?b"
        Output: false
*/

class Solution {
    string str;
    string pattern;
    vector<vector<int>> dp;

   public:
    bool isMatchRecursive(int i, int j) {
        //if both pattern and string are empty
        if (i == 0 && j == 0) {
            return true;
        }
        //if pattern is empty but there is some portion of string left
        if (i == 0) {
            return false;
        }
        //if string itself is empty but pattern has some portion left. if that portion consists of * only then it is true
        if (j == 0) {
            return (pattern[i - 1] == '*' ? isMatchRecursive(i - 1, j) : false);
        }
        if (dp[i][j] != -1) {
            return dp[i][j];
        }
        if (pattern[i - 1] == '*') {
            return dp[i][j] =
                       isMatchRecursive(i - 1, j) ||
                       (j == 0 ? false : isMatchRecursive(i, j - 1));  //treating * as empty
        }
        if (pattern[i - 1] == str[j - 1] || pattern[i - 1] == '?') {
            return dp[i][j] = isMatchRecursive(i - 1, j - 1);
        }
        return dp[i][j] = false;
    }
    //dp[i][j] depends on (i - 1, j) (i, j - 1) (i - 1, j - 1)
    //store (i - 1, j - 1) separately if we want to use only one array for dp
    bool isMatchIterativeSpaceOptimized() {
        vector<int> dp_i(str.length() + 1, false);  //initially dp[0][....]
        dp_i[0] = true;

        for (int i = 1; i <= pattern.length(); ++i) {
            int prev = dp_i[0];  //dp[i-1][0]
            //update dp[i][0] (j == 0 case)
            dp_i[0] = (pattern[i - 1] == '*' ? dp_i[0] : false);
            for (int j = 1; j <= str.length(); ++j) {
                bool temp = dp_i[j];  //dp[i-1][j]
                if (pattern[i - 1] == '*') {
                    // dp[i][j] = dp[i-1][j] || dp[i][j-1];
                    dp_i[j] = dp_i[j] || dp_i[j - 1];
                } else if (pattern[i - 1] == str[j - 1] || pattern[i - 1] == '?') {
                    //dp[i][j] = dp[i-1][j-1]
                    dp_i[j] = prev;
                } else {
                    dp_i[j] = false;
                }
                prev = temp;  //for the next (i, j+1) we should store (i-1,j)
            }
        }

        return dp_i[str.length()];
    }

    bool isMatch(string s, string p) {
        str = s;
        pattern = p;
        // dp.resize(p.length() + 1, vector<int>(s.length() + 1, -1));
        // return isMatchRecursive(p.length(), s.length());
        return isMatchIterativeSpaceOptimized();
    }
};