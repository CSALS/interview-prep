#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/regular-expression-matching/
Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.
'.' Matches any single character.
'*' Matches zero or more of the preceding element.
*/

class Solution {
    string pattern;
    string str;
    vector<vector<int>> dp;

   public:
    bool isMatchRecursive(int i, int j) {
        if (i == 0 && j == 0) {
            return true;
        }
        if (i == 0) {
            return false;
        }
        if (j == 0) {
            return (pattern[i - 1] == '*' ? isMatchRecursive(i - 2, 0) : false);
        }

        if (dp[i][j] != -1) {
            return dp[i][j];
        }

        if (pattern[i - 1] == str[j - 1] || pattern[i - 1] == '.') {
            return dp[i][j] = isMatchRecursive(i - 1, j - 1);
        } else if (pattern[i - 1] == '*') {
            bool result = false;
            if (str[j - 1] == pattern[i - 2] || pattern[i - 2] == '.') {
                result = isMatchRecursive(i, j - 1);
            }
            return dp[i][j] = isMatchRecursive(i - 2, j)  //ignore *. treat it as empty
                              || result;
        } else {
            return dp[i][j] = false;
        }
    }
    bool isMatch(string s, string p) {
        str = s;
        pattern = p;
        dp.resize(p.length() + 1, vector<int>(s.length() + 1, -1));
        return isMatchRecursive(p.length(), s.length());
    }
};