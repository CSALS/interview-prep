#include <bits/stdc++.h>
using namespace std;

/*
1. Just checking longest common substring between the string & reversed doesn't work.
    Ex:- str = abcdefcba    rev(str) = abcfedcab
    Longest Common Substring is abc but that isn't a palindrome. So fails when the reverse of non-palindromic substring is also present in the string.
2. DP Solution = O(N^2) Time | O(N^2) Space. Can Be Reduced To O(N) space
3. O(N^2) Time | O(1) Space.
    Expanding From Middle. 
    For even palindromes we should do expandFromMiddle(i, i)
    For odd palindromes we should do expandFromMiddle(i, i + 1)
    where expandFromMiddle expands till the characters on either side of mirror are equal.
    For all indices do this so O(N^2) Time.
4. O(N) Time | O(N) Space = Manacher's Algorithm
*/

class Solution {
   public:
    string longestPalindrome(string s) {
        int n = s.length();

        vector<vector<bool>> dp(n, vector<bool>(n, true));

        int startIndex = 0, endIndex = 0;
        for (int gap = 1; gap < n; ++gap) {
            for (int i = 0; i < n; ++i) {
                int j = gap + i;
                if (j >= n) {
                    break;
                }
                if (i + 1 == j) {
                    dp[i][j] = s[i] == s[j];
                } else if (s[i] == s[j]) {
                    dp[i][j] = dp[i + 1][j - 1];
                } else {
                    dp[i][j] = false;
                }
                if (dp[i][j]) {
                    if (j - i + 1 > endIndex - startIndex + 1) {
                        startIndex = i;
                        endIndex = j;
                    }
                }
            }
        }
        return s.substr(startIndex, endIndex - startIndex + 1);
    }
};

class SpaceOptimized {
   public:
    //O(N^2) T | O(1) S
    string longestPalindrome(string s) {
        if (s.empty()) {
            return "";
        }

        int n = s.length();
        int startIndex = 0, endIndex = 0;
        for (int i = 0; i < n; ++i) {
            pair<int, int> bounds = expandFromMiddle(s, i);
            if (endIndex - startIndex < bounds.second - bounds.first) {
                startIndex = bounds.first;
                endIndex = bounds.second;
            }
        }
        return s.substr(startIndex, endIndex - startIndex + 1);
    }

    pair<int, int> expandFromMiddle(string s, int index) {
        int n = s.length();
        int i = index - 1, j = index + 1;
        //odd length
        while (i >= 0 && j < n && s[i] == s[j]) {
            i--;
            j++;
        }
        //need to do this since we break when we find invalid i, j
        i++;
        j--;
        pair<int, int> result = {i, j};

        //even length
        i = index, j = index + 1;
        while (i >= 0 && j < n && s[i] == s[j]) {
            i--;
            j++;
        }
        i++;
        j--;
        if (result.second - result.first < j - i) {
            result = {i, j};
        }
        return result;
    }
};

/*
abcba
*/