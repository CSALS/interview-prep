#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/longest-valid-parentheses/

Given a string A containing just the characters ’(‘ and ’)’.
Find the length of the longest valid (well-formed) parentheses substring.
*/

int longestValidParentheses(string A) {
    int n = A.length();
    int max_len = 0;
    if (n == 0 || n == 1) {
        return max_len;
    }
    vector<int> dp(n, 0);  //dp[i] = longest valid paranthesis ending ith position
    for (int i = 1; i < n; ++i) {
        if (A[i] == '(') {
            dp[i] = 0;  //valid paranthesis can't end at (
        } else {
            //......()
            if (A[i - 1] == '(') {
                dp[i] = 2 + (i < 2 ? 0 : dp[i - 2]);
            } else {  //....((..))
                if (i - dp[i - 1] - 1 >= 0 && A[i - dp[i - 1] - 1] == '(')
                    dp[i] = 2 + dp[i - 1] +
                            (i - dp[i - 1] - 2 < 0 ? 0 : dp[i - dp[i - 1] - 2]);
            }
            max_len = max(max_len, dp[i]);
        }
    }
    return max_len;
}
