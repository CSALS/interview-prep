#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/min-cost-path-dp-6/

Min cost to go from top left to bottom right
from a given cell (i, j), cells (i+1, j), (i, j+1) and (i+1, j+1)
All cells have positive integers
If it has negative integers then we may need to use dijkstra's algorithm.
*/

int minCostPath(vector<vector<int>> &matrix, int rows, int cols) {
    int dp[rows][cols];  //dp[i][j] = min cost to reach from (0, 0) to (i, j)

    //Base case
    dp[0][0] = matrix[0][0];
    for (int r = 1; r < rows; ++r) {
        dp[r][0] = matrix[r][0] + dp[r - 1][0];
    }
    for (int c = 1; c < cols; ++c) {
        dp[0][c] = matrix[0][c] + dp[0][c - 1];
    }

    //Recurrence
    for (int r = 1; r < rows; ++r) {
        for (int c = 1; c < cols; ++c) {
            dp[r][c] = matrix[r][c] + min({dp[r - 1][c], dp[r][c - 1], dp[r - 1][c - 1]});
        }
    }

    return dp[rows - 1][cols - 1];
}

/*
Extension
    https://www.geeksforgeeks.org/minimum-cost-path-left-right-bottom-moves-allowed/

    If at any cell we can go left, right, top or bottom then dp can't be used.
    we have to treat this as graph question.
    undirected weighted graph. use dijkstra's algorithm
    Don't need to construct graph separately directly use the matrix itself
*/