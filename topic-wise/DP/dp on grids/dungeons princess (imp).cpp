#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/dungeon-princess/
https://www.geeksforgeeks.org/minimum-positive-points-to-reach-destination/

    only works when we calc from Bottom right to top left. 
    Doesn't work if we calculate from top left to bottom right. 
    Direction in which we calculate the answer also matters. 
    Explanation = https://tinyurl.com/ya7oueud
*/

int top_down(vector<vector<int>> grid, vector<vector<int>> &dp, int row, int col) {
    int n = grid.size(), m = grid[0].size();
    if (col >= m || row >= n)
        return INT_MAX;
    if (col == m - 1 && row == n - 1) {
        return dp[n - 1][m - 1] = grid[n - 1][m - 1] >= 0 ? 1 : abs(grid[n - 1][m - 1]) + 1;
    }
    if (dp[row][col] != -1)
        return dp[row][col];

    dp[row][col] = min(top_down(grid, dp, row + 1, col), top_down(grid, dp, row, col + 1)) - grid[row][col];
    if (dp[row][col] <= 0)
        dp[row][col] = 1;
    return dp[row][col];
}

// DP On Grids
int calculateMinimumHP(vector<vector<int>> &grid) {
    if (grid.empty() || grid[0].empty())
        return 0;

    //dp[i][j] = min health needed to go from (i, j) to bottom right
    int n = grid.size(), m = grid[0].size();
    vector<vector<int>> dp(n, vector<int>(m, -1));
    return top_down(grid, dp, 0, 0);
}