#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/?ref=lbp
    dp[i][j] = number of ways to reach from (0, 0) to (i, j)
    dp[i][j] = dp[i - 1][j] + dp[i][j - 1]

https://www.geeksforgeeks.org/print-all-possible-paths-from-top-left-to-bottom-right-of-a-mxn-matrix/
    You need to dfs from each cell (starting from (0, 0))
*/

void printPathsHelper(vector<vector<int>> &matrix, int rows, int cols, int x, int y, vector<int> path) {
    if (x < 0 || y < 0 || x >= rows || y >= cols) {
        return;
    }
    path.push_back(matrix[x][y]);
    if (x == rows - 1 && y == cols - 1) {
        //print path
        for (int num : path) {
            cout << num << " ";
        }
        cout << endl;
        return;
    }
    printPathsHelper(matrix, rows, cols, x, y + 1, path);
    printPathsHelper(matrix, rows, cols, x + 1, y, path);
    path.pop_back();
}

void printPaths(vector<vector<int>> matrix) {
    printPathsHelper(matrix, matrix.size(), matrix[0].size(), 0, 0, {});
}

/*
https://www.geeksforgeeks.org/unique-paths-in-a-grid-with-obstacles/
    Same as counting paths but here obstacles are present
    https://atcoder.jp/contests/dp/tasks/dp_h
    obstacle = '#' else '.'
*/
const int N = 1010;
const int mod = 1e9 + 7;
int H, W;
char grid[N][N];
int dp[N][N];
int countPathsWithObstacles() {
    int dp[H][W];  //dp[i][j] = number of paths from (0, 0) to (i, j)

    //Base Case
    dp[0][0] = 1;
    for (int i = 1; i < H; ++i) {
        if (grid[i][0] == '#') {
            //obstacle
            dp[i][0] = 0;
        } else {
            dp[i][0] = dp[i - 1][0];
        }
    }
    for (int j = 1; j < W; ++j) {
        if (grid[0][j] == '#') {
            dp[0][j] = 0;
        } else {
            dp[0][j] = dp[0][j - 1];
        }
    }

    //Recurrence
    for (int i = 1; i < H; ++i) {
        for (int j = 1; j < W; ++j) {
            if (grid[i][j] == '#') {
                dp[i][j] = 0;
            } else {
                dp[i][j] = (dp[i - 1][j] % mod + dp[i][j - 1] % mod) % mod;
            }
        }
    }
    return dp[H - 1][W - 1] % mod;
}

/*
https://www.geeksforgeeks.org/find-the-longest-path-in-a-matrix-with-given-constraints/
    Although from one cell you can to move any direction but actually you can move to one direction if possible.
    since numbers are distinct and we can move to adjacent vertex only if their diff. is 1
    dp[i][j] = longest path starting from (i, j)
    answer = max(dp[i][j])

    if(arr[i + 1][j] = 1 + arr[i][j])
        dp[i][j] = 1 + dp[i + 1][j]
    ... for other directions also same
*/

int main() {
    printPaths({{1, 2, 3}, {4, 5, 6}});
    printPaths({{1, 2}, {3, 4}});
}