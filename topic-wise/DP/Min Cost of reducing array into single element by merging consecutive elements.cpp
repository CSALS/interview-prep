#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/merge-elements/
https://www.geeksforgeeks.org/minimum-cost-of-reducing-array-by-merging-any-adjacent-elements-repetitively/


Given an array arr[] of N numbers. We can merge two adjacent numbers into one and the cost of merging the two numbers is equal to the sum of the two values. The task is to find the total minimum cost of merging all the numbers.


[1, 2, 3, 4] = 19
*/


/*
one element => min cost is 0
two element => min cost is sum of them

a b c d e

Basically choose a point and compute min cost for left elements and right elements and now to merge them the cost will sum[i...j]

(a b c) (d e) => (a+b+c) (d+e) => cost is a+b+c+d+e + cost(a,b,c) + cost(d, e)
*/

int solve(vector<int> &A) {
    if(A.empty()) return 0;
    
    int N = A.size();
    
    vector<int> prefixSum(N, A[0]);
    for(int i = 1; i < N; ++i) {
        prefixSum[i] = prefixSum[i - 1] + A[i];
    }
    
    int dp[N][N]; //dp[i][j] = min cost of merging A[i....j]
    
    for(int i = 0; i < N; ++i) {
        dp[i][i] = 0;
    }
    
    for(int gap = 1; gap < N; ++gap) {
        for(int i = 0; i < N; ++i) {
            int j = gap + i;
            if(j >= N) break;
            
            int sum = prefixSum[j] - (i != 0 ? prefixSum[i - 1] : 0); 
            dp[i][j] = INT_MAX;
            for(int k = i; k < j; ++k) {
                dp[i][j] = min(dp[i][j],
                               dp[i][k] + dp[k + 1][j] + sum);
            }
        }
    }
    
    return dp[0][N - 1];
}
