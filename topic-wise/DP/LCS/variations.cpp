#include <bits/stdc++.h>
using namespace std;

/*
Longest common substring =>
    dp[i][j] = LCS ending at ith character in string A and jth character in string B
    if(A[i] == A[j]) {
        dp[i][j] = 1 + dp[i - 1][j - 1]
    } else {
        dp[i][j] = 0
    }
    Answer = max(dp[i][j]) for all i, j

Longest palindromic subsequence =>
    LCS of string A and reverse of A

Minimum number of deletions required to make string to palindrome =>
    length of string - length of longest palindromic subsequence

Minimum number of insertions required to make string to palindrome =>
    For every character which doesn't have opposite character we add it.
    So apart from LPS we add mirror chars
    length of string - length of longest palindromic subsequence

Longest repeating subsequence => 
    We will take subsequence which repeats two times
    Since let's say "ab" repeats 3 times
    ab    ab    ab then we can consider it repeating 2 times with double the length
    abab     abab  here length is more. 
    So it is same as LCS but whenever characters are same the indices shouldn't same
*/

/*
shortest common supersequence =>
    Supersequence of a and b is a string where both a & b will occur as sub-sequence
    Length of SCS = len(A) + len(B) - LCS (common chars should occur only once)
*/
void printSCS(string A, string B, vector<vector<int>> dp) {
    //call this function after dp table is filled
    int n1 = A.length(), n2 = B.length();
    string scs = "";

    //n1 and n2 are length so that's why > 0
    while (n1 > 0 && n2 > 0) {
        if (A[n1 - 1] == B[n2 - 1]) {
            //common character so add only once
            scs = A[n1 - 1] + scs;
            n1--;
            n2--;
        } else {
            if (dp[n1][n2] == dp[n1 - 1][n2]) {
                scs = A[n1 - 1] + scs;
                n1--;
            } else {
                scs = B[n2 - 1] + scs;
                n2--;
            }
        }
    }
    while (n1 > 0) {
        scs = A[n1 - 1] + scs;
        n1--;
    }
    while (n2 > 0) {
        scs = B[n2 - 1] + scs;
        n2--;
    }
}

/*
Counting subsequences =>
    Number of common subsequence between two strings =
        (i,j) = (i-1,j) + (i,j-1) but here (i-1,j-1) is counted twice
        so when s[i]!=s[j] we need to substract by (i-1,j-1)
        when s[i]==s[j] we don't need to substract since we will count them as different set of subsequences by attaching the s[i]

        dp[i][j] = number of common subsequence in A[0...i-1] and B[0...j-1]
        if(A[n1 - 1] == B[n2 - 1]) {
            dp[n1][n2] = 1 + dp[n1 - 1][n2] + dp[n1][n2 - 1] (extra 1 for current chars)
        } else {
            dp[n1][n2] = dp[n1 - 1][n2] + dp[n1][n2 - 1] - dp[n1 - 1][n2 - 1]
        }

    Number of palindromic subsequences in a string =
        1. Can use above code b/w string and reversed string
        2. We don't need to do that. Instead have one pointer at start and one at end
            i = 0, j = n - 1
            if(A[i] == A[j]) {
                dp[i][j] = 1 + dp[i + 1][j] + dp[i][j - 1]
            } else {
                dp[i][j] = dp[i + 1][j] + dp[i][j - 1] - dp[i + 1][j - 1]
            }
*/

/*
variation https://www.geeksforgeeks.org/find-number-times-string-occurs-given-string/
https://www.geeksforgeeks.org/count-subsequence-of-length-three-in-a-given-string/
*/
