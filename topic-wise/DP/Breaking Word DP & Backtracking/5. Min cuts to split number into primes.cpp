#include <bits/stdc++.h>
using namespace std;

/*
Given a string str that represents a large number, 
the task is to find the minimum number of segments the given string can be divided such that each segment
is a prime number in the range of 1 to 10^6;

    Similar to min. cuts word break.
    We can first do sieve of eratothenes on min(len(str), 99999) [since 100000 can't be prime anyway]
    We need primes of maximum 5 digits. So our prefix length don't need to exceed 5

    //dp[i] = min cuts to split str[i...N-1] into primes
    int minCuts(string str, int i) {
        int n = str.length();
        if(i >= n) return 0;

        dp[i] = -1 //it might not be possible to split at all

        for(int j = i; j < min(i + 5, n); ++j) {
            if str[i...j] is prime number (O(1) to check in sieve constructed before)
                if(minCuts(str, j + 1) != -1) {
                    //it is possible to split [j+1...N-1] into primes
                    dp[i] = min(dp[i], 1 + dp[j + 1])
                }
        }
        return dp[i]; //it might be -1 if we can't split [i...N-1] into primes
    }

    O(N * 5) + O(NloglogN) T
*/