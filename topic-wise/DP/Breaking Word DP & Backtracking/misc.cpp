/*
https://stackoverflow.com/questions/52562585/maximal-value-among-shortest-distances-in-a-matrix


state = (matrix, N)

recursion(matrix, N) {
    if(N == 0) {
        //all office buildings are kept
        //do bfs to find max distance b/w building and non-building
        //if it is less than min then store the matrix
    }

    min_dist = INF
    for(int i = 0; i < W; ++i) {
        for(int j = 0; j < H; ++j) {
            if(matrix[i][j] == -1) {
                matrix[i][j] = 0 //place building there
                min_dist = min(min_dist, recursion(matrix, N - 1))
                matrix[i][j] = -1 //undo
            }
        }
    }
    return min_dist
}

To memoize a 2D matrix, represent it as bitmask
*/