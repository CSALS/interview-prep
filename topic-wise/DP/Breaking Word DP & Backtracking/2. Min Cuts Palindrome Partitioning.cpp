#include <bits/stdc++.h>
using namespace std;

/**
* Given a string cut into segments such that each segment is palindrome.
* Find min cuts needed.
*/

#define INF 1000000
const int MAX_N = 1001;
bool isPalindrome[MAX_N][MAX_N];  //isPalindrome[i][j] = if str[i...j] is palindrome
int dp[MAX_N];                    //dp[i] = min. cuts for str[i...N-1]

int minCutsRecursive(string str, int index) {
    int n = str.length();

    if (index >= n) return true;

    if (dp[index] != -1) return dp[index];

    // if [ind...N-1] is itself palindrome then 0 cuts
    if (isPalindrome[index][n - 1]) return dp[index] = 0;

    dp[index] = INF;
    for (int i = index; i < n - 1; ++i) {
        if (isPalindrome[index][i]) {
            //If this is word break problem and need to find min cuts then we need to first check if (i+1) is not -1 and then add.
            //if it is -1 then we can't add 1 to it since -1 implies not possible to segment at all.
            //Or we can return INF so that it never gets considered due to min()
            dp[index] = min(dp[index], 1 + minCutsRecursive(str, i + 1));
        }
    }
    return dp[index];
}

int minCut(string str) {
    int n = str.length();
    //compute palindromic substrings
    for (int gap = 0; gap < n; ++gap) {
        for (int i = 0; i < n; ++i) {
            int j = i + gap;
            if (j >= n) break;
            if (i == j)
                isPalindrome[i][i] = true;
            else if (i + 1 == j)
                isPalindrome[i][j] = str[i] == str[j];
            else
                isPalindrome[i][j] = (str[i] == str[j]) && isPalindrome[i + 1][j - 1];
        }
    }

    //compute min. cuts needed
    memset(dp, -1, sizeof(dp));
    return minCutsRecursive(str, 0);
}
