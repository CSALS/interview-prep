#include <bits/stdc++.h>
using namespace std;

/**
 * Same as 3. Word Break 2
 * Only difference is the subproblem results here won't be empty for sure since any string can be
 * paritioned into palindromes (individual chars)
 * abc => a | b | c
 */

const int MAX_N = 1001;
bool isPalindrome[MAX_N][MAX_N];                //isPalindrome[i][j] = if str[i...j] is palindrome
unordered_map<int, vector<vector<string>>> dp;  //dp[i] = all possible palindrome partitions for str[i...N-1]

vector<vector<string>> partitionRecursive(string str, int index) {
    int n = str.length();

    if (index >= n) return {};

    if (dp.find(index) != dp.end()) dp[index];

    vector<vector<string>> result;
    for (int i = index; i < n; ++i) {
        if (isPalindrome[index][i]) {
            string prefix = str.substr(index, i - index + 1);
            vector<vector<string>> subproblemResult = partitionRecursive(str, i + 1);
            //only possible when i = n - 1 since any word can be partitioned into palindrome in atleast 1 way
            if (subproblemResult.empty()) {
                result.push_back({prefix});
            } else {
                for (vector<string> vec : subproblemResult) {
                    vec.insert(vec.begin(), prefix);
                    result.push_back(vec);
                }
            }
        }
    }
    return dp[index] = result;
}

vector<vector<string>> partition(string str) {
    int n = str.length();
    //compute palindromic substrings
    memset(isPalindrome, false, sizeof(isPalindrome));
    for (int gap = 0; gap < n; ++gap) {
        for (int i = 0; i < n; ++i) {
            int j = i + gap;
            if (j >= n) break;
            if (i == j)
                isPalindrome[i][i] = true;
            else if (i + 1 == j)
                isPalindrome[i][j] = (str[i] == str[j]);
            else
                isPalindrome[i][j] = ((str[i] == str[j]) && (isPalindrome[i + 1][j - 1]));
        }
    }

    //compute all palindrome partitions
    dp.clear();
    return partitionRecursive(str, 0);
}