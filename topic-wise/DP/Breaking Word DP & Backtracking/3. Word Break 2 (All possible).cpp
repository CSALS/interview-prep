#include <bits/stdc++.h>
using namespace std;

/**
* Given a string cut into segments such that each segment belongs the dictionary.
* Find all possible cuts.
*/

class Solution {
   public:
    vector<string> wordBreak(string s, vector<string> &wordDict);
};

unordered_set<string> dict;
unordered_map<int, vector<string>> cache;
// cache[ind] = all possible word breaks in [ind...N-1]
vector<string> backtrack(string s, int ind) {
    if (ind >= s.length()) {
        return {};
    }

    if (cache.find(ind) != cache.end()) {
        return cache[ind];
    }

    string word = "";
    vector<string> result = {};
    for (int i = ind; i < s.length(); ++i) {
        word += s[i];
        if (dict.find(word) != dict.end()) {
            vector<string> subproblem = backtrack(s, i + 1);
            // if subproblem is empty and at the last character then add it to
            // result since the word is present in dictionary, but if subproblem
            // is empty but we are not at last character that means answer
            // doesn't exist for (i + 1) so don't add.
            if (subproblem.empty() && i == s.length() - 1) {
                result.push_back(word);
            } else if (!subproblem.empty()) {
                for (string str : subproblem) {
                    result.push_back(word + " " + str);
                }
            }
        }
    }
    return cache[ind] = result;
}

vector<string> Solution::wordBreak(string s, vector<string> &wordDict) {
    dict.clear();
    for (string s : wordDict) {
        dict.insert(s);
    }
    cache.clear();
    //
    return backtrack(s, 0);
}