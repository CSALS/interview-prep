# DP Resources

* https://www.facebook.com/utkarsh028/posts/113560319994926
* https://qr.ae/pNtB42
* https://qr.ae/pNvCJF
* https://www.codechef.com/wiki/tutorial-dynamic-programming (codechef tut)
* https://codeforces.com/blog/entry/20935 (DP On Trees)
* https://icpc.ninja/Algorithms/Tree/DP/ (Tree DP)
* https://cs.stackexchange.com/questions/119707/thought-process-to-solve-tree-based-dynamic-programming-problems (Tree DP IMP)
* https://www.iarcs.org.in/inoi/online-study-material/topics/dp-trees.php
* https://blogarithms.github.io/articles/2019-10/inout-dp-tree (in-out dp)
* https://codeforces.com/blog/entry/67679 (Tutorial)
* https://codeforces.com/blog/entry/8219 (Optimization)
* https://codeforces.com/blog/entry/16437 (Link to some Blogs)
* https://codeforces.com/contest/455/problem/A