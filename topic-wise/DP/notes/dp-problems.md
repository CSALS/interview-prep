# Patterns
0-1 Knapsack, Unbounded Knapsack, Fibonacci, LCS, LIS, Kadane's Algo, Matrix Chain Multiplication, DP on Trees, DP on grid/matrix.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

1. 0-1 Knapsack (https://www.geeksforgeeks.org/space-optimized-dp-solution-0-1-knapsack-problem/)
* O(N*W) Time & Space
* O(N*W) Time | O(W) space using 2 Arrays
* O(N*W) Time | O(W) space using 1 Arrays

Identify =
- If question contains subsets + sum then think in terms of 0-1 Knapsack.
- subsets + optimization = DP (0-1 Knapsack)
- count + sets + given sum/diff = DP (0-1 Knapsack)
- paritioning the sets into two subsets = DP

Variation = (All numbers in the array should be non-negative)
1. Subset Sum Problem, Printing all subsets with given sum
2. Equal Sum Parition Problem = Parition Array into two subsets
3. Count Number Of Subsets with a given sum
4. Min Subset Diff. = Paritition array into 2 subsets s.t absolute diff b/w their sums must be min
5. Count Number of subset partitions with given difference
6. Target Sum = No of ways to arrange +, - to array to get a given sum
7. 3D DP. While solving for choice diagram you will get s1 = n1 * Avg_Set. s1 & n1 are variables. 
https://www.geeksforgeeks.org/partition-an-array-of-non-negative-integers-into-two-subsets-such-that-average-of-both-the-subsets-is-equal/

----------------------------------------------------------------------------------------------------------------------------------------------------------------

2. Unbounded Knapsack
* Here an item can be selected any number of items and place it in knapsack

Variations =
1. Rod Cutting Problem
2. Coin Change = No of ways to make sum, Min. no of coins to make sum
Base Case = IMP.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

3. Longet Common Subsequence (LCS)

Identify =
* 2 strings and some optimal question (LCSubstring, SCS, Min # Insertions & Deletions from a to b)
* Even 1 String but something related to subsequence (LPSubsequence) Then you need to see how you can get 2 strings from that 1 string and then apply LCS
* Insertions/Deletions + Palindrome - Think in terms of Longest Palindromic Subsequence

Variations =
0. Print LCS
1. Longest common substring
2. Shortest Common Supersequence & Printing SCS
3. Min # Of insertions & deletions to convert A to B
4. Edit Distance
5. Longest Palindromic Subsequence
6. (**IMP**)Min # of deletions/insertions in string to make it a palindrome
7. (**IMP**)Longest Repeating Subsequence (https://www.geeksforgeeks.org/longest-repeating-subsequence/)
8. Sequence Pattern Matching = Another Imp Pattern (Many problems can be solved using this pattern)
Can be solved by DP. But optimized approach is different.
9. (**IMP**)Counting Common subsequences
https://www.geeksforgeeks.org/count-common-subsequence-in-two-strings/
https://www.geeksforgeeks.org/count-palindromic-subsequence-given-string/

TODO: 
https://www.geeksforgeeks.org/find-if-a-string-is-interleaved-of-two-other-strings-dp-33/ (check no duplicates version also)
https://www.interviewbit.com/problems/interleaving-strings/
https://www.geeksforgeeks.org/count-distinct-subsequences/


----------------------------------------------------------------------------------------------------------------------------------------------------------------

4. Matrix Chain Multiplication (Top Down better than Bottom Up)
[i...j] = i represents ending dimension of starting matrix, j represents ending dimension of ending matrix

In Bottom up approach we fill diagonally
    outer loop = length of diagonal (1 to N - 1)
    length = j - i
    so inner loop on i and you can get j

Identify =
parenthesizing/putting brackets + min/max/# of ways = MCM variation

Variations =
1. Printing MCM (See Your Code). Good One. https://practice.geeksforgeeks.org/problems/brackets-in-matrix-chain-multiplication/0/
2. Palindrome Partitioning =
Following standard MCM approach = O(N^3). Can optimize it.
3. Boolean Paraenthesization = Tricky Solution. 
4. Scramble String = Very Complex Problem
5. https://leetcode.com/problems/burst-balloons/ = Very Good Problem
3 1 2
Bursting balloon 1 = 3 * 1 * 2 coins think of as multiplying (3*1) & (1*2) matrices
6. Egg Dropping Problem

----------------------------------------------------------------------------------------------------------------------------------------------------------------

5. Longest Increasing Subsequence
https://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/


----------------------------------------------------------------------------------------------------------------------------------------------------------------

7. DP On Grid

Tips = 
- In grid dp, try both ways go from top left to bottom right manner (answer will be dp[n-1][m-1]) and also from bottom right to top left manner (answer will be dp[0][0])

1. https://www.geeksforgeeks.org/min-cost-path-dp-6/
2. Max Falling Sum = https://practice.geeksforgeeks.org/problems/path-in-matrix/0
3. https://www.geeksforgeeks.org/find-the-longest-path-in-a-matrix-with-given-constraints/
4. (**IMP**)https://www.interviewbit.com/problems/dungeon-princess/ (Grid DP) (Very IMPPP) = 
5. https://www.geeksforgeeks.org/minimum-positive-points-to-reach-destination/ (Dungeon Princess)
6. https://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/

----------------------------------------------------------------------------------------------------------------------------------------------------------------

8. DP + Bitmasking
Tips = 
- Try to reduce arguments (maybe can derive other args from set bits/unset bits from mask)


1. (**IMP**)https://www.geeksforgeeks.org/travelling-salesman-problem-set-1/
2. (**IMP**)Assignment Problem (Try to think of any redundant arguments using the tip)
3. Check for hamiltonian path in graph

TODO: https://leetcode.com/problems/partition-to-k-equal-sum-subsets/

----------------------------------------------------------------------------------------------------------------------------------------------------------------

9. Digit DP

* https://www.interviewbit.com/problems/numbers-of-length-n-and-value-less-than-k/
- https://www.geeksforgeeks.org/split-the-given-string-into-primes-digit-dp/?ref=leftbar-rightbar
- https://www.interviewbit.com/problems/n-digit-numbers-with-digit-sum-s-/?ref=random-problem

----------------------------------------------------------------------------------------------------------------------------------------------------------------

10. String DP
if string dp then identify if one index is sufficient in state or 2 index are required. The following are common :-
[0...i]
[i...n-1]
[i...j]

## Problems
* Longest Palindromic Substring =


11. Breaking Words DP & Backtracking (Similar Problems)
1. is it possible? (DP)
2. min cuts needed (DP)
3. all possible cuts (DP + backtrackings)
- https://leetcode.com/problems/word-break/ (Is it possible?)
- https://practice.geeksforgeeks.org/problems/palindromic-patitioning/0 (Minimum Cuts)
- (**IMP**)https://leetcode.com/problems/word-break-ii/ (all possible cuts)
- (**IMP**)https://www.interviewbit.com/problems/palindrome-partitioning/?ref=similar_problems (all possible parititions)
- https://www.geeksforgeeks.org/split-the-given-string-into-primes-digit-dp/?ref=leftbar-rightbar (Digit DP)


---------------------------------------------------------------------------------------------------------------------------------

# Misc
https://www.geeksforgeeks.org/mobile-numeric-keypad-problem-set-2/
https://www.geeksforgeeks.org/painters-partition-problem/
https://www.geeksforgeeks.org/dice-throw-dp-30/
https://www.interviewbit.com/problems/longest-valid-parentheses/ (A Different Type Of Problem)
https://atcoder.jp/contests/dp/tasks/dp_m 
https://atcoder.jp/contests/dp/tasks/dp_n 


#### Catalan's Number & Applications =
- Catalan Number = C(n+1) = sigma i=0 to N (C(i) * C(n-i))  = Use DP 
N = 0 -> 1
N = 1 -> 1
N = 2 -> 2
N = 3 -> 5
N = 4 -> 14
N = 5 -> 42
Application = 
1. Finding Nth Catalan Number
2. Find the number of valid parentheses expressions of given length
3. Count No of BSTs that can be formed using N nodes (1 to N numbered) (Notes)
4. Number Of Binary Trees using N nodes labelled from 1 to N
5. Number of unlabelled binary trees
6. Count Binary Tree for a given preorder array of length N



# DP Identify
optimization problem (max / min)
number of ways problems
when greedy fails try this
whenever we have choice you can build recurren relation. cache if overlapping subproblems exist


# TODO


https://www.geeksforgeeks.org/partition-an-array-of-non-negative-integers-into-two-subsets-such-that-average-of-both-the-subsets-is-equal/
box stacking

house robber i, ii, iii
ways to decode-1,2 (leetcode)

https://www.geeksforgeeks.org/count-number-ways-reach-given-score-game/ (distinct combinations)
https://www.interviewbit.com/problems/deepu-and-girlfriend/ (Game Theory + DP)
when u have 2 players u dont need to maintain the current player in the state 

array picking from end (tushar roy)
https://www.geeksforgeeks.org/count-number-of-ways-to-divide-a-number-in-4-parts/?ref=lbp
https://www.geeksforgeeks.org/count-number-of-ways-to-divide-a-number-in-4-parts/?ref=lbp (only distinct combinations)
https://www.geeksforgeeks.org/maximize-arrj-arri-arrl-arrk-such-that-i-j-k-l/ 
https://www.geeksforgeeks.org/longest-subarray-such-that-adjacent-elements-have-at-least-one-common-digit/


https://www.hackerearth.com/problem/algorithm/string-partition/ Getting RE