# my dp template

- identify if it is DP (min, max, largest, smallest, count number of, is it possible etc....) + (choices in elements)

- think of recurrence index
[i...j] or [i....N-1] or [0....i]
- in reuccrence better to consider size as argument rather than index (handling base case will be easy)

- think of recurrence relation
this comes from choice diagram. think of what choices you have and constraints on that choice

- get the base case

- try to remove redudant args (if any)
redundant args = argument which can be derived from others

- convert top-down to bottom-up (optional? But can optimize better if in bottom-up)

- optimize space complexity. based on recurrence relation.



## TIPS
Suppose I am a layman and I want you to explain what is dynamic programming, how would you explain this to me?

I answered this :-

Let me say,
Write down 1+2+6+3
Whats is value , its equal to 12 right
Now I will say add 8
You will say 20 quickly

How did you say 20 quickly, because you know the previous sum right.

You don't need to recount because you remembered the last count as 12. That is DP just a fancy way to say "remembering the stuff to save time later "

## OPTIMAL SUBSTRUCTURE & OVERLAPPING SUBPROBLEMS
- If problem can be solved using sub problems then use recursion.
- So if the solution to a problem can be formed using solution of sub-problem then start forming the recurrence relation and use recursion
- Now see if you can optimize over the plain recursion. If there are overlapping subproblems.

# Steps to solve DP problem :-
1. Create recursive backtrack solution.
2. Avoid redundant arguments in the recursive function.
If arguments can be created using other arguments then
they are redundant.
3. Minimize the range of possible values of function arguments.
Range of possible values the function arguments can get from a valid input
4. Try to optimize the time complexity of one function call (remember, you can treat recursive calls as they would run in O(1) time)
5. Cache the values and don't calculate the same things twice
6. Final Time Compleixty = Range Of Possible Values The Function Can Be Called With * Time Complexity Of One Function Call
7. Converting Top-Down to Bottom-Up
- if in recursion index reduces from N to 0(in base-case)
use loop from 1 to N (exclude base-case. Initialize base-case before the loop starts)
- return top-down === break bottom-up
8. If arguments cant be reduced further but still you want to optimize then think of preprocessing.
9. Think of first or last in the sequence to get idea. Think of including or excluding element in choice diagram
12. When N is less and it might be DP then it is DP + Bitmasking

## always think of optimizing both time and space

# General
- Constructing solution set =
If in DP, you are not just asked to compute value but also the set of numbers which form the solution.
Then first compute the value and then use the matrix to form the solution set.
- Indexes in recurrence ->
opt(i) = 0....i (including ith ele or not)
opt(i) = i....n-1
opt(i, j) = i...j

- Preprocessing some things really reduces complexity.

- Think of optimizing both in time & space.
    1. If used 2D dp, try to use 1D dp.
    2. If N^3 solution on 2D dp then try to think of N^2 solution.
    3. If recurrence depends on only one previous value then use one variable (IMP)
    4. If recurrence depends on only two previous value then use two variable (IMP)

- In top-down:
    if(dp[x][y] != -1) we are using this condition
    but if dp[x][y] can take any value then take visited array and use vis[x][y]


# converting top-down to bottom-up

- check the recurrence relation. if it depends on previous row values then we can fill row-wise
- if it depends on diagonally below values then need to fill diagonally


- diagonally filling loops =>
for(int gap=0;gap<n;++gap) {
    for(int i=0; i<n; ++i) {
        int j = gap+i;
        if(j >= n) break;
    }
}
gap = (j - i)


# optimizing

- try optimization

- if using dp[i][j] for [i...j] try to use dp[i] for [0...i]