#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/cherry-pickup/discuss/290654/Java-brute-force-with-explanation-and-how-to-approach-this-problem-in-real-life

1. First go from (0, 0) to (N - 1, N - 1) and then from (N - 1, N - 1) to (0, 0)
 In the first journey we should mark cherries picked as zero. But problem is if that path choosen isn't maximized we should place the 
 cherry back. This is difficult

2. So start journey of both persons at same time. One from (0, 0) to (n - 1, n - 1) and other from (n - 1, n - 1) to (0, 0)
but again we get the problem like 1st approach. we need to place the cherry back if path isn't maximized

3. so best approach would be to start joruney of both persons from same position (0, 0) to (n-1, n-1)
Only thing we need to take care is to not double count

We get (r1, c1, r2, c2) as state of dp
We can further optimize by removing redundant args.
Both persons move only step bottom or right everytime
so r1 + c1 == r2 + c2 everytime
We could just use 3 points for state and derive third one (r1, c1, r2) and c2 = (r1 + c1 - r2)

*/
class Solution {
   public:
    //Idea is for both persons to travel from (0, 0) to (N - 1, N - 1) and pick max cherries
    //along the way and also don't double count
    int cherryPickup(vector<vector<int>>& grid) {
        if (grid.empty() || grid[0].empty()) {
            return 0;
        }
        memset(dp, -1, sizeof(dp));
        //We might get negative result if it is not possible since in base case we return -INF
        //So just return max(0, result)
        return max(0, cherryPickup(grid, grid.size(), grid[0].size()));
    }

   private:
    int dp[51][51][51][51];
    int cherryPickup(vector<vector<int>>& grid,
                     int rows,
                     int cols,
                     int r1 = 0,
                     int c1 = 0,
                     int r2 = 0,
                     int c2 = 0) {
        if (r1 >= rows || r2 >= rows || c1 >= cols || c2 >= cols || grid[r1][c1] == -1 || grid[r2][c2] == -1) {
            return INT_MIN;
        }

        if (dp[r1][c1][r2][c2] != -1) {
            return dp[r1][c1][r2][c2];
        }

        if (r1 == rows - 1 && c1 == cols - 1) {
            return grid[r1][c1];
        }

        if (r2 == rows - 1 && c2 == cols - 1) {
            return grid[r2][c2];
        }

        int cherries = 0;

        //Both persons at the same time so don't double count.
        if (r1 == r2 && c1 == c2) {
            cherries = grid[r1][c1];
        } else {
            cherries = grid[r1][c1] + grid[r2][c2];
        }

        //Both persons can go down or right. So we will get 4 combinations. Take max out of those
        cherries += max({cherryPickup(grid, rows, cols, r1 + 1, c1, r2 + 1, c2),
                         cherryPickup(grid, rows, cols, r1 + 1, c1, r2, c2 + 1),
                         cherryPickup(grid, rows, cols, r1, c1 + 1, r2 + 1, c2),
                         cherryPickup(grid, rows, cols, r1, c1 + 1, r2, c2 + 1)});

        return dp[r1][c1][r2][c2] = cherries;
    }
};