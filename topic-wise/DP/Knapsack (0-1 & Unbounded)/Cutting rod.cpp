#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/cutting-a-rod-dp-13/
 * See tushar roy video for explanation
 * Given a rod of length n inches and an array of prices that contains prices of all pieces of size smaller than n. 
 * Determine the maximum value obtainable by cutting up the rod and selling the pieces.
 * Example =>
 * Length of rod = 5
 * prices => 1 2 3 4
 *           2 5 7 8
 * Selling 3 length of rod will give 7 profit
 * Selling 2 length of rod will give 5 profit
 * 
 * Variation = https://practice.geeksforgeeks.org/problems/cutted-segments/0 (Here base case is really important. For not possible case use -INF not 0)
 */

int maxProfitByCuttingRod(vector<int> prices, int lengthOfRod) {
    int N = prices.size();
    //dp[n][length] = max. profit you can get by selling "length" amount of rod and you can cut maximum of length "n"
    //first n choices = [1,2,3,...n] from these lengths of rods you can cut
    vector<vector<int>> dp(N + 1, vector<int>(lengthOfRod + 1));
    //n = 0 you won't use it.
    //length = 0 then you can get only 0 profit
    for (int n = 1; n <= N; ++n) {
        dp[n][0] = 0;
    }

    //Only one choice to cut => length [1]
    for (int length = 1; length <= lengthOfRod; ++length) {
        dp[1][length] = prices[1] * length;
    }

    for (int n = 2; n <= N; ++n) {
        for (int length = 1; length <= lengthOfRod; ++length) {
            if (length >= n) {
                dp[n][length] = max(prices[n] + dp[n][length - n], dp[n - 1][length]);
            } else {
                dp[n][length] = dp[n - 1][length];
            }
        }
    }

    return dp[N][lengthOfRod];
}

//Unbounded knapsack type recurrence relation. So optimize
int maxProfitByCuttingRodOptimized(vector<int> prices, int lengthOfRod) {
    int N = prices.size();
    vector<int> dp_n(lengthOfRod + 1);

    //Only one choice to cut => length [1]
    dp_n[0] = 0;
    for (int length = 1; length <= lengthOfRod; ++length) {
        dp_n[length] = prices[1] * length;
    }

    for (int n = 2; n <= N; ++n) {
        for (int length = 1; length <= lengthOfRod; ++length) {
            if (length >= n) {
                dp_n[length] = max(prices[n] + dp_n[length - n], dp_n[length]);
            }
        }
    }

    return dp_n[lengthOfRod];
}

int main() {
    cout << maxProfitByCuttingRod({0, 2, 5, 7, 8}, 5) << endl;                  //12
    cout << maxProfitByCuttingRod({0, 1, 5, 8, 9, 10, 17, 17, 20}, 8) << endl;  //22
    cout << maxProfitByCuttingRod({0, 3, 5, 8, 9, 10, 17, 17, 20}, 8) << endl;  //24

    cout << maxProfitByCuttingRodOptimized({0, 2, 5, 7, 8}, 5) << endl;                  //12
    cout << maxProfitByCuttingRodOptimized({0, 1, 5, 8, 9, 10, 17, 17, 20}, 8) << endl;  //22
    cout << maxProfitByCuttingRodOptimized({0, 3, 5, 8, 9, 10, 17, 17, 20}, 8) << endl;  //24
}