#include <bits/stdc++.h>
using namespace std;

int dp[100000][100000];

// O(W * N) Time | O(W * N) Space
// Returns max value obtained by W capacity knapsack and n items
int knapsackTopDown(int wt[], int val[], int W, int n) {
    if (n == 0 || W == 0) return 0;

    if (dp[n][W] != -1) return dp[n][W];

    if (wt[n - 1] > W) return dp[n][W] = knapsackTopDown(wt, val, W, n - 1);

    return dp[n][W] =
               max(knapsackTopDown(wt, val, W, n - 1),
                   val[n - 1] + knapsackTopDown(wt, val, W - wt[n - 1], n - 1));
}

int knapsackBottomUp(int wt[], int val[], int W, int n) {
    // O(N*W) Time | O(N*W) Space
    int dp1[n + 1][W + 1];
    // Base Case
    for (int i = 0; i <= W; ++i) dp[0][i] = 0;
    for (int i = 0; i <= n; ++i) dp[i][0] = 0;
    // Recurrence
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= W; ++j) {
            if (wt[i - 1] > j)
                dp[i][j] = dp[i - 1][j];
            else {
                dp[i][j] =
                    max(dp[i - 1][j], val[i - 1] + dp[i - 1][j - wt[i - 1]]);
            }
        }
    }
    return dp[n][W];

    // O(N*W) Time | O(W) 1 Array
    int dp2[W + 1];
    for (int i = 0; i <= W; ++i) dp2[i] = 0;
    for (int i = 1; i <= n; ++i) {
        for (int j = W; j >= 1; --j) {
            if (wt[i - 1] <= j)
                dp2[j] = max(dp2[j], val[i - 1] + dp2[j - wt[i - 1]]);
        }
    }
    return dp2[W];
}