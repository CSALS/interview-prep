#include <bits/stdc++.h>
using namespace std;

class NumberOfWays {
   public:
    int change(int amount, vector<int>& coins) {
        int N = coins.size();
        vector<vector<int>> dp(N + 1, vector<int>(amount + 1));
        //dp[n][sum] = number of ways to make "sum" considering maximum of n coins

        //To get "0" amount just don't take any coins
        for (int n = 0; n <= N; ++n) {
            dp[n][0] = 1;
        }
        //To get sum (>1) from 0 coins we can't so 0 ways
        for (int sum = 1; sum <= amount; ++sum) {
            dp[0][sum] = 0;
        }

        for (int n = 1; n <= N; ++n) {
            for (int sum = 1; sum <= amount; ++sum) {
                if (coins[n - 1] <= sum) {
                    dp[n][sum] = dp[n][sum - coins[n - 1]] + dp[n - 1][sum];
                } else {
                    dp[n][sum] = dp[n - 1][sum];
                }
            }
        }
        return dp[N][amount];
    }
};

class MinimumCoinsNeeded {
   private:
    const int INF = 1e9;

   public:
    int coinChange(vector<int>& coins, int amount) {
        int N = coins.size();
        //         dp[n][amount] = min. number of coins to make "amount" from n coins
        //         vector<vector<int>> dp(N + 1, vector<int>(amount + 1));
        //         for(int j = 1; j <= amount; ++j) {
        //             dp[0][j] = INF; //really important base case.
        //         }
        //         for(int i = 0; i <= N; ++i) {
        //             dp[i][0] = 0;
        //         }

        //         for(int i = 1; i <= N; ++i) {
        //             for(int j = 1; j <= amount; ++j) {
        //                 if(coins[i - 1] <= j) {
        //                     dp[i][j] = min(1 + dp[i][j - coins[i - 1]], dp[i - 1][j]);
        //                 }
        //                 else {
        //                     dp[i][j] = dp[i - 1][j];
        //                 }
        //             }
        //         }
        //         return (dp[N][amount] >= INF) ? -1 : dp[N][amount];
        //Unbounded Knapsack optimization
        vector<int> dp(amount + 1, INF);
        dp[0] = 0;
        for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= amount; ++j) {
                if (coins[i - 1] <= j) {
                    dp[j] = min(dp[j], 1 + dp[j - coins[i - 1]]);
                }
            }
        }
        return (dp[amount] >= INF) ? -1 : dp[amount];
    }
};