#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/scramble-string/

unordered_map<string, int> cache;

int isScramble(const string A, const string B) {
    if (A == B) {
        return true;
    }
    if (A.length() != B.length()) {
        return false;
    }

    if (cache.find(A + B) != cache.end()) {
        return cache[A + B];
    }

    //after first i chars we split the word into two nodes
    //1st condition is when we don't the children of the current node
    //2nd condition is when we swap children of the current node also
    for (int i = 1; i < A.length(); ++i) {
        //first i chars in A and first i chars in B
        //rest portion in A and rest portion in B
        if (
            isScramble(A.substr(0, i), B.substr(0, i)) &&
            isScramble(A.substr(i), B.substr(i))) return cache[A + B] = true;
        //first i chars in A and last i chars in B
        //rest portion in A and rest portion in B
        if (
            isScramble(A.substr(0, i), B.substr(B.length() - i)) &&
            isScramble(A.substr(i), B.substr(0, B.length() - i))) return cache[A + B] = true;
    }
    return cache[A + B] = false;
}
