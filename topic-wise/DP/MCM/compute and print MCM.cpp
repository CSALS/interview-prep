#include <bits/stdc++.h>
using namespace std;

int dp[101][101];
int breakPoint[101][101];

/*
    i represents 2nd dimension of starting bracket
    j represents 2nd dimension of ending bracket
    need multiply all brackets b/w
    Time => O(N^3)
*/
int computeMCM(vector<int> &array, int i, int j) {
    if (i == j) {
        return dp[i][j] = 0;
    }

    if (dp[i][j] != -1) {
        return dp[i][j];
    }

    int minCost = INT_MAX;
    for (int k = i; k < j; ++k) {
        int result = computeMCM(array, i, k) + computeMCM(array, k + 1, j) + array[i - 1] * array[k] * array[j];

        if (result < minCost) {
            breakPoint[i][j] = k;
            minCost = result;
        }
    }
    return dp[i][j] = minCost;
}

int computeMCMIterative(vector<int> &array) {
    int n = array.size();
    for (int i = 1; i <= n; ++i) {
        dp[i][i] = 0;
    }

    for (int gap = 1; gap < n; ++gap) {
        for (int i = 1; i <= n; ++i) {
            int j = gap + i;
            if (j >= n) {
                break;
            }
            dp[i][j] = INT_MAX;
            for (int k = i; k < j; ++k) {
                int result = dp[i][k] + dp[k + 1][j] + array[i - 1] * array[k] * array[j];
                if (result < dp[i][j]) {
                    breakPoint[i][j] = k;
                    dp[i][j] = result;
                }
            }
        }
    }
    return dp[1][n - 1];
}

string printBracketsMCM(int i, int j, char &c) {
    if (i == j) {
        string matrix = "";
        matrix += c;
        c++;
        return matrix;
    }

    int k = breakPoint[i][j];
    string left = printBracketsMCM(i, k, c);
    string right = printBracketsMCM(k + 1, j, c);
    return "(" + left + right + ")";
}

string solution(int n, vector<int> array) {
    memset(dp, -1, sizeof(dp));
    memset(breakPoint, -1, sizeof(breakPoint));
    computeMCM(array, 1, n - 1);
    char c = 'A';
    return printBracketsMCM(1, n - 1, c);
}