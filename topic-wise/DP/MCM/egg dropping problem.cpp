#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/super-egg-drop/
class Solution {
   private:
    vector<vector<int>> cache;
    //cache[n][f] = min attempts to find critical floor with n eggs and f floors in the worst case
   public:
    //N eggs F floors
    //IMP => Only number of floors matter not the actual floor numbers
    int superEggDropRecursive(int N, int F) {
        //Base cases
        if (N == 1) return F;
        if (F == 0) return 0;
        if (F == 1) return 1;

        if (cache[N][F] != -1) {
            return cache[N][F];
        }

        //Recurrence
        int minAttempts = INT_MAX;

        for (int f = 1; f <= F; ++f) {
            //If egg breaks then go down. f - 1 floors left to explore.
            //If egg doesn't break then go explore above floors. below f floors are useless so explore F-f floors

            //taking max. since we need to find for the worst case
            int temp = max(superEggDropRecursive(N - 1, f - 1), superEggDropRecursive(N, F - f));
            //out of all worst cases take min.
            minAttempts = min(minAttempts, 1 + temp);
        }

        return cache[N][F] = minAttempts;
    }

    //dp[n][f] depends only on previous row values. So compute row-wise
    int superEggsDropIterative(int N, int F) {
        //Base
        for (int n = 1; n <= N; ++n) {
            cache[n][0] = 0;
            cache[n][1] = 1;
        }
        for (int f = 1; f <= F; ++f) {
            cache[1][f] = f;
        }

        //Recurrence
        for (int n = 2; n <= N; ++n) {
            for (int f = 2; f <= F; ++f) {
                cache[n][f] = INT_MAX;
                for (int k = 1; k <= f; ++k) {
                    int temp = max(cache[n - 1][k - 1], cache[n][f - k]);
                    cache[n][f] = min(cache[n][f], 1 + temp);
                }
            }
        }

        return cache[N][F];
    }

    int superEggDrop(int N, int F) {
        cache.resize(N + 1, vector<int>(F + 1, -1));
        return superEggsDropIterative(N, F);
    }
};