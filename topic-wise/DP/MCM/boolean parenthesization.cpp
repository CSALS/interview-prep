#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/evaluate-expression-to-true/

const int mod = 1e3 + 3;

//dp[i][j][0] = number of ways to eval A[i...j] to F
//dp[i][j][1] = number of ways to eval A[i...j] to T
int dp[200][200][2];

int countWaysToGetResult(string expr, int i, int j, bool exprVal) {
    if (i == j) {
        if (exprVal == true)
            return expr[i] == 'T';

        if (exprVal == false)
            return expr[i] == 'F';
    }

    // If the problem has already been computed then return it
    if (dp[i][j][exprVal] != -1)
        return dp[i][j][exprVal];

    int ways = 0;
    for (int k = i + 1; k < j; k = k + 2) {
        // Evaluate all the 4 possible sub-problems
        int leftT = countWaysToGetResult(expr, i, k - 1, true);
        int leftF = countWaysToGetResult(expr, i, k - 1, false);

        int rightT = countWaysToGetResult(expr, k + 1, j, true);
        int rightF = countWaysToGetResult(expr, k + 1, j, false);

        if (expr[k] == '|') {
            if (exprVal == true) {
                ways += (leftT * rightT) + (leftF * rightT) + (leftT * rightF);
            } else {
                ways += (leftF * rightF);
            }
        } else if (expr[k] == '&') {
            if (exprVal == true) {
                ways += (leftT * rightT);
            } else {
                ways += (leftF * rightF) + (leftF * rightT) + (leftT * rightF);
            }
        } else if (expr[k] == '^') {
            if (exprVal == true) {
                ways += (leftF * rightT) + (leftT * rightF);
            } else {
                ways += (leftF * rightF) + (leftT * rightT);
            }
        }
    }

    dp[i][j][exprVal] = ways;
    return ways;
}

int cnttrue(string A) {
    memset(dp, -1, sizeof(dp));
    return countWaysToGetResult(A, 0, A.length() - 1, true);
}
