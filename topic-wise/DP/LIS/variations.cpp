#include <bits/stdc++.h>
using namespace std;

/*
Minimum deletions to make an array sorted
    Answer = array size - LIS of the array

Maximum sum increasing subsequence = of all increasing subsequences find the one which has max. sum
    same as LIS but here dp[i] = max. sum of increasing subsequenc ending at i
    int dp[n]
    for(int i = 0; i < n; ++i)
        dp[i] = arr[i]; //initially it is arr[i]

    for(int i = 1; i < n; ++i) {
        for(int j = 0; j < i; ++j) {
            if(arr[j] < arr[i]) { //checking for increasing subsequence
                if(dp[j] + arr[i] > dp[i]) {
                    dp[i] = dp[j] + arr[i]
                }
            }
        }
    }

    answer = max(dp[i])

Maximum length of bitonic sequence = first increasing and then increasing
    compute both LIS[] and LDS[] (increasing seq ending at i and decreasing seq starting at i)
    answer = max(LIS[i] + LDS[i] - 1) //-1 since arr[i] is repeated twice


Longest chain of pairs
Given array of pairs (a, b) where a < b
need to form longest chain of pairs s.t (a, b) and (c, d) form chain if b < c
    1. sort pairs according to first value and then find LIS
    2. better solution is to do activity selection algorithm (sort by end time and choose non-overlapping intervals)
*/

/*
variations

https://www.geeksforgeeks.org/longest-increasing-consecutive-subsequence/
*/
