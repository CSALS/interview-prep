#include <bits/stdc++.h>
using namespace std;

/*
Question
    Consider a 2-D map with a horizontal river passing through its center.
    There are n cities on the southern bank with x-coordinates a(1) … a(n) and n
    cities on the northern bank with x-coordinates b(1) … b(n). You want to connect
    as many north-south pairs of cities as possible with bridges such that no two
    bridges cross. When connecting cities, you can only connect city a(i) on the
    northern bank to city b(i) on the southern bank. Maximum number of bridges that
    can be built to connect north-south pairs with the aforementioned constraints.
*/

/*
Solution
    1. sort all points based on south values
    2. then in the sorted array we just need to find longest non-decreasing subsequence using north values
    LIS coz we don't want crossing
*/

struct CityPair {
    int north, south;
};

bool comp(CityPair a, CityPair b) {
    // return true if a comes before b
    if (a.south < b.south) return true;

    if (a.south > b.south) return false;

    return (a.north <= b.north);
}

int maxBridges(CityPair values[], int n) {
    //sorting those pairs based on their south values
    sort(values, values + n, comp);

    vector<int> lis(n, 1);  // lis[i] = length of lis ending at ith position

    int bridges = 0;
    for (int i = 0; i < n; ++i) {
        cout << values[i].north << " " << values[i].south << endl;
    }
    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            if (values[j].north <= values[i].north) {
                lis[i] = max(lis[i], 1 + lis[j]);
            }
        }
    }

    for (int i = 0; i < n; ++i) bridges = max(bridges, lis[i]);
    return bridges;
}

int main() {
    struct CityPair values[] = {{6, 2}, {4, 3}, {2, 6}, {1, 5}};
    int n = 4;
    cout << "Maximum number of bridges = " << maxBridges(values, n);
    return 0;
}