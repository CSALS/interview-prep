#include <bits/stdc++.h>
using namespace std;
#define int long long
#define deb(x) cout << #x << " " << x << endl;

const int MAX_N = 1e6 + 5;

vector<string> arr;
string target;

int numberOfWays(int stringStart = 0, int targetIndex = 0) {
    if (targetIndex >= target.length()) {
        return 1;
    }

    int ways = 0;
    for (int arrayIndex = 0; arrayIndex < arr.size(); ++arrayIndex) {
        string str = arr[arrayIndex];
        for (int stringIndex = stringStart; stringIndex < str.length(); ++stringIndex) {
            if (str[stringIndex] == target[targetIndex]) {
                ways += numberOfWays(stringIndex + 1, targetIndex + 1);
            }
        }
    }
    return ways;
}

/*
    https://www.geeksforgeeks.org/count-the-number-of-ways-to-construct-the-target-string/
    Since the order of strings doesn't matter store all characters to indices map from all strings
*/
unordered_map<char, vector<int>> charToIndicesMap;
int numberOfWaysOptimized(int stringStart = 0, int previousIndexUsed = -1) {
    if (stringStart >= target.length()) {
        return 1;
    }

    int ways = 0;
    vector<int> indices = charToIndicesMap[target[stringStart]];
    for (int index : indices) {
        if (index > previousIndexUsed) {
            ways += numberOfWaysOptimized(stringStart + 1, index);
        }
    }
    return ways;
}

int optimizedSolution() {
    charToIndicesMap.clear();
    for (string s : arr) {
        for (int i = 0; i < s.length(); ++i) {
            charToIndicesMap[s[i]].push_back(i);
        }
    }
    return numberOfWaysOptimized();
}

int32_t main() {
    arr = {"adc", "aec", "erg"};
    target = "ac";
    cout << numberOfWays() << " " << optimizedSolution() << endl;  //4
    arr = {"afsdc", "aeeeedc", "ddegerg"};
    target = "ae";
    cout << numberOfWays() << " " << optimizedSolution() << endl;  //12
}