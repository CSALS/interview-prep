## GREEDY

- https://leetcode.com/discuss/general-discussion/669996/Greedy-for-Beginners-Problems-or-Sample-solutions

Where to use Greedy algorithms?

A problem must comprise these two components for a greedy algorithm to work:
- It has optimal substructures. The optimal solution for the problem contains optimal solutions to the sub-problems.
- It has a greedy property (hard to prove its correctness!). If you make a choice that seems the best at the moment and solve the remaining sub-problems later, you still reach an optimal solution. You will never have to reconsider your earlier choices.

TODO:
- https://www.geeksforgeeks.org/coin-game-of-two-corners-greedy-approach/ (based on a puzzle)


- https://www.geeksforgeeks.org/minimize-cash-flow-among-given-set-friends-borrowed-money/



https://www.geeksforgeeks.org/minimum-incrementdecrement-to-make-array-non-increasing/
https://codeforces.com/blog/entry/63745


//very good => https://leetcode.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/discuss/722564/c-greedy-mountain-visualization-technique-with-pictures



https://www.interviewbit.com/problems/minimum-lights-to-activate/