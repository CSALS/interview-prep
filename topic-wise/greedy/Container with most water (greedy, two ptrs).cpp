#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/container-with-most-water/
 * Choose two vertical lines === so that should direct you to two pointers
 */
class Solution {
   public:
    int maxArea(vector<int>& height) {
        int N = height.size();
        int i = 0, j = N - 1;
        int maxWater = 0;

        while (i < j) {
            maxWater = max(maxWater, (j - i) * min(height[i], height[j]));
            if (height[i] < height[j])
                ++i;
            else
                --j;
        }

        return maxWater;
    }
};