#include <bits/stdc++.h>
using namespace std;
/**
 * @question
 * https://www.interviewbit.com/problems/distribute-candy/
 * Variation = https://www.geeksforgeeks.org/minimum-salary-hike-for-each-employee-such-that-no-employee-feels-unfair/?ref=leftbar-rightbar
 * 
 * There are N children standing in a line. Each child is assigned a rating value.
    You are giving candies to these children subjected to the following requirements:

    1. Each child must have at least one candy.
    2. Children with a higher rating get more candies than their neighbors.
 * Minimize Candies you must give
 * 
 * @solution
 * 1. O(NlogN) Time | O(1) Space
 * Sort array
 * Assign from small ratings to large ratings
 * 
 * 2. O(N) Time | O(2N) Space
 * Maintain two arrays left2right, right2left 
 * left2right[i] = number of candies assigned to ith guy considering only left neighbor (the guy with rating higher than left neighbor will get more candies)
 * right2left[i] = number of candies assigned to ith guy considering only right neighbor (the guy with rating higher than right neighbor will get more candies)
 * Final result[i] = max(left2right[i], right2left[i])
 * 
 * 3. O(N) Time | O(N) Space
 * You can do it using one array
 */

int candy1(vector<int> &A) {
#define ppi pair<int, int>
    vector<ppi> sortedArray;
    int N = A.size();
    for (int i = 0; i < N; ++i) {
        sortedArray.push_back({A[i], i});
    }
    sort(sortedArray.begin(), sortedArray.end());

    vector<int> result(N);
    result[sortedArray[0].second] = 1;

    for (int i = 1; i < N; ++i) {
        int curr_rating = sortedArray[i].first;
        int curr_index = sortedArray[i].second;
        int candies = 1;
        //All the less rating guys will be alloted candies already
        if (curr_index - 1 >= 0 && A[curr_index - 1] < curr_rating) {
            candies = result[curr_index - 1] + 1;
        }
        if (curr_index + 1 <= N - 1 && A[curr_index + 1] < curr_rating) {
            candies = max(candies, result[curr_index + 1] + 1);
        }
        result[curr_index] = candies;
    }
    int sum = 0;
    for (int amt : result) sum += amt;
    return sum;
}

//2 arrays linear time
int candy2(vector<int> &A) {
    int N = A.size();
    vector<int> left2right(N, 1), right2left(N, 1);
    // left2right[i] = number of candies assigned to ith guy considering only left neighbor (the guy with rating higher than left neighbor will get more candies)
    left2right[0] = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] > A[i - 1])
            left2right[i] = left2right[i - 1] + 1;
    }

    right2left[N - 1] = 1;
    for (int i = N - 2; i >= 0; --i) {
        if (A[i] > A[i + 1])
            right2left[i] = right2left[i + 1] + 1;
    }
    int sum = 0;
    for (int i = 0; i < N; ++i)
        sum += max(left2right[i], right2left[i]);
    return sum;
}

//one array linear time
int candy3(vector<int> &A) {
    int N = A.size();
    vector<int> left2right(N, 1);

    left2right[0] = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] > A[i - 1])
            left2right[i] = left2right[i - 1] + 1;
    }

    int rightNeighbor = 1;
    int sum = max(rightNeighbor, left2right[N - 1]);  //for last guy

    for (int i = N - 2; i >= 0; --i) {
        if (A[i] > A[i + 1]) {
            rightNeighbor = rightNeighbor + 1;
        } else {
            rightNeighbor = 1;
        }
        sum += max(rightNeighbor, left2right[i]);
    }
    return sum;
}
