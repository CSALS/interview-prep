#include <bits/stdc++.h>
using namespace std;

//Activity selection problem = maximize non-overlapping intervals
//Variation = https://www.interviewbit.com/problems/chain-of-pairs/
struct Activitiy {
    int start, finish;
};
bool activityCompare(Activitiy s1, Activitiy s2) {
    return (s1.finish < s2.finish);
}
void printMaxActivities(Activitiy arr[], int n) {
    // Sort jobs according to finish time
    sort(arr, arr + n, activityCompare);

    // The first activity always gets selected
    int i = 0;
    //print interval arr[i]

    // Consider rest of the activities
    for (int j = 1; j < n; j++) {
        // If this activity has start time greater than or
        // equal to the finish time of previously selected
        // activity, then select it
        if (arr[i].finish <= arr[j].start) {
            //print interval arr[j]
            i = j;
        }
    }
}

/*
https://www.geeksforgeeks.org/job-sequencing-problem/

Given an array of jobs where every job has a deadline and associated profit if the job is finished before the deadline. 
It is also given that every job takes single unit of time, so the minimum possible deadline for any job is 1. 
How to maximize total profit if only one job can be scheduled at a time.

Input: Four Jobs with following 
deadlines and profits
JobID  Deadline  Profit
  a      4        20   
  b      1        10
  c      1        40  
  d      1        30
Output: Following is maximum 
profit sequence of jobs
        c, a   
*/

//O(N^2) Time | O(N) Space
class QuadraticTime {
   public:
    struct Job {
        int deadline;
        int profit;
        Job(int d, int p) : deadline(d), profit(p) {}
    };

    int maxDeadline;
    bool comp(Job a, Job b) {
        //true if a should come before b
        maxDeadline = max({maxDeadline, a.deadline, b.deadline});
        return a.profit >= b.profit;
    }

    int solve(vector<int> &deadline, vector<int> &profit) {
        int N = deadline.size();
        vector<Job> jobs;
        for (int i = 0; i < N; ++i) {
            jobs.push_back(Job(deadline[i], profit[i]));
        }
        maxDeadline = INT_MIN;
        sort(jobs.begin(), jobs.end(), comp);
        vector<bool> slots(maxDeadline - 1, false);  //slot[i] = true means we allotted a job in (i, i + 1) frame

        int maxProfit = 0;
        for (Job job : jobs) {
            //Try to allot this job as last as possible so as to accomadate other jobs in the front
            for (int i = job.deadline - 1; i >= 0; --i) {
                if (slots[i] == false) {
                    slots[i] = true;  //assign this job in that slot
                    maxProfit += job.profit;
                    break;
                }
            }
        }
        return maxProfit;
    }
};

/**
 * Optimization
 * for (Job job : jobs) {
 *      This loop for finding free slot is the bottleneck
        for (int i = job.deadline - 1; i >= 0; --i) {
            if (slots[i] == false) {
                slots[i] = true;  //assign this job in that slot
                maxProfit += job.profit;
                break;
            }
        }
    }
 * We will use DSU to optimize it
 * https://www.geeksforgeeks.org/job-sequencing-using-disjoint-set-union/
 */
struct Job {
    int deadline;
    int profit;
    Job(int d, int p) : deadline(d), profit(p) {}
};

int maxDeadline;
bool comp(Job a, Job b) {
    //true if a should come before b
    maxDeadline = max({maxDeadline, a.deadline, b.deadline});
    return a.profit >= b.profit;
}

class DisjointSet {
   private:
    vector<int> parent;

   public:
    DisjointSet(int size) {
        parent.resize(size + 1);
        for (int i = 0; i <= size; ++i)
            parent[i] = i;
    }

    //Finds a free slot v <= u
    //0 is dummy node so if find(u) returns 0 then means no free slot
    int find(int u) {
        //if u is the root
        if (u == parent[u])
            return u;

        //path by compression
        return parent[u] = find(parent[u]);
    }

    //Slot v has become allotted. So u is the previous slot so whenever another
    //job needs slot before v we will return representative of u.
    void _union(int u, int v) {
        parent[v] = find(u);
    }
};

int solve(vector<int> &deadline, vector<int> &profit) {
    int N = deadline.size();
    vector<Job> jobs;
    for (int i = 0; i < N; ++i) {
        jobs.push_back(Job(deadline[i], profit[i]));
    }
    maxDeadline = INT_MIN;
    sort(jobs.begin(), jobs.end(), comp);

    DisjointSet dsu(maxDeadline);

    int maxProfit = 0;
    for (Job job : jobs) {
        //for deadline we are considering [deadline - 1, deadline] in dsu as the interval
        int freeSlot = dsu.find(job.deadline);
        if (freeSlot != 0) {
            dsu._union(freeSlot - 1, freeSlot);
            maxProfit += job.profit;
        }
    }
    return maxProfit;
}

/**
 * Variation of both activity selection problem and job scheduling problem
 * https://www.geeksforgeeks.org/weighted-job-scheduling/
 * https://www.spoj.com/problems/RENT/
 * For each activity we have {start, end, profit}
 * Get the max. profit for non-overlapping intervals
 * Activity selection = weighted job schedling when weight = 1
 * Job scheduling = weighted job scheding when start = deadline-1 and end=deadline
 * Sort by end time and then do dp
 * max_profit(n) = max(
 *                  max_profit(n - 1) //ignore this interval
 *                  profit[n] + max_profit(i)) // where i is the first non-overlapping interval with this interval
 */
int dp[10001];

struct Job {
    int start, end, profit;
    Job(int s, int e, int p) : start(s), end(e), profit(p) {}
};

bool comp(Job a, Job b) {
    return a.end < b.end;
}

int latestNonOverlap(vector<Job> &jobs, int index) {
    //In [0...index-1] find the right most non-overlapping interval with this index
    //Can use linear search.
    //Use binary search since predicate will be TTTTTFFFFF find last T (true is non-overlapping)
    int low = 0, high = index - 1;
    while (low < high) {
        int mid = low + (high - low + 1) / 2;
        if (jobs[mid].end < jobs[index].start) {
            low = mid;
        } else {
            high = mid - 1;
        }
    }
    //Do sanity check
    if (jobs[low].end < jobs[index].start)
        return low;
    else
        return -1;
}

int maxProfitRecursive(vector<Job> &jobs, int index) {
    if (index == 0) return jobs[0].profit;

    if (dp[index] != -1)
        return dp[index];

    //2 choices for this interval = Include it or exclude it
    int nonoverlapIndex = latestNonOverlap(jobs, index);

    if (nonoverlapIndex == -1)
        return dp[index] = max(maxProfitRecursive(jobs, index - 1), jobs[index].profit);
    else
        return dp[index] = max(
                   maxProfitRecursive(jobs, index - 1),                            //exclude it
                   jobs[index].profit + maxProfitRecursive(jobs, nonoverlapIndex)  //include it
               );
}

int weightedJobScheduling(vector<Job> &jobs, int n) {
    memset(dp, -1, sizeof(dp));
    //Sort by end time
    sort(jobs.begin(), jobs.end(), comp);

    return maxProfitRecursive(jobs, n - 1);
}