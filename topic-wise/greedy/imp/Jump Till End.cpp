#include <bits/stdc++.h>
using namespace std;

/**
 * Can jump to the end?
 * @question
 * https://leetcode.com/problems/jump-game/
 * 
 * 
 * @solution
 * There is a simple dp solution both top-down and bottom-up
 * Takes O(N^2) Time | O(N) Space
 * However if we carefully observe
 * At an index i we only one index to its right which should be GOOD (since after we found such index we break)
 * That means it's better to keep track of leftmost good index towards the right of this index 
 * and check if currIndex+nums[currIndex] >= leftmost_good_index then that means we can reach the end.
 */

int dp[100000];
bool top_down(vector<int>& nums, int ind) {
    if (ind == nums.size() - 1)
        return true;

    if (dp[ind] != -1)
        return dp[ind];

    int maxJump = nums[ind];
    for (int k = ind + 1; k <= ind + maxJump; ++k) {
        if (top_down(nums, k))
            return dp[ind] = true;
    }
    return dp[ind] = false;
}
bool canJump(vector<int>& nums) {
    memset(dp, -1, sizeof(dp));
    return top_down(nums, 0);

    //Bottom-up
    int n = nums.size();
    dp[n - 1] = true;
    for (int i = n - 2; i >= 0; --i) {
        int maxReach = min(n - 1, i + nums[i]);
        //instead you can from minReach to i + 1
        for (int k = i + 1; k <= maxReach; ++k) {
            if (dp[k] == true) {
                dp[i] = true;
                break;
            }
        }
        if (dp[i] == -1)
            dp[i] = false;
    }
    return dp[0];
}

//O(N) Time | O(1) Space
bool canJump(vector<int>& nums) {
    if (nums.empty())
        return false;
    int n = nums.size();
    //Careful observation => at an index i we only one index to its right which should be GOOD
    //That means it's better to keep track of leftmost good index and check if currIndex+nums[currIndex] >= leftmost_good_index then that means we can reach the end
    int leftmost_good_index = n - 1;
    for (int i = n - 2; i >= 0; --i) {
        if (i + nums[i] >= leftmost_good_index) {
            //i will be the leftmost_good_index now
            leftmost_good_index = i;
        }
    }
    return (leftmost_good_index == 0);
}

//------------

/*

https://leetcode.com/problems/jump-game-ii/

Good explanation = https://www.youtube.com/watch?v=hZkb_Dqu7YY&t=1467s

O(N) Time | O(1) Space


    Say we are at ith index. We can jump to jth index belonging to [i + 1, ...., i + a[i]]
    we want such j such that it has maximum of j + a[j] among all other js.
    It's better to jump to that j

    IMPPPP
    Now wait. Say there's i which doesn't have maximum of (i + a[i]) but it can reach an index which can directly go to ending
    so shouldn't we go that index?
    Nope. jth index has maximum (j + a[j]) that means it can reach all indices which i can reach so it can also reach the index which
    can directly go to ending
*/
class Solution {
   public:
    int jump(vector<int>& nums) {
        if (nums.empty() || nums.size() == 1) {
            return 0;
        }

        int n = nums.size();

        int minimumJumps = 0;
        int limit = nums[0];
        int maxReachableIndex = 0 + nums[0];

        for (int i = 1; i < n; ++i) {
            if (i > limit) {
                minimumJumps++;
                limit = maxReachableIndex;
            }
            maxReachableIndex = max(maxReachableIndex, i + nums[i]);
        }
        return minimumJumps + 1;
    }
};

/**
 * IMPPP COMPLEX
 * https://leetcode.com/problems/jump-game-ii/
 * Best explanation = https://leetcode.com/problems/jump-game-ii/discuss/696926/C%2B%2B-Intuitive-Greedy-with-Explanation-O(n)
 * First approach that comes to mind is the O(n^2) DP approach. A better approach =>
 * To design a greedy algorithm we can use the following observation -
 * Say we want to reach the index number i in minimum jumps. 
 * Let j denote all the candidates for the previous index (all indices such that j+nums[j]>=i and j<i). 
 * Then, for the optimal path, we will choose the j that is the farthest left from i. 
 * The reason for this is that if we dont choose the farthest left( but instead choose some later j) 
 * then we can always create a better or equivalent solution by replacing this value with the farthest value. 
 * This is our greedy choice.
 * So we will pre-compute for all indexes the leftMostIndex which can reach this index in one step
 */

int jump(vector<int>& nums) {
    int n = nums.size();
    vector<int> leftMostIndex(n);
    leftMostIndex[0] = -1;

    //O(N) Time
    int l = 0;
    for (int r = 1; r < n; ++r) {
        if (l + nums[l] >= r) {
            leftMostIndex[r] = l;
        } else {
            while (l + nums[l] < r) {
                l++;
            }
            leftMostIndex[r] = l;
        }
    }

    //O(N) Time
    int currentIndex = n - 1;
    int minJumps = 0;
    while (currentIndex != 0) {
        //leftMostIndex[currInd] ---> currInd (taking this jump)
        minJumps++;
        currentIndex = leftMostIndex[currentIndex];
    }
    return minJumps;
}

/*

https://leetcode.com/problems/jump-game-iii/

Given an array of non-negative integers arr, you are initially positioned at start index of the array. 
When you are at index i, you can jump to i + arr[i] or i - arr[i], check if you can reach to any index with value 0.

Notice that you can not jump outside of the array at any time.

    @solution

        At every position we have only two choices
        Think of recursive approach. It is DFS.
        If we want min. jumps then use BFS
*/
class Solution {
    vector<bool> visited;
    vector<int> arr;
    int n;

   public:
    bool canReach(vector<int>& arr, int start) {
        this->arr = arr;
        this->n = arr.size();
        this->visited.resize(n, false);  //to prevent cycles
        return dfs(start);
    }

   private:
    //return true if start can reach to index with value 0
    bool dfs(int start) {
        if (start >= n || start < 0) return false;

        visited[start] = true;
        if (arr[start] == 0) return true;

        //from start we only have two choices
        if (start + arr[start] < n && !visited[start + arr[start]]) {
            if (dfs(start + arr[start])) {
                return true;
            }
        }
        if (start - arr[start] >= 0 && !visited[start - arr[start]]) {
            if (dfs(start - arr[start])) {
                return true;
            }
        }
        return false;
    }
};