#include <bits/stdc++.h>
using namespace std;

/**
 * Refer question for more clarity = https://leetcode.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/
 * For explanation = https://leetcode.com/problems/maximum-nesting-depth-of-two-valid-parentheses-strings/discuss/722564/C%2B%2B-Greedy-or-Mountain-Visualization-Technique
 */
class Solution {
   public:
    vector<int> maxDepthAfterSplit(string seq) {
        if (seq.empty())
            return {};

        int currLevel = 0;
        int maxLevel = 0;
        int N = (int)seq.length();
        vector<int> levels(N);
        for (int i = 0; i < N; ++i) {
            char c = seq[i];
            if (c == '(') {
                levels[i] = currLevel;
                currLevel++;
            } else {
                currLevel--;
                levels[i] = currLevel;
            }
            maxLevel = max(maxLevel, currLevel);
        }

        int threshold = maxLevel / 2;
        //level[i] >= threshold then it belongs to B else A
        for (int i = 0; i < levels.size(); ++i) {
            if (levels[i] >= threshold)
                levels[i] = 1;
            else
                levels[i] = 0;
        }

        return levels;
    }
};