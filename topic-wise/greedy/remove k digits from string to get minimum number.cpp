#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/remove_k-_-digits/
//Remove ith digit if it is greater than (i+1)th digit
string solve(string A, int B) {
    for (int i = 0; i < A.length() - 1; ++i) {
        if (A[i] == '.') continue;
        if (B > 0 && A[i] >= A[i + 1]) {
            A[i] = '.';
            B--;
        }
    }
    if (B > 0) {
        //remove the last digits to make it smaller
        int i = A.length() - 1;
        for (int j = 0; j < B;)  //j is for counting of removing
        {
            if (A[i] == '.') continue;

            if (i >= 0) {
                A[i--] = '.';
                j++;
            } else
                break;
        }
    }
    string result = "";
    for (char c : A) {
        if (c != '.') result += c;
    }
    int index = 0;
    while (result[index] == '0')
        index++;
    return result.substr(index);
}
