//StlGod.cpp -> Entry point for the console application

#include <algorithm>
#include <iostream>
#include <limits>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>
using namespace std;

bool comparatorFunction(int x, int y) {
    //comparatorFunction is just a function which returns true / false
    return x > y;
}

void vectorDemo() {
    vector<int> A = {11, 2, 3, 14};

    //Accessing 2nd element of a vector
    cout << A[1] << endl;

    // O(NlogN) sort (ascending)
    sort(A.begin(), A.end());

    //Binary Search on the sorted vector. O(logN)
    //Search Space -> A.begin() to A.end()
    bool present = binary_search(A.begin(), A.end(), 3);
    cout << present << endl;
    present = binary_search(A.begin(), A.end(), 4);
    cout << present << endl;

    //Inserting element at the end
    A.push_back(100);
    A.push_back(100);
    A.push_back(100);
    A.push_back(100);
    A.push_back(100);
    A.push_back(123);
    //2 , 3 , 11 , 14 , 100 , 100 , 100 , 100 , 123

    //Lowerbound , upperbound on sorted vector -> Binary Search -> O(logN)
    //lower bound returns vector iterator to 1st element >= 100
    //Iterators are pointers
    vector<int>::iterator itL = lower_bound(A.begin(), A.end(), 100);
    //upper bound returns vector iterator to 1st element > 100
    vector<int>::iterator itU = upper_bound(A.begin(), A.end(), 100);

    cout << *itL << " " << *itU << endl;

    //We can perform arithmetic operations on iterators in constant time
    cout << itU - itL << endl;  //5  (Number of times 100 occurred)

    //Sort in DESCENDING ORDER , pass comparatorFunction which defines our ordering
    sort(A.begin(), A.end(), comparatorFunction);

    //How to print vector??
    vector<int>::iterator itr;
    for (itr = A.begin(); itr != A.end(); itr++) {
        cout << *itr << " ";
    }
    cout << endl;

    for (int x : A) {  //For each integer x in vector A
        cout << x << " ";
    }
    cout << endl;

    for (int &x : A) {  //Iterate by reference
        //So we can change contents of the vector
        x++;
    }
    cout << endl;

    for (auto it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;
}

void setDemo() {
    //Everytime you insert in vector you may need to sort every time
    //Set -> inserts elements in logN time
    //Set maintains ascending order sequence of elements
    set<int> S;
    S.insert(1);
    S.insert(2);
    S.insert(-1);
    S.insert(-10);

    for (int x : S)
        cout << x << " ";
    cout << endl;

    //-10  -1  1  2

    //Searching element
    auto it = S.find(-1);  // set<int>::iterator it = S.find(-1);
    if (it == S.end())
        cout << "ELEMENT NOT PRESEENT" << endl;
    else
        cout << "ELEMENT PRESENT" << endl;

    //lowerbound , upperbound directly implemented in set class
    //since it is always sorted you need not send S.begin() and S.end() for it to sort
    //First element >= -1
    auto it2 = S.lower_bound(-1);
    //First element > -1
    auto it3 = S.upper_bound(-1);

    auto it4 = S.upper_bound(2);
    if (it4 == S.end())
        cout << "NO upper bound exists" << endl;
    else
        cout << *it4 << endl;

    //O(logN) time for deletion
    S.erase(1);
}

void mapDemo() {
    //KEY ---> PAIR
    map<int, int> A;
    //Mapping takes O(logN) time , Retrieving takes O(logN) time
    A[1] = 100;
    A[2] = -1;
    A[3] = 200;
    A[10000232] = 1;
    //Finding/deleting a key in O(logN) time

    //Counting frequency of characters
    map<char, int> cnt;
    string x = "charan sai";

    for (char c : x) {
        cnt[c]++;
    }

    cout << cnt['c'] << " " << cnt['z'] << endl;
}

void unorderedMapDemo() {
    map<char, int> M;
    unordered_map<char, int> U;
    string s = "Charan Sai";

    //Ordered Map / Map -> inserting deletion -> O(logN)
    for (char c : s) M[c]++;
    //Unordered Map , inserting deletion-> O(N)
    for (char c : s) U[c]++;
}

void PowerOfStl() {
    //Add non overlapping intervals [x,y] like [2,3],[10,20],[30,400]
    //Print interval which contains 13

    set<pair<int, int> > S;
    S.insert({401, 450});
    S.insert({2, 3});
    S.insert({10, 20});
    S.insert({30, 400});
    //S ->  {2,3} {10,20} {30,400} {401,450}
    //Ascending order in terms of pairs?
    /*
        {a,c} < {b,d} iff (a < b) or (a == b and c < d)
    */
    int pointToCheck = 50;
    auto it = S.upper_bound({pointToCheck, INT_MAX});
    if (it == S.begin()) {
        //No interval to go back
        cout << "The given point not lying in any interval" << endl;
        return;
    }
    it--;
    pair<int, int> current = *it;
    if (current.first <= pointToCheck && pointToCheck <= current.second) {
        //Print this interval
        cout << current.first << "," << current.second << endl;
    } else {
        cout << "The given point not lying in any interval" << endl;
    }
}
int main() {
    //C++ STL

    PowerOfStl();
    return 0;
}