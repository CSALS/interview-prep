## **RECURSION**

Identify = <br>
Can always convert recurison to iteration and vice-versa. (Iteration is like tail-recursion) <br>
So if a problem has iterative solution but taking extra space then think in terms of recursion. <br>
Recursion = Think in terms of smaller sub problems. <br>
OPTIMAL SUBSTRUCTURE = <br>
If problem can be solved using sub problems then use recursion.
So if the solution to a problem can be formed using solution of sub-problem then start forming the recurrence relation and use recursion

Strategy = <br>
Assume smaller sub-problems are already solved. We have the answer
for them. Now use those sub-problems to get the original problem solution

Tricks =
- if tail recursion then use iteration to reduce to O(1) space

Problems = 
1. (**IMP**)https://www.geeksforgeeks.org/josephus-problem-set-1-a-on-solution/ 
2. (**IMP**) Hanoi problem = Recurisve & Iterative
3. Printing Permutations Of A String
4. (**IMP**) Next Permutation Of String (Not Recursion. But think using above problem)
[See B2B Swe Video for Awesome explanation]


## **BACKTRACKING**
3 Keys =
1. Our choice
2. Our constraints on those choices
3. Our goal

Identify = 
* All possibilites/combinations
* Instead of exploring all possiblities try to think of cutting down some recursive calls *
* If question can be boiled down to = Generate all possibilities with some constraints then use backtracking + pruning *
* (**IMP**)To Find = Either return one solution if possible, return all possible solution

Tips =
- write isSafe() call before calling next recursive call.

- (**IMP**)
if we just need one solution not all then use bool function as return type and in recursive call check if it returns true then stop else we need to backtrack & continue
if we need all solutions then just void recursive function.

- backtracking problems = find time complexity
Generally it is N! in case of string problems,
some K^N in case of grid problems where K will be number of possibilities each cell in grid can take.
some problems you can frame recurrence relation like T(N) = k*T(N-1) + O(q)
- solving using iteration

Problems = 
1. (**IMP**)Generate all permutations of a string without "AB" in it. (Very IMP)
constraint = string shouldn't have AB as substring
2. Rat in Maze 
generate all paths from top left to bottom right
constraint = in that path there shouldn't be 0 (block)
Follow-up = Count number of ways to reach destination in a Maze
3. https://www.geeksforgeeks.org/the-knights-tour-problem-backtracking-1/
N = # squares in chessboard
Time Complexity : Worst Case = 8^N, Best Case = O(N)
Use the recursion tree for finding the time complexity
4. N Queens - Generating one possibility, all possibilities. (N! Check code for details)
5. Sudoku Problem (Time = 9^N where N is total squares. 9 because each cell can take 9 numbers)
6. M Coloring Problem
7. (**IMP**)Generate all paranthesis


TODO =
- https://www.interviewbit.com/problems/kth-permutation-sequence/
- https://www.interviewbit.com/problems/strobogrammatic-number-iii/?ref=random-problem
- https://www.geeksforgeeks.org/the-skyline-problem-using-divide-and-conquer-algorithm/

- largest permutation in array of [1..N] vs https://www.geeksforgeeks.org/find-maximum-number-possible-by-doing-at-most-k-swaps/



# IMPLEMENT
- next permutation
- gray code
- partition array into k subsets with equal sum