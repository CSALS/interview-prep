#include <bits/stdc++.h>
using namespace std;

/*

Delete middle element of a stack

Given a stack with push(), pop(), empty() operations, delete middle of it without using any additional data structure.

Input  : Stack[] = [1, 2, 3, 4, 5]
Output : Stack[] = [1, 2, 4, 5]

Input  : Stack[] = [1, 2, 3, 4, 5, 6]
Output : Stack[] = [1, 2, 4, 5, 6]


    1. Use extra stack to store the right most elements after middle element. And after removing middle element push all from the stack
        O(N/2) T | O(N/2) S
    
    2. Use recursion stack space to hold information. Although auxilary space is still O(N/2) but if we don't consider that it is O(1)
*/

void deleteMid(stack<char> &st, int N, int count = 0) {
    int x = st.top();
    st.pop();

    count++;

    if (count == (N / 2 + 1)) {
        return;
    }
    deleteMid(st, N, count);
    st.push(x);
}

int main() {
    stack<char> st;

    //push elements into the stack
    st.push('1');
    st.push('2');
    st.push('3');
    st.push('4');
    st.push('5');
    // st.push('6');

    deleteMid(st, st.size());

    // Printing stack after deletion
    // of middle.
    while (!st.empty()) {
        char p = st.top();
        st.pop();
        cout << p << " ";
    }
    return 0;
}