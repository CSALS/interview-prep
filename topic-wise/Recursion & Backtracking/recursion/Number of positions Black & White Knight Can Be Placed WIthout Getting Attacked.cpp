#include <bits/stdc++.h>
using namespace std;

unsigned long long solve(int m, int n) {
    int row[8] = {2, -2, 1, -1, 2, -2, 1, -1};
    int col[8] = {1, 1, 2, 2, -1, -1, -2, -2};
    // Total Number Of Positions They Can Be Placed. C(m*n, 2) * 2!
    unsigned long long res = m * n * (m * n - 1);
    // Subtract all attacking positions
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            // If one knight is at (i, j) then it can attack 8 positions given by (row[k], col[k])
            for (int k = 0; k < 8; ++k) {
                int x = i + row[k], y = j + col[k];
                //if (x, y) is valid position then knights can't be placed at (i, j) and (x, y)
                if (x >= 0 && y >= 0 && x < m && y < n) res--;
            }
        }
    }
    return res;
}