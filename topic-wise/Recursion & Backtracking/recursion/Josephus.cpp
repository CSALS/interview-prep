int rec(int n, int k) {
    if (n == 0) return 1;

    return (rec(n - 1, k) + k - 1) % n;
}