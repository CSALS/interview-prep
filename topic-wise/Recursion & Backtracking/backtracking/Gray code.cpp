#include <bits/stdc++.h>
using namespace std;

/*
https://www.interviewbit.com/problems/gray-code/ (IMP Problem.)



The gray code is a binary numeral system where two successive values differ in only one bit.

Given a non-negative integer n representing the total number of bits in the code, 
print the sequence of gray code. A gray code sequence must begin with 0.

For example, given n = 2, return [0,1,3,2]. Its gray code sequence is:

00 - 0
01 - 1
11 - 3
10 - 2

There might be multiple gray code sequences possible for a given n.
Return any such sequence.


*/
//codeMask will be global. any change in bit should reflect in prev. recursive calls also.
//draw recursive tree to understand

int codeMask = 0; //initially contains N 0-bits
void flip(int i) {
    //if ith bit is set then clear it
    if (codeMask & (1 << i)) {
        codeMask = codeMask & (~(1 << i));
    } else {
        codeMask = codeMask | (1 << i);
    }
}

//index = 0,1,2...N-1
void grayCodeRecursive(int index, int N, vector<int> &result) {
    if (index == N) {
        result.push_back(codeMask);
        return;
    }

    //dont do anything
    grayCodeRecursive(index + 1, N, result);
    //flip ith bit in codeMask. this flipping should reflect in prev. recursive calls also since we need only one bit different
    flip(index);
    grayCodeRecursive(index + 1, N, result);
}

vector<int> grayCode(int N) {
    vector<int> result;
    grayCodeRecursive(0, N, result);
    return result;
}
