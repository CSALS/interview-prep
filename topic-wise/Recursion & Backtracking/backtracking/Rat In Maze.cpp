//https://www.geeksforgeeks.org/rat-in-a-maze-with-multiple-steps-jump-allowed/
/*
Rat in maze with multiple jumps allowed.
Question = generate all paths with constraint = valid path (no zero in the path)
So use backtracking

1. Printing all valid paths
2. Printing only one path (Here we are doing this)
3. Printing shortest path 
*/

#include <bits/stdc++.h>
using namespace std;

#define ppi pair<int, int>
#define N 4
void printSolution(int sol[N][N]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++)
            printf(" %d ", sol[i][j]);
        printf("\n");
    }
}
ppi dest = {N - 1, N - 1};

bool isSafe(int maze[N][N], ppi src) {
    int x = src.first, y = src.second;
    return (x >= 0 && y >= 0 && x < N && y < N && maze[x][y] != 0);
}

bool solveMazeRec(int maze[N][N], int sol[N][N], ppi src) {
    if (src == dest) {
        sol[src.first][src.second] = 1;
        return true;
    }

    if (isSafe(maze, src)) {
        sol[src.first][src.second] = 1;
        //K amount of jump is allowed
        for (int k = 1; k <= maze[src.first][src.second]; ++k) {
            if (solveMazeRec(maze, sol, {src.first, src.second + k})) return true;
            if (solveMazeRec(maze, sol, {src.first + k, src.second})) return true;
        }
        sol[src.first][src.second] = 0;  //Backtrack
    }

    return false;
}

bool solveMaze(int maze[N][N]) {
    int sol[N][N] = {{0, 0, 0, 0},
                     {0, 0, 0, 0},
                     {0, 0, 0, 0},
                     {0, 0, 0, 0}};

    if (solveMazeRec(maze, sol, {0, 0}) == false) {
        return false;
    }
    printSolution(sol);
    return true;
}

int main() {
    int maze[N][N] = {{2, 1, 0, 0},
                      {3, 0, 0, 1},
                      {0, 1, 0, 1},
                      {0, 0, 0, 1}};

    if (!solveMaze(maze)) cout << -1 << endl;
    return 0;
}