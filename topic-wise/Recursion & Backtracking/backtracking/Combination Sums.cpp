#include <bits/stdc++.h>
using namespace std;

class EachNumberManyTimes {
    vector<vector<int>> result;

    //Choice space = [index....N-1]. Don't use [0....index-1]
    void combinationSumRecursive(vector<int>& candidates, int index, int targetSum, vector<int> combination) {
        int n = candidates.size();

        int currentSum = 0;
        for (int num : combination)
            currentSum += num;

        if (currentSum == targetSum) {
            result.push_back(combination);
            return;
        }

        if (index >= n) {
            return;
        }
        // By doing i = index, we are making sure we find all the combinations at the lower numbers, exhaust them
        // then move up, making combinations with higher numbers. That way, we do not need to back track
        // to the lower numbers when we are at the higher numbers. This prevents us from needing to check
        // whether or not we have made a duplicate combination. Also this guarentees lexicographic ordering
        for (int i = index; i < n; ++i) {
            if (currentSum + candidates[i] <= targetSum) {
                //To avoid duplicates
                if (i != index && candidates[i] == candidates[i - 1]) {
                    continue;
                }
                combination.push_back(candidates[i]);
                combinationSumRecursive(candidates, i, targetSum, combination);
                combination.pop_back();
            }
        }
    }

   public:
    vector<vector<int>> combinationSum(vector<int>& array, int sum) {
        result.clear();
        sort(array.begin(), array.end());
        combinationSumRecursive(array, 0, sum, {});
        return result;
    }
};

class EachNumberOneTimeOnly {
    vector<vector<int>> result;

    //Choice space = [index....N-1]. Don't use [0....index-1]
    void combinationSumRecursive(vector<int>& candidates, int index, int targetSum, vector<int> combination) {
        int n = candidates.size();

        int currentSum = 0;
        for (int num : combination)
            currentSum += num;

        if (currentSum == targetSum) {
            result.push_back(combination);
            return;
        }

        if (index >= n) {
            return;
        }

        for (int i = index; i < n; ++i) {
            if (currentSum + candidates[i] <= targetSum) {
                if (i != index && candidates[i] == candidates[i - 1]) {
                    continue;
                }
                combination.push_back(candidates[i]);
                combinationSumRecursive(candidates, i + 1, targetSum, combination);
                combination.pop_back();
            }
        }
    }

   public:
    vector<vector<int>> combinationSum(vector<int>& array, int sum) {
        result.clear();
        sort(array.begin(), array.end());
        combinationSumRecursive(array, 0, sum, {});
        return result;
    }
};

/*
    https://leetcode.com/problems/combination-sum-iii/
    Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used 
    and each combination should be a unique set of numbers.
*/
class Solution {
   private:
    vector<vector<int>> result;
    vector<bool> visited;

   public:
    vector<vector<int>> combinationSum3(int k, int targetSum) {
        result.clear();
        visited.resize(10, false);

        combinationSum3Recursive(k, targetSum, {});
        return result;
    }

    //Choice space = [startNumber....9]. Don't use [1...startNumber-1]
    void combinationSum3Recursive(int k, int targetSum, vector<int> array, int startNumber = 1) {
        int currentSum = 0;
        for (int num : array)
            currentSum += num;

        if (currentSum == targetSum && array.size() == k) {
            result.push_back(array);
            return;
        }

        for (int number = startNumber; number <= 9; ++number) {
            if (!visited[number] && currentSum + number <= targetSum) {
                //Make choice = use number in our array
                visited[number] = true;
                array.push_back(number);
                //Recurse for next choices
                combinationSum3Recursive(k, targetSum, array, number + 1);
                //Undo the choice
                array.pop_back();
                visited[number] = false;
            }
        }
    }
};