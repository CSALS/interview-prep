#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/beautiful-arrangement/
class Solution {
   public:
    int countArrangement(int N) {
        visited.resize(N + 1, false);
        int result = 0;
        countArrangementRecursive(1, result, N);
        return result;
    }

   private:
    vector<bool> visited;
    void countArrangementRecursive(int index, int &result, int N) {
        if (index > N) {
            result++;
            return;
        }

        for (int value = 1; value <= N; ++value) {
            if (value % index == 0 || index % value == 0) {
                if (!visited[value]) {
                    visited[value] = true;
                    countArrangementRecursive(index + 1, result, N);
                    visited[value] = false;
                }
            }
        }
    }
};