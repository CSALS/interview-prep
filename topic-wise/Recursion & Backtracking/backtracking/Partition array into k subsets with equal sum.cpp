#include <bits/stdc++.h>
using namespace std;

/*
https://leetcode.com/problems/partition-to-k-equal-sum-subsets (IMP)

Given an array of integers nums and a positive integer k, 
find whether it's possible to divide this array into k non-empty subsets whose sums are all equal. 

Example 1:

Input: nums = [4, 3, 2, 3, 5, 2, 1], k = 4
Output: True
Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
*/

class Backtracking {
   public:
    //O(k * 2^n) Time is the upper bound
    //for each subset we take 2^n to find the correct subset
    //O(n) space since we only have n elements on which we select and recurse
    bool canPartitionKSubsets(vector<int> &nums, int k) {
        if (k == 0) {
            return false;
        }
        if (k == 1) {
            //only one partition needed with subset_sum as sum/1 = sum which is the entire set itself
            return true;
        }
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        /*
            s1 + s2 + ... +sk = sum
            given s1=s2=s3=...sk
            so k*s = sum
            so k should divide sum
        */
        if (sum % k != 0) {
            return false;
        }
        vector<bool> visited(nums.size(), false);  //if the element is visited or not
        return canPartitionKSubsetsRecursive(0, nums, k, visited, sum / k, 0);
    }

    //index represents the starting index in nums array from where we check
    //we won't use [0...index-1] elements now
    bool canPartitionKSubsetsRecursive(int index,
                                       vector<int> &nums,
                                       int k,
                                       vector<bool> &visited,
                                       int targetSubsetSum,
                                       int currentSubsetSum) {
        //no subsets left
        if (k == 0) return true;
        //we can stop if we have just 1 subset is left since we filled k-1 subsets

        if (currentSubsetSum == targetSubsetSum) {
            return canPartitionKSubsetsRecursive(0, nums, k - 1, visited, targetSubsetSum, 0);
        }

        for (int i = index; i < nums.size(); ++i) {
            if (!visited[i] && nums[i] + currentSubsetSum <= targetSubsetSum) {
                visited[i] = true;  //place nums[i] in the current subset
                if (canPartitionKSubsetsRecursive(i + 1, nums, k, visited, targetSubsetSum, currentSubsetSum + nums[i])) {
                    return true;
                }
                visited[i] = false;  //undo our choice. remove nums[i] from the current subset
            }
        }

        return false;
    }
};