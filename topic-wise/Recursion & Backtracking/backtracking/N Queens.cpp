#include <bits/stdc++.h>
using namespace std;

vector<vector<string>> result;

//can we place a queen at (row, col) in solution such that it is not attacked
bool isSafe(int row, int col, vector<string> solution) {
    for (int i = 0; i < solution.size(); ++i) {
        for (int j = 0; j < solution.size(); ++j) {
            if (solution[i][j] == 'Q') {
                // for diagonal checking we move to a cell with equal distance
                // so abs(row-i) should be same as abs(col-j)
                if (row == i || col == j || abs(row - i) == abs(col - j))
                    return false;
            }
        }
    }
    return true;
}

/*
    1. T(N) = N * (T(N - 1) + O(N^2))
    N rows choices for each queen
    and N^2 for is safe and T(N - 1) is for backtrack
    2. Since skipping rows we already added queen
    first queen has N choices
    second queen has N - 1 choices
    and so on which gives O(N!)
*/
void backtrack(int col, int N, vector<string> solution) {
    if (col >= N) {
        result.push_back(solution);
        return;
    }

    for (int row = 0; row < N; ++row) {
        if (isSafe(row, col, solution)) {
            solution[row][col] = 'Q';
            backtrack(col + 1, N, solution);
            solution[row][col] = '.';
        }
    }
}

vector<vector<string>> solveNQueens(int N) {
    vector<string> solution(N);
    string s = "";
    for (int i = 0; i < N; ++i) s += '.';
    for (string &x : solution) x = s;
    backtrack(0, N, solution);
    return result;
}
