#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/sudoku-solver/

class Solution {
   public:
    void solveSudoku(vector<vector<char>> &board) {
        if (board.empty() || board[0].empty()) {
            return;
        }
        solveSudokuUtil(board);
    }

    bool solveSudokuUtil(vector<vector<char>> &board) {
        for (int row = 0; row < 9; ++row) {
            for (int col = 0; col < 9; ++col) {
                if (board[row][col] != '.') continue;
                //Try to fill this position
                for (char number = '1'; number <= '9'; ++number) {
                    if (isSafe(row, col, board, number)) {
                        board[row][col] = number;
                        if (solveSudokuUtil(board)) {
                            return true;
                        }
                        board[row][col] = '.';
                    }
                }
                return false;
            }
        }
        return true;
    }

    bool isSafe(int row, int col, vector<vector<char>> &A, char number) {
        // check in the row
        for (int j = 0; j < 9; ++j) {
            if (A[row][j] == number) return false;
        }
        // check in the col
        for (int i = 0; i < 9; ++i) {
            if (A[i][col] == number) return false;
        }

        // check in the same box
        int x = row / 3 * 3;
        int y = col / 3 * 3;
        for (int i = x; i < x + 3; ++i) {
            for (int j = y; j < y + 3; ++j) {
                if (A[i][j] == number) return false;
            }
        }

        return true;
    }
};