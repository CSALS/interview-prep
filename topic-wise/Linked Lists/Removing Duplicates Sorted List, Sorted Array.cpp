#include "LinkedList.h"

/**
 * Delete duplicates of each node such that in the final result there are no duplicates
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list/
 */
Node* deleteDuplicates1(Node* head) {
    if (!head || head->next == NULL)
        return head;

    Node* trav = head;
    while (trav != NULL) {
        Node* curr = trav;
        while (trav->next != NULL && trav->val == trav->next->val)
            trav = trav->next;
        trav = trav->next;
        curr->next = trav;
    }
    return head;
}

/**
 * Delete all the duplicates of the node. In the final result only the nodes which were distinct should appear
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list-ii/
 */
Node* deleteDuplicates2(Node* head) {
    if (!head || head->next == NULL)
        return head;

    Node *prev = NULL, *curr = head;
    while (curr != NULL && curr->next != NULL) {
        if (curr->val != curr->next->val) {
            prev = curr;
            curr = curr->next;
            continue;
        }
        while (curr->next != NULL && curr->val == curr->next->val)
            curr = curr->next;
        if (prev == NULL)
            head = curr->next;
        else
            prev->next = curr->next;
        curr = curr->next;
    }
    return head;
}

/* SORTED ARRAYS  === IMP*/

/**
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-array/
 * Given a sorted array, remove the duplicates in place such that each element appears only once and return the new length.
 */
int removeDuplicates1(vector<int>& A) {
    if (A.empty()) return 0;
    int N = (int)A.size();

    //1. Two traversals

    //replace all duplicates with dummy value (which won't occur in the array)
    int dummy = A[0] - 1;
    int prev = A[0];
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev)
            A[i] = dummy;
        else
            prev = A[i];
    }

    //segregrate dummy values to the end
    int uniqueTail = 0;  //where we will put the unique values
    for (int i = 0; i < N; ++i) {
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }

    return uniqueTail;

    //2. Only one traversal
    int dummy = A[0] - 1;
    int prev = A[0];
    int uniqueTail = 0;

    for (int i = 0; i < N; ++i) {
        if (i != 0) {
            if (A[i] == prev)
                A[i] = dummy;
            else
                prev = A[i];
        }
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }
    return uniqueTail;

    //3. Only one traversal + no modification
    int prev = A[0];
    int uniqueTail = 1;
    for (int i = 1; i < N; ++i) {
        // if (i != 0) {
        if (A[i] == prev) {
            continue;
        } else {
            prev = A[i];
            swap(A[i], A[uniqueTail++]);
        }
        // }
    }
    return uniqueTail;
}
/**
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-array-ii/
 * Given a sorted array, remove the duplicates in place such that each element appears atmost twice and return the new length.
 */
int removeDuplicates(vector<int>& A) {
    if (A.empty()) {
        return 0;
    }

    int N = (int)A.size();

    /* Two Traversals */

    //replace all duplicates with dummy value (which won't occur in the array)
    int dummy = A[0] - 1;
    int prev = A[0];
    int freq = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev) {
            if (freq >= 2)
                A[i] = dummy;
            else
                freq++;
        } else {
            prev = A[i];
            freq = 1;
        }
    }

    //segregrate dummy values to the end
    int uniqueTail = 0;  //where we will put the unique values
    for (int i = 0; i < N; ++i) {
        if (A[i] != dummy) {
            swap(A[i], A[uniqueTail++]);
        }
    }

    return uniqueTail;

    /* One traversal + no modify. converting above code to this. 
        just check when you are not making A[i] as dummy. in those cases swap */
    int uniqueTail = 1;  //where we will put the unique values
    int prev = A[0];
    int freq = 1;
    for (int i = 1; i < N; ++i) {
        if (A[i] == prev) {
            if (freq >= 2)
                continue;
            else
                freq++;
        } else {
            prev = A[i];
            freq = 1;
        }
        swap(A[i], A[uniqueTail++]);
    }

    return uniqueTail;
}
