#include "LinkedList.h"

/**
 * Reversing entire linked list
 * Iterative & Recursive Approach
 */
Node* reverseListRecursive(Node* curr, Node*& head) {
    if (curr == NULL)
        return NULL;
    if (curr->next == NULL) {
        head = curr;
        return curr;
    }
    Node* prev = reverseListRecursive(curr->next, head);
    prev->next = curr;
    curr->next = NULL;
    return curr;
}
Node* reverseList(Node* head) {
    Node* curr = head;
    reverseListRecursive(curr, head);
    return head;
}
Node* reverseListIterative(Node* head) {
    Node *curr = head, *prev = NULL, *temp = NULL;
    while (curr != NULL) {
        temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
    }
    head = prev;
    return head;
}

/**
 * Reversing only some part of linked list from mth index to nth index
 */
Node* reversePartialList(Node* head, Node* last) {
    Node *curr = head, *prev = NULL, *temp = NULL;
    while (curr != last) {
        temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
    }
    head->next = curr;
    head = prev;
    return head;
}

//prev->2->3->4->last
//reverse 2,3,4
Node* reverseBetween(Node* head, int m, int n) {
    if (m == n)
        return head;
    int count = 0;
    Node* trav = head;
    while (count != n) {
        count++;
        trav = trav->next;
    }
    Node* last = trav;
    if (m == 1)  //need to reverse from head to last
        return reversePartialList(head, last);
    count = 1;
    Node *prev = NULL, *first = head;
    while (count != m) {
        count++;
        prev = first;
        first = first->next;
    }
    prev->next = reversePartialList(first, last);
    return head;
}

/**
 * Reversing K nodes at a time
 * Input :  1->2->3->4 and k = 2
 * Output : 2->1->4->3
 */
Node* reverseListKNodes(Node* head, int K) {
    if (!head)
        return head;

    Node *curr = head, *prev = NULL;
    int count = 0;
    while (count != K && curr != NULL) {
        Node* temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
        ++count;
    }
    head->next = reverseListKNodes(curr, K);
    return prev;
}
