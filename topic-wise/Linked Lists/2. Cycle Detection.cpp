#include "LinkedList.h"

/**
 * 1. Cycle Detection
 * 2. Finding Starting Node of cycle
 * 3. Removing cycle
 */
void cycle(Node* head) {
    //1. Cycle Detection
    Node *slow = head, *fast = head;
    while (fast != NULL && fast->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast)
            break;
    }
    if (slow != fast) {
        cout << "No Cycle\n";
        return;
    }
    cout << "Cycle Present\n";
    //2. Finding Starting Node of cycle
    slow = head;
    while (slow != fast) {
        slow = slow->next;
        fast = fast->next;
    }
    cout << "Starting node is " << slow->val << endl;
    // 3. Removing cycle
    Node* startingNode = slow;
    while (slow->next != startingNode) {
        slow = slow->next;
    }
    slow->next = NULL;  //remove just the loop not actual nodes
    printList(head);

    //Removing cycle without finding starting node. This will be done after cycle detection step
    slow = head;
    while (slow->next != fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
    fast->next = NULL;
}

int main() {
    //10-15-5-20->15
    Node* head = new Node(10);
    head->next = new Node(15);
    head->next->next = new Node(5);
    head->next->next->next = new Node(20);
    head->next->next->next->next = head->next;
    cycle(head);
}