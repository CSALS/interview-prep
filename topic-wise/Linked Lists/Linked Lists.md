## **LINKED LISTS**
Tricks =
- splitting list (using 2 or 3 head pointers for separate lists)
- using dummy nodes
- fast & slow pointer algo
- maintaing head & tail of the new lists to be created
- reverse some part of linked list (reversing can be done in constant space)
- think of modify the list to achieve constant space
- recursion

Problems =
maintaing head & tail of the new lists to be created
1. Merge Two Sorted Linked Lists (Iteration & Recursion)
2. Sort binary linked list
3. Sort even & odd nodes of linked list
4. https://www.interviewbit.com/problems/partition-list/
5. Sort list containing only 0, 1, 2

Use of slow & fast pointer algo
1. Cycle Detection, First Node In The Cycle, Tail of the loop, Removing The Loop, Length of loop in list. (Also see the proof for finding first node algorithm)
3 ways for cycle detection => hashing of nodes, modify linked list to add isVisited flag, slow & fast pointer algo.
2. Nth node from end in linked list
3. Find middle node (using count, slow&fast pointer)
4. https://www.interviewbit.com/problems/remove-nth-node-from-list-end/

* Swapping two nodes in linked list. (Application -> https://www.geeksforgeeks.org/sort-linked-list-0s-1s-2s-changing-links/)

Cloning
1. (**IMP**)https://www.geeksforgeeks.org/clone-linked-list-next-random-pointer-o1-space/
2. (**IMP**)Clone a binary tree

Reversing list
1. Reverse entire linked list (iterative, recursive)
2. (**IMP**)Reverse 2nd half of linked list 
Application = used in palindrome list, https://www.interviewbit.com/problems/reorder-list/, https://www.geeksforgeeks.org/modify-contents-linked-list/)
3. Reverse from mth node to nth node
4. K Reverse Linked List
5. Reverse Doubly Linked List (In constant time. Just swap head & tail pointers xD)

Sorting
1. Merge Sort
Application = (**IMP**)https://www.geeksforgeeks.org/point-to-next-higher-value-node-in-a-linked-list-with-an-arbitrary-pointer/
2. Insertion Sort

Misc
1. https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list/
2. https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list-ii/
3. https://www.interviewbit.com/problems/rotate-list/
4. https://www.geeksforgeeks.org/modify-contents-linked-list/
    - reverse 2nd half of list
    - modify values
    - again reverse 2nd half of list to get final list

TODO:
https://www.geeksforgeeks.org/delete-all-the-nodes-from-the-list-which-are-less-than-k/
nth node from end in single linked list
pairwise swap of nodes
swap two nodes

std::list => DLL