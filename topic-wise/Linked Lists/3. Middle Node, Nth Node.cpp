#include "LinkedList.h"

Node* middleNode(Node* head) {
    if (head == NULL) return NULL;
    Node *slow = head, *fast = head;
    while (fast != NULL && fast->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}

/*
    1. Move fast pointer n nodes ahead of head
    2. then both of them will travel only one step at a time
    3. when fast points to null slow points to nth node
    4. since you want previous to nth node to delete it you can stop when fast->next==NULL
*/
Node* removeNthFromEnd(Node* head, int end) {
    Node *slow = head, *fast = head;

    for (int count = 0; count < end && fast != NULL; ++count) {
        fast = fast->next;
    }

    //n greater than size so remove head
    if (fast == NULL) {
        Node* finalHead = head->next;
        delete head;
        return finalHead;
    }

    while (fast->next != NULL) {
        slow = slow->next;
        fast = fast->next;
    }
    //slow points to previous of nth node
    Node* nthNode = slow->next;
    slow->next = slow->next->next;
    delete nthNode;
    return head;
}

//Cant use slow&fast pointer here. Only use count
int solve(Node* head, int K) {
    int N = 0;
    Node* trav = head;
    while (trav != NULL) {
        ++N;
        trav = trav->next;
    }

    int countRequired = N / 2 + 1 - K;
    if (countRequired <= 0) {
        return -1;
    }
    Node* current = head;
    for (int count = 1; count < countRequired; ++count) {
        current = current->next;
    }
    return current->val;
}
