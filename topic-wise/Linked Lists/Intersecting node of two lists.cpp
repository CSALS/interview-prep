#include "LinkedList.h"

//https://www.interviewbit.com/problems/intersection-of-linked-lists/
ListNode* getIntersectionNode(ListNode* head1, ListNode* head2) {
    ListNode *ptr1 = head1, *ptr2 = head2;
    if (!ptr1 || !ptr2)
        return NULL;
    while (ptr1 != ptr2) {
        if (ptr1 == NULL)
            ptr1 = head2;
        else
            ptr1 = ptr1->next;
        if (ptr2 == NULL)
            ptr2 = head1;
        else
            ptr2 = ptr2->next;
    }
    return ptr1;
}