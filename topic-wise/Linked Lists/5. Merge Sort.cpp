#include "LinkedList.h"

//Merge Sort On Linked List. O(NlogN)
Node* mergeTwoLists(Node* head1, Node* head2) {
    if (!head1)
        return head2;
    if (!head2)
        return head1;

    if (head1->val < head2->val) {
        head1->next = mergeTwoLists(head1->next, head2);
        return head1;
    } else {
        head2->next = mergeTwoLists(head1, head2->next);
        return head2;
    }
}
Node* sortList(Node* head) {
    if (!head || head->next == NULL)
        return head;

    Node *slow = head, *prev = NULL, *fast = head;
    while (fast != NULL && fast->next != NULL) {
        prev = slow;
        fast = fast->next->next;
        slow = slow->next;
    }
    prev->next = NULL;  //prev points to ending of first half, slow points to starting of 2nd half
    Node* head1 = sortList(head);
    Node* head2 = sortList(slow);
    return mergeTwoLists(head1, head2);
}
