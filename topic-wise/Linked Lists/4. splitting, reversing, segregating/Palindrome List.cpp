#include "LinkedList.h"

Node *reverseList(Node *head) {
    if (head->next == NULL)
        return head;
    Node *curr = head, *prev = NULL, *temp = NULL;
    while (curr != NULL) {
        temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
    }
    head = prev;
    return head;
}

//Reverse the 2nd half of list. And compare 1st half with 2nd half. To restore to original list reverse the second half again
bool isPalindrome(Node *head) {
    if (!head || head->next == NULL)
        return 1;
    Node *slow = head, *fast = head, *prev = NULL;
    while (fast != NULL && fast->next != NULL) {
        prev = slow;
        slow = slow->next;
        fast = fast->next->next;
    }
    Node *middlePrev = prev;
    if (fast != NULL)
        middlePrev = slow;

    //reverse [middlePrev->next ...... end]
    middlePrev->next = reverseList(middlePrev->next);

    Node *first = head, *second = middlePrev->next;
    while (second != NULL) {
        if (first->val != second->val)
            return 0;
        first = first->next;
        second = second->next;
    }
    return 1;
}
