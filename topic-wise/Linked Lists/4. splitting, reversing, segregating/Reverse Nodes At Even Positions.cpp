#include "../LinkedList.h"

Node *reverseListIterative(Node *head) {
    Node *curr = head, *prev = NULL, *temp = NULL;
    while (curr != NULL) {
        temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
    }
    head = prev;
    return head;
}

/*
    1. split list into odd position list and even position list
    2. reverse even list
    3. link alternate nodes from odd -> even -> odd -> even ...
*/
Node *solve(Node *head) {
    if (!head || head->next == NULL || head->next->next == NULL) {
        return head;
    }

    //splitting list
    Node *oddHead = head;
    Node *oddTail = head;
    Node *evenHead = head->next;
    Node *evenTail = head->next;
    Node *current = head->next->next;
    bool odd = true;
    while (current != NULL) {
        if (odd) {
            oddTail->next = current;
            oddTail = current;
        } else {
            evenTail->next = current;
            evenTail = current;
        }
        odd = !odd;
        current = current->next;
    }
    oddTail->next = evenTail->next = NULL;

    //reverse even list
    evenHead = reverseListIterative(evenHead);

    //link alternate nodes
    head = oddHead;
    odd = true;
    while (oddHead != NULL && evenHead != NULL) {
        if (odd) {
            Node *temp = oddHead->next;
            oddHead->next = evenHead;
            oddHead = temp;
        } else {
            Node *temp = evenHead->next;
            evenHead->next = oddHead;
            evenHead = temp;
        }
        odd = !odd;
    }

    return head;
}
