#include "LinkedList.h"

//https://www.interviewbit.com/problems/reorder-list/
Node* reverseList(Node* head) {
    Node *curr = head, *prev = NULL, *temp = NULL;
    while (curr != NULL) {
        temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
    }
    head = prev;
    return head;
}
Node* reorderList(Node* head) {
    if (!head || head->next == NULL)
        return head;
    Node *slow = head, *fast = head, *prev = head;
    while (fast != NULL && fast->next != NULL) {
        prev = slow;
        slow = slow->next;
        fast = fast->next->next;
    }
    Node* middlePrev = slow;
    if (fast == NULL)
        middlePrev = prev;

    Node* secondHalf = middlePrev->next;
    middlePrev->next = NULL;
    secondHalf = reverseList(secondHalf);
    Node* firstHalf = head;
    head = firstHalf;
    while (firstHalf != NULL && secondHalf != NULL) {
        Node* temp = firstHalf->next;
        firstHalf->next = secondHalf;
        firstHalf = temp;
        temp = secondHalf->next;
        secondHalf->next = firstHalf;
        secondHalf = temp;
    }
    return head;
}
