#include "LinkedList.h"

/**
 * https://www.interviewbit.com/problems/partition-list/
 * Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
 * You should preserve the original relative order of the nodes in each of the two partitions.
 */
Node* partition(Node* head, int X) {
    Node *lessHead = NULL, *lessTail = NULL, *greatHead = NULL, *greatTail = NULL;
    Node* trav = head;
    while (trav != NULL) {
        if (trav->val < X) {
            if (lessHead == NULL) {
                lessHead = trav;
                lessTail = trav;
            } else {
                lessTail->next = trav;
                lessTail = trav;
            }
        } else {
            if (greatHead == NULL) {
                greatHead = trav;
                greatTail = trav;
            } else {
                greatTail->next = trav;
                greatTail = trav;
            }
        }
        trav = trav->next;
    }
    if (greatTail == NULL) {
        return lessHead;
    } else {
        greatTail->next = NULL;
    }

    if (lessTail != NULL) {
        lessTail->next = greatHead;
        return lessHead;
    } else {
        return greatHead;
    }
}
