#include "LinkedList.h"

//Both iterative & recursive approach

Node* mergeTwoListsRecursive(Node* head1, Node* head2) {
    if (!head1)
        return head2;
    if (!head2)
        return head1;

    if (head1->val < head2->val) {
        head1->next = mergeTwoListsRecursive(head1->next, head2);
        return head1;
    } else {
        head2->next = mergeTwoListsRecursive(head1, head2->next);
        return head2;
    }
}
Node* mergeTwoListsIterative(Node* head1, Node* head2) {
    Node *i = head1, *j = head2;
    Node* tail = NULL;

    while (i != NULL && j != NULL) {
        if (i->val < j->val) {
            if (tail == NULL)
                tail = i;
            else {
                tail->next = i;
                tail = i;
            }
            i = i->next;
        } else {
            if (tail == NULL)
                tail = j;
            else {
                tail->next = j;
                tail = j;
            }
            j = j->next;
        }
    }
    if (i != NULL)
        tail->next = i;
    if (j != NULL)
        tail->next = j;
    if (head1->val < head2->val)
        return head1;
    else
        return head2;
}
