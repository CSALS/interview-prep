#include "LinkedList.h"

//https://www.interviewbit.com/problems/rotate-list/
Node *rotateRight(Node *head, int K) {
    int count = 0;
    Node *trav = head;
    while (trav != NULL) {
        count++;
        trav = trav->next;
    }
    if (K == 0 || K == count)
        return head;
    K = K % count;
    if (K == 0)
        return head;
    Node *prevFast = NULL, *fast = head;
    for (int i = 0; i < K; ++i) {
        prevFast = fast;
        fast = fast->next;
    }
    if (fast == NULL)
        return head;
    Node *prevSlow = NULL, *slow = head;
    while (fast != NULL) {
        prevFast = fast;
        fast = fast->next;
        prevSlow = slow;
        slow = slow->next;
    }
    prevSlow->next = NULL;
    prevFast->next = head;
    head = slow;
    return head;
}
