#include "LinkedList.h"

/*
https://leetcode.com/problems/add-two-numbers-ii/


MSB at the head

Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 8 -> 0 -> 7

    1. Reverse the lists and just keep adding
    2. create hashmap of node->previous pointer and use it to traverse from tail to head (emulating DLL behaviour)
    3. like bottom-up, first go towards right and then use that result (recursion)

*/
class Solution {
   public:
    Node* addTwoNumbers(Node* l1, Node* l2) {
        int size1 = getSize(l1);
        int size2 = getSize(l2);
        int carry = 0;

        if (size1 == size2) {
            Node* result = addTwoNumbersRecursive(l1, l2, carry);
            if (carry == 1) {
                Node* finalResult = new Node(1);
                finalResult->next = result;
                return finalResult;
            } else {
                return result;
            }
        }

        //we will handle for size1 > size2 case. if it is otherwise call with reverse args
        if (size1 < size2) {
            return addTwoNumbers(l2, l1);
        }

        //now size1 > size2. go ahead size1-size2 nodes in l1
        Node* startOfL1 = l1;
        for (int count = 0; count < size1 - size2; ++count) {
            startOfL1 = startOfL1->next;
        }
        Node* rightHalf = addTwoNumbersRecursive(startOfL1, l2, carry);
        Node* leftHalf = addOneList(l1, startOfL1, carry);
        Node* trav = leftHalf;
        while (trav->next != NULL) {
            trav = trav->next;
        }
        trav->next = rightHalf;
        if (carry == 1) {
            Node* finalResult = new Node(1);
            finalResult->next = leftHalf;
            return finalResult;
        } else {
            return leftHalf;
        }
    }

   private:
    Node* addOneList(Node* l, Node* end, int& carry) {
        if (l == end || l == NULL) {
            return NULL;
        }

        Node* subproblem = addOneList(l->next, end, carry);
        int data = l->val + carry;
        if (data > 9) {
            data %= 10;
            carry = 1;
        } else {
            carry = 0;
        }
        Node* result = new Node(data);
        result->next = subproblem;
        return result;
    }
    Node* addTwoNumbersRecursive(Node* l1, Node* l2, int& carry) {
        if (l1 == NULL && l2 == NULL) {
            return NULL;
        }
        Node* subproblemResult = addTwoNumbersRecursive(l1->next, l2->next, carry);

        int sum = l1->val + l2->val + carry;
        if (sum > 9) {
            carry = 1;
            sum %= 10;
        } else {
            carry = 0;
        }

        Node* currentResult = new Node(sum);
        currentResult->next = subproblemResult;
        return currentResult;
    }

    int getSize(Node* current) {
        int count = 0;
        while (current) {
            ++count;
            current = current->next;
        }
        return count;
    }
};