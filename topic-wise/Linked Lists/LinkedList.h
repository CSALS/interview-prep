#include <bits/stdc++.h>
using namespace std;

struct Node {
    int val;
    Node* next;
    Node(int data) : val(data), next(NULL) {}
};

void printList(Node* head) {
    Node* trav = head;
    while (trav != NULL) {
        cout << trav->val << " ";
        trav = trav->next;
    }
}