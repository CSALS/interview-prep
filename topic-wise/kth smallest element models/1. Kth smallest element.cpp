#include <bits/stdc++.h>
using namespace std;

/**
 * 1. Solve using heaps => O(nlogk) T | O(k) S, no modification of array
 * 2. Quick select => Avg O(n) T | O(logn) S, modifying array
 * 3. Median of medians => worst case O(n) T , modifying array
 * 4. Binary search on answer => O(nlogn) T, no modification of array
 * 
 * Median can also be found using these methods
 * For median there should n/2 elements less than it
 * Can do the same method for matrix also
 * 
 * Can be expanded to Kth Smallest Element among N sorted arrays
 */

//https://www.geeksforgeeks.org/kth-smallest-element-in-the-array-using-constant-space-when-array-cant-be-modified/
int kthsmallest(const vector<int> &A, int K) {
    int minEle = *min_element(A.begin(), A.end());
    int maxEle = *max_element(A.begin(), A.end());

    while (minEle <= maxEle) {
        //if mid is kth smallest element then its lessEqualCount should be K and it should be present in array
        int mid = (minEle + maxEle) / 2;

        int lessEqualCount = 0;  //number of elements less than equal to mid
        for (int num : A) lessEqualCount += (num <= mid);

        if (lessEqualCount == K) {
            //imp condition
            for (int num : A)
                if (num == mid) return num;
            maxEle = mid - 1;
        }

        if (lessEqualCount < K)
            minEle = mid + 1;
        else
            maxEle = mid - 1;
    }
    return minEle;
}
