#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-median-row-wise-sorted-matrix/
 * 1. Use 1D array of size r * c. Place all matrix elements in it and sort it and find median
 * O(r*c log(r*c)) Time | O(r*c) Space
 * 2. Use 1D array of size r * c. Place all matrix elements in it. Find median using median of medians logic
 * O(r*c) Time | O(r*c) Space
 * 3. Use Binary search. Since guarenteed that r*c is odd 
 * There are exactly (r*c)/2 elements less than median and (r*c)/2 elements greater than median
 * Use minimum of entire matrix as low and maximum of entire matrix as high
 * If smaller elements than mid is less than (r*c)/2 or larger elements than mid is > (r*c/2) then go towards right
 * O(r*logc * log(max - min + 1)) Time | O(1) Space
 * log(max - min + 1) <= log(2^32) <= 32
 * O(32*r*logc) Time
 * Finding minium of entire matrix = minimum of all elements in 1st column since row-wise sorted   O(c) Time
 * Finding maximum of entire matrix = maximum of all elements in last column since row-wise sorted O(c) Time
 */
int binaryMedian(vector<vector<int>> &m, int r, int c) {
    int min = INT_MAX, max = INT_MIN;
    for (int i = 0; i < r; i++) {
        // Finding the minimum element
        if (m[i][0] < min)
            min = m[i][0];

        // Finding the maximum element
        if (m[i][c - 1] > max)
            max = m[i][c - 1];
    }

    while (min <= max) {
        int mid = min + (max - min) / 2;
        int smaller_count = 0;  //number of elements smaller than mid
        int larger_count = 0;
        for (int i = 0; i < r; ++i) {
            int upper_ind = upper_bound(m[i].begin(), m[i].end(), mid) - m[i].begin();
            smaller_count += upper_ind;
            larger_count += (c - upper_ind);
        }
        int desired_count = (r * c) / 2;
        if (smaller_count == desired_count && larger_count == desired_count) {
            return mid;
        }
        if (smaller_count < desired_count || larger_count > desired_count) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
}