#include <bits/stdc++.h>
using namespace std;

/**
 * @question
 * Find median of two sorted arrays of same size when combined.
 * 
 * @solution
 * Since same size the result array will be even so we need to return average of n/2th and (n/2 + 1)th smallest elements
 * O(logN) Time | O(1) Space
 */
double median_two_sorted_arrays_same_size(vector<int> A, vector<int> B) {
    int M = A.size(), N = B.size();
    int size = M + N;
    double middle1 = kth_smallest_two_sorted_arrays(A, B, 0, M, 0, N, size / 2);
    double middle2 = kth_smallest_two_sorted_arrays(A, B, 0, M, 0, N, size / 2 + 1);
    return (middle1 + middle2) / 2;
}

//O(log(M + N)) Time
int kth_smallest_two_sorted_arrays(vector<int> A, vector<int> B, int A_left, int A_right, int B_left, int B_right, int K) {
    if (A_left > A_right)
        return B[B_left + K - 1];
    if (B_left > B_right)
        return A[A_left + K - 1];

    int A_mid = (A_left + A_right) / 2;
    int B_mid = (B_left + B_right) / 2;

    int X = A_mid - A_left, Y = B_mid - B_left;

    if (A[A_mid] <= B[B_mid]) {
        if (K <= (X + Y + 1)) {
            return kth_smallest_two_sorted_arrays(A, B, A_left, A_right, B_left, B_mid - 1, K);
        } else {
            return kth_smallest_two_sorted_arrays(A, B, A_mid + 1, A_right, B_left, B_right, K - (X + 1));
        }
    } else {
        return kth_smallest_two_sorted_arrays(B, A, B_left, B_right, A_left, A_right, K);
    }
}

/**
 * Find median of two sorted arrays of different size when combined.
 * 1. Using kth smallest function => O(log(M + N)) Time
 * 2. Using partition logic => O(min(M, N)) Time
 */
double median_two_sorted_arrays_different_size1(vector<int> A, vector<int> B) {
    int M = A.size(), N = B.size();
    int K = (M + N + 1) / 2;  // +1 to handle odd length
    double middle1 = kth_smallest_two_sorted_arrays(A, B, 0, M, 0, N, K);
    if ((M + N) & 1) {
        return middle1;
    } else {
        double middle2 = kth_smallest_two_sorted_arrays(A, B, 0, M, 0, N, K + 1);
        return (middle1 + middle2) / 2;
    }
}

/**
 * IMPPPPPPPPPPPPPPPPPPPPPP
 * https://www.interviewbit.com/problems/median-of-array/ (Good explanation)
 * 
 * Parition logic = O(min(M, N)) Time
 * Consider in first array [a0, a1, a2.......ai-1] i elements
 * In second array [b0, b1, b2,.....bj-1] j elements
 * Combine these two left sets => (i + j) set size
 * Right set size => (m + n) - (i + j) set size
 * 
 * CONDITIONS 
 * 1. If left size == right size or left size == right size + 1 and all elements in left set <= all elements in right set then we have found the correct partition.
 * 2. Max(ai-1, bj-1) <= Min(ai, bj) then all left <= all right
 * 3. If total set size is odd then Max(ai-1, bj-1) is median
 * 4. If total set size is even then avg of Max(ai-1, bj-1) and Min(ai, bj) is median
 * 
 * Finding the correct parition size =>
 * 1) i + j (left set size) = m + n - (i + j) (right set size)
 * 2) i + j = m + n - (i + j) + 1
 * Both cases equation will reduce to => i + j = (m + n + 1)/2 (+1 added to take care for odd case)
 * 
 * Two variables but one equation.
 * Fix one variable and iterate over other and check for the correct.
 * Pick the minimum size to iterate over.
 * Iterate over small sized array
 * for (i = 0; i <= m; ++i) {
 *      j = (m + n + 1)/2 - i
 *      Check the conditions
 * }
 * This will be O(min(M, N)) Time
 * Since search space is sorted (0, 1, 2, .... M)
 * you can do binary search to get O(log(min(M, N))) Time
 */
double median_two_sorted_arrays_different_size2(vector<int> A, vector<int> B) {
    int M = A.size(), N = B.size();
    //We will write code for M <= N case. So if M > N then call the function with reversed args
    if (M > N) {
        return median_two_sorted_arrays_different_size2(B, A);
    }
    //Now M <= N

    int low = 0, high = M;
    //i => size of partition in x, j => size of partition in y
    while (low <= high) {
        int i = (low + high) / 2;
        int j = (M + N + 1) / 2 - i;

        int maxLeftX = i == 0 ? INT_MIN : A[i - 1];
        int maxLeftY = j == 0 ? INT_MIN : B[j - 1];
        int minRightX = i == M ? INT_MAX : A[i];
        int minRightY = j == N ? INT_MAX : B[j];

        //Conditions satisifed
        if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
            //if combined array is of odd length
            if ((M + N) & 1) {
                return max(maxLeftX, maxLeftY);
            } else {
                return (max(maxLeftX, maxLeftY) + min(minRightX, minRightY)) / 2;
            }
        }

        //We have too much j size. Reduce it so need to increase i. So go right
        if (maxLeftY > minRightX) {
            low = i + 1;
        } else if (maxLeftX > minRightY) {
            high = i - 1;
        }
    }
    //Control won't come here
    return -1;
}

int main() {
    cout << median_two_sorted_arrays_same_size({1, 12, 15, 26, 38}, {2, 13, 17, 30, 45}) << endl;
}