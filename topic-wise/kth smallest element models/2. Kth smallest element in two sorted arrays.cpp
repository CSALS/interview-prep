#include <bits/stdc++.h>
using namespace std;

/**
 * @question
 * https://www.geeksforgeeks.org/k-th-element-two-sorted-arrays/
 * Find kth smallest element in the combined array of the two sorted arrays
 * Kth smallest element is same as kth element in a sorted array
 * 
 * @solution
 * 1. Can do binary search on answer but not efficient
 * 2. Merge two sorted arrays and then return (k-1)th element = O(M + N) Time
 *  Don't need to form merged sorted array. Just count till you get kth element
 * 2. Use Prune-and-search = O(log(M + N)) Time | O(1) Space
 */

int kth_smallest_two_sorted_arrays(vector<int> A, vector<int> B, int A_left, int A_right, int B_left, int B_right, int K) {
    //only B array is present and we want Kth element from that
    if (A_left > A_right) {
        return B[B_left + K - 1];
    }
    //only A array is present and we want Kth element from that
    if (B_left > B_right) {
        return A[A_left + K - 1];
    }

    int A_mid = (A_left + A_right) / 2;
    int B_mid = (B_left + B_right) / 2;

    //We are only checking for A[mid] <= B[mid] condition. For A[mid] > B[mid] just call this function with arrays params reversed
    if (A[A_mid] > B[B_mid]) {
        return kth_smallest_two_sorted_arrays(B, A, B_left, B_right, A_left, A_right, K);
    }

    int X = A_mid - A_left;
    int Y = B_mid - B_left;

    //See picture for more clarity
    if (K <= (X + Y + 1)) {
        return kth_smallest_two_sorted_arrays(A, B, A_left, A_right, B_left, B_mid - 1, K);
    } else {
        return kth_smallest_two_sorted_arrays(A, B, A_mid + 1, A_right, B_left, B_right, K - (X + 1));
    }
}

//Unit tests
int main() {
    // cout << kth_smallest_two_sorted_arrays({0, 1, 2}, {3, 4, 5, 6, 7}, 0, 2, 0, 4, 5);
    // for (int k = 1; k <= 10; ++k)
    //     cout << kth_smallest_two_sorted_arrays({2, 3, 5, 7, 9}, {1, 4, 6, 8, 10}, 0, 4, 0, 4, k) << endl;
    for (int k = 1; k <= 5; ++k)
        cout << k << " " << kth_smallest_two_sorted_arrays({1, 2}, {3, 4, 5}, 0, 1, 0, 2, k) << endl;
}