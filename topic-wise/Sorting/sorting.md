- fastest sort = https://qr.ae/pNsoFi
- https://www.geeksforgeeks.org/when-to-use-each-sorting-algorithms/


- qsort function uses QuickSort algorithm to sort the given array, although the C standard does not require it to implement quicksort.

- sort() guarentees O(nlogn) in the worst case uses introsort

- disadv of merge sort is it has O(nlogn) irrespective of the data. 
if array is sorted already then insertion sort takes O(n) but merge sort takes O(nlogn)
And also it takes additional space. 

- intorsort = quicksort + heapsort + insertionsort

- timsort = insertion sort + merge sort used by java sort() and python sorted()
First sort small pieces using Insertion Sort, then merges the pieces using merge of merge sort.
best case - O(n)
worst case and avg case - O(nlogn)


- merge sort works well with data structures which doesn't support random access. since it works sequentially
- use it when random access can be expensive