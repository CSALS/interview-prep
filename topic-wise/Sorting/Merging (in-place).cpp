#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/merge-two-sorted-lists-ii/
 * Merge A and B and result should be in B
 * 
 * @solution
 * without using extra array =
 * we can increase size of A.
 * come from back of both arrays
 */
void merge(vector<int> &A, vector<int> &B) {
    int i = A.size() - 1, j = B.size() - 1;
    A.resize(A.size() + B.size());
    int resultIndex = A.size() - 1;
    while (i >= 0 && j >= 0) {
        if (A[i] > B[j]) {
            A[resultIndex--] = A[i--];
        } else {
            A[resultIndex--] = B[j--];
        }
    }
    //no need to check i >= 0 condiition since those elements are already in correct order since B was exhausted
    //only need to check if there are elements left in B
    while (j >= 0) {
        A[resultIndex--] = B[j--];
    }
}
