/**
 * https://www.geeksforgeeks.org/mode/
 * https://www.geeksforgeeks.org/frequent-element-array/
 * https://www.geeksforgeeks.org/program-for-mean-and-median-of-an-unsorted-array/
 * 
 * Mean = O(N). Divide sum by number of elements
 * Mode = O(NlogN) if using sorting. O(N) Time + O(N) Space if using hashing. If numbers are in limited range using counting sort
 * Median => Many Ways
 *          1. Sort the array and take n/2th element in case of odd length or average of n/2 and n/2-1 in case of even length. 
 *              O(NlogN) Time
 *          2. Using Quick Select algorithm. Average O(N) Time but in worst case O(N^2). Will modify the array
 *          3. Median of medians algorithm => uses something similar to quick select algo. But always chooses a good pivot.
 *              Worst case O(N) Time
 *          4. Binary search on the answer. O(NlogN) Time but O(1) Space and also won't modify the array
 */