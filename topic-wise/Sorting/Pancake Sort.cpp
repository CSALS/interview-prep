#include <bits/stdc++.h>
using namespace std;

/*
https://www.geeksforgeeks.org/pancake-sorting/

Sort array using only flip(arr, i) operation (reverses arr[0...i])
*/

void flip(vector<int>& arr, int i) {
    int temp, start = 0;
    while (start < i) {
        temp = arr[start];
        arr[start] = arr[i];
        arr[i] = temp;
        start++;
        i--;
    }
}

vector<int> pancakeSortArray(vector<int>& nums) {
    if (nums.empty()) return {};
    int n = (int)nums.size();
    int currSize = n;

    while (currSize > 1) {
        //find max_ele in [0...currSize - 1]
        int maxEleIndex = max_element(nums.begin(), nums.begin() + currSize) - nums.begin();
        //flip(0...maxEleIndex) will place max_ele at beginning
        flip(nums, maxEleIndex);
        //flip(0....currSize-1) will place max_ele at end
        flip(nums, currSize - 1);
        currSize--;
    }
    return nums;
}
